#pragma checksum "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6e76f122e42198a329d70203c68243a2e22a29ef"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Authentication_Index), @"mvc.1.0.view", @"/Views/Authentication/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\_ViewImports.cshtml"
using IdentityManager;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\_ViewImports.cshtml"
using IdentityManager.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6e76f122e42198a329d70203c68243a2e22a29ef", @"/Views/Authentication/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8403cd412d70eae7c19a855cf3d145e815fc63a0", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Authentication_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IdentityManager.Models.Login>
    #nullable disable
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
  
    ViewData["Title"] = "Index";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"container\" id=\"loginpage\">\r\n");
#nullable restore
#line 8 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
     using (Html.BeginForm("Login", "Authentication", FormMethod.Post))
    {
        

#line default
#line hidden
#nullable disable
            WriteLiteral(@"        <div class=""d-flex justify-content-center h-100"">
            <div class=""card"">
                <div class=""card-header"">
                    <h3 class=""text-center align-middle"">Sign In</h3>
                </div>
                <div class=""card-body"">
                    <div class=""input-group form-group"">
                        <div class=""input-group-prepend"">
                            <span class=""input-group-text""><i class=""fas fa-user""></i></span>
                        </div>
                        ");
#nullable restore
#line 23 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
                   Write(Html.TextBoxFor(model => model.Username, new
               {
                   @class = "form-control text-left",
                   @tabIndex = 4,
                   @maxlength = 30,
                   @width = 300
               }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        <p id=\"UsernameValidation\" class=\"help-block text-danger invalid-feedback\">");
#nullable restore
#line 30 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
                                                                                              Write(Html.ValidationMessageFor(m => m.Username));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</p>

                    </div>
                    <div class=""input-group form-group"">
                        <div class=""input-group-prepend"">
                            <span class=""input-group-text""><i class=""fas fa-key""></i></span>
                        </div>
                        ");
#nullable restore
#line 37 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
                   Write(Html.TextBoxFor(model => model.Password, new
              {
                  @class = "form-control text-left",
                  @type = "password",
                  @tabIndex = 4,
                  @maxlength = 30,
                  @width = 300
              }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        <p id=\"PasswordValidation\" class=\"help-block text-danger invalid-feedback\">");
#nullable restore
#line 45 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
                                                                                              Write(Html.ValidationMessageFor(m => m.Password));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</p>
                    </div>
                    <div class=""form-group"">
                        <button id=""loginbutton"" class=""btn btn-primary  pull-right"">Login</button>
                    </div>

                </div>
            </div>
        </div>
");
#nullable restore
#line 54 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
   Write(Html.HiddenFor(m => m.ReleaseVersion));

#line default
#line hidden
#nullable disable
#nullable restore
#line 55 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
   Write(Html.HiddenFor(m => m.ReleaseDate));

#line default
#line hidden
#nullable disable
#nullable restore
#line 56 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
   Write(Html.HiddenFor(m => m.Environment));

#line default
#line hidden
#nullable disable
#nullable restore
#line 56 "C:\Development\IdentityManager\IdentityManager\IdentityManager\Views\Authentication\Index.cshtml"
                                           
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n\r\n");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IdentityManager.Models.Login> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
