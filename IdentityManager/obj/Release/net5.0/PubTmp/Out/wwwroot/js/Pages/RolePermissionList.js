﻿$(document).ready(function () {
    //hide the first column
    hidecolumn(0);
    hidecolumn(1);
    $("#RoleId").change(function () {               
        LoadPermissions($("#RoleId").val());
    });

    function LoadPermissions(roleId) {
        //load permissions
        $.ajax({
            type: 'GET',
            url: "/Roles/GetAllAccessGroups",
            contentType: "text/plain",
            data: {
                "RoleId": roleId
            },
            dataType: 'json',
            success: function (response) {
                populateData(response);
            },
            error: function (e) {
                console.log("There was an error with your request...");
                console.log("error: " + JSON.stringify(e));
            }
        });
    }

    function hidecolumn(column) {
        var table = $('#AccessGroupsDataTable').DataTable();
        // Hide a column
        table.column(column).visible(false);
    }
    function populateData(data) {
        $(".spinner-borderassetsorted").css("display", "block");

        var table1 = $("#AccessGroupsDataTable").DataTable({
            "destroy": true,
            "data": data,
            "paging": false,
            "ordering": true,
            "info": true,
            "bLengthChange": true, // show x
            "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]], // value : text - first is the default displayed (20 in this case).
            "pageLength": 10, // default number to show
            "fixedHeader": true,
            "paging": true,
            "info": true,
            "responsive": true,
            "bFilter": true, // the search box,
            "language": {
                "search": "Find",
                "zeroRecords": "Currently there are no records to display"
            },
                  
            //"dom": '<"toolbar">frtip',
            //"dom": "lBfrtip",
            "order": [[1, 'asc']],
            "columns": [
                { "data": "id", "className": "text-left noshow" },                
                { "data": "roleName", "className": "text-center" },
                { "data": "areaName", "className": "text-center" },
                { "data": "createValue", "className": "text-center" },
                { "data": "readValue", "className": "text-center" },
                { "data": "updateValue", "className": "text-center" },
                { "data": "deleteValue", "className": "text-center" }

            ],            
            "columnDefs": [
                {
                    "targets": [0],                    
                    "searchable": false,
                    "visible": false
                },
                {

                    "targets": 7,
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "defaultContent": "<button type=\"button\" class=\"btn btn-sm btn-primary btn-edit\">Edit Permissions</button>"
                },
            ],
            
            "initComplete": function (settings, json) {
                var $wrapper = $(this).parents(".dataTables_wrapper");
                ////Create button
                //insertToolbarButtonsRight("JobLotStatusMismatchTable", "GroupPlusJobLotStatusMismatch");
            }            
        });
        $(".spinner-borderassetsorted").css("display", "none");
    }

    //When a row is clicked on; redirect to the edit screen
    $('#AccessGroupsDataTable tbody').on('click', 'tr', function () {

        var table = $('#AccessGroupsDataTable').DataTable();
        var id = table.cells(table.row(this), 0).data()[0];
        var url = "/Roles/EditRolePermissions/" + id;
        //blockUIWithSpinner();
        //Load Partial view
        if (id.length !== 0) {   //Load Partial view
            window.location.href = url;
        }
        else {
            //$.unblockUI();
        }

    });
    
  
});