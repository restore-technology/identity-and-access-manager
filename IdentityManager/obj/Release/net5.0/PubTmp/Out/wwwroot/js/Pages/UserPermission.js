﻿$(document).ready(function () {
    $("#editpermissions").hide();
    
    $("#AreaId").change(function () {
        var permissionArea = $("#AreaId").val();
        var roleId = $("#RoleId").val();
        if (permissionArea != "") {
            //load permissions
            var model = {
                roleid: roleId,
                id: permissionArea

            };
            $.ajax({
                type: 'GET',
                url: "/UserPermission/GetPermissions",
                contentType: "text/plain",
                data: model,
                dataType: 'json',
                success: function (response) {
                    populateData(response);
                },
                error: function (e) {
                    console.log("There was an error with your request...");
                    console.log("error: " + JSON.stringify(e));
                }
            });
        }
    });

    
    function populateData(response) {
        $("#RecId").val(response.id);
        if (response.create == true) {
            $("#CreateValue").val("1");
        }
        else {
            $("#CreateValue").val("0");
        }
        if (response.read == true) {
            $("#ReadValue").val("1");
        }
        else {
            $("#ReadValue").val("0");
        }
        if (response.update == true) {
            $("#UpdateValue").val("1");
        }
        else {
            $("#UpdateValue").val("0");
        }
        if (response.delete == true) {
            $("#DeleteValue").val("1");
        }
        else {
            $("#DeleteValue").val("0");
        }
        $("#editpermissions").show();
    }
});