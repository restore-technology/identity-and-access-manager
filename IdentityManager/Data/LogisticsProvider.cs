﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsProvider
    {
        public LogisticsProvider()
        {
            Jobs = new HashSet<Job>();
            Jobs4444s = new HashSet<Jobs4444>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Job> Jobs { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444s { get; set; }
    }
}
