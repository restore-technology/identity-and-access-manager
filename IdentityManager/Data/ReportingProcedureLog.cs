﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingProcedureLog
    {
        public int ProcedureLogId { get; set; }
        public string ProcedureName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string ProcedureParameters { get; set; }
    }
}
