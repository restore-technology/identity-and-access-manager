﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VwInventorySerialized
    {
        public Guid JobId { get; set; }
        public int Ref { get; set; }
        public string Name { get; set; }
        public int? Expected { get; set; }
    }
}
