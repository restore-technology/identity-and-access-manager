﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class QualityControl
    {
        public Guid Id { get; set; }
        public int Reference { get; set; }
        public Guid JobAsset { get; set; }
        public Guid SalesApprovalStatus { get; set; }
        public string SalesRejectionReason { get; set; }
        public DateTime? SalesApprovalDate { get; set; }
        public Guid? SalesApprovedBy { get; set; }
        public Guid? SalesRequeueTo { get; set; }
        public Guid OperationApprovalStatus { get; set; }
        public string OperationRejectionReason { get; set; }
        public DateTime? OperationApprovalDate { get; set; }
        public Guid? OperationApprovedBy { get; set; }
        public Guid? OperationRequeueTo { get; set; }
        public bool? IsDeferred { get; set; }
        public DateTime? DeferredDateTime { get; set; }

        public virtual JobAsset JobAssetNavigation { get; set; }
        public virtual ApprovalStatus OperationApprovalStatusNavigation { get; set; }
        public virtual Contact OperationApprovedByNavigation { get; set; }
        public virtual ProcessingSystemProcess OperationRequeueToNavigation { get; set; }
        public virtual ApprovalStatus SalesApprovalStatusNavigation { get; set; }
        public virtual Contact SalesApprovedByNavigation { get; set; }
        public virtual ProcessingSystemProcess SalesRequeueToNavigation { get; set; }
    }
}
