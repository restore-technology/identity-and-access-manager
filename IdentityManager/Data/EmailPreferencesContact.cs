﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailPreferencesContact
    {
        public EmailPreferencesContact()
        {
            EmailPreferencesRoles = new HashSet<EmailPreferencesRole>();
        }

        public long EmailPreferenceContactId { get; set; }
        public Guid EmailPreferenceId { get; set; }
        public Guid ContactId { get; set; }

        public virtual ICollection<EmailPreferencesRole> EmailPreferencesRoles { get; set; }
    }
}
