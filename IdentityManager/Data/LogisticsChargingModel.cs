﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsChargingModel
    {
        public int LogisticsChargingModelId { get; set; }
        public string LogisticsChargingModelName { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
