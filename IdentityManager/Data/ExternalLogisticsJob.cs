﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ExternalLogisticsJob
    {
        public Guid Id { get; set; }
        public Guid Job { get; set; }
        public Guid JobType { get; set; }
        public DateTime CreatedAt { get; set; }
        public string LogisticsSystemJobId { get; set; }
        public int Order { get; set; }
        public bool IsFinished { get; set; }
        public string RouteNumber { get; set; }
        public Guid? WorkflowLogisticsStage { get; set; }

        public virtual Job JobNavigation { get; set; }
        public virtual JobType JobTypeNavigation { get; set; }
        public virtual WorkflowLogisticsInhouseStage WorkflowLogisticsStageNavigation { get; set; }
    }
}
