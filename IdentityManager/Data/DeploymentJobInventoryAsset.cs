﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class DeploymentJobInventoryAsset
    {
        public long InventoryAssetsId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid? JobId { get; set; }
        public int AssetId { get; set; }
        public int AssetTypeId { get; set; }
        public int ManufacturerId { get; set; }
        public int ModelId { get; set; }
        public string SerialNumber { get; set; }
        public string AssetTag { get; set; }
        public int Stnumber { get; set; }
        public int GradeId { get; set; }
        public string Ponumber { get; set; }
        public Guid? Project { get; set; }
        public Guid? BillingCode { get; set; }
        public string CustomizableField1 { get; set; }
        public string CustomizableField2 { get; set; }
        public string CustomizableField3 { get; set; }
        public Guid? RequestedBy { get; set; }
        public Guid? DeploymentLocation { get; set; }
        public Guid? DeploymentContact { get; set; }
        public DateTime? RequestedDeliveryDate { get; set; }
        public Guid? ShipFromLocation { get; set; }
        public Guid? ShipFromContact { get; set; }
        public string CustomerComments { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
