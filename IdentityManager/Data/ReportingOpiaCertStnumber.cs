﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingOpiaCertStnumber
    {
        public int? Stnumber { get; set; }
        public DateTime? LastExecutedDate { get; set; }
    }
}
