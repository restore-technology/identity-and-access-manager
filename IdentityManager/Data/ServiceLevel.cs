﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ServiceLevel
    {
        public ServiceLevel()
        {
            ServiceLevelCharges = new HashSet<ServiceLevelCharge>();
        }

        public int ServiceLevelId { get; set; }
        public string ServiceLevelName { get; set; }
        public int ServiceLevelRating { get; set; }

        public virtual ICollection<ServiceLevelCharge> ServiceLevelCharges { get; set; }
    }
}
