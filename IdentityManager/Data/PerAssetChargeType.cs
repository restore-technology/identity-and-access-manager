﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class PerAssetChargeType
    {
        public PerAssetChargeType()
        {
            CustomerCharges = new HashSet<CustomerCharge>();
            ServiceLevelCharges = new HashSet<ServiceLevelCharge>();
        }

        public int PerAssetChargeTypeId { get; set; }
        public string PerAssetChargeTypeName { get; set; }

        public virtual ICollection<CustomerCharge> CustomerCharges { get; set; }
        public virtual ICollection<ServiceLevelCharge> ServiceLevelCharges { get; set; }
    }
}
