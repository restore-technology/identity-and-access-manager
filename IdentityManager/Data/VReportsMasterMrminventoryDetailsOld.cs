﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsMasterMrminventoryDetailsOld
    {
        public int AssetId { get; set; }
        public string AssetType { get; set; }
        public int? OriginLot { get; set; }
        public string OrigAccount { get; set; }
        public string OrigLocation { get; set; }
        public string Class { get; set; }
        public string SubClass { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string ModelNumber { get; set; }
        public string SerialNumber { get; set; }
        public string CustomerAssetId { get; set; }
        public string Grade { get; set; }
        public double? Weight { get; set; }
        public string AuditCputype { get; set; }
        public string AuditCpuspeed { get; set; }
        public string AuditTotalMemory { get; set; }
        public string AuditHddcapacity { get; set; }
        public string AuditHddtype { get; set; }
        public string AuditScreenSize { get; set; }
        public string AuditScreenType { get; set; }
        public string FinalCputype { get; set; }
        public string FinalCpuspeed { get; set; }
        public string FinalTotalMemory { get; set; }
        public string FinalHddcapacity { get; set; }
        public string FinalHddtype { get; set; }
        public string FinalScreenSize { get; set; }
        public string FinalScreenType { get; set; }
        public string AuditOperatingSystem { get; set; }
        public int? HardDriveCount { get; set; }
        public int? PartsQuantityFree { get; set; }
        public int? PartsQuantityOnHand { get; set; }
        public string AuditDescription { get; set; }
        public string AuditFunctionalDescription { get; set; }
        public string AuditCosmeticDescription { get; set; }
        public string GradeAudit { get; set; }
        public DateTime? AuditCompletedOn { get; set; }
        public string AuditCompleteBy { get; set; }
        public string UpgradeDescription { get; set; }
        public string UpgradeFunctionalDescription { get; set; }
        public string UpgradeCosmeticsDescription { get; set; }
        public string StatusName { get; set; }
        public DateTime? StatusModifiedOn { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? QccompleteDate { get; set; }
        public string Warehouse { get; set; }
        public string WarehouseLocation { get; set; }
        public string PalletId { get; set; }
        public string ResaleChannel { get; set; }
        public decimal SettlementValue { get; set; }
        public decimal TotalUpgradeValue { get; set; }
        public decimal? FairMarketValue { get; set; }
        public decimal? ResalePrice { get; set; }
        public decimal? RemarketingPercentage { get; set; }
        public string ProcurementMethod { get; set; }
        public decimal FinancialTotal { get; set; }
        public decimal StandardTotal { get; set; }
        public decimal? AdditionalTotal { get; set; }
        public string ServiceStatus { get; set; }
        public string TrueModelDescription { get; set; }
        public string TruePartNumber { get; set; }
        public string AccountingRefNum { get; set; }
        public string AccountName { get; set; }
    }
}
