﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingOpiaXmlManualExport
    {
        public int? StreamsSerialNo { get; set; }
    }
}
