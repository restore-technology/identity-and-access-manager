﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class BlanccoReportsDatum
    {
        public long Id { get; set; }
        public Guid? ReportId { get; set; }
        public string AssetId { get; set; }
        public string LotNumber { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string AssetSerialNumber { get; set; }
        public string Size { get; set; }
        public string Bus { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Method { get; set; }
        public string Status { get; set; }
        public DateTime? DateInsert { get; set; }
    }
}
