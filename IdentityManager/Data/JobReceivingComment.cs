﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobReceivingComment
    {
        public Guid? Id { get; set; }
        public Guid? JobId { get; set; }
        public Guid? AssetTypeId { get; set; }
        public int? Stnumber { get; set; }
        public Guid? VarianceReasonId { get; set; }
        public string ExecutiveOverrideReason { get; set; }
        public string AssetsReferredTo { get; set; }
        public Guid? ReasonBy { get; set; }
    }
}
