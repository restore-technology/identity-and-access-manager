﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobsUpdatePermission
    {
        public int Id { get; set; }
        public int ContactReferenceId { get; set; }
        public bool? Status { get; set; }
        public int? AuthoriseToPush { get; set; }
    }
}
