﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ErasureLicenceCharge
    {
        public long LicenceChargeId { get; set; }
        public string LicenceTier { get; set; }
        public int? LowerValue { get; set; }
        public int? UpperValue { get; set; }
        public decimal? LicenceCharge { get; set; }
        public bool? IsActive { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
