﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ServiceLevelCharge
    {
        public long ServiceLevelChargeId { get; set; }
        public int? ServiceLevelId { get; set; }
        public int? PerAssetChargeTypeId { get; set; }
        public decimal? ServiceLevelCharge1 { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual PerAssetChargeType PerAssetChargeType { get; set; }
        public virtual ServiceLevel ServiceLevel { get; set; }
    }
}
