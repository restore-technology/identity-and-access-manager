﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class MiddlewareCommentConversion
    {
        public int ConversionId { get; set; }
        public string AssetType { get; set; }
        public string CodeType { get; set; }
        public string Itadcomments { get; set; }
        public string OutputComments { get; set; }
        public string OutputCode { get; set; }
        public string OutputGrade { get; set; }
    }
}
