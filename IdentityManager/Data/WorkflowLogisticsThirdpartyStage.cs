﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class WorkflowLogisticsThirdpartyStage
    {
        public Guid Id { get; set; }

        public virtual WorkflowStage IdNavigation { get; set; }
    }
}
