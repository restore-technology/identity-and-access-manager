﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailTemplatesContactRole
    {
        public Guid EmailTemplateId { get; set; }
        public Guid ContactRoleId { get; set; }

        public virtual ContactRole ContactRole { get; set; }
        public virtual EmailTemplate EmailTemplate { get; set; }
    }
}
