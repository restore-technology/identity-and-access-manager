﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobVariance
    {
        public Guid? JobVarianceId { get; set; }
        public Guid? JobId { get; set; }
        public Guid? AssetTypeId { get; set; }
        public Guid? VarianceReasonId { get; set; }
        public int? VarianceReasonCount { get; set; }
        public int? VarianceError { get; set; }
        public string ExecutiveOverrideReason { get; set; }
        public bool? IsSubmit { get; set; }
        public Guid? ReasonBy { get; set; }
    }
}
