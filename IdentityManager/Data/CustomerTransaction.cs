﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerTransaction
    {
        public Guid Id { get; set; }
        public int Reference { get; set; }
        public DateTime Date { get; set; }
        public Guid Customer { get; set; }
        public Guid? Job { get; set; }
        public Guid TransactionType { get; set; }
        public Guid ChargeType { get; set; }
        public Guid Nature { get; set; }
        public Guid? BillingCode { get; set; }
        public decimal EstimatedCost { get; set; }
        public decimal? ActualCost { get; set; }
        public string Ponumber { get; set; }
        public string BillingPeriod { get; set; }
        public string Note { get; set; }
        public Guid Status { get; set; }
        public bool IsActive { get; set; }
        public string ChargeDescription { get; set; }
        public bool IsProcessingSystemCharge { get; set; }
        public bool LogisticsNotChargeable { get; set; }
        public bool? IsAutoCharge { get; set; }
        public Guid? QuotedChargeType { get; set; }

        public virtual BillingCode BillingCodeNavigation { get; set; }
        public virtual ChargeType ChargeTypeNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual Job JobNavigation { get; set; }
        public virtual TransactionNature NatureNavigation { get; set; }
        public virtual CustomerTransactionStatus StatusNavigation { get; set; }
        public virtual TransactionType TransactionTypeNavigation { get; set; }
    }
}
