﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class MiddlewareCputypeConversion
    {
        public int CputypeId { get; set; }
        public string Mrmcputype { get; set; }
        public string Ltscputype { get; set; }
    }
}
