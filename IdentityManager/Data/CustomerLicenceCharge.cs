﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerLicenceCharge
    {
        public long CustomerLicenceChargeId { get; set; }
        public Guid? CustomerId { get; set; }
        public long? LicenceChargeId { get; set; }
        public decimal? LicenceCharge { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
