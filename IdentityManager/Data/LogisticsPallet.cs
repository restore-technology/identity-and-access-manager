﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsPallet
    {
        public long PalletsId { get; set; }
        public int? PostCodeZone { get; set; }
        public decimal? FirstItem { get; set; }
        public decimal? _2to14Items { get; set; }
        public decimal? _14plusItems { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
