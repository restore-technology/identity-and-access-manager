﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class AssetPricePredictedPrice
    {
        public int AssetPricePredictedCostId { get; set; }
        public Guid CustomerId { get; set; }
        public string AssetType { get; set; }
        public decimal? AssetCost { get; set; }
        public bool PriceType { get; set; }
    }
}
