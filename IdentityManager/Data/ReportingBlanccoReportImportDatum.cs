﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingBlanccoReportImportDatum
    {
        public string DocumentId { get; set; }
        public string Timestamp { get; set; }
        public string State { get; set; }
        public string Uuid { get; set; }
        public string Stnumber { get; set; }
        public byte? ReportGenerated { get; set; }
    }
}
