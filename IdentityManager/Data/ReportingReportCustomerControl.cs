﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingReportCustomerControl
    {
        public Guid CustomerGuid { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public byte Dispositions { get; set; }
    }
}
