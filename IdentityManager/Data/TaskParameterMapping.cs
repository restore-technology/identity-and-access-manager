﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class TaskParameterMapping
    {
        public int TaskParameterId { get; set; }
        public int? TaskId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
        public bool? ParameterStatus { get; set; }

        public virtual TaskType TaskParameter { get; set; }
    }
}
