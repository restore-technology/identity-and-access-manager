﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerServiceLine
    {
        public long CustomerServiceLineId { get; set; }
        public Guid? CustomerId { get; set; }
        public long? ServiceLineId { get; set; }
        public bool? IsAvailable { get; set; }
        public decimal? ApplicableCharge { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
