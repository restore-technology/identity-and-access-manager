﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerServiceBundle
    {
        public long CustomerServiceBundleId { get; set; }
        public Guid? CustomerId { get; set; }
        public long? ServiceBundleId { get; set; }
        public bool? IsAvailable { get; set; }
        public decimal? ApplicableCharges { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
