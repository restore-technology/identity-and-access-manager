﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingOpiaAssetDatum
    {
        public int? JobNo { get; set; }
        public string CustomerName { get; set; }
        public string CustomerReference { get; set; }
        public string Project { get; set; }
        public string Requestor { get; set; }
        public string CollectionDate { get; set; }
        public string ReceivedDate { get; set; }
        public string AuditedDate { get; set; }
        public string ProcessedDate { get; set; }
        public string AssetType { get; set; }
        public string SecurityStatus { get; set; }
        public string OemserialNo { get; set; }
        public string ReferenceId { get; set; }
        public int StreamsSerialNo { get; set; }
        public string Oem { get; set; }
        public string ModelDescription { get; set; }
        public string EquipmentDescription { get; set; }
        public string RecoveryMediaPresent { get; set; }
        public string Processor { get; set; }
        public string Speed { get; set; }
        public string MemoryMb { get; set; }
        public int? HardDriveCount { get; set; }
        public int? HardDriveWiped { get; set; }
        public int? HardDriveDestroyed { get; set; }
        public string Hddsize { get; set; }
        public string Hddtype { get; set; }
        public string MonitorSize { get; set; }
        public string ManufactureDate { get; set; }
        public string Coa { get; set; }
        public string Disposition { get; set; }
        public string ConditionCode { get; set; }
        public string ConditionComments { get; set; }
        public decimal? TotalServiceCharges { get; set; }
        public decimal? SalePrice { get; set; }
    }
}
