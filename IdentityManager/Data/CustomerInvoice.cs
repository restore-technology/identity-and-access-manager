﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerInvoice
    {
        public long CustomerInvoiceId { get; set; }
        public Guid? CustomerId { get; set; }
        public Guid? BillingEntity { get; set; }
        public long? BillingCurrency { get; set; }
        public double? ExchangeRate { get; set; }
        public int? InvoiceType { get; set; }
        public string InvoiceEmailAddress { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? ContactId { get; set; }
        public int? SettlementModel { get; set; }
        public int? RemarketingSla { get; set; }
        public long? VatrateId { get; set; }

        public virtual SettlementModel SettlementModelNavigation { get; set; }
    }
}
