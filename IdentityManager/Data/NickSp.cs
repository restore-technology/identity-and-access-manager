﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class NickSp
    {
        public int? Id { get; set; }
        public int? Asset { get; set; }
        public int? Category { get; set; }
        public string DataBearing { get; set; }
        public string Dsmethod { get; set; }
        public double? Charge { get; set; }
        public string MrmassetClass { get; set; }
        public string StreamsAssetClass { get; set; }
        public int? Hddcount { get; set; }
        public int? StreamsJobNo { get; set; }
    }
}
