﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Project
    {
        public Project()
        {
            Jobs = new HashSet<Job>();
            Jobs4444s = new HashSet<Jobs4444>();
        }

        public Guid Id { get; set; }
        public int Reference { get; set; }
        public string Name { get; set; }
        public Guid Customer { get; set; }
        public Guid? ProjectManager { get; set; }
        public Guid? BillingCode { get; set; }
        public Guid? CustomerContact { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }

        public virtual BillingCode BillingCodeNavigation { get; set; }
        public virtual Contact CustomerContactNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual Contact ProjectManagerNavigation { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444s { get; set; }
    }
}
