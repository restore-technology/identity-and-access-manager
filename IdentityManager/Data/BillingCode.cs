﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class BillingCode
    {
        public BillingCode()
        {
            CustomerTransactions = new HashSet<CustomerTransaction>();
            Jobs = new HashSet<Job>();
            Jobs4444s = new HashSet<Jobs4444>();
            Projects = new HashSet<Project>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid Customer { get; set; }
        public Guid Contact { get; set; }

        public virtual Contact ContactNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual ICollection<CustomerTransaction> CustomerTransactions { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444s { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}
