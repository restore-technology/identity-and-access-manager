﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class OpiaImportStagingFileList
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}
