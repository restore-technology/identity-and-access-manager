﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class WorkflowLogisticsInhouseStage
    {
        public WorkflowLogisticsInhouseStage()
        {
            ExternalLogisticsJobs = new HashSet<ExternalLogisticsJob>();
        }

        public Guid Id { get; set; }
        public bool IsHub { get; set; }
        public Guid? HubLocation { get; set; }

        public virtual Location HubLocationNavigation { get; set; }
        public virtual WorkflowStage IdNavigation { get; set; }
        public virtual ICollection<ExternalLogisticsJob> ExternalLogisticsJobs { get; set; }
    }
}
