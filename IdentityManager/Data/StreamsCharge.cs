﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class StreamsCharge
    {
        public int Reference { get; set; }
        public int AssetId { get; set; }
        public decimal? StreamsCharges { get; set; }
    }
}
