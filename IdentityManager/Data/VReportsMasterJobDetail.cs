﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsMasterJobDetail
    {
        public Guid Id { get; set; }
        public int Reference { get; set; }
        public string Ponumber { get; set; }
        public DateTime RequestedDate { get; set; }
        public string CustomizableField1 { get; set; }
        public string CustomizableField2 { get; set; }
        public string CustomizableField3 { get; set; }
        public bool IsSerializedCollection { get; set; }
        public bool ArePalletisedForTransportation { get; set; }
        public int? NumberOfPallets { get; set; }
        public string PalletsLocation { get; set; }
        public bool MultipleAssetsLocations { get; set; }
        public string AssetsLocation { get; set; }
        public string AssetsDeploymentLocation { get; set; }
        public bool AreOversizedItems { get; set; }
        public string CustomerComments { get; set; }
        public string RedemtechNotes { get; set; }
        public string CustomerApprovalComments { get; set; }
        public string SalesApprovalComments { get; set; }
        public string LogisticsApprovalComments { get; set; }
        public DateTime? CancelledAt { get; set; }
        public string UploadedAssets { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? SubmittedAt { get; set; }
        public DateTime? RequestedDeliveryDate { get; set; }
        public DateTime? SiteVisitScheduledAt { get; set; }
        public Guid? LogisticsSupplier { get; set; }
        public DateTime? CompletedAt { get; set; }
        public DateTime? ReadyToInvoiceAt { get; set; }
        public DateTime? InvoicedAt { get; set; }
        public bool IsLogisticsCompleted { get; set; }
        public string CustomerPrintName { get; set; }
        public string CustomerSignatureFileName { get; set; }
        public DateTime? LogisticsCollectionComplete { get; set; }
        public string VehicleNumber { get; set; }
        public decimal? QuotedPrice { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public int CustomerReference { get; set; }
        public string RequestedByForename { get; set; }
        public string RequestedBySurname { get; set; }
        public string CustomerApprovalStatus { get; set; }
        public string SalesApprovalStatus { get; set; }
        public string LogisticsApprovalStatus { get; set; }
        public string CreatedByForename { get; set; }
        public string CreatedBySurname { get; set; }
        public string RequestedJobType { get; set; }
        public Guid? ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int ProjectReference { get; set; }
        public string LogisticsJobType { get; set; }
        public string ProcessingJobType { get; set; }
        public string SourceContactForename { get; set; }
        public string SourceContactSurname { get; set; }
        public string SourceAddressReference { get; set; }
        public string SourceAddressLogisticsSystemId { get; set; }
        public string DestinationContactForename { get; set; }
        public string DestinationContactSurname { get; set; }
        public string DestinationAddressReference { get; set; }
        public string DestinationAddressLogisticsSystemId { get; set; }
        public string CustomerApprovedByForename { get; set; }
        public string CustomerApprovedBySurname { get; set; }
        public string SalesApprovedByForename { get; set; }
        public string SalesApprovedBySurname { get; set; }
        public string LogisticsApprovedByForename { get; set; }
        public string LogisticsApprovedBySurname { get; set; }
        public string CancelledByForename { get; set; }
        public string CancelledBySurname { get; set; }
        public string CompletedByForename { get; set; }
        public string CompletedBySurname { get; set; }
        public string SubmittedByForename { get; set; }
        public string SubmittedBySurname { get; set; }
        public string ReadyToInvoiceByForename { get; set; }
        public string ReadyToInvoiceBySurname { get; set; }
        public string InvoicedByForename { get; set; }
        public string InvoicedBySurname { get; set; }
        public string BillingCode { get; set; }
        public int? ProcessingSystemJobId { get; set; }
        public bool? ProcessingSystemJobIsFinished { get; set; }
        public DateTime? ActualCollectionDate { get; set; }
        public DateTime? ActualReceivedDate { get; set; }
        public string SourceAddressPostCode { get; set; }
        public int? OversizedItems { get; set; }
    }
}
