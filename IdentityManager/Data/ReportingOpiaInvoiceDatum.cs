﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingOpiaInvoiceDatum
    {
        public int Stnumber { get; set; }
        public string InvoiceNumber { get; set; }
    }
}
