﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class MasterCompany
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
