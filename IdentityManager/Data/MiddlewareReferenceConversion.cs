﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class MiddlewareReferenceConversion
    {
        public int ReferenceId { get; set; }
        public string ReferenceType { get; set; }
        public string ReferenceDescription { get; set; }
    }
}
