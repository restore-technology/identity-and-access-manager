﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class WorkflowProcessingInboundStage
    {
        public Guid Id { get; set; }

        public virtual WorkflowStage IdNavigation { get; set; }
    }
}
