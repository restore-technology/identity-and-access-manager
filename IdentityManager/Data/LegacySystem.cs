﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LegacySystem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
