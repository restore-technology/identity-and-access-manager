﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingWipUnbilledPca
    {
        public string MasterCompany { get; set; }
        public int StreamsJobNo { get; set; }
        public int? LotId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNumber { get; set; }
        public string ServiceLevelName { get; set; }
        public string PrimaryContact { get; set; }
        public string JobType { get; set; }
        public int? AssetCount { get; set; }
        public string WorkflowStatus { get; set; }
        public string DeploymentLocation { get; set; }
        public string InvoiceReference { get; set; }
        public string Rmcredit { get; set; }
        public int? CountRedeployed { get; set; }
        public int? CountSold { get; set; }
        public int? CountRecycled { get; set; }
        public int? InventoryCount { get; set; }
        public int? DaysOnShelf { get; set; }
        public int? DaysTurnaround { get; set; }
        public decimal? ProfitShare { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? PartsCharges { get; set; }
        public decimal? ClientEarnings { get; set; }
        public decimal? Rebate { get; set; }
        public decimal? AdHoc { get; set; }
        public decimal? Engineering { get; set; }
        public decimal? Logistics { get; set; }
        public decimal? AuditTrash { get; set; }
        public decimal? ProcessingService { get; set; }
    }
}
