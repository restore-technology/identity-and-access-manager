﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class SettingsReporting
    {
        public string ColBrm { get; set; }
        public string ColCrd { get; set; }
        public string ColDun { get; set; }
        public string ColPor { get; set; }
        public string ColLaptop { get; set; }
        public string ColServer { get; set; }
        public string ColMonitor { get; set; }
        public string ColPrinter { get; set; }
        public string ColDataStorage { get; set; }
        public string ColDesktop { get; set; }
        public string ColCabinet { get; set; }
        public string ColMisc { get; set; }
        public string ColOther { get; set; }
        public string ColMobile { get; set; }
        public string ColTelecomms { get; set; }
        public string ColNetwork { get; set; }
        public string ColStorage { get; set; }
        public string ColHardDrives { get; set; }
        public string ColTapes { get; set; }
        public string ColResale { get; set; }
        public string ColRedeployment { get; set; }
        public string ColHardDriveQueue { get; set; }
        public string ColPreSort { get; set; }
        public string ColProduction { get; set; }
        public string ColQualityControl { get; set; }
        public string ColReceiving { get; set; }
        public string ColRun { get; set; }
        public string ColBrs { get; set; }
    }
}
