﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsItem
    {
        public long ItemId { get; set; }
        public long? JobId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public int? ExpectedQuantity { get; set; }
        public Guid? Uuid { get; set; }
        public string Other { get; set; }
        public int? ActualQuantity { get; set; }
    }
}
