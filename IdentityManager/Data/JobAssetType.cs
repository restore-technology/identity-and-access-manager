﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobAssetType
    {
        public Guid Id { get; set; }
        public Guid Job { get; set; }
        public Guid AssetType { get; set; }
        public int Collected { get; set; }
        public int Expected { get; set; }
        public Guid? Container { get; set; }

        public virtual AssetType AssetTypeNavigation { get; set; }
        public virtual Container ContainerNavigation { get; set; }
        public virtual Job JobNavigation { get; set; }
    }
}
