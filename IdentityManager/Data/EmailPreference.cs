﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailPreference
    {
        public EmailPreference()
        {
            EmailPreferencesContact1s = new HashSet<EmailPreferencesContact1>();
            EmailPreferencesContactRoles = new HashSet<EmailPreferencesContactRole>();
            EmailPreferencesDefaultToContactRoles = new HashSet<EmailPreferencesDefaultToContactRole>();
        }

        public Guid Id { get; set; }
        public Guid Customer { get; set; }
        public Guid EmailTemplate { get; set; }
        public bool Enabled { get; set; }

        public virtual Customer CustomerNavigation { get; set; }
        public virtual EmailTemplate EmailTemplateNavigation { get; set; }
        public virtual ICollection<EmailPreferencesContact1> EmailPreferencesContact1s { get; set; }
        public virtual ICollection<EmailPreferencesContactRole> EmailPreferencesContactRoles { get; set; }
        public virtual ICollection<EmailPreferencesDefaultToContactRole> EmailPreferencesDefaultToContactRoles { get; set; }
    }
}
