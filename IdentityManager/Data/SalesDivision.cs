﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class SalesDivision
    {
        public int Id { get; set; }
        public string Division { get; set; }
    }
}
