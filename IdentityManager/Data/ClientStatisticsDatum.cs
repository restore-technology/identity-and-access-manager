﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ClientStatisticsDatum
    {
        public int StatisticsId { get; set; }
        public Guid Customer { get; set; }
        public int Draft { get; set; }
        public int Submitted { get; set; }
        public int Approved { get; set; }
        public int Scheduled { get; set; }
        public int FieldBasedWip { get; set; }
        public int DepotBasedWip { get; set; }
        public int Completed { get; set; }
        public int DepotBasedWinProgress { get; set; }
        public int AvailableForRedeployment { get; set; }
        public int OnDepOrderUnallocated { get; set; }
        public int AwaitingSale { get; set; }
        public int AwaitingRecycling { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? Job { get; set; }
    }
}
