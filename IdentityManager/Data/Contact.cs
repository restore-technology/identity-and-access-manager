﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Contact
    {
        public Contact()
        {
            BillingCodes = new HashSet<BillingCode>();
            ContactsRoles = new HashSet<ContactsRole>();
            CustomerPrimaryContactNavigations = new HashSet<Customer>();
            CustomerSecondaryContactNavigations = new HashSet<Customer>();
            EmailPreferencesContact1s = new HashSet<EmailPreferencesContact1>();
            JobCancelledByNavigations = new HashSet<Job>();
            JobCompletedByNavigations = new HashSet<Job>();
            JobCreatedByNavigations = new HashSet<Job>();
            JobCustomerApprovedByNavigations = new HashSet<Job>();
            JobDestinationContactNavigations = new HashSet<Job>();
            JobInvoicedByNavigations = new HashSet<Job>();
            JobLogisticsApprovedByNavigations = new HashSet<Job>();
            JobNotes = new HashSet<JobNote>();
            JobReadyToInvoiceByNavigations = new HashSet<Job>();
            JobRequestedByNavigations = new HashSet<Job>();
            JobSalesApprovedByNavigations = new HashSet<Job>();
            JobSourceContactNavigations = new HashSet<Job>();
            JobSubmittedByNavigations = new HashSet<Job>();
            JobUpdatedByNavigations = new HashSet<Job>();
            Jobs4444CancelledByNavigations = new HashSet<Jobs4444>();
            Jobs4444CompletedByNavigations = new HashSet<Jobs4444>();
            Jobs4444CreatedByNavigations = new HashSet<Jobs4444>();
            Jobs4444CustomerApprovedByNavigations = new HashSet<Jobs4444>();
            Jobs4444DestinationContactNavigations = new HashSet<Jobs4444>();
            Jobs4444InvoicedByNavigations = new HashSet<Jobs4444>();
            Jobs4444LogisticsApprovedByNavigations = new HashSet<Jobs4444>();
            Jobs4444ReadyToInvoiceByNavigations = new HashSet<Jobs4444>();
            Jobs4444RequestedByNavigations = new HashSet<Jobs4444>();
            Jobs4444SalesApprovedByNavigations = new HashSet<Jobs4444>();
            Jobs4444SourceContactNavigations = new HashSet<Jobs4444>();
            Jobs4444SubmittedByNavigations = new HashSet<Jobs4444>();
            LoginAttempts = new HashSet<LoginAttempt>();
            Notes = new HashSet<Note>();
            ProjectCustomerContactNavigations = new HashSet<Project>();
            ProjectProjectManagerNavigations = new HashSet<Project>();
            QualityControlOperationApprovedByNavigations = new HashSet<QualityControl>();
            QualityControlSalesApprovedByNavigations = new HashSet<QualityControl>();
            WorkflowSteps = new HashSet<WorkflowStep>();
        }

        public Guid Id { get; set; }
        public int Reference { get; set; }
        public Guid Customer { get; set; }
        public Guid? Location { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string JobTitle { get; set; }
        public string Telephone { get; set; }
        public string Ddi { get; set; }
        public string Mobile { get; set; }
        public Guid? Salutation { get; set; }
        public bool IsSystemUser { get; set; }
        public Guid? AccessGroup { get; set; }
        public string Username { get; set; }
        public Guid? Status { get; set; }
        public bool IsActive { get; set; }
        public bool? IsAssetVarianceOverride { get; set; }
        public string HashPassword { get; set; }
        public string Salt { get; set; }
        public bool IsDpo { get; set; }

        public virtual AccessGroup AccessGroupNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual Location LocationNavigation { get; set; }
        public virtual Salutation SalutationNavigation { get; set; }
        public virtual ContactStatus StatusNavigation { get; set; }
        public virtual ICollection<BillingCode> BillingCodes { get; set; }
        public virtual ICollection<ContactsRole> ContactsRoles { get; set; }
        public virtual ICollection<Customer> CustomerPrimaryContactNavigations { get; set; }
        public virtual ICollection<Customer> CustomerSecondaryContactNavigations { get; set; }
        public virtual ICollection<EmailPreferencesContact1> EmailPreferencesContact1s { get; set; }
        public virtual ICollection<Job> JobCancelledByNavigations { get; set; }
        public virtual ICollection<Job> JobCompletedByNavigations { get; set; }
        public virtual ICollection<Job> JobCreatedByNavigations { get; set; }
        public virtual ICollection<Job> JobCustomerApprovedByNavigations { get; set; }
        public virtual ICollection<Job> JobDestinationContactNavigations { get; set; }
        public virtual ICollection<Job> JobInvoicedByNavigations { get; set; }
        public virtual ICollection<Job> JobLogisticsApprovedByNavigations { get; set; }
        public virtual ICollection<JobNote> JobNotes { get; set; }
        public virtual ICollection<Job> JobReadyToInvoiceByNavigations { get; set; }
        public virtual ICollection<Job> JobRequestedByNavigations { get; set; }
        public virtual ICollection<Job> JobSalesApprovedByNavigations { get; set; }
        public virtual ICollection<Job> JobSourceContactNavigations { get; set; }
        public virtual ICollection<Job> JobSubmittedByNavigations { get; set; }
        public virtual ICollection<Job> JobUpdatedByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444CancelledByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444CompletedByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444CreatedByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444CustomerApprovedByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444DestinationContactNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444InvoicedByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444LogisticsApprovedByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444ReadyToInvoiceByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444RequestedByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444SalesApprovedByNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444SourceContactNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444SubmittedByNavigations { get; set; }
        public virtual ICollection<LoginAttempt> LoginAttempts { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<Project> ProjectCustomerContactNavigations { get; set; }
        public virtual ICollection<Project> ProjectProjectManagerNavigations { get; set; }
        public virtual ICollection<QualityControl> QualityControlOperationApprovedByNavigations { get; set; }
        public virtual ICollection<QualityControl> QualityControlSalesApprovedByNavigations { get; set; }
        public virtual ICollection<WorkflowStep> WorkflowSteps { get; set; }
    }
}
