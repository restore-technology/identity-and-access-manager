﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class AssetProcessedExtract
    {
        public int DockId { get; set; }
        public string DockName { get; set; }
        public Guid? UserId { get; set; }
        public string TechName { get; set; }
        public string DispositionName { get; set; }
        public int AssetRecId { get; set; }
        public string Class { get; set; }
        public string HarmonizeCode { get; set; }
        public int? SubClassId { get; set; }
        public string SubClass { get; set; }
        public string Manufacturer { get; set; }
        public string ItemDescription { get; set; }
        public string AuditDescription { get; set; }
        public DateTime? AuditCompletedOn { get; set; }
        public string AuditCompletedByUserId { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
    }
}
