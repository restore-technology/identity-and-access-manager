﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailPreferencesDefaultToContactRole
    {
        public Guid EmailPreferenceId { get; set; }
        public Guid DefaultToContactRoleId { get; set; }

        public virtual ContactRole DefaultToContactRole { get; set; }
        public virtual EmailPreference EmailPreference { get; set; }
    }
}
