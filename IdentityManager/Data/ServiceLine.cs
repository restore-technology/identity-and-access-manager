﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ServiceLine
    {
        public long ServiceLineId { get; set; }
        public string ServiceLineName { get; set; }
        public string NominalCode { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
