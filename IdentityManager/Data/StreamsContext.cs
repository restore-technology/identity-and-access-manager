﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
#nullable disable

namespace IdentityManager.Data
{
    public partial class StreamsContext : DbContext
    {
        private readonly IConfiguration _configuration;
        public StreamsContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public StreamsContext(DbContextOptions<StreamsContext> options, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
        }

        public virtual DbSet<AccessGroup> AccessGroups { get; set; }
        public virtual DbSet<Apiworkflow> Apiworkflows { get; set; }
        public virtual DbSet<ApplicationEvent> ApplicationEvents { get; set; }
        public virtual DbSet<ApprovalStatus> ApprovalStatuses { get; set; }
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<AssetDataBearingCharge> AssetDataBearingCharges { get; set; }
        public virtual DbSet<AssetPricePredictedPrice> AssetPricePredictedPrices { get; set; }
        public virtual DbSet<AssetProcessedExtract> AssetProcessedExtracts { get; set; }
        public virtual DbSet<AssetProcessedSummary> AssetProcessedSummaries { get; set; }
        public virtual DbSet<AssetSortedExtract> AssetSortedExtracts { get; set; }
        public virtual DbSet<AssetSortedSummary> AssetSortedSummaries { get; set; }
        public virtual DbSet<AssetStatus> AssetStatuses { get; set; }
        public virtual DbSet<AssetType> AssetTypes { get; set; }
        public virtual DbSet<BackupEmailTemplatesNjh> BackupEmailTemplatesNjhs { get; set; }
        public virtual DbSet<BillingCode> BillingCodes { get; set; }
        public virtual DbSet<BlanccoReportsDataBackup> BlanccoReportsDataBackups { get; set; }
        public virtual DbSet<BlanccoReportsDatum> BlanccoReportsData { get; set; }
        public virtual DbSet<CancellationReason> CancellationReasons { get; set; }
        public virtual DbSet<ChargeType> ChargeTypes { get; set; }
        public virtual DbSet<Cj1> Cj1s { get; set; }
        public virtual DbSet<ClientStatisticsDatum> ClientStatisticsData { get; set; }
        public virtual DbSet<CompanyServiceLine> CompanyServiceLines { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<ContactRole> ContactRoles { get; set; }
        public virtual DbSet<ContactStatus> ContactStatuses { get; set; }
        public virtual DbSet<ContactsImportStagingWiz> ContactsImportStagingWizs { get; set; }
        public virtual DbSet<ContactsRole> ContactsRoles { get; set; }
        public virtual DbSet<ContactsRuncornImportTemp> ContactsRuncornImportTemps { get; set; }
        public virtual DbSet<Container> Containers { get; set; }
        public virtual DbSet<ContractStatus> ContractStatuses { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<CurrencyVat> CurrencyVats { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerCharge> CustomerCharges { get; set; }
        public virtual DbSet<CustomerImportStagingWiz> CustomerImportStagingWizs { get; set; }
        public virtual DbSet<CustomerInvoice> CustomerInvoices { get; set; }
        public virtual DbSet<CustomerInvoiceAsset> CustomerInvoiceAssets { get; set; }
        public virtual DbSet<CustomerInvoiceAssetsDefault> CustomerInvoiceAssetsDefaults { get; set; }
        public virtual DbSet<CustomerLicenceCharge> CustomerLicenceCharges { get; set; }
        public virtual DbSet<CustomerLookup> CustomerLookups { get; set; }
        public virtual DbSet<CustomerNote> CustomerNotes { get; set; }
        public virtual DbSet<CustomerPortalPermission> CustomerPortalPermissions { get; set; }
        public virtual DbSet<CustomerServiceBundle> CustomerServiceBundles { get; set; }
        public virtual DbSet<CustomerServiceLine> CustomerServiceLines { get; set; }
        public virtual DbSet<CustomerTransaction> CustomerTransactions { get; set; }
        public virtual DbSet<CustomerTransactionStatus> CustomerTransactionStatuses { get; set; }
        public virtual DbSet<CustomerType> CustomerTypes { get; set; }
        public virtual DbSet<CustomersImp1> CustomersImp1s { get; set; }
        public virtual DbSet<CustomersRuncornImportTemp> CustomersRuncornImportTemps { get; set; }
        public virtual DbSet<DashboardLog> DashboardLogs { get; set; }
        public virtual DbSet<DashboardSetting> DashboardSettings { get; set; }
        public virtual DbSet<DataClassificationType> DataClassificationTypes { get; set; }
        public virtual DbSet<Ddd> Ddds { get; set; }
        public virtual DbSet<DelmenowJob> DelmenowJobs { get; set; }
        public virtual DbSet<DelmenowM> DelmenowMs { get; set; }
        public virtual DbSet<DeploymentAssetsTemp> DeploymentAssetsTemps { get; set; }
        public virtual DbSet<DeploymentJobInventoryAsset> DeploymentJobInventoryAssets { get; set; }
        public virtual DbSet<DepotSetting> DepotSettings { get; set; }
        public virtual DbSet<DispositionsStatic> DispositionsStatics { get; set; }
        public virtual DbSet<Elog> Elogs { get; set; }
        public virtual DbSet<EmailPreference> EmailPreferences { get; set; }
        public virtual DbSet<EmailPreferencesContact> EmailPreferencesContacts { get; set; }
        public virtual DbSet<EmailPreferencesContact1> EmailPreferencesContacts1 { get; set; }
        public virtual DbSet<EmailPreferencesContactRole> EmailPreferencesContactRoles { get; set; }
        public virtual DbSet<EmailPreferencesDefaultToContactRole> EmailPreferencesDefaultToContactRoles { get; set; }
        public virtual DbSet<EmailPreferencesRole> EmailPreferencesRoles { get; set; }
        public virtual DbSet<EmailQueueItem> EmailQueueItems { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }
        public virtual DbSet<EmailTemplatesContactRole> EmailTemplatesContactRoles { get; set; }
        public virtual DbSet<EmailTemplatesDefaultToContactRole> EmailTemplatesDefaultToContactRoles { get; set; }
        public virtual DbSet<EmailTriggerEvent> EmailTriggerEvents { get; set; }
        public virtual DbSet<ErasureLicenceCharge> ErasureLicenceCharges { get; set; }
        public virtual DbSet<ErasureReportExtract> ErasureReportExtracts { get; set; }
        public virtual DbSet<ErasureReportSummary> ErasureReportSummaries { get; set; }
        public virtual DbSet<ExternalLogisticsJob> ExternalLogisticsJobs { get; set; }
        public virtual DbSet<ExternalProcessingJob> ExternalProcessingJobs { get; set; }
        public virtual DbSet<FinancialDatum> FinancialData { get; set; }
        public virtual DbSet<HardDrife> HardDrives { get; set; }
        public virtual DbSet<IndustrySector> IndustrySectors { get; set; }
        public virtual DbSet<InvoiceApproval> InvoiceApprovals { get; set; }
        public virtual DbSet<InvoiceHistory> InvoiceHistories { get; set; }
        public virtual DbSet<InvoiceHistoryOld> InvoiceHistoryOlds { get; set; }
        public virtual DbSet<InvoiceOverrideTemp> InvoiceOverrideTemps { get; set; }
        public virtual DbSet<InvoiceTemplate> InvoiceTemplates { get; set; }
        public virtual DbSet<InvoiceType> InvoiceTypes { get; set; }
        public virtual DbSet<ItemReconciliationStatus> ItemReconciliationStatuses { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobAsset> JobAssets { get; set; }
        public virtual DbSet<JobAssetType> JobAssetTypes { get; set; }
        public virtual DbSet<JobClientStatus> JobClientStatuses { get; set; }
        public virtual DbSet<JobNote> JobNotes { get; set; }
        public virtual DbSet<JobProcessing> JobProcessings { get; set; }
        public virtual DbSet<JobReceivingComment> JobReceivingComments { get; set; }
        public virtual DbSet<JobReceivingCommentsBackup20180523> JobReceivingCommentsBackup20180523s { get; set; }
        public virtual DbSet<JobStatus> JobStatuses { get; set; }
        public virtual DbSet<JobStatus1> JobStatuses1 { get; set; }
        public virtual DbSet<JobTaskParameter> JobTaskParameters { get; set; }
        public virtual DbSet<JobType> JobTypes { get; set; }
        public virtual DbSet<JobVariance> JobVariances { get; set; }
        public virtual DbSet<Jobs4444> Jobs4444s { get; set; }
        public virtual DbSet<JobsHistoryOlddd> JobsHistoryOlddds { get; set; }
        public virtual DbSet<JobsOld> JobsOlds { get; set; }
        public virtual DbSet<JobsTemp> JobsTemps { get; set; }
        public virtual DbSet<JobsUpdatePermission> JobsUpdatePermissions { get; set; }
        public virtual DbSet<LegacySystem> LegacySystems { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<LocationsImportStagingWiz> LocationsImportStagingWizs { get; set; }
        public virtual DbSet<LocationsRuncornImportTemp> LocationsRuncornImportTemps { get; set; }
        public virtual DbSet<LogSeverityLevel> LogSeverityLevels { get; set; }
        public virtual DbSet<LoginAttempt> LoginAttempts { get; set; }
        public virtual DbSet<LogisticsActivity> LogisticsActivities { get; set; }
        public virtual DbSet<LogisticsAdjustmentFactor> LogisticsAdjustmentFactors { get; set; }
        public virtual DbSet<LogisticsAttachment> LogisticsAttachments { get; set; }
        public virtual DbSet<LogisticsBranch> LogisticsBranches { get; set; }
        public virtual DbSet<LogisticsCharge> LogisticsCharges { get; set; }
        public virtual DbSet<LogisticsChargingModel> LogisticsChargingModels { get; set; }
        public virtual DbSet<LogisticsCheckList> LogisticsCheckLists { get; set; }
        public virtual DbSet<LogisticsCheckListQuestion> LogisticsCheckListQuestions { get; set; }
        public virtual DbSet<LogisticsCongestionCharge> LogisticsCongestionCharges { get; set; }
        public virtual DbSet<LogisticsCustomer> LogisticsCustomers { get; set; }
        public virtual DbSet<LogisticsDeliveryContact> LogisticsDeliveryContacts { get; set; }
        public virtual DbSet<LogisticsItem> LogisticsItems { get; set; }
        public virtual DbSet<LogisticsJob> LogisticsJobs { get; set; }
        public virtual DbSet<LogisticsMobileUser> LogisticsMobileUsers { get; set; }
        public virtual DbSet<LogisticsOversizeItem> LogisticsOversizeItems { get; set; }
        public virtual DbSet<LogisticsPallet> LogisticsPallets { get; set; }
        public virtual DbSet<LogisticsPhoto> LogisticsPhotos { get; set; }
        public virtual DbSet<LogisticsPostCodeZone> LogisticsPostCodeZones { get; set; }
        public virtual DbSet<LogisticsProvider> LogisticsProviders { get; set; }
        public virtual DbSet<LogisticsSerialisedSurcharge> LogisticsSerialisedSurcharges { get; set; }
        public virtual DbSet<LogisticsStandardItem> LogisticsStandardItems { get; set; }
        public virtual DbSet<LogisticsStatisticsDashboard> LogisticsStatisticsDashboards { get; set; }
        public virtual DbSet<LogisticsSystemOrderStatus> LogisticsSystemOrderStatuses { get; set; }
        public virtual DbSet<LogisticsTrashCharge> LogisticsTrashCharges { get; set; }
        public virtual DbSet<LogisticsWorkflow> LogisticsWorkflows { get; set; }
        public virtual DbSet<LotErasureReport> LotErasureReports { get; set; }
        public virtual DbSet<MasterCompany> MasterCompanies { get; set; }
        public virtual DbSet<MiddlewareAssetComponentConversion> MiddlewareAssetComponentConversions { get; set; }
        public virtual DbSet<MiddlewareAssetComponentConversion20140717> MiddlewareAssetComponentConversion20140717s { get; set; }
        public virtual DbSet<MiddlewareAssetComponentConversionTextMatch> MiddlewareAssetComponentConversionTextMatches { get; set; }
        public virtual DbSet<MiddlewareAssetTypeConversion> MiddlewareAssetTypeConversions { get; set; }
        public virtual DbSet<MiddlewareCommentConversion> MiddlewareCommentConversions { get; set; }
        public virtual DbSet<MiddlewareCputypeConversion> MiddlewareCputypeConversions { get; set; }
        public virtual DbSet<MiddlewareReferenceConversion> MiddlewareReferenceConversions { get; set; }
        public virtual DbSet<ModelMan> ModelMen { get; set; }
        public virtual DbSet<MrmlotIjobIdSync> MrmlotIjobIdSyncs { get; set; }
        public virtual DbSet<NickSp> NickSps { get; set; }
        public virtual DbSet<Njhloc> Njhlocs { get; set; }
        public virtual DbSet<NominalCode> NominalCodes { get; set; }
        public virtual DbSet<NominalCodeChargeTypeMapping> NominalCodeChargeTypeMappings { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<OpiaImportStaging> OpiaImportStagings { get; set; }
        public virtual DbSet<OpiaImportStagingFileList> OpiaImportStagingFileLists { get; set; }
        public virtual DbSet<PerAssetChargeType> PerAssetChargeTypes { get; set; }
        public virtual DbSet<PermissionArea> PermissionAreas { get; set; }
        public virtual DbSet<PortalPermission> PortalPermissions { get; set; }
        public virtual DbSet<ProcessingStatisticsDashboard> ProcessingStatisticsDashboards { get; set; }
        public virtual DbSet<ProcessingSystemProcess> ProcessingSystemProcesses { get; set; }
        public virtual DbSet<ProcessingWorkflow> ProcessingWorkflows { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<QualityControl> QualityControls { get; set; }
        public virtual DbSet<ReportParameterContextValue> ReportParameterContextValues { get; set; }
        public virtual DbSet<ReportingAuthorisedRecyclingPartner> ReportingAuthorisedRecyclingPartners { get; set; }
        public virtual DbSet<ReportingBlanccoControlDatum> ReportingBlanccoControlData { get; set; }
        public virtual DbSet<ReportingBlanccoReportImportDatum> ReportingBlanccoReportImportData { get; set; }
        public virtual DbSet<ReportingCalendarTable> ReportingCalendarTables { get; set; }
        public virtual DbSet<ReportingCollectionsAndMovesGroupedAccount> ReportingCollectionsAndMovesGroupedAccounts { get; set; }
        public virtual DbSet<ReportingCollectionsAndMovesTest> ReportingCollectionsAndMovesTests { get; set; }
        public virtual DbSet<ReportingConsolidatedFinancial> ReportingConsolidatedFinancials { get; set; }
        public virtual DbSet<ReportingConsolidatedfinancialsRevisited> ReportingConsolidatedfinancialsRevisiteds { get; set; }
        public virtual DbSet<ReportingConsolidatedfinancialsRevisitedX> ReportingConsolidatedfinancialsRevisitedXes { get; set; }
        public virtual DbSet<ReportingCustomerAccountGrouping> ReportingCustomerAccountGroupings { get; set; }
        public virtual DbSet<ReportingDellDataJobNumber> ReportingDellDataJobNumbers { get; set; }
        public virtual DbSet<ReportingLiveForecastReport> ReportingLiveForecastReports { get; set; }
        public virtual DbSet<ReportingLiveForecastReportAssetTypeValue> ReportingLiveForecastReportAssetTypeValues { get; set; }
        public virtual DbSet<ReportingLiveForecastReportDashboard> ReportingLiveForecastReportDashboards { get; set; }
        public virtual DbSet<ReportingMrmassetDataStaging> ReportingMrmassetDataStagings { get; set; }
        public virtual DbSet<ReportingMrmassetDatum> ReportingMrmassetData { get; set; }
        public virtual DbSet<ReportingMrmserviceAppliedGrouping> ReportingMrmserviceAppliedGroupings { get; set; }
        public virtual DbSet<ReportingOpiaAssetDatum> ReportingOpiaAssetData { get; set; }
        public virtual DbSet<ReportingOpiaCertStnumber> ReportingOpiaCertStnumbers { get; set; }
        public virtual DbSet<ReportingOpiaControlDatum> ReportingOpiaControlData { get; set; }
        public virtual DbSet<ReportingOpiaInvoiceDataOld> ReportingOpiaInvoiceDataOlds { get; set; }
        public virtual DbSet<ReportingOpiaInvoiceDataPricingLogic> ReportingOpiaInvoiceDataPricingLogics { get; set; }
        public virtual DbSet<ReportingOpiaInvoiceDatum> ReportingOpiaInvoiceData { get; set; }
        public virtual DbSet<ReportingOpiaXmlConditionCode> ReportingOpiaXmlConditionCodes { get; set; }
        public virtual DbSet<ReportingOpiaXmlManualExport> ReportingOpiaXmlManualExports { get; set; }
        public virtual DbSet<ReportingOpiaXmlReportProject> ReportingOpiaXmlReportProjects { get; set; }
        public virtual DbSet<ReportingOpiaXmldataCopy> ReportingOpiaXmldataCopies { get; set; }
        public virtual DbSet<ReportingOpiaXmldatum> ReportingOpiaXmldata { get; set; }
        public virtual DbSet<ReportingProcedureLog> ReportingProcedureLogs { get; set; }
        public virtual DbSet<ReportingReportCustomerControl> ReportingReportCustomerControls { get; set; }
        public virtual DbSet<ReportingShiinvoiceDatum> ReportingShiinvoiceData { get; set; }
        public virtual DbSet<ReportingWipUnbilledPca> ReportingWipUnbilledPcas { get; set; }
        public virtual DbSet<ReportingWorkflowForecastReportCategory> ReportingWorkflowForecastReportCategories { get; set; }
        public virtual DbSet<ReportingWorkflowStatusDatum> ReportingWorkflowStatusData { get; set; }
        public virtual DbSet<ReportingWtwinvoiceDatum> ReportingWtwinvoiceData { get; set; }
        public virtual DbSet<ReportsConsolidatedFinancialsFilteredForRebate> ReportsConsolidatedFinancialsFilteredForRebates { get; set; }
        public virtual DbSet<RequestLogApi> RequestLogApis { get; set; }
        public virtual DbSet<RolePermission> RolePermissions { get; set; }
        public virtual DbSet<SalesDivision> SalesDivisions { get; set; }
        public virtual DbSet<Salutation> Salutations { get; set; }
        public virtual DbSet<ServiceBundle> ServiceBundles { get; set; }
        public virtual DbSet<ServiceBundleLineMapping> ServiceBundleLineMappings { get; set; }
        public virtual DbSet<ServiceLevel> ServiceLevels { get; set; }
        public virtual DbSet<ServiceLevelCharge> ServiceLevelCharges { get; set; }
        public virtual DbSet<ServiceLine> ServiceLines { get; set; }
        public virtual DbSet<ServiceType> ServiceTypes { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<SettingsReporting> SettingsReportings { get; set; }
        public virtual DbSet<SettlementModel> SettlementModels { get; set; }
        public virtual DbSet<SqlReport> SqlReports { get; set; }
        public virtual DbSet<SqlReportParameter> SqlReportParameters { get; set; }
        public virtual DbSet<SqlReportsAccessGroup> SqlReportsAccessGroups { get; set; }
        public virtual DbSet<SqlReportsContactRole> SqlReportsContactRoles { get; set; }
        public virtual DbSet<SqlReportsCustomer> SqlReportsCustomers { get; set; }
        public virtual DbSet<SqlreportSubCategory> SqlreportSubCategories { get; set; }
        public virtual DbSet<StageType> StageTypes { get; set; }
        public virtual DbSet<StandardReportType> StandardReportTypes { get; set; }
        public virtual DbSet<StreamsCharge> StreamsCharges { get; set; }
        public virtual DbSet<StreamsMrmstatus> StreamsMrmstatuses { get; set; }
        public virtual DbSet<TaskApiworkflowMapping> TaskApiworkflowMappings { get; set; }
        public virtual DbSet<TaskParameterMapping> TaskParameterMappings { get; set; }
        public virtual DbSet<TaskType> TaskTypes { get; set; }
        public virtual DbSet<TransactionNature> TransactionNatures { get; set; }
        public virtual DbSet<TransactionType> TransactionTypes { get; set; }
        public virtual DbSet<VDepot> VDepots { get; set; }
        public virtual DbSet<VReportsAssetFinancialDatum> VReportsAssetFinancialData { get; set; }
        public virtual DbSet<VReportsCertifiedAssetRecyclingReport> VReportsCertifiedAssetRecyclingReports { get; set; }
        public virtual DbSet<VReportsCertifiedDataSecurityReport> VReportsCertifiedDataSecurityReports { get; set; }
        public virtual DbSet<VReportsCertifiedDetailReport> VReportsCertifiedDetailReports { get; set; }
        public virtual DbSet<VReportsMasterAssetDetail> VReportsMasterAssetDetails { get; set; }
        public virtual DbSet<VReportsMasterJobDetail> VReportsMasterJobDetails { get; set; }
        public virtual DbSet<VReportsMasterMrminventoryDetail> VReportsMasterMrminventoryDetails { get; set; }
        public virtual DbSet<VReportsMasterMrminventoryDetailsMinimal> VReportsMasterMrminventoryDetailsMinimals { get; set; }
        public virtual DbSet<VReportsMasterMrminventoryDetailsNew> VReportsMasterMrminventoryDetailsNews { get; set; }
        public virtual DbSet<VReportsMasterMrminventoryDetailsOld> VReportsMasterMrminventoryDetailsOlds { get; set; }
        public virtual DbSet<VReportsMiddlewareSourceDatum> VReportsMiddlewareSourceData { get; set; }
        public virtual DbSet<VReportsMiddlewareSourceDatum1> VReportsMiddlewareSourceData1 { get; set; }
        public virtual DbSet<VReportsMrmtrashDatum> VReportsMrmtrashData { get; set; }
        public virtual DbSet<VReportsSlareportingInProgressDraftWip> VReportsSlareportingInProgressDraftWips { get; set; }
        public virtual DbSet<VReportsSlareportingInProgressDraftWipDetV3> VReportsSlareportingInProgressDraftWipDetV3s { get; set; }
        public virtual DbSet<VarianceReason> VarianceReasons { get; set; }
        public virtual DbSet<Vatcode> Vatcodes { get; set; }
        public virtual DbSet<Vatrate> Vatrates { get; set; }
        public virtual DbSet<VwAwaitingInvoicingQueue> VwAwaitingInvoicingQueues { get; set; }
        public virtual DbSet<VwDownloadCollection> VwDownloadCollections { get; set; }
        public virtual DbSet<VwInventory> VwInventories { get; set; }
        public virtual DbSet<VwInventorySerialized> VwInventorySerializeds { get; set; }
        public virtual DbSet<VwInvoiceApprovalQueue> VwInvoiceApprovalQueues { get; set; }
        public virtual DbSet<WorkflowFinishStage> WorkflowFinishStages { get; set; }
        public virtual DbSet<WorkflowLogisticsInhouseStage> WorkflowLogisticsInhouseStages { get; set; }
        public virtual DbSet<WorkflowLogisticsThirdpartyStage> WorkflowLogisticsThirdpartyStages { get; set; }
        public virtual DbSet<WorkflowProcessingInboundStage> WorkflowProcessingInboundStages { get; set; }
        public virtual DbSet<WorkflowProcessingOutboundStage> WorkflowProcessingOutboundStages { get; set; }
        public virtual DbSet<WorkflowStage> WorkflowStages { get; set; }
        public virtual DbSet<WorkflowStageStatus> WorkflowStageStatuses { get; set; }
        public virtual DbSet<WorkflowStartStage> WorkflowStartStages { get; set; }
        public virtual DbSet<WorkflowStep> WorkflowSteps { get; set; }


        public DbSet<IdentityManager.Models.AccessGroupPermissions> AccessGroupPermissions { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionstring = _configuration.GetConnectionString("StreamsConnection");
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                //optionsBuilder.UseSqlServer("Server=ITADTESTSQL;Database=Streams;User Id=anit;password=Vort3x;Trusted_Connection=False;MultipleActiveResultSets=true;");                
                optionsBuilder.UseSqlServer(connectionstring);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<AccessGroup>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__AccessGr__3214EC067F60ED59")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Apiworkflow>(entity =>
            {
                entity.ToTable("APIWorkflows");

                entity.Property(e => e.ApiworkflowId).HasColumnName("APIWorkflowID");

                entity.Property(e => e.ApiworkflowName)
                    .HasMaxLength(250)
                    .HasColumnName("APIWorkflowName");

                entity.Property(e => e.ApiworkflowStatus).HasColumnName("APIWorkflowStatus");
            });

            modelBuilder.Entity<ApplicationEvent>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Applicat__3214EC0603317E3D")
                    .IsClustered(false);

                entity.HasIndex(e => e.Date, "IX_Date")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Event, "IX_Event")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Event)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Ip)
                    .HasMaxLength(200)
                    .HasColumnName("IP");

                entity.Property(e => e.ItemKey).HasMaxLength(500);

                entity.Property(e => e.ItemType).HasMaxLength(200);

                entity.Property(e => e.UserId).HasMaxLength(200);
            });

            modelBuilder.Entity<ApprovalStatus>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Approval__3214EC0607020F21")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Asset>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Assets__3214EC060AD2A005")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Reference, e.AssetType, e.Customer, e.SerialNumber, e.Stnumber, e.CustomerAssetTag, e.Status, e.Model, e.Manufacturer, e.Disposition }, "ClusteredIndex-20190729-102647")
                    .IsClustered()
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.AssetType, "IX_Assets->AssetType")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Customer, "IX_Assets->Customer")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Stnumber, "IX_STNumber")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.SerialNumber, "IX_SerialNumber")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Condition).HasMaxLength(2);

                entity.Property(e => e.Cost).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.CustomerAssetTag).HasMaxLength(50);

                entity.Property(e => e.DataSecurityCharge).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.DiscoveryCharge).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.Disposition).HasMaxLength(200);

                entity.Property(e => e.Manufacturer).HasMaxLength(200);

                entity.Property(e => e.Model).HasMaxLength(200);

                entity.Property(e => e.OtherCharge).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.Price).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.RecyclingCharge).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.RepairCharge).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.SerialNumber).HasMaxLength(200);

                entity.Property(e => e.Stnumber).HasColumnName("STNumber");

                entity.Property(e => e.TestingCharge).HasColumnType("numeric(27, 2)");

                entity.HasOne(d => d.AssetTypeNavigation)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.AssetType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Asset.AssetType");

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Asset.Customer");

                entity.HasOne(d => d.StatusNavigation)
                    .WithMany(p => p.Assets)
                    .HasForeignKey(d => d.Status)
                    .HasConstraintName("FK_Asset.Status->AssetStatus");
            });

            modelBuilder.Entity<AssetDataBearingCharge>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("AssetDataBearingCharge", "Materialised");

                entity.Property(e => e.DataBearing)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Dsmethod)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("DSMethod");

                entity.Property(e => e.Hddcount).HasColumnName("HDDCount");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MrmassetClass)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("MRMAssetClass");

                entity.Property(e => e.StreamsAssetClass)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AssetPricePredictedPrice>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("AssetPricePredictedPrice");

                entity.Property(e => e.AssetCost).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.AssetPricePredictedCostId).ValueGeneratedOnAdd();

                entity.Property(e => e.AssetType)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AssetProcessedExtract>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("AssetProcessedExtract");

                entity.Property(e => e.AuditCompletedByUserId)
                    .HasMaxLength(50)
                    .HasColumnName("AuditCompletedByUserID")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCompletedOn).HasColumnType("datetime");

                entity.Property(e => e.AuditDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Class)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DateCompleted).HasColumnType("datetime");

                entity.Property(e => e.DispositionName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DockId).HasColumnName("DockID");

                entity.Property(e => e.DockName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.HarmonizeCode)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ItemDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubClass)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubClassId).HasColumnName("SubClassID");

                entity.Property(e => e.TechName)
                    .HasMaxLength(101)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<AssetProcessedSummary>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("AssetProcessedSummary");

                entity.Property(e => e.DepotName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.TechId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TechName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AssetSortedExtract>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("AssetSortedExtract");

                entity.Property(e => e.Class)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateCompleted).HasColumnType("datetime");

                entity.Property(e => e.DateEnteredSort).HasColumnType("date");

                entity.Property(e => e.DockName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.TechName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AssetSortedSummary>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("AssetSortedSummary");

                entity.Property(e => e.DepotName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.TechName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AssetStatus>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__AssetSta__3214EC060EA330E9")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<AssetType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__AssetTyp__3214EC061273C1CD")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<BackupEmailTemplatesNjh>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Backup_EmailTemplates_NJH");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<BillingCode>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__BillingC__3214EC06164452B1")
                    .IsClustered(false);

                entity.HasIndex(e => e.Customer, "IX_BillingCodes->Customer")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.ContactNavigation)
                    .WithMany(p => p.BillingCodes)
                    .HasForeignKey(d => d.Contact)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BillingCode.Contact");

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.BillingCodes)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BillingCode.Customer");
            });

            modelBuilder.Entity<BlanccoReportsDataBackup>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("BlanccoReportsDataBackup");

                entity.Property(e => e.AssetId)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("AssetID");

                entity.Property(e => e.AssetSerialNumber)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Bus)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateInsert).HasColumnType("datetime");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.LotNumber)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Method)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.Property(e => e.Size)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BlanccoReportsDatum>(entity =>
            {
                entity.Property(e => e.AssetId)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("AssetID");

                entity.Property(e => e.AssetSerialNumber)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Bus)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateInsert)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.LotNumber)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Method)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.Property(e => e.Size)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CancellationReason>(entity =>
            {
                entity.Property(e => e.CancellationReason1)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("CancellationReason");
            });

            modelBuilder.Entity<ChargeType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__ChargeTy__3214EC061A14E395")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Cj1>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("_____CJ1");

                entity.Property(e => e.AssetTag).HasMaxLength(150);

                entity.Property(e => e.AssetType).HasMaxLength(400);

                entity.Property(e => e.ClientEarnings).HasColumnType("decimal(34, 8)");

                entity.Property(e => e.ConditionCode).HasMaxLength(250);

                entity.Property(e => e.Hddsize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDSize");

                entity.Property(e => e.Hddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDType");

                entity.Property(e => e.InboundActualCollectionDate)
                    .HasColumnType("date")
                    .HasColumnName("Inbound_ActualCollectionDate");

                entity.Property(e => e.InboundActualReceivedDate)
                    .HasColumnType("date")
                    .HasColumnName("Inbound_ActualReceivedDate");

                entity.Property(e => e.InboundBillingCode)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_BillingCode");

                entity.Property(e => e.InboundCollectionDate)
                    .HasColumnType("date")
                    .HasColumnName("Inbound_CollectionDate");

                entity.Property(e => e.InboundCustomerName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_CustomerName");

                entity.Property(e => e.InboundCustomerNumber)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_CustomerNumber");

                entity.Property(e => e.InboundDestinationLocation)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_DestinationLocation");

                entity.Property(e => e.InboundJobId).HasColumnName("Inbound_JobId");

                entity.Property(e => e.InboundJobNo).HasColumnName("Inbound_JobNo");

                entity.Property(e => e.InboundOriginatingLocation)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_OriginatingLocation");

                entity.Property(e => e.InboundPonumber)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_PONumber");

                entity.Property(e => e.InboundProductionNo).HasColumnName("Inbound_ProductionNo");

                entity.Property(e => e.InboundProject)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_Project");

                entity.Property(e => e.InboundReference).HasColumnName("Inbound_Reference");

                entity.Property(e => e.InboundRequestedBy)
                    .IsRequired()
                    .HasMaxLength(401)
                    .HasColumnName("Inbound_RequestedBy");

                entity.Property(e => e.InventoryStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicedDate).HasColumnType("datetime");

                entity.Property(e => e.MemoryMb)
                    .HasMaxLength(500)
                    .HasColumnName("MemoryMB");

                entity.Property(e => e.ModelDescription).HasMaxLength(4000);

                entity.Property(e => e.MonitorSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.NetOutcome).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Oem)
                    .HasMaxLength(400)
                    .HasColumnName("OEM");

                entity.Property(e => e.OemserialNo)
                    .HasMaxLength(50)
                    .HasColumnName("OEMSerialNo");

                entity.Property(e => e.OutboundAssetId).HasColumnName("Outbound_AssetId");

                entity.Property(e => e.OutboundCompletedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Outbound_CompletedAt");

                entity.Property(e => e.OutboundCustomerName)
                    .HasMaxLength(150)
                    .HasColumnName("Outbound_CustomerName")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OutboundCustomerNumber).HasColumnName("Outbound_CustomerNumber");

                entity.Property(e => e.OutboundDestinationLocation)
                    .HasMaxLength(150)
                    .HasColumnName("Outbound_DestinationLocation")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OutboundInvoicedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Outbound_InvoicedAt");

                entity.Property(e => e.OutboundInvoicedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Outbound_InvoicedDate");

                entity.Property(e => e.OutboundJobId).HasColumnName("Outbound_JobID");

                entity.Property(e => e.OutboundJobNo).HasColumnName("Outbound_JobNo");

                entity.Property(e => e.OutboundJobNo2).HasColumnName("Outbound_JobNo2");

                entity.Property(e => e.OutboundOrderValue)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("Outbound_OrderValue");

                entity.Property(e => e.OutboundPonumber)
                    .HasMaxLength(50)
                    .HasColumnName("Outbound_PONumber")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OutboundReference).HasColumnName("Outbound_Reference");

                entity.Property(e => e.OutboundRequestedBy).HasColumnName("Outbound_RequestedBy");

                entity.Property(e => e.OutboundShippedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Outbound_ShippedDate");

                entity.Property(e => e.PartsCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Processor).HasMaxLength(1000);

                entity.Property(e => e.SalePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ShippedDate).HasColumnType("datetime");

                entity.Property(e => e.Speed).HasMaxLength(1000);

                entity.Property(e => e.StatusName).HasMaxLength(55);

                entity.Property(e => e.StreamsDisposition)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsDispositionFinal)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WasteTransferNo).HasMaxLength(55);
            });

            modelBuilder.Entity<ClientStatisticsDatum>(entity =>
            {
                entity.HasKey(e => e.StatisticsId);

                entity.Property(e => e.StatisticsId).HasColumnName("StatisticsID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepotBasedWinProgress).HasColumnName("DepotBasedWInProgress");

                entity.Property(e => e.DepotBasedWip).HasColumnName("DepotBasedWIP");

                entity.Property(e => e.FieldBasedWip).HasColumnName("FieldBasedWIP");
            });

            modelBuilder.Entity<CompanyServiceLine>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("CompanyServiceLine");

                entity.Property(e => e.ApplicableCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CompanyServiceLineId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("CompanyServiceLineID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepotSettingsId).HasColumnName("DepotSettingsID");

                entity.Property(e => e.MasterCompanyId).HasColumnName("MasterCompanyID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceLineId).HasColumnName("ServiceLineID");
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Contacts__3214EC061DE57479")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Reference, e.Customer, e.Location, e.Forename, e.Surname }, "ClusteredIndex-20190729-102845")
                    .IsClustered();

                entity.HasIndex(e => e.Customer, "IX_Contacts->Customer")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Username, "IX_Contacts->Username")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Ddi)
                    .HasMaxLength(200)
                    .HasColumnName("DDI");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.Forename)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.HashPassword).HasMaxLength(255);

                entity.Property(e => e.IsDpo).HasColumnName("IsDPO");

                entity.Property(e => e.JobTitle).HasMaxLength(200);

                entity.Property(e => e.Mobile).HasMaxLength(200);

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.Salt).HasMaxLength(255);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Telephone).HasMaxLength(200);

                entity.Property(e => e.Username).HasMaxLength(200);

                entity.HasOne(d => d.AccessGroupNavigation)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.AccessGroup)
                    .HasConstraintName("FK_Contact.AccessGroup");

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Contact.Customer");

                entity.HasOne(d => d.LocationNavigation)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.Location)
                    .HasConstraintName("FK_Contact.Location");

                entity.HasOne(d => d.SalutationNavigation)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.Salutation)
                    .HasConstraintName("FK_Contact.Salutation");

                entity.HasOne(d => d.StatusNavigation)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.Status)
                    .HasConstraintName("FK_Contact.Status->ContactStatus");
            });

            modelBuilder.Entity<ContactRole>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__ContactR__3214EC0621B6055D")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ContactStatus>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__ContactS__3214EC0625869641")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ContactsImportStagingWiz>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ContactsImportStagingWiz");

                entity.Property(e => e.Ddi)
                    .HasMaxLength(255)
                    .HasColumnName("DDI");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.Forename)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.HashPassword).HasMaxLength(255);

                entity.Property(e => e.JobTitle).HasMaxLength(255);

                entity.Property(e => e.Mobile).HasMaxLength(255);

                entity.Property(e => e.Password).HasMaxLength(255);

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.Salt).HasMaxLength(255);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Telephone).HasMaxLength(255);

                entity.Property(e => e.Username).HasMaxLength(255);
            });

            modelBuilder.Entity<ContactsRole>(entity =>
            {
                entity.HasKey(e => new { e.ContactId, e.ContactRoleId })
                    .HasName("Contacts_Roles_PK");

                entity.ToTable("Contacts_Roles");

                entity.HasIndex(e => e.ContactId, "IX_Contacts_Roles->ContactId")
                    .HasFillFactor((byte)80);

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.ContactsRoles)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_Contacts_Roles.ContactId");

                entity.HasOne(d => d.ContactRole)
                    .WithMany(p => p.ContactsRoles)
                    .HasForeignKey(d => d.ContactRoleId)
                    .HasConstraintName("FK_Contacts_Roles.ContactRoleId");
            });

            modelBuilder.Entity<ContactsRuncornImportTemp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Contacts_Runcorn_Import_Temp");

                entity.Property(e => e.Ddi)
                    .HasMaxLength(200)
                    .HasColumnName("DDI");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.ExternalData1).HasMaxLength(200);

                entity.Property(e => e.ExternalData2).HasMaxLength(200);

                entity.Property(e => e.Forename)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.HashPassword).HasMaxLength(255);

                entity.Property(e => e.IsDpo).HasColumnName("IsDPO");

                entity.Property(e => e.JobTitle).HasMaxLength(200);

                entity.Property(e => e.Mobile).HasMaxLength(200);

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.Salt).HasMaxLength(255);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Telephone).HasMaxLength(200);

                entity.Property(e => e.Username).HasMaxLength(200);
            });

            modelBuilder.Entity<Container>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Containe__3214EC0629572725")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ContainerId)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("ContainerID");

                entity.Property(e => e.Seal1).HasMaxLength(200);

                entity.Property(e => e.Seal2).HasMaxLength(200);

                entity.Property(e => e.Seal3).HasMaxLength(200);

                entity.Property(e => e.Seal4).HasMaxLength(200);

                entity.HasOne(d => d.JobNavigation)
                    .WithMany(p => p.Containers)
                    .HasForeignKey(d => d.Job)
                    .HasConstraintName("FK_Container.Job");
            });

            modelBuilder.Entity<ContractStatus>(entity =>
            {
                entity.ToTable("ContractStatus");

                entity.Property(e => e.ContractStatusId).HasColumnName("ContractStatusID");

                entity.Property(e => e.ContractStatusName)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Countrie__3214EC062D27B809")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<CurrencyVat>(entity =>
            {
                entity.HasKey(e => e.CurrencyId)
                    .HasName("PK_Customer_CurrencyVAT");

                entity.ToTable("CurrencyVAT");

                entity.Property(e => e.CurrencyId).HasColumnName("CurrencyID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency).HasMaxLength(50);

                entity.Property(e => e.DisplayName).HasMaxLength(50);

                entity.Property(e => e.ExchangeRate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ImageUrl)
                    .HasMaxLength(250)
                    .HasColumnName("ImageURL");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RemarketingPercentage).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.VatcodeId).HasColumnName("VATCodeID");

                entity.Property(e => e.Vatrate)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("VATRate");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Customer__3214EC0630F848ED")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Reference, e.Name, e.CustomerNumber, e.ProcessingSystemId, e.MasterCompanyId }, "ClusteredIndex-20190729-102938")
                    .IsClustered();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountStatus)
                    .HasMaxLength(50)
                    .HasColumnName("Account Status");

                entity.Property(e => e.AdDateEdited)
                    .HasColumnType("datetime")
                    .HasColumnName("AD_DATE_EDITED");

                entity.Property(e => e.AdDatePutin)
                    .HasColumnType("datetime")
                    .HasColumnName("AD_DATE_PUTIN");

                entity.Property(e => e.Adisadiallevel).HasColumnName("ADISADIALLevel");

                entity.Property(e => e.Adisadialreference)
                    .HasMaxLength(50)
                    .HasColumnName("ADISADIALReference");

                entity.Property(e => e.AllowedGrades).HasMaxLength(50);

                entity.Property(e => e.BadDebtor).HasColumnName("Bad Debtor");

                entity.Property(e => e.Branch).HasMaxLength(10);

                entity.Property(e => e.ChargedVat).HasColumnName("ChargedVAT");

                entity.Property(e => e.Collection).HasMaxLength(50);

                entity.Property(e => e.Company).HasMaxLength(50);

                entity.Property(e => e.CompanyRegNo)
                    .HasMaxLength(50)
                    .HasColumnName("Company Reg No.");

                entity.Property(e => e.ContractEndDate).HasColumnType("datetime");

                entity.Property(e => e.ContractStartDate).HasColumnType("datetime");

                entity.Property(e => e.ContractStatusId).HasColumnName("ContractStatusID");

                entity.Property(e => e.CorpType)
                    .HasMaxLength(250)
                    .HasColumnName("Corp Type");

                entity.Property(e => e.CorporateAccount).HasColumnName("Corporate Account");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CreditController)
                    .HasMaxLength(50)
                    .HasColumnName("Credit Controller?");

                entity.Property(e => e.CreditLimit)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("Credit Limit");

                entity.Property(e => e.CuDateEdited)
                    .HasColumnType("datetime")
                    .HasColumnName("CU_DATE_EDITED");

                entity.Property(e => e.CuDatePutin)
                    .HasColumnType("datetime")
                    .HasColumnName("CU_DATE_PUTIN");

                entity.Property(e => e.CustomerApikey)
                    .HasMaxLength(20)
                    .HasColumnName("CustomerAPIKey");

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.CustomerSpecificContent).HasColumnType("text");

                entity.Property(e => e.CustomizableField1Name).HasMaxLength(200);

                entity.Property(e => e.CustomizableField2Name).HasMaxLength(200);

                entity.Property(e => e.CustomizableField3Name).HasMaxLength(200);

                entity.Property(e => e.DataApiaccess)
                    .HasMaxLength(100)
                    .HasColumnName("DataAPIAccess");

                entity.Property(e => e.DepotSettingsId).HasColumnName("DepotSettingsID");

                entity.Property(e => e.GrantApiaccess).HasColumnName("GrantAPIAccess");

                entity.Property(e => e.Group).HasMaxLength(250);

                entity.Property(e => e.LogisticsSystemId)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("LogisticsSystemID");

                entity.Property(e => e.MasterCompanyId).HasColumnName("MasterCompanyID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.PORequired).HasColumnName("P/O Required");

                entity.Property(e => e.ParentCompanyId).HasColumnName("ParentCompanyID");

                entity.Property(e => e.PayMethod)
                    .HasMaxLength(50)
                    .HasColumnName("Pay Method");

                entity.Property(e => e.PaymentTerms)
                    .HasMaxLength(50)
                    .HasColumnName("Payment Terms");

                entity.Property(e => e.ProcessingSystemId).HasColumnName("ProcessingSystemID");

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.RemarketingPercentage).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ResellerCustomerId).HasColumnName("ResellerCustomerID");

                entity.Property(e => e.ServiceLevelId).HasColumnName("ServiceLevelID");

                entity.Property(e => e.Sla).HasColumnName("SLA");

                entity.Property(e => e.Sladays).HasColumnName("SLADays");

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(10)
                    .HasColumnName("Tax Code");

                entity.Property(e => e.Telemarketing).HasMaxLength(50);

                entity.Property(e => e.VatReg)
                    .HasMaxLength(50)
                    .HasColumnName("VAT Reg");

                entity.Property(e => e.WebAddress)
                    .HasMaxLength(250)
                    .HasColumnName("Web address");

                entity.HasOne(d => d.PrimaryContactNavigation)
                    .WithMany(p => p.CustomerPrimaryContactNavigations)
                    .HasForeignKey(d => d.PrimaryContact)
                    .HasConstraintName("FK_Customer.PrimaryContact->Contact");

                entity.HasOne(d => d.SecondaryContactNavigation)
                    .WithMany(p => p.CustomerSecondaryContactNavigations)
                    .HasForeignKey(d => d.SecondaryContact)
                    .HasConstraintName("FK_Customer.SecondaryContact->Contact");

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.Type)
                    .HasConstraintName("FK_Customer.Type->CustomerType");
            });

            modelBuilder.Entity<CustomerCharge>(entity =>
            {
                entity.HasIndex(e => new { e.CustomerId, e.PerAssetChargeTypeId, e.CustomerInvoiceAssetId }, "ceri_20201213_coveringIndex1");

                entity.Property(e => e.CustomerChargeId).HasColumnName("CustomerChargeID");

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerCharge1)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("CustomerCharge");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CustomerInvoiceAssetId).HasColumnName("CustomerInvoiceAssetID");

                entity.Property(e => e.PerAssetChargeTypeId).HasColumnName("PerAssetChargeTypeID");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerCharges)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_CustomerCharges_Customers");

                entity.HasOne(d => d.PerAssetChargeType)
                    .WithMany(p => p.CustomerCharges)
                    .HasForeignKey(d => d.PerAssetChargeTypeId)
                    .HasConstraintName("FK_CustomerCharges_PerAssetChargeType");
            });

            modelBuilder.Entity<CustomerImportStagingWiz>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("CustomerImportStagingWiz");

                entity.Property(e => e.AccountStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Account Status");

                entity.Property(e => e.AdDateEdited)
                    .HasColumnType("datetime")
                    .HasColumnName("AD_DATE_EDITED");

                entity.Property(e => e.AdDatePutin)
                    .HasColumnType("datetime")
                    .HasColumnName("AD_DATE_PUTIN");

                entity.Property(e => e.AllowedGrades).HasMaxLength(255);

                entity.Property(e => e.BadDebtor).HasColumnName("Bad Debtor");

                entity.Property(e => e.Branch).HasMaxLength(255);

                entity.Property(e => e.ChargedVat).HasColumnName("ChargedVAT");

                entity.Property(e => e.Collection).HasMaxLength(255);

                entity.Property(e => e.Company).HasMaxLength(255);

                entity.Property(e => e.CompanyRegNo)
                    .HasMaxLength(255)
                    .HasColumnName("Company Reg No.");

                entity.Property(e => e.ContractEndDate).HasColumnType("datetime");

                entity.Property(e => e.ContractStartDate).HasColumnType("datetime");

                entity.Property(e => e.CorpType)
                    .HasMaxLength(255)
                    .HasColumnName("Corp Type");

                entity.Property(e => e.CorporateAccount).HasColumnName("Corporate Account");

                entity.Property(e => e.CreditController)
                    .HasMaxLength(255)
                    .HasColumnName("Credit Controller?");

                entity.Property(e => e.CreditLimit)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("Credit Limit");

                entity.Property(e => e.CuDateEdited)
                    .HasColumnType("datetime")
                    .HasColumnName("CU_DATE_EDITED");

                entity.Property(e => e.CuDatePutin)
                    .HasColumnType("datetime")
                    .HasColumnName("CU_DATE_PUTIN");

                entity.Property(e => e.CustomerApikey)
                    .HasMaxLength(255)
                    .HasColumnName("CustomerAPIKey");

                entity.Property(e => e.CustomerNumber).HasMaxLength(255);

                entity.Property(e => e.CustomerSpecificContent).HasColumnType("text");

                entity.Property(e => e.CustomizableField1Name).HasMaxLength(255);

                entity.Property(e => e.CustomizableField2Name).HasMaxLength(255);

                entity.Property(e => e.CustomizableField3Name).HasMaxLength(255);

                entity.Property(e => e.DataApiaccess)
                    .HasMaxLength(255)
                    .HasColumnName("DataAPIAccess");

                entity.Property(e => e.DepotSettingsId).HasColumnName("DepotSettingsID");

                entity.Property(e => e.GrantApiaccess).HasColumnName("GrantAPIAccess");

                entity.Property(e => e.Group).HasMaxLength(255);

                entity.Property(e => e.LogisticsSystemId)
                    .HasMaxLength(255)
                    .HasColumnName("LogisticsSystemID");

                entity.Property(e => e.MasterCompanyId).HasColumnName("MasterCompanyID");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.PORequired).HasColumnName("P/O Required");

                entity.Property(e => e.PayMethod)
                    .HasMaxLength(255)
                    .HasColumnName("Pay Method");

                entity.Property(e => e.PaymentTerms)
                    .HasMaxLength(255)
                    .HasColumnName("Payment Terms");

                entity.Property(e => e.ProcessingSystemId).HasColumnName("ProcessingSystemID");

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.RemarketingPercentage).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ServiceLevelId).HasColumnName("ServiceLevelID");

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(255)
                    .HasColumnName("Tax Code");

                entity.Property(e => e.Telemarketing).HasMaxLength(255);

                entity.Property(e => e.VatReg)
                    .HasMaxLength(255)
                    .HasColumnName("VAT Reg");

                entity.Property(e => e.WebAddress)
                    .HasMaxLength(255)
                    .HasColumnName("Web address");
            });

            modelBuilder.Entity<CustomerInvoice>(entity =>
            {
                entity.ToTable("CustomerInvoice");

                entity.Property(e => e.CustomerInvoiceId).HasColumnName("CustomerInvoiceID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.InvoiceEmailAddress).HasMaxLength(250);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RemarketingSla).HasColumnName("RemarketingSLA");

                entity.Property(e => e.VatrateId).HasColumnName("VATRateId");

                entity.HasOne(d => d.SettlementModelNavigation)
                    .WithMany(p => p.CustomerInvoices)
                    .HasForeignKey(d => d.SettlementModel)
                    .HasConstraintName("fk");
            });

            modelBuilder.Entity<CustomerInvoiceAsset>(entity =>
            {
                entity.HasKey(e => e.AssetClassId);

                entity.Property(e => e.AssetClassId).HasColumnName("AssetClassID");

                entity.Property(e => e.AssetClassName).HasMaxLength(250);
            });

            modelBuilder.Entity<CustomerInvoiceAssetsDefault>(entity =>
            {
                entity.HasKey(e => e.CustomerInvoiceAssetsDefaultsId)
                    .HasName("PK__Customer__99B007FEE328B65F");

                entity.Property(e => e.CustomerInvoiceAssetsDefaultsId).HasColumnName("CustomerInvoiceAssetsDefaultsID");

                entity.Property(e => e.AssetClassId).HasColumnName("AssetClassID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.PerAssetChargeTypeId).HasColumnName("PerAssetChargeTypeID");
            });

            modelBuilder.Entity<CustomerLicenceCharge>(entity =>
            {
                entity.Property(e => e.CustomerLicenceChargeId).HasColumnName("CustomerLicenceChargeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.LicenceCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LicenceChargeId).HasColumnName("LicenceChargeID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<CustomerLookup>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("CustomerLookup", "Lookups");

                entity.Property(e => e.AddressLine1).HasMaxLength(200);

                entity.Property(e => e.AddressLine2).HasMaxLength(200);

                entity.Property(e => e.AddressLine3).HasMaxLength(200);

                entity.Property(e => e.City).HasMaxLength(200);

                entity.Property(e => e.Country).HasMaxLength(200);

                entity.Property(e => e.County).HasMaxLength(200);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.IsStreams).HasDefaultValueSql("((0))");

                entity.Property(e => e.LegacySystem).HasMaxLength(50);

                entity.Property(e => e.Postcode).HasMaxLength(200);
            });

            modelBuilder.Entity<CustomerNote>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Customer__3214EC0634C8D9D1")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.CustomerNotes)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerNote.Customer");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.CustomerNote)
                    .HasForeignKey<CustomerNote>(d => d.Id)
                    .HasConstraintName("FK_CustomerNote.Id->Note");
            });

            modelBuilder.Entity<CustomerPortalPermission>(entity =>
            {
                entity.ToTable("CustomerPortalPermission");

                entity.Property(e => e.CustomerPortalPermissionId).HasColumnName("CustomerPortalPermissionID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.PortalPermissionId).HasColumnName("PortalPermissionID");
            });

            modelBuilder.Entity<CustomerServiceBundle>(entity =>
            {
                entity.ToTable("CustomerServiceBundle");

                entity.Property(e => e.CustomerServiceBundleId).HasColumnName("CustomerServiceBundleID");

                entity.Property(e => e.ApplicableCharges).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceBundleId).HasColumnName("ServiceBundleID");
            });

            modelBuilder.Entity<CustomerServiceLine>(entity =>
            {
                entity.ToTable("CustomerServiceLine");

                entity.Property(e => e.CustomerServiceLineId).HasColumnName("CustomerServiceLineID");

                entity.Property(e => e.ApplicableCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceLineId).HasColumnName("ServiceLineID");
            });

            modelBuilder.Entity<CustomerTransaction>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Customer__3214EC0638996AB5")
                    .IsClustered(false);

                entity.HasIndex(e => e.Customer, "IX_CustomerTransactions->Customer")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Job, "IX_CustomerTransactions->Job")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ActualCost).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.BillingPeriod).HasMaxLength(6);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EstimatedCost).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(200)
                    .HasColumnName("PONumber");

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.HasOne(d => d.BillingCodeNavigation)
                    .WithMany(p => p.CustomerTransactions)
                    .HasForeignKey(d => d.BillingCode)
                    .HasConstraintName("FK_CustomerTransaction.BillingCode");

                entity.HasOne(d => d.ChargeTypeNavigation)
                    .WithMany(p => p.CustomerTransactions)
                    .HasForeignKey(d => d.ChargeType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerTransaction.ChargeType");

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.CustomerTransactions)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerTransaction.Customer");

                entity.HasOne(d => d.JobNavigation)
                    .WithMany(p => p.CustomerTransactions)
                    .HasForeignKey(d => d.Job)
                    .HasConstraintName("FK_CustomerTransaction.Job");

                entity.HasOne(d => d.NatureNavigation)
                    .WithMany(p => p.CustomerTransactions)
                    .HasForeignKey(d => d.Nature)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerTransaction.Nature->TransactionNature");

                entity.HasOne(d => d.StatusNavigation)
                    .WithMany(p => p.CustomerTransactions)
                    .HasForeignKey(d => d.Status)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerTransaction.Status->CustomerTransactionStatus");

                entity.HasOne(d => d.TransactionTypeNavigation)
                    .WithMany(p => p.CustomerTransactions)
                    .HasForeignKey(d => d.TransactionType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerTransaction.TransactionType");
            });

            modelBuilder.Entity<CustomerTransactionStatus>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Customer__3214EC063C69FB99")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<CustomerType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Customer__3214EC06403A8C7D")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<CustomersImp1>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Customers_IMP1");

                entity.Property(e => e.AccountStatus)
                    .HasMaxLength(50)
                    .HasColumnName("Account Status");

                entity.Property(e => e.AdDateEdited)
                    .HasColumnType("datetime")
                    .HasColumnName("AD_DATE_EDITED");

                entity.Property(e => e.AdDatePutin)
                    .HasColumnType("datetime")
                    .HasColumnName("AD_DATE_PUTIN");

                entity.Property(e => e.Adisadiallevel).HasColumnName("ADISADIALLevel");

                entity.Property(e => e.Adisadialreference)
                    .HasMaxLength(50)
                    .HasColumnName("ADISADIALReference");

                entity.Property(e => e.AllowedGrades).HasMaxLength(50);

                entity.Property(e => e.BadDebtor).HasColumnName("Bad Debtor");

                entity.Property(e => e.Branch).HasMaxLength(10);

                entity.Property(e => e.ChargedVat).HasColumnName("ChargedVAT");

                entity.Property(e => e.Collection).HasMaxLength(50);

                entity.Property(e => e.Company).HasMaxLength(50);

                entity.Property(e => e.CompanyRegNo)
                    .HasMaxLength(50)
                    .HasColumnName("Company Reg No.");

                entity.Property(e => e.ContractEndDate).HasColumnType("datetime");

                entity.Property(e => e.ContractStartDate).HasColumnType("datetime");

                entity.Property(e => e.ContractStatusId).HasColumnName("ContractStatusID");

                entity.Property(e => e.CorpType)
                    .HasMaxLength(250)
                    .HasColumnName("Corp Type");

                entity.Property(e => e.CorporateAccount).HasColumnName("Corporate Account");

                entity.Property(e => e.CreditController)
                    .HasMaxLength(50)
                    .HasColumnName("Credit Controller?");

                entity.Property(e => e.CreditLimit)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("Credit Limit");

                entity.Property(e => e.CuDateEdited)
                    .HasColumnType("datetime")
                    .HasColumnName("CU_DATE_EDITED");

                entity.Property(e => e.CuDatePutin)
                    .HasColumnType("datetime")
                    .HasColumnName("CU_DATE_PUTIN");

                entity.Property(e => e.CustomerApikey)
                    .HasMaxLength(20)
                    .HasColumnName("CustomerAPIKey");

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.CustomerSpecificContent).HasColumnType("text");

                entity.Property(e => e.CustomizableField1Name).HasMaxLength(200);

                entity.Property(e => e.CustomizableField2Name).HasMaxLength(200);

                entity.Property(e => e.CustomizableField3Name).HasMaxLength(200);

                entity.Property(e => e.DataApiaccess)
                    .HasMaxLength(100)
                    .HasColumnName("DataAPIAccess");

                entity.Property(e => e.DepotSettingsId).HasColumnName("DepotSettingsID");

                entity.Property(e => e.GrantApiaccess).HasColumnName("GrantAPIAccess");

                entity.Property(e => e.Group).HasMaxLength(250);

                entity.Property(e => e.LogisticsSystemId)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("LogisticsSystemID");

                entity.Property(e => e.MasterCompanyId).HasColumnName("MasterCompanyID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.OldReference)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("OLD_Reference");

                entity.Property(e => e.PORequired).HasColumnName("P/O Required");

                entity.Property(e => e.ParentCompanyId).HasColumnName("ParentCompanyID");

                entity.Property(e => e.PayMethod)
                    .HasMaxLength(50)
                    .HasColumnName("Pay Method");

                entity.Property(e => e.PaymentTerms)
                    .HasMaxLength(50)
                    .HasColumnName("Payment Terms");

                entity.Property(e => e.ProcessingSystemId).HasColumnName("ProcessingSystemID");

                entity.Property(e => e.RemarketingPercentage).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ResellerCustomerId).HasColumnName("ResellerCustomerID");

                entity.Property(e => e.ServiceLevelId).HasColumnName("ServiceLevelID");

                entity.Property(e => e.Sla).HasColumnName("SLA");

                entity.Property(e => e.Sladays).HasColumnName("SLADays");

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(10)
                    .HasColumnName("Tax Code");

                entity.Property(e => e.Telemarketing).HasMaxLength(50);

                entity.Property(e => e.VatReg)
                    .HasMaxLength(50)
                    .HasColumnName("VAT Reg");

                entity.Property(e => e.WebAddress)
                    .HasMaxLength(250)
                    .HasColumnName("Web address");
            });

            modelBuilder.Entity<CustomersRuncornImportTemp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Customers_Runcorn_Import_Temp");

                entity.Property(e => e.AccountStatus)
                    .HasMaxLength(50)
                    .HasColumnName("Account Status");

                entity.Property(e => e.AdDateEdited)
                    .HasColumnType("datetime")
                    .HasColumnName("AD_DATE_EDITED");

                entity.Property(e => e.AdDatePutin)
                    .HasColumnType("datetime")
                    .HasColumnName("AD_DATE_PUTIN");

                entity.Property(e => e.Adisadiallevel).HasColumnName("ADISADIALLevel");

                entity.Property(e => e.Adisadialreference)
                    .HasMaxLength(50)
                    .HasColumnName("ADISADIALReference");

                entity.Property(e => e.AllowedGrades).HasMaxLength(50);

                entity.Property(e => e.BadDebtor).HasColumnName("Bad Debtor");

                entity.Property(e => e.Branch).HasMaxLength(10);

                entity.Property(e => e.ChargedVat).HasColumnName("ChargedVAT");

                entity.Property(e => e.Collection).HasMaxLength(50);

                entity.Property(e => e.Company).HasMaxLength(50);

                entity.Property(e => e.CompanyRegNo)
                    .HasMaxLength(50)
                    .HasColumnName("Company Reg No.");

                entity.Property(e => e.ContractEndDate).HasColumnType("datetime");

                entity.Property(e => e.ContractStartDate).HasColumnType("datetime");

                entity.Property(e => e.ContractStatusId).HasColumnName("ContractStatusID");

                entity.Property(e => e.CorpType)
                    .HasMaxLength(250)
                    .HasColumnName("Corp Type");

                entity.Property(e => e.CorporateAccount).HasColumnName("Corporate Account");

                entity.Property(e => e.CreditController)
                    .HasMaxLength(50)
                    .HasColumnName("Credit Controller?");

                entity.Property(e => e.CreditLimit)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("Credit Limit");

                entity.Property(e => e.CuDateEdited)
                    .HasColumnType("datetime")
                    .HasColumnName("CU_DATE_EDITED");

                entity.Property(e => e.CuDatePutin)
                    .HasColumnType("datetime")
                    .HasColumnName("CU_DATE_PUTIN");

                entity.Property(e => e.CustomerApikey)
                    .HasMaxLength(20)
                    .HasColumnName("CustomerAPIKey");

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.CustomerSpecificContent).HasColumnType("text");

                entity.Property(e => e.CustomizableField1Name).HasMaxLength(200);

                entity.Property(e => e.CustomizableField2Name).HasMaxLength(200);

                entity.Property(e => e.CustomizableField3Name).HasMaxLength(200);

                entity.Property(e => e.DataApiaccess)
                    .HasMaxLength(100)
                    .HasColumnName("DataAPIAccess");

                entity.Property(e => e.DepotSettingsId).HasColumnName("DepotSettingsID");

                entity.Property(e => e.GrantApiaccess).HasColumnName("GrantAPIAccess");

                entity.Property(e => e.Group).HasMaxLength(250);

                entity.Property(e => e.LogisticsSystemId)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("LogisticsSystemID");

                entity.Property(e => e.MasterCompanyId).HasColumnName("MasterCompanyID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.PORequired).HasColumnName("P/O Required");

                entity.Property(e => e.ParentCompanyId).HasColumnName("ParentCompanyID");

                entity.Property(e => e.PayMethod)
                    .HasMaxLength(50)
                    .HasColumnName("Pay Method");

                entity.Property(e => e.PaymentTerms)
                    .HasMaxLength(50)
                    .HasColumnName("Payment Terms");

                entity.Property(e => e.ProcessingSystemId).HasColumnName("ProcessingSystemID");

                entity.Property(e => e.RemarketingPercentage).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ResellerCustomerId).HasColumnName("ResellerCustomerID");

                entity.Property(e => e.ServiceLevelId).HasColumnName("ServiceLevelID");

                entity.Property(e => e.Sla).HasColumnName("SLA");

                entity.Property(e => e.Sladays).HasColumnName("SLADays");

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(10)
                    .HasColumnName("Tax Code");

                entity.Property(e => e.Telemarketing).HasMaxLength(50);

                entity.Property(e => e.VatReg)
                    .HasMaxLength(50)
                    .HasColumnName("VAT Reg");

                entity.Property(e => e.WebAddress)
                    .HasMaxLength(250)
                    .HasColumnName("Web address");
            });

            modelBuilder.Entity<DashboardLog>(entity =>
            {
                entity.ToTable("DashboardLog");

                entity.Property(e => e.ApplicationName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Context)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Environment)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorDescription0).IsUnicode(false);

                entity.Property(e => e.ErrorDescription1).IsUnicode(false);

                entity.Property(e => e.ErrorDescription2).IsUnicode(false);

                entity.Property(e => e.ErrorMessage).IsUnicode(false);

                entity.Property(e => e.Jsondata)
                    .IsUnicode(false)
                    .HasColumnName("JSONData");

                entity.Property(e => e.LogDateTime).HasColumnType("datetime");

                entity.Property(e => e.UniqueKeyName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UniqueKeyValue)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("UserID");

                entity.Property(e => e.VersionNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DashboardSetting>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Environment)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReleaseDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReleaseVersion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SlarefreshRate).HasColumnName("SLARefreshRate");
            });

            modelBuilder.Entity<DataClassificationType>(entity =>
            {
                entity.Property(e => e.DataClassificationName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Ddd>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("_______________________________________DDD");

                entity.Property(e => e.AdHoc)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Ad Hoc");

                entity.Property(e => e.AssetCount).HasColumnName("Asset Count");

                entity.Property(e => e.AssetsInInventory).HasColumnName("Assets in Inventory");

                entity.Property(e => e.AssetsRecycled).HasColumnName("Assets  Recycled");

                entity.Property(e => e.AssetsRedeployed).HasColumnName("Assets Redeployed");

                entity.Property(e => e.AssetsSold).HasColumnName("Assets Sold");

                entity.Property(e => e.AuditTrash)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Audit Trash");

                entity.Property(e => e.ClientEarnings)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Client Earnings");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("Customer Name");

                entity.Property(e => e.CustomerNumber)
                    .HasMaxLength(200)
                    .HasColumnName("Customer Number");

                entity.Property(e => e.DataStorageCount).HasColumnName("Data Storage Count");

                entity.Property(e => e.DaysOnShelf).HasColumnName("Days On Shelf");

                entity.Property(e => e.DaysTurnaround).HasColumnName("Days Turnaround");

                entity.Property(e => e.DeploymentLocation)
                    .HasMaxLength(200)
                    .HasColumnName("Deployment Location");

                entity.Property(e => e.Engineering).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.Finalsettlementavailablesince)
                    .HasColumnType("datetime")
                    .HasColumnName("finalsettlementavailablesince");

                entity.Property(e => e.InvoiceReference).HasColumnName("Invoice Reference");

                entity.Property(e => e.JobType)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("Job Type");

                entity.Property(e => e.LastUpdated)
                    .HasColumnType("datetime")
                    .HasColumnName("Last Updated");

                entity.Property(e => e.Logistics).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.LotId).HasColumnName("Lot ID");

                entity.Property(e => e.MasterCompany)
                    .HasMaxLength(255)
                    .HasColumnName("Master Company");

                entity.Property(e => e.PartsCharges)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Parts Charges");

                entity.Property(e => e.PrimaryContact)
                    .IsRequired()
                    .HasMaxLength(401)
                    .HasColumnName("Primary Contact");

                entity.Property(e => e.ProfitShare)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("Profit Share %");

                entity.Property(e => e.Rebate).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.Rmcredit).HasColumnName("RMCredit");

                entity.Property(e => e.SalePrice)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Sale Price");

                entity.Property(e => e.Service).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.StreamsJobNo).HasColumnName("Streams Job No");

                entity.Property(e => e.WorkflowStatus)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("Workflow Status");
            });

            modelBuilder.Entity<DelmenowJob>(entity =>
            {
                entity.HasKey(e => e.ProcessingSystemJobId)
                    .HasName("PK_Jobs");

                entity.ToTable("delmenow_Jobs", "Materialised");

                entity.Property(e => e.ProcessingSystemJobId)
                    .ValueGeneratedNever()
                    .HasColumnName("ProcessingSystemJobID");

                entity.Property(e => e.CollectionDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(200);

                entity.Property(e => e.CustomerNo).HasMaxLength(200);

                entity.Property(e => e.CustomerReference).HasMaxLength(200);

                entity.Property(e => e.DestinationLocation).HasMaxLength(200);

                entity.Property(e => e.OriginatingLocation).HasMaxLength(200);

                entity.Property(e => e.PromptBillingCode)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_BillingCode");

                entity.Property(e => e.PromptCollectionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("prompt_CollectionDate");

                entity.Property(e => e.PromptCollectionNo).HasColumnName("prompt_CollectionNo");

                entity.Property(e => e.PromptCustomerGuid).HasColumnName("prompt_CustomerGUID");

                entity.Property(e => e.PromptCustomerName)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_CustomerName");

                entity.Property(e => e.PromptCustomerNo)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_CustomerNo");

                entity.Property(e => e.PromptProject)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_Project");

                entity.Property(e => e.ReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.Requestor).HasMaxLength(401);
            });

            modelBuilder.Entity<DelmenowM>(entity =>
            {
                entity.HasKey(e => new { e.OriginLot, e.AssetRecId })
                    .HasName("PK_M");

                entity.ToTable("DELMENOW_M", "Materialised");

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.Class).HasMaxLength(400);

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit");

                entity.Property(e => e.Manufacturer).HasMaxLength(400);

                entity.Property(e => e.ProductName).HasMaxLength(400);

                entity.Property(e => e.ResaleChannel).HasMaxLength(150);

                entity.Property(e => e.SerialNumber).HasMaxLength(50);

                entity.Property(e => e.SubClass).HasMaxLength(250);

                entity.Property(e => e.Weight).HasColumnType("decimal(18, 4)");
            });

            modelBuilder.Entity<DeploymentAssetsTemp>(entity =>
            {
                entity.ToTable("DeploymentAssetsTemp");

                entity.Property(e => e.DeploymentAssetsTempId).HasColumnName("DeploymentAssetsTempID");

                entity.Property(e => e.AssetRecId).HasColumnName("AssetRecID");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.CustomerAssetNumber).HasMaxLength(150);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.ManufacturerId).HasColumnName("ManufacturerID");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.SerialNumber).HasMaxLength(50);
            });

            modelBuilder.Entity<DeploymentJobInventoryAsset>(entity =>
            {
                entity.HasKey(e => e.InventoryAssetsId)
                    .HasName("PK_DeploymentJob_InventoryAssets1");

                entity.ToTable("DeploymentJob_InventoryAssets");

                entity.Property(e => e.InventoryAssetsId).HasColumnName("InventoryAssetsID");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.AssetTag)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.CustomizableField2).HasMaxLength(200);

                entity.Property(e => e.CustomizableField3).HasMaxLength(200);

                entity.Property(e => e.GradeId).HasColumnName("GradeID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.ManufacturerId).HasColumnName("ManufacturerID");

                entity.Property(e => e.ModelId).HasColumnName("ModelID");

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(200)
                    .HasColumnName("PONumber");

                entity.Property(e => e.RequestedDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.SerialNumber)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Stnumber).HasColumnName("STNumber");
            });

            modelBuilder.Entity<DepotSetting>(entity =>
            {
                entity.HasKey(e => e.DepotSettingsId);

                entity.Property(e => e.DepotSettingsId).HasColumnName("DepotSettingsID");

                entity.Property(e => e.Analysis).HasMaxLength(250);

                entity.Property(e => e.CompanyName).HasMaxLength(250);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepotAddress).HasMaxLength(500);

                entity.Property(e => e.DepotCity).HasMaxLength(200);

                entity.Property(e => e.DepotCounty).HasMaxLength(200);

                entity.Property(e => e.DepotDockId).HasColumnName("DepotDockID");

                entity.Property(e => e.DepotName).HasMaxLength(250);

                entity.Property(e => e.DepotPostCode).HasMaxLength(100);

                entity.Property(e => e.DepotPrefix).HasMaxLength(10);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PhoneNumber).HasMaxLength(100);
            });

            modelBuilder.Entity<DispositionsStatic>(entity =>
            {
                entity.HasKey(e => e.StreamsSerialNo);

                entity.ToTable("DispositionsStatic", "Materialised");

                entity.HasIndex(e => e.InboundReference, "i_Inbound_Reference");

                entity.Property(e => e.StreamsSerialNo).ValueGeneratedNever();

                entity.Property(e => e.AssetTag).HasMaxLength(150);

                entity.Property(e => e.AssetType).HasMaxLength(400);

                entity.Property(e => e.ClientEarnings).HasColumnType("decimal(34, 8)");

                entity.Property(e => e.ConditionCode).HasMaxLength(250);

                entity.Property(e => e.Hddsize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDSize");

                entity.Property(e => e.Hddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDType");

                entity.Property(e => e.InboundActualCollectionDate)
                    .HasColumnType("date")
                    .HasColumnName("Inbound_ActualCollectionDate");

                entity.Property(e => e.InboundActualReceivedDate)
                    .HasColumnType("date")
                    .HasColumnName("Inbound_ActualReceivedDate");

                entity.Property(e => e.InboundBillingCode)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_BillingCode");

                entity.Property(e => e.InboundCollectionDate)
                    .HasColumnType("date")
                    .HasColumnName("Inbound_CollectionDate");

                entity.Property(e => e.InboundCustomerName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_CustomerName");

                entity.Property(e => e.InboundCustomerNumber)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_CustomerNumber");

                entity.Property(e => e.InboundDestinationLocation)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_DestinationLocation");

                entity.Property(e => e.InboundJobId).HasColumnName("Inbound_JobId");

                entity.Property(e => e.InboundJobNo).HasColumnName("Inbound_JobNo");

                entity.Property(e => e.InboundOriginatingLocation)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_OriginatingLocation");

                entity.Property(e => e.InboundPonumber)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_PONumber");

                entity.Property(e => e.InboundProductionNo).HasColumnName("Inbound_ProductionNo");

                entity.Property(e => e.InboundProject)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_Project");

                entity.Property(e => e.InboundReference).HasColumnName("Inbound_Reference");

                entity.Property(e => e.InboundRequestedBy)
                    .IsRequired()
                    .HasMaxLength(401)
                    .HasColumnName("Inbound_RequestedBy");

                entity.Property(e => e.InventoryStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicedDate).HasColumnType("datetime");

                entity.Property(e => e.MemoryMb)
                    .HasMaxLength(500)
                    .HasColumnName("MemoryMB");

                entity.Property(e => e.ModelDescription).HasMaxLength(4000);

                entity.Property(e => e.MonitorSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.NetOutcome).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Oem)
                    .HasMaxLength(400)
                    .HasColumnName("OEM");

                entity.Property(e => e.OemserialNo)
                    .HasMaxLength(50)
                    .HasColumnName("OEMSerialNo");

                entity.Property(e => e.OutboundAssetId).HasColumnName("Outbound_AssetId");

                entity.Property(e => e.OutboundCompletedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Outbound_CompletedAt");

                entity.Property(e => e.OutboundCustomerName)
                    .HasMaxLength(150)
                    .HasColumnName("Outbound_CustomerName");

                entity.Property(e => e.OutboundCustomerNumber).HasColumnName("Outbound_CustomerNumber");

                entity.Property(e => e.OutboundDestinationLocation)
                    .HasMaxLength(150)
                    .HasColumnName("Outbound_DestinationLocation");

                entity.Property(e => e.OutboundInvoicedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("Outbound_InvoicedAt");

                entity.Property(e => e.OutboundInvoicedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Outbound_InvoicedDate");

                entity.Property(e => e.OutboundJobId).HasColumnName("Outbound_JobID");

                entity.Property(e => e.OutboundJobNo).HasColumnName("Outbound_JobNo");

                entity.Property(e => e.OutboundJobNo2).HasColumnName("Outbound_JobNo2");

                entity.Property(e => e.OutboundOrderValue)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("Outbound_OrderValue");

                entity.Property(e => e.OutboundPonumber)
                    .HasMaxLength(50)
                    .HasColumnName("Outbound_PONumber");

                entity.Property(e => e.OutboundReference).HasColumnName("Outbound_Reference");

                entity.Property(e => e.OutboundRequestedBy).HasColumnName("Outbound_RequestedBy");

                entity.Property(e => e.OutboundShippedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Outbound_ShippedDate");

                entity.Property(e => e.PartsCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Processor).HasMaxLength(1000);

                entity.Property(e => e.SalePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ShippedDate).HasColumnType("datetime");

                entity.Property(e => e.Speed).HasMaxLength(1000);

                entity.Property(e => e.StatusName).HasMaxLength(55);

                entity.Property(e => e.StreamsDisposition)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsDispositionFinal)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WasteTransferNo).HasMaxLength(55);
            });

            modelBuilder.Entity<Elog>(entity =>
            {
                entity.ToTable("ELog");

                entity.Property(e => e.ApplicationName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Context)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Environment)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorDescription0).IsUnicode(false);

                entity.Property(e => e.ErrorDescription1).IsUnicode(false);

                entity.Property(e => e.ErrorDescription2).IsUnicode(false);

                entity.Property(e => e.ErrorMessage).IsUnicode(false);

                entity.Property(e => e.Jsondata)
                    .IsUnicode(false)
                    .HasColumnName("JSONData");

                entity.Property(e => e.LogDateTime).HasColumnType("datetime");

                entity.Property(e => e.UniqueKeyName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UniqueKeyValue)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("UserID");

                entity.Property(e => e.VersionNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmailPreference>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__EmailPre__3214EC06440B1D61")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.EmailPreferences)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmailPreference.Customer");

                entity.HasOne(d => d.EmailTemplateNavigation)
                    .WithMany(p => p.EmailPreferences)
                    .HasForeignKey(d => d.EmailTemplate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmailPreference.EmailTemplate");
            });

            modelBuilder.Entity<EmailPreferencesContact>(entity =>
            {
                entity.HasKey(e => e.EmailPreferenceContactId);

                entity.ToTable("EmailPreferences_Contact");
            });

            modelBuilder.Entity<EmailPreferencesContact1>(entity =>
            {
                entity.HasKey(e => new { e.EmailPreferenceId, e.ContactId })
                    .HasName("EmailPreferences_Contacts_PK");

                entity.ToTable("EmailPreferences_Contacts");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.EmailPreferencesContact1s)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_EmailPreferences_Contacts.ContactId");

                entity.HasOne(d => d.EmailPreference)
                    .WithMany(p => p.EmailPreferencesContact1s)
                    .HasForeignKey(d => d.EmailPreferenceId)
                    .HasConstraintName("FK_EmailPreferences_Contacts.EmailPreferenceId");
            });

            modelBuilder.Entity<EmailPreferencesContactRole>(entity =>
            {
                entity.HasKey(e => new { e.EmailPreferenceId, e.ContactRoleId })
                    .HasName("EmailPreferences_ContactRoles_PK");

                entity.ToTable("EmailPreferences_ContactRoles");

                entity.HasOne(d => d.ContactRole)
                    .WithMany(p => p.EmailPreferencesContactRoles)
                    .HasForeignKey(d => d.ContactRoleId)
                    .HasConstraintName("FK_EmailPreferences_ContactRoles.ContactRoleId");

                entity.HasOne(d => d.EmailPreference)
                    .WithMany(p => p.EmailPreferencesContactRoles)
                    .HasForeignKey(d => d.EmailPreferenceId)
                    .HasConstraintName("FK_EmailPreferences_ContactRoles.EmailPreferenceId");
            });

            modelBuilder.Entity<EmailPreferencesDefaultToContactRole>(entity =>
            {
                entity.HasKey(e => new { e.EmailPreferenceId, e.DefaultToContactRoleId })
                    .HasName("EmailPreferences_DefaultToContactRoles_PK");

                entity.ToTable("EmailPreferences_DefaultToContactRoles");

                entity.HasOne(d => d.DefaultToContactRole)
                    .WithMany(p => p.EmailPreferencesDefaultToContactRoles)
                    .HasForeignKey(d => d.DefaultToContactRoleId)
                    .HasConstraintName("FK_EmailPreferences_DefaultToContactRoles.DefaultToContactRoleId");

                entity.HasOne(d => d.EmailPreference)
                    .WithMany(p => p.EmailPreferencesDefaultToContactRoles)
                    .HasForeignKey(d => d.EmailPreferenceId)
                    .HasConstraintName("FK_EmailPreferences_DefaultToContactRoles.EmailPreferenceId");
            });

            modelBuilder.Entity<EmailPreferencesRole>(entity =>
            {
                entity.HasKey(e => e.EmailPreferenceContactRoles);

                entity.ToTable("EmailPreferences_Roles");

                entity.HasOne(d => d.EmailPreferenceContact)
                    .WithMany(p => p.EmailPreferencesRoles)
                    .HasForeignKey(d => d.EmailPreferenceContactId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmailPreferences_Roles_EmailPreferences_Contact");
            });

            modelBuilder.Entity<EmailQueueItem>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__EmailQue__3214EC0647DBAE45")
                    .IsClustered(false);

                entity.HasIndex(e => e.Date, "IX_Date")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Deleted, "IX_Deleted")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Attachments).HasMaxLength(4000);

                entity.Property(e => e.Category).HasMaxLength(200);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Deleted).HasColumnName(".Deleted");

                entity.Property(e => e.Password).HasMaxLength(200);

                entity.Property(e => e.SenderAddress).HasMaxLength(200);

                entity.Property(e => e.SenderName).HasMaxLength(200);

                entity.Property(e => e.SmtpHost).HasMaxLength(200);

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.To)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Username).HasMaxLength(200);

                entity.Property(e => e.VcalendarView).HasColumnName("VCalendarView");
            });

            modelBuilder.Entity<EmailTemplate>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__EmailTem__3214EC064BAC3F29")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.EmailTriggerEventNavigation)
                    .WithMany(p => p.EmailTemplates)
                    .HasForeignKey(d => d.EmailTriggerEvent)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmailTemplate.EmailTriggerEvent");
            });

            modelBuilder.Entity<EmailTemplatesContactRole>(entity =>
            {
                entity.HasKey(e => new { e.EmailTemplateId, e.ContactRoleId })
                    .HasName("EmailTemplates_ContactRoles_PK");

                entity.ToTable("EmailTemplates_ContactRoles");

                entity.HasOne(d => d.ContactRole)
                    .WithMany(p => p.EmailTemplatesContactRoles)
                    .HasForeignKey(d => d.ContactRoleId)
                    .HasConstraintName("FK_EmailTemplates_ContactRoles.ContactRoleId");

                entity.HasOne(d => d.EmailTemplate)
                    .WithMany(p => p.EmailTemplatesContactRoles)
                    .HasForeignKey(d => d.EmailTemplateId)
                    .HasConstraintName("FK_EmailTemplates_ContactRoles.EmailTemplateId");
            });

            modelBuilder.Entity<EmailTemplatesDefaultToContactRole>(entity =>
            {
                entity.HasKey(e => new { e.EmailTemplateId, e.DefaultToContactRoleId })
                    .HasName("EmailTemplates_DefaultToContactRoles_PK");

                entity.ToTable("EmailTemplates_DefaultToContactRoles");

                entity.HasOne(d => d.DefaultToContactRole)
                    .WithMany(p => p.EmailTemplatesDefaultToContactRoles)
                    .HasForeignKey(d => d.DefaultToContactRoleId)
                    .HasConstraintName("FK_EmailTemplates_DefaultToContactRoles.DefaultToContactRoleId");

                entity.HasOne(d => d.EmailTemplate)
                    .WithMany(p => p.EmailTemplatesDefaultToContactRoles)
                    .HasForeignKey(d => d.EmailTemplateId)
                    .HasConstraintName("FK_EmailTemplates_DefaultToContactRoles.EmailTemplateId");
            });

            modelBuilder.Entity<EmailTriggerEvent>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__EmailTri__3214EC064F7CD00D")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.MandatoryPlaceholders).HasMaxLength(4000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ErasureLicenceCharge>(entity =>
            {
                entity.HasKey(e => e.LicenceChargeId);

                entity.ToTable("ErasureLicenceCharge");

                entity.Property(e => e.LicenceChargeId).HasColumnName("LicenceChargeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LicenceCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LicenceTier).HasMaxLength(250);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ErasureReportExtract>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ErasureReportExtract");

                entity.Property(e => e.Capacity)
                    .HasMaxLength(155)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DepotName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ParentAssetId).HasColumnName("ParentAssetID");

                entity.Property(e => e.SerialNumber)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ServiceCompletedBy)
                    .HasMaxLength(101)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ServiceCompletedDate).HasColumnType("datetime");

                entity.Property(e => e.WarehouseName)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<ErasureReportSummary>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ErasureReportSummary");

                entity.Property(e => e.DepotName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ExternalLogisticsJob>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__External__3214EC06534D60F1")
                    .IsClustered(false);

                entity.HasIndex(e => e.Job, "IX_ExternalLogisticsJobs->Job")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.WorkflowLogisticsStage, "IX_ExternalLogisticsJobs->WorkflowLogisticsStage")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.LogisticsSystemJobId)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("LogisticsSystemJobID");

                entity.Property(e => e.RouteNumber).HasMaxLength(200);

                entity.HasOne(d => d.JobNavigation)
                    .WithMany(p => p.ExternalLogisticsJobs)
                    .HasForeignKey(d => d.Job)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExternalLogisticsJob.Job");

                entity.HasOne(d => d.JobTypeNavigation)
                    .WithMany(p => p.ExternalLogisticsJobs)
                    .HasForeignKey(d => d.JobType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExternalLogisticsJob.JobType");

                entity.HasOne(d => d.WorkflowLogisticsStageNavigation)
                    .WithMany(p => p.ExternalLogisticsJobs)
                    .HasForeignKey(d => d.WorkflowLogisticsStage)
                    .HasConstraintName("FK_ExternalLogisticsJob.WorkflowLogisticsStage->WorkflowLogisticsInhouseStage");
            });

            modelBuilder.Entity<ExternalProcessingJob>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__External__3214EC06571DF1D5")
                    .IsClustered(false);

                entity.HasIndex(e => e.Job, "IX_ExternalProcessingJobs->Job")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ProcessingSystemJobId).HasColumnName("ProcessingSystemJobID");

                entity.HasOne(d => d.JobNavigation)
                    .WithMany(p => p.ExternalProcessingJobs)
                    .HasForeignKey(d => d.Job)
                    .HasConstraintName("FK_ExternalProcessingJob.Job");
            });

            modelBuilder.Entity<FinancialDatum>(entity =>
            {
                entity.HasKey(e => e.AssetId)
                    .HasName("PK_F");

                entity.ToTable("FinancialData", "Materialised");

                entity.Property(e => e.AssetId).ValueGeneratedNever();

                entity.Property(e => e.GrandTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.StandardTotal).HasColumnType("decimal(38, 4)");
            });

            modelBuilder.Entity<HardDrife>(entity =>
            {
                entity.HasKey(e => new { e.OriginLot, e.AssetId, e.ParentAssetId });

                entity.ToTable("HardDrives", "Materialised");

                entity.Property(e => e.ParentAssetId).HasColumnName("ParentAssetID");

                entity.Property(e => e.Capacity)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerAssetId).HasMaxLength(150);

                entity.Property(e => e.Interface)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ParentClass)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SerialNumber)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceAssigned)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ServicePerformed)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceStatus)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<IndustrySector>(entity =>
            {
                entity.ToTable("IndustrySector");

                entity.Property(e => e.Sector)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoiceApproval>(entity =>
            {
                entity.HasKey(e => new { e.PromptCollectionNo, e.AssetId });

                entity.ToTable("InvoiceApproval", "Materialised");

                entity.HasIndex(e => e.PromptCollectionDate, "i_CollectionDate");

                entity.HasIndex(e => e.PromptCustomerGuid, "i_CustomerGUID");

                entity.Property(e => e.PromptCollectionNo).HasColumnName("prompt_CollectionNo");

                entity.Property(e => e.AdditionalServices).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.AuditCpuspeed)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUSpeed");

                entity.Property(e => e.AuditCputype)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUType");

                entity.Property(e => e.AuditHddcapacity)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDCapacity");

                entity.Property(e => e.AuditHddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDType");

                entity.Property(e => e.AuditScreenSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_ScreenSize");

                entity.Property(e => e.AuditTotalMemory)
                    .HasMaxLength(500)
                    .HasColumnName("Audit_TotalMemory");

                entity.Property(e => e.Class).HasMaxLength(400);

                entity.Property(e => e.CollectionDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerAssetId).HasMaxLength(150);

                entity.Property(e => e.CustomerName).HasMaxLength(200);

                entity.Property(e => e.CustomerNo).HasMaxLength(200);

                entity.Property(e => e.CustomerReference).HasMaxLength(200);

                entity.Property(e => e.DestinationLocation).HasMaxLength(200);

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit");

                entity.Property(e => e.Manufacturer).HasMaxLength(400);

                entity.Property(e => e.Model).HasMaxLength(400);

                entity.Property(e => e.OriginatingLocation).HasMaxLength(200);

                entity.Property(e => e.PromptBillingCode)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_BillingCode");

                entity.Property(e => e.PromptCollectionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("prompt_CollectionDate");

                entity.Property(e => e.PromptCustomerGuid).HasColumnName("prompt_CustomerGUID");

                entity.Property(e => e.PromptCustomerName)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_CustomerName");

                entity.Property(e => e.PromptCustomerNo)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_CustomerNo");

                entity.Property(e => e.PromptProject)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_Project");

                entity.Property(e => e.ReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.Requestor).HasMaxLength(401);

                entity.Property(e => e.ResaleChannel).HasMaxLength(150);

                entity.Property(e => e.SecurityStatus).HasMaxLength(50);

                entity.Property(e => e.SerialNumber).HasMaxLength(50);

                entity.Property(e => e.StandardServices).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.StreamsCharges).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.SubClass).HasMaxLength(250);

                entity.Property(e => e.TotalServices).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.Weight).HasColumnType("decimal(18, 4)");
            });

            modelBuilder.Entity<InvoiceHistory>(entity =>
            {
                entity.HasKey(e => e.ExportInvoiceId)
                    .HasName("PK_InvoiceExport");

                entity.ToTable("InvoiceHistory");

                entity.Property(e => e.ExportInvoiceId).HasColumnName("ExportInvoiceID");

                entity.Property(e => e.Analysis).HasMaxLength(250);

                entity.Property(e => e.Currency).HasMaxLength(100);

                entity.Property(e => e.CustomerCode).HasMaxLength(250);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CustomerName).HasMaxLength(250);

                entity.Property(e => e.CustomerOrderNumber).HasMaxLength(250);

                entity.Property(e => e.ExchangeRate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Glcode)
                    .HasMaxLength(50)
                    .HasColumnName("GLCode");

                entity.Property(e => e.InvoiceDate).HasColumnType("datetime");

                entity.Property(e => e.InvoiceDescription).HasMaxLength(250);

                entity.Property(e => e.InvoiceNumber).HasMaxLength(250);

                entity.Property(e => e.InvoiceType).HasMaxLength(50);

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.JobNumber).HasMaxLength(250);

                entity.Property(e => e.SalesPersonId).HasColumnName("SalesPersonID");

                entity.Property(e => e.SalesPersonName).HasMaxLength(250);

                entity.Property(e => e.TaxAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TaxCode).HasMaxLength(10);

                entity.Property(e => e.ValueGrossBase).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ValueGrossCurrency).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ValueNet).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ValueNetCurrency).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.VatamountCurrency)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("VATAmountCurrency");
            });

            modelBuilder.Entity<InvoiceHistoryOld>(entity =>
            {
                entity.HasKey(e => e.InvoiceHistoryId)
                    .HasName("PK_InvoiceHistory");

                entity.ToTable("InvoiceHistory_OLD");

                entity.Property(e => e.InvoiceHistoryId).HasColumnName("InvoiceHistoryID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.GrossInvoiceValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InvoiceNumber).HasMaxLength(100);

                entity.Property(e => e.IsVatcharged).HasColumnName("IsVATCharged");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.MasterCompanyId).HasColumnName("MasterCompanyID");

                entity.Property(e => e.NetInvoiceValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PrimaryContactId).HasColumnName("PrimaryContactID");

                entity.Property(e => e.SecondaryContactId).HasColumnName("SecondaryContactID");

                entity.Property(e => e.Vatamount)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("VATAmount");
            });

            modelBuilder.Entity<InvoiceOverrideTemp>(entity =>
            {
                entity.ToTable("InvoiceOverrideTemp");

                entity.Property(e => e.InvoiceOverrideTempId).HasColumnName("InvoiceOverrideTempID");

                entity.Property(e => e.AssetClass)
                    .IsRequired()
                    .HasMaxLength(400);

                entity.Property(e => e.AssetClassId).HasColumnName("AssetClassID");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.NewCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PreviousCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ServiceId).HasColumnName("ServiceID");

                entity.Property(e => e.ServiceName)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<InvoiceTemplate>(entity =>
            {
                entity.ToTable("InvoiceTemplate");

                entity.Property(e => e.InvoiceTemplateId).HasColumnName("InvoiceTemplateID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<InvoiceType>(entity =>
            {
                entity.ToTable("InvoiceType");

                entity.Property(e => e.InvoiceTypeId).HasColumnName("InvoiceTypeID");

                entity.Property(e => e.InvoiceType1)
                    .HasMaxLength(100)
                    .HasColumnName("InvoiceType");
            });

            modelBuilder.Entity<ItemReconciliationStatus>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__ItemReco__3214EC065AEE82B9")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Jobs__3214EC065EBF139D")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Reference, e.Customer, e.SourceContact, e.CustomerApprovalStatus, e.SalesApprovalStatus, e.LogisticsApprovalStatus, e.Itadstatus }, "ClusteredIndex-20190729-102050")
                    .IsClustered()
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Customer, "IX_Jobs->Customer")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Reference, "cj_Reference_20200324");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AdHocCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.AuditTrash).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CancelledAt).HasColumnType("datetime");

                entity.Property(e => e.ChargingModel).HasComment("1 - Contract Pricing, 2 - Quote-Based");

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.ConvertedPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CustomerPrintName).HasMaxLength(2000);

                entity.Property(e => e.CustomerSignatureFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("CustomerSignature_FileName");

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.CustomizableField2).HasMaxLength(200);

                entity.Property(e => e.CustomizableField3).HasMaxLength(200);

                entity.Property(e => e.EngineeringCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExchangeRate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.GoodsIn)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GrossRebate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InvoicedAt).HasColumnType("datetime");

                entity.Property(e => e.IsRmapproved).HasColumnName("IsRMApproved");

                entity.Property(e => e.IsRmissued).HasColumnName("IsRMIssued");

                entity.Property(e => e.Itadstatus).HasColumnName("ITADStatus");

                entity.Property(e => e.LogisticsCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LogisticsCollectionComplete).HasColumnType("datetime");

                entity.Property(e => e.LogisticsCollectionOnSite).HasColumnType("datetime");

                entity.Property(e => e.Mmwlogistics).HasColumnName("MMWLogistics");

                entity.Property(e => e.PalletsLocation).HasMaxLength(200);

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(200)
                    .HasColumnName("PONumber");

                entity.Property(e => e.ProcessingCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.QuotedLogisticsCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.QuotedPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ReadyToInvoiceAt).HasColumnType("datetime");

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.RequestedDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.SiteVisitScheduledAt).HasColumnType("datetime");

                entity.Property(e => e.SubmittedAt).HasColumnType("datetime");

                entity.Property(e => e.TaskId).HasColumnName("TaskID");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.Property(e => e.VehicleNumber).HasMaxLength(200);

                entity.Property(e => e.WasteCollection)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.WorkflowType).HasMaxLength(50);

                entity.HasOne(d => d.BillingCodeNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.BillingCode)
                    .HasConstraintName("FK_Job.BillingCode");

                entity.HasOne(d => d.CancelledByNavigation)
                    .WithMany(p => p.JobCancelledByNavigations)
                    .HasForeignKey(d => d.CancelledBy)
                    .HasConstraintName("FK_Job.CancelledBy->Contact");

                entity.HasOne(d => d.ClientStatusNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.ClientStatus)
                    .HasConstraintName("FK_Job.ClientStatus->JobClientStatus");

                entity.HasOne(d => d.CompletedByNavigation)
                    .WithMany(p => p.JobCompletedByNavigations)
                    .HasForeignKey(d => d.CompletedBy)
                    .HasConstraintName("FK_Job.CompletedBy->Contact");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.JobCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job.CreatedBy->Contact");

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job.Customer");

                entity.HasOne(d => d.CustomerApprovalStatusNavigation)
                    .WithMany(p => p.JobCustomerApprovalStatusNavigations)
                    .HasForeignKey(d => d.CustomerApprovalStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job.CustomerApprovalStatus->ApprovalStatus");

                entity.HasOne(d => d.CustomerApprovedByNavigation)
                    .WithMany(p => p.JobCustomerApprovedByNavigations)
                    .HasForeignKey(d => d.CustomerApprovedBy)
                    .HasConstraintName("FK_Job.CustomerApprovedBy->Contact");

                entity.HasOne(d => d.DataClassificationType)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.DataClassificationTypeId)
                    .HasConstraintName("FK__Jobs__DataClassi__6C59D134");

                entity.HasOne(d => d.DestinationContactNavigation)
                    .WithMany(p => p.JobDestinationContactNavigations)
                    .HasForeignKey(d => d.DestinationContact)
                    .HasConstraintName("FK_Job.DestinationContact->Contact");

                entity.HasOne(d => d.DestinationLocationNavigation)
                    .WithMany(p => p.JobDestinationLocationNavigations)
                    .HasForeignKey(d => d.DestinationLocation)
                    .HasConstraintName("FK_Job.DestinationLocation->Location");

                entity.HasOne(d => d.InvoicedByNavigation)
                    .WithMany(p => p.JobInvoicedByNavigations)
                    .HasForeignKey(d => d.InvoicedBy)
                    .HasConstraintName("FK_Job.InvoicedBy->Contact");

                entity.HasOne(d => d.ItadstatusNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.Itadstatus)
                    .HasConstraintName("FK_Job.ITADStatus->JobStatus");

                entity.HasOne(d => d.LogisticsApprovalStatusNavigation)
                    .WithMany(p => p.JobLogisticsApprovalStatusNavigations)
                    .HasForeignKey(d => d.LogisticsApprovalStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job.LogisticsApprovalStatus->ApprovalStatus");

                entity.HasOne(d => d.LogisticsApprovedByNavigation)
                    .WithMany(p => p.JobLogisticsApprovedByNavigations)
                    .HasForeignKey(d => d.LogisticsApprovedBy)
                    .HasConstraintName("FK_Job.LogisticsApprovedBy->Contact");

                entity.HasOne(d => d.LogisticsJobTypeNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.LogisticsJobType)
                    .HasConstraintName("FK_Job.LogisticsJobType->LogisticsWorkflow");

                entity.HasOne(d => d.LogisticsSupplierNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.LogisticsSupplier)
                    .HasConstraintName("FK_Job.LogisticsSupplier->LogisticsProvider");

                entity.HasOne(d => d.ProcessingJobTypeNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.ProcessingJobType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job.ProcessingJobType->ProcessingWorkflow");

                entity.HasOne(d => d.ProjectNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.Project)
                    .HasConstraintName("FK_Job.Project");

                entity.HasOne(d => d.ReadyToInvoiceByNavigation)
                    .WithMany(p => p.JobReadyToInvoiceByNavigations)
                    .HasForeignKey(d => d.ReadyToInvoiceBy)
                    .HasConstraintName("FK_Job.ReadyToInvoiceBy->Contact");

                entity.HasOne(d => d.RemarketingSettlementModelNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.RemarketingSettlementModel)
                    .HasConstraintName("fk_SettlementModel");

                entity.HasOne(d => d.RequestedByNavigation)
                    .WithMany(p => p.JobRequestedByNavigations)
                    .HasForeignKey(d => d.RequestedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job.RequestedBy->Contact");

                entity.HasOne(d => d.RequestedJobTypeNavigation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.RequestedJobType)
                    .HasConstraintName("FK_Job.RequestedJobType->JobType");

                entity.HasOne(d => d.SalesApprovalStatusNavigation)
                    .WithMany(p => p.JobSalesApprovalStatusNavigations)
                    .HasForeignKey(d => d.SalesApprovalStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job.SalesApprovalStatus->ApprovalStatus");

                entity.HasOne(d => d.SalesApprovedByNavigation)
                    .WithMany(p => p.JobSalesApprovedByNavigations)
                    .HasForeignKey(d => d.SalesApprovedBy)
                    .HasConstraintName("FK_Job.SalesApprovedBy->Contact");

                entity.HasOne(d => d.SourceContactNavigation)
                    .WithMany(p => p.JobSourceContactNavigations)
                    .HasForeignKey(d => d.SourceContact)
                    .HasConstraintName("FK_Job.SourceContact->Contact");

                entity.HasOne(d => d.SourceLocationNavigation)
                    .WithMany(p => p.JobSourceLocationNavigations)
                    .HasForeignKey(d => d.SourceLocation)
                    .HasConstraintName("FK_Job.SourceLocation->Location");

                entity.HasOne(d => d.SubmittedByNavigation)
                    .WithMany(p => p.JobSubmittedByNavigations)
                    .HasForeignKey(d => d.SubmittedBy)
                    .HasConstraintName("FK_Job.SubmittedBy->Contact");

                entity.HasOne(d => d.UpdatedByNavigation)
                    .WithMany(p => p.JobUpdatedByNavigations)
                    .HasForeignKey(d => d.UpdatedBy)
                    .HasConstraintName("FK__Jobs__UpdatedOn__6D4DF56D");
            });

            modelBuilder.Entity<JobAsset>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__JobAsset__3214EC06628FA481")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Asset, e.Job, e.ProcessingSystemProcess, e.ProcessingSystemReconciliationStatus, e.LogisticsSystemStatus, e.AssetType, e.Customer, e.Stnumber }, "ClusteredIndex-20190729-103133")
                    .IsClustered()
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Asset, "IX_JobAssets->Asset")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Job, "IX_JobAssets->Job")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.CollectionDate).HasColumnType("datetime");

                entity.Property(e => e.Stnumber).HasColumnName("STNumber");

                entity.HasOne(d => d.AssetNavigation)
                    .WithMany(p => p.JobAssets)
                    .HasForeignKey(d => d.Asset)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_JobAsset.Asset");

                entity.HasOne(d => d.AssetTypeNavigation)
                    .WithMany(p => p.JobAssets)
                    .HasForeignKey(d => d.AssetType)
                    .HasConstraintName("FK_JobAsset.AssetType");

                entity.HasOne(d => d.ContainerNavigation)
                    .WithMany(p => p.JobAssets)
                    .HasForeignKey(d => d.Container)
                    .HasConstraintName("FK_JobAsset.Container");

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.JobAssets)
                    .HasForeignKey(d => d.Customer)
                    .HasConstraintName("FK_JobAsset.Customer");

                entity.HasOne(d => d.JobNavigation)
                    .WithMany(p => p.JobAssets)
                    .HasForeignKey(d => d.Job)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_JobAsset.Job");

                entity.HasOne(d => d.LogisticsSystemStatusNavigation)
                    .WithMany(p => p.JobAssetLogisticsSystemStatusNavigations)
                    .HasForeignKey(d => d.LogisticsSystemStatus)
                    .HasConstraintName("FK_JobAsset.LogisticsSystemStatus->ItemReconciliationStatus");

                entity.HasOne(d => d.ProcessingSystemProcessNavigation)
                    .WithMany(p => p.JobAssets)
                    .HasForeignKey(d => d.ProcessingSystemProcess)
                    .HasConstraintName("FK_JobAsset.ProcessingSystemProcess");

                entity.HasOne(d => d.ProcessingSystemReconciliationStatusNavigation)
                    .WithMany(p => p.JobAssetProcessingSystemReconciliationStatusNavigations)
                    .HasForeignKey(d => d.ProcessingSystemReconciliationStatus)
                    .HasConstraintName("FK_JobAsset.ProcessingSystemReconciliationStatus->ItemReconciliationStatus");
            });

            modelBuilder.Entity<JobAssetType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__JobAsset__3214EC0666603565")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Job, e.AssetType, e.Collected, e.Expected }, "ClusteredIndex-20190729-103322")
                    .IsClustered();

                entity.HasIndex(e => e.Job, "IX_JobAssetTypes->Job")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.AssetTypeNavigation)
                    .WithMany(p => p.JobAssetTypes)
                    .HasForeignKey(d => d.AssetType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_JobAssetType.AssetType");

                entity.HasOne(d => d.ContainerNavigation)
                    .WithMany(p => p.JobAssetTypes)
                    .HasForeignKey(d => d.Container)
                    .HasConstraintName("FK_JobAssetType.Container");

                entity.HasOne(d => d.JobNavigation)
                    .WithMany(p => p.JobAssetTypes)
                    .HasForeignKey(d => d.Job)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_JobAssetType.Job");
            });

            modelBuilder.Entity<JobClientStatus>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__JobClien__3214EC0657A801BA")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<JobNote>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__JobNotes__3214EC066A30C649")
                    .IsClustered(false);

                entity.HasIndex(e => e.Job, "ceri_2020_02_12_Job");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AttachmentFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("Attachment_FileName");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.JobNotes)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_JobNote.CreatedBy->Contact");

                entity.HasOne(d => d.JobNavigation)
                    .WithMany(p => p.JobNotes)
                    .HasForeignKey(d => d.Job)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_JobNote.Job");

                entity.HasOne(d => d.StandardReportTypeNavigation)
                    .WithMany(p => p.JobNotes)
                    .HasForeignKey(d => d.StandardReportType)
                    .HasConstraintName("FK_JobNote.StandardReportType");
            });

            modelBuilder.Entity<JobProcessing>(entity =>
            {
                entity.HasKey(e => e.ProcessingId)
                    .HasName("PK_JobProcessings");

                entity.ToTable("JobProcessing");

                entity.Property(e => e.ProcessingId).HasColumnName("ProcessingID");

                entity.Property(e => e.ProcessingName).HasMaxLength(250);
            });

            modelBuilder.Entity<JobReceivingComment>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.AssetsReferredTo).HasMaxLength(500);

                entity.Property(e => e.ExecutiveOverrideReason).HasMaxLength(1000);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.Stnumber).HasColumnName("STNumber");

                entity.Property(e => e.VarianceReasonId).HasColumnName("VarianceReasonID");
            });

            modelBuilder.Entity<JobReceivingCommentsBackup20180523>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("JobReceivingComments_BACKUP_20180523");

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.AssetsReferredTo).HasMaxLength(500);

                entity.Property(e => e.ExecutiveOverrideReason).HasMaxLength(1000);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.Stnumber).HasColumnName("STNumber");

                entity.Property(e => e.VarianceReasonId).HasColumnName("VarianceReasonID");
            });

            modelBuilder.Entity<JobStatus>(entity =>
            {
                entity.HasKey(e => e.JobId)
                    .HasName("PK__Job_Stat__056690C22D70FB15");

                entity.ToTable("Job_Status");

                entity.Property(e => e.JobId).ValueGeneratedNever();
            });

            modelBuilder.Entity<JobStatus1>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__JobStatu__3214EC0668D28DBC")
                    .IsClustered(false);

                entity.ToTable("JobStatuses");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Stage).HasMaxLength(200);
            });

            modelBuilder.Entity<JobTaskParameter>(entity =>
            {
                entity.ToTable("JobTaskParameter");

                entity.Property(e => e.JobTaskParameterId).HasColumnName("JobTaskParameterID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.ParameterName).HasMaxLength(250);

                entity.Property(e => e.TaskId).HasColumnName("TaskID");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.JobTaskParameters)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("FK_JobTaskParameter_TaskTypes");
            });

            modelBuilder.Entity<JobType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__JobTypes__3214EC066E01572D")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<JobVariance>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("JobVariance");

                entity.Property(e => e.AssetTypeId).HasColumnName("AssetTypeID");

                entity.Property(e => e.ExecutiveOverrideReason).HasMaxLength(255);

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.JobVarianceId).HasColumnName("JobVarianceID");

                entity.Property(e => e.VarianceReasonId).HasColumnName("VarianceReasonID");
            });

            modelBuilder.Entity<Jobs4444>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Jobs__3214EC067525DCA7")
                    .IsClustered(false);

                entity.ToTable("Jobs_4444");

                entity.HasIndex(e => new { e.Id, e.Reference, e.Customer, e.SourceContact, e.CustomerApprovalStatus, e.SalesApprovalStatus, e.LogisticsApprovalStatus, e.Itadstatus }, "ClusteredIndex-20190729-102050")
                    .IsClustered()
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Customer, "IX_Jobs->Customer")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Reference, "cj_Reference_20200324");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AdHocCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.AuditTrash).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CancelledAt).HasColumnType("datetime");

                entity.Property(e => e.ChargingModel).HasComment("1 - Contract Pricing, 2 - Quote-Based");

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.ConvertedPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CustomerPrintName).HasMaxLength(2000);

                entity.Property(e => e.CustomerSignatureFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("CustomerSignature_FileName");

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.CustomizableField2).HasMaxLength(200);

                entity.Property(e => e.CustomizableField3).HasMaxLength(200);

                entity.Property(e => e.EngineeringCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExchangeRate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.GrossRebate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InvoicedAt).HasColumnType("datetime");

                entity.Property(e => e.Itadstatus).HasColumnName("ITADStatus");

                entity.Property(e => e.LogisticsCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LogisticsCollectionComplete).HasColumnType("datetime");

                entity.Property(e => e.LogisticsCollectionOnSite).HasColumnType("datetime");

                entity.Property(e => e.Mmwlogistics).HasColumnName("MMWLogistics");

                entity.Property(e => e.PalletsLocation).HasMaxLength(200);

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(200)
                    .HasColumnName("PONumber");

                entity.Property(e => e.ProcessingCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.QuotedLogisticsCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.QuotedPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ReadyToInvoiceAt).HasColumnType("datetime");

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.RequestedDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.SiteVisitScheduledAt).HasColumnType("datetime");

                entity.Property(e => e.SubmittedAt).HasColumnType("datetime");

                entity.Property(e => e.TaskId).HasColumnName("TaskID");

                entity.Property(e => e.VehicleNumber).HasMaxLength(200);

                entity.Property(e => e.WasteCollection)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.WorkflowType).HasMaxLength(50);

                entity.HasOne(d => d.BillingCodeNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.BillingCode)
                    .HasConstraintName("FKJob.BillingCode");

                entity.HasOne(d => d.CancelledByNavigation)
                    .WithMany(p => p.Jobs4444CancelledByNavigations)
                    .HasForeignKey(d => d.CancelledBy)
                    .HasConstraintName("FKJob.CancelledBy->Contact");

                entity.HasOne(d => d.ClientStatusNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.ClientStatus)
                    .HasConstraintName("FKJob.ClientStatus->JobClientStatus");

                entity.HasOne(d => d.CompletedByNavigation)
                    .WithMany(p => p.Jobs4444CompletedByNavigations)
                    .HasForeignKey(d => d.CompletedBy)
                    .HasConstraintName("FKJob.CompletedBy->Contact");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.Jobs4444CreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKJob.CreatedBy->Contact");

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKJob.Customer");

                entity.HasOne(d => d.CustomerApprovalStatusNavigation)
                    .WithMany(p => p.Jobs4444CustomerApprovalStatusNavigations)
                    .HasForeignKey(d => d.CustomerApprovalStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKJob.CustomerApprovalStatus->ApprovalStatus");

                entity.HasOne(d => d.CustomerApprovedByNavigation)
                    .WithMany(p => p.Jobs4444CustomerApprovedByNavigations)
                    .HasForeignKey(d => d.CustomerApprovedBy)
                    .HasConstraintName("FKJob.CustomerApprovedBy->Contact");

                entity.HasOne(d => d.DestinationContactNavigation)
                    .WithMany(p => p.Jobs4444DestinationContactNavigations)
                    .HasForeignKey(d => d.DestinationContact)
                    .HasConstraintName("FKJob.DestinationContact->Contact");

                entity.HasOne(d => d.DestinationLocationNavigation)
                    .WithMany(p => p.Jobs4444DestinationLocationNavigations)
                    .HasForeignKey(d => d.DestinationLocation)
                    .HasConstraintName("FKJob.DestinationLocation->Location");

                entity.HasOne(d => d.InvoicedByNavigation)
                    .WithMany(p => p.Jobs4444InvoicedByNavigations)
                    .HasForeignKey(d => d.InvoicedBy)
                    .HasConstraintName("FKJob.InvoicedBy->Contact");

                entity.HasOne(d => d.ItadstatusNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.Itadstatus)
                    .HasConstraintName("FKJob.ITADStatus->JobStatus");

                entity.HasOne(d => d.LogisticsApprovalStatusNavigation)
                    .WithMany(p => p.Jobs4444LogisticsApprovalStatusNavigations)
                    .HasForeignKey(d => d.LogisticsApprovalStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKJob.LogisticsApprovalStatus->ApprovalStatus");

                entity.HasOne(d => d.LogisticsApprovedByNavigation)
                    .WithMany(p => p.Jobs4444LogisticsApprovedByNavigations)
                    .HasForeignKey(d => d.LogisticsApprovedBy)
                    .HasConstraintName("FKJob.LogisticsApprovedBy->Contact");

                entity.HasOne(d => d.LogisticsJobTypeNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.LogisticsJobType)
                    .HasConstraintName("FKJob.LogisticsJobType->LogisticsWorkflow");

                entity.HasOne(d => d.LogisticsSupplierNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.LogisticsSupplier)
                    .HasConstraintName("FKJob.LogisticsSupplier->LogisticsProvider");

                entity.HasOne(d => d.ProcessingJobTypeNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.ProcessingJobType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKJob.ProcessingJobType->ProcessingWorkflow");

                entity.HasOne(d => d.ProjectNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.Project)
                    .HasConstraintName("FKJob.Project");

                entity.HasOne(d => d.ReadyToInvoiceByNavigation)
                    .WithMany(p => p.Jobs4444ReadyToInvoiceByNavigations)
                    .HasForeignKey(d => d.ReadyToInvoiceBy)
                    .HasConstraintName("FKJob.ReadyToInvoiceBy->Contact");

                entity.HasOne(d => d.RemarketingSettlementModelNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.RemarketingSettlementModel)
                    .HasConstraintName("fkSettlementModel");

                entity.HasOne(d => d.RequestedByNavigation)
                    .WithMany(p => p.Jobs4444RequestedByNavigations)
                    .HasForeignKey(d => d.RequestedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKJob.RequestedBy->Contact");

                entity.HasOne(d => d.RequestedJobTypeNavigation)
                    .WithMany(p => p.Jobs4444s)
                    .HasForeignKey(d => d.RequestedJobType)
                    .HasConstraintName("FKJob.RequestedJobType->JobType");

                entity.HasOne(d => d.SalesApprovalStatusNavigation)
                    .WithMany(p => p.Jobs4444SalesApprovalStatusNavigations)
                    .HasForeignKey(d => d.SalesApprovalStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKJob.SalesApprovalStatus->ApprovalStatus");

                entity.HasOne(d => d.SalesApprovedByNavigation)
                    .WithMany(p => p.Jobs4444SalesApprovedByNavigations)
                    .HasForeignKey(d => d.SalesApprovedBy)
                    .HasConstraintName("FKJob.SalesApprovedBy->Contact");

                entity.HasOne(d => d.SourceContactNavigation)
                    .WithMany(p => p.Jobs4444SourceContactNavigations)
                    .HasForeignKey(d => d.SourceContact)
                    .HasConstraintName("FKJob.SourceContact->Contact");

                entity.HasOne(d => d.SourceLocationNavigation)
                    .WithMany(p => p.Jobs4444SourceLocationNavigations)
                    .HasForeignKey(d => d.SourceLocation)
                    .HasConstraintName("FKJob.SourceLocation->Location");

                entity.HasOne(d => d.SubmittedByNavigation)
                    .WithMany(p => p.Jobs4444SubmittedByNavigations)
                    .HasForeignKey(d => d.SubmittedBy)
                    .HasConstraintName("FKJob.SubmittedBy->Contact");
            });

            modelBuilder.Entity<JobsHistoryOlddd>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("JobsHistory_olddd");

                entity.HasIndex(e => new { e.EndTime, e.StartTime }, "ix_JobsHistory")
                    .IsClustered();

                entity.Property(e => e.AdHocCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.AuditTrash).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CancelledAt).HasColumnType("datetime");

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.ConvertedPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CustomerPrintName).HasMaxLength(2000);

                entity.Property(e => e.CustomerSignatureFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("CustomerSignature_FileName");

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.CustomizableField2).HasMaxLength(200);

                entity.Property(e => e.CustomizableField3).HasMaxLength(200);

                entity.Property(e => e.EngineeringCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExchangeRate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.GrossRebate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InvoicedAt).HasColumnType("datetime");

                entity.Property(e => e.Itadstatus).HasColumnName("ITADStatus");

                entity.Property(e => e.LogisticsCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LogisticsCollectionComplete).HasColumnType("datetime");

                entity.Property(e => e.LogisticsCollectionOnSite).HasColumnType("datetime");

                entity.Property(e => e.Mmwlogistics).HasColumnName("MMWLogistics");

                entity.Property(e => e.PalletsLocation).HasMaxLength(200);

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(200)
                    .HasColumnName("PONumber");

                entity.Property(e => e.ProcessingCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.QuotedLogisticsCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.QuotedPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ReadyToInvoiceAt).HasColumnType("datetime");

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.RequestedDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.SiteVisitScheduledAt).HasColumnType("datetime");

                entity.Property(e => e.SubmittedAt).HasColumnType("datetime");

                entity.Property(e => e.TaskId).HasColumnName("TaskID");

                entity.Property(e => e.VehicleNumber).HasMaxLength(200);

                entity.Property(e => e.WasteCollection)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.WorkflowType).HasMaxLength(50);
            });

            modelBuilder.Entity<JobsOld>(entity =>
            {
                entity.ToTable("Jobs_old");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AdHocCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.AuditTrash).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CancelledAt).HasColumnType("datetime");

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.ConvertedPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CustomerPrintName).HasMaxLength(2000);

                entity.Property(e => e.CustomerSignatureFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("CustomerSignature_FileName");

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.CustomizableField2).HasMaxLength(200);

                entity.Property(e => e.CustomizableField3).HasMaxLength(200);

                entity.Property(e => e.EngineeringCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExchangeRate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.GrossRebate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InvoicedAt).HasColumnType("datetime");

                entity.Property(e => e.Itadstatus).HasColumnName("ITADStatus");

                entity.Property(e => e.LogisticsCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LogisticsCollectionComplete).HasColumnType("datetime");

                entity.Property(e => e.LogisticsCollectionOnSite).HasColumnType("datetime");

                entity.Property(e => e.Mmwlogistics).HasColumnName("MMWLogistics");

                entity.Property(e => e.PalletsLocation).HasMaxLength(200);

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(200)
                    .HasColumnName("PONumber");

                entity.Property(e => e.ProcessingCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.QuotedLogisticsCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.QuotedPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ReadyToInvoiceAt).HasColumnType("datetime");

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.RequestedDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.SiteVisitScheduledAt).HasColumnType("datetime");

                entity.Property(e => e.SubmittedAt).HasColumnType("datetime");

                entity.Property(e => e.TaskId).HasColumnName("TaskID");

                entity.Property(e => e.VehicleNumber).HasMaxLength(200);

                entity.Property(e => e.WasteCollection)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.WorkflowType).HasMaxLength(50);
            });

            modelBuilder.Entity<JobsTemp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("JobsTemp");

                entity.Property(e => e.Itadstatus).HasColumnName("ITADStatus");
            });

            modelBuilder.Entity<JobsUpdatePermission>(entity =>
            {
                entity.ToTable("JobsUpdatePermission");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<LegacySystem>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("LegacySystems", "Lookups");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Location__3214EC0671D1E811")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Reference, e.Customer, e.Postcode, e.IsBillingEntity }, "ClusteredIndex-20190729-103418")
                    .IsClustered();

                entity.HasIndex(e => e.Customer, "IX_Locations->Customer")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => e.Postcode, "NonClusteredIndex-20210810-162521");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AddressLine1)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.AddressLine2).HasMaxLength(200);

                entity.Property(e => e.AddressLine3).HasMaxLength(200);

                entity.Property(e => e.AddressReference)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.AttachmentFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("Attachment_FileName");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.County).HasMaxLength(200);

                entity.Property(e => e.ExternalData1)
                    .HasMaxLength(200)
                    .IsFixedLength(true);

                entity.Property(e => e.ExternalData2).HasMaxLength(200);

                entity.Property(e => e.FromAvoidTime).HasMaxLength(20);

                entity.Property(e => e.Gpslat)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("GPSLat");

                entity.Property(e => e.Gpslon)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("GPSLon");

                entity.Property(e => e.Gpsvalid).HasColumnName("GPSValid");

                entity.Property(e => e.IsInUlezzone).HasColumnName("IsInULEZZone");

                entity.Property(e => e.IsLoadingAreaCctv).HasColumnName("IsLoadingAreaCCTV");

                entity.Property(e => e.LocalAuthority).HasMaxLength(250);

                entity.Property(e => e.LogisticsSystemId)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("LogisticsSystemID");

                entity.Property(e => e.ParkingDistance).HasMaxLength(250);

                entity.Property(e => e.Postcode)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.ProcessGivingRiseToWaste).HasMaxLength(250);

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.Siccode)
                    .HasMaxLength(250)
                    .HasColumnName("SICCode");

                entity.Property(e => e.ToAvoidTime).HasMaxLength(20);

                entity.Property(e => e.WasteLicenceNumber).HasMaxLength(250);

                entity.Property(e => e.WasteLicenceType).HasMaxLength(250);

                entity.Property(e => e.WastenoteDeliveryEmail).HasMaxLength(250);

                entity.HasOne(d => d.CountryNavigation)
                    .WithMany(p => p.Locations)
                    .HasForeignKey(d => d.Country)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Location.Country");

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.Locations)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Location.Customer");
            });

            modelBuilder.Entity<LocationsImportStagingWiz>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("LocationsImportStagingWiz");

                entity.Property(e => e.AddressLine1)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.AddressLine2).HasMaxLength(255);

                entity.Property(e => e.AddressLine3).HasMaxLength(255);

                entity.Property(e => e.AddressReference)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.AttachmentFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("Attachment_FileName");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.County).HasMaxLength(255);

                entity.Property(e => e.FromAvoidTime).HasMaxLength(255);

                entity.Property(e => e.IsInUlezzone).HasColumnName("IsInULEZZone");

                entity.Property(e => e.IsLoadingAreaCctv).HasColumnName("IsLoadingAreaCCTV");

                entity.Property(e => e.LocalAuthority).HasMaxLength(255);

                entity.Property(e => e.LogisticsSystemId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("LogisticsSystemID");

                entity.Property(e => e.ParkingDistance).HasMaxLength(255);

                entity.Property(e => e.Postcode)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ProcessGivingRiseToWaste).HasMaxLength(255);

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.Siccode)
                    .HasMaxLength(255)
                    .HasColumnName("SICCode");

                entity.Property(e => e.ToAvoidTime).HasMaxLength(255);

                entity.Property(e => e.WasteLicenceNumber).HasMaxLength(255);

                entity.Property(e => e.WasteLicenceType).HasMaxLength(255);

                entity.Property(e => e.WastenoteDeliveryEmail).HasMaxLength(255);
            });

            modelBuilder.Entity<LocationsRuncornImportTemp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Locations_Runcorn_Import_Temp");

                entity.Property(e => e.AddressLine1)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.AddressLine2).HasMaxLength(200);

                entity.Property(e => e.AddressLine3).HasMaxLength(200);

                entity.Property(e => e.AddressReference)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.AttachmentFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("Attachment_FileName");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.County).HasMaxLength(200);

                entity.Property(e => e.CustomerId)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("CustomerID");

                entity.Property(e => e.ExternalData1)
                    .HasMaxLength(200)
                    .IsFixedLength(true);

                entity.Property(e => e.FromAvoidTime).HasMaxLength(20);

                entity.Property(e => e.Gpslat)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("GPSLat");

                entity.Property(e => e.Gpslon)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("GPSLon");

                entity.Property(e => e.Gpsvalid).HasColumnName("GPSValid");

                entity.Property(e => e.IsInUlezzone).HasColumnName("IsInULEZZone");

                entity.Property(e => e.IsLoadingAreaCctv).HasColumnName("IsLoadingAreaCCTV");

                entity.Property(e => e.LocalAuthority).HasMaxLength(250);

                entity.Property(e => e.LogisticsSystemId)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("LogisticsSystemID");

                entity.Property(e => e.ParkingDistance).HasMaxLength(250);

                entity.Property(e => e.Postcode)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.ProcessGivingRiseToWaste).HasMaxLength(250);

                entity.Property(e => e.Siccode)
                    .HasMaxLength(250)
                    .HasColumnName("SICCode");

                entity.Property(e => e.ToAvoidTime).HasMaxLength(20);

                entity.Property(e => e.WasteLicenceNumber).HasMaxLength(250);

                entity.Property(e => e.WasteLicenceType).HasMaxLength(250);

                entity.Property(e => e.WastenoteDeliveryEmail).HasMaxLength(250);
            });

            modelBuilder.Entity<LogSeverityLevel>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("LogSeverityLevel");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SeverityLevelId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<LoginAttempt>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__LoginAtt__3214EC0675A278F5")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Browser).IsRequired();

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.InputtedLoginUser)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.InputtedPassword)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Ip)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("IP");

                entity.Property(e => e.Salt).HasMaxLength(255);

                entity.HasOne(d => d.ContactNavigation)
                    .WithMany(p => p.LoginAttempts)
                    .HasForeignKey(d => d.Contact)
                    .HasConstraintName("FK_LoginAttempt.Contact");
            });

            modelBuilder.Entity<LogisticsActivity>(entity =>
            {
                entity.HasKey(e => e.ActivityId);

                entity.ToTable("Logistics_Activities");

                entity.Property(e => e.ActivityId).HasColumnName("ActivityID");

                entity.Property(e => e.Accuracy).HasMaxLength(50);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.FixTime).HasMaxLength(50);

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.Provider).HasMaxLength(100);

                entity.Property(e => e.Type).HasMaxLength(100);
            });

            modelBuilder.Entity<LogisticsAdjustmentFactor>(entity =>
            {
                entity.HasKey(e => e.AdjustmentFactorId);

                entity.ToTable("Logistics_AdjustmentFactor");

                entity.Property(e => e.AdjustmentFactorId).HasColumnName("AdjustmentFactorID");

                entity.Property(e => e.AssetGroup).HasMaxLength(200);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogisticsAttachment>(entity =>
            {
                entity.HasKey(e => e.AttachmentId);

                entity.ToTable("Logistics_Attachments");

                entity.Property(e => e.AttachmentId).HasColumnName("AttachmentID");

                entity.Property(e => e.AttachmentFile).HasMaxLength(255);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.JobStatus).HasMaxLength(50);
            });

            modelBuilder.Entity<LogisticsBranch>(entity =>
            {
                entity.HasKey(e => e.BranchId);

                entity.ToTable("Logistics_Branch");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.ContactEmail).HasMaxLength(255);

                entity.Property(e => e.ContactName).HasMaxLength(255);

                entity.Property(e => e.ContactNumber).HasMaxLength(20);

                entity.Property(e => e.CreatedAt).HasMaxLength(50);

                entity.Property(e => e.CreatedAtDateTime).HasColumnType("datetime");

                entity.Property(e => e.ExternalId)
                    .HasMaxLength(100)
                    .HasColumnName("ExternalID");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Number).HasMaxLength(20);
            });

            modelBuilder.Entity<LogisticsCharge>(entity =>
            {
                entity.HasKey(e => e.LogisticsChargesId);

                entity.Property(e => e.LogisticsChargesId).HasColumnName("LogisticsChargesID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LogisticsCharge1)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("LogisticsCharge");

                entity.Property(e => e.LogisticsChargingModelId).HasColumnName("LogisticsChargingModelID");

                entity.Property(e => e.ServiceLevelId).HasColumnName("ServiceLevelID");
            });

            modelBuilder.Entity<LogisticsChargingModel>(entity =>
            {
                entity.ToTable("LogisticsChargingModel");

                entity.Property(e => e.LogisticsChargingModelId).HasColumnName("LogisticsChargingModelID");

                entity.Property(e => e.LogisticsChargingModelName).HasMaxLength(255);
            });

            modelBuilder.Entity<LogisticsCheckList>(entity =>
            {
                entity.HasKey(e => e.CheckListId);

                entity.ToTable("Logistics_CheckList");

                entity.Property(e => e.CheckListId).HasColumnName("CheckListID");

                entity.Property(e => e.CreatedAt).HasMaxLength(50);

                entity.Property(e => e.CreatedAtDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id)
                    .HasMaxLength(20)
                    .HasColumnName("ID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.OrderId)
                    .HasMaxLength(50)
                    .HasColumnName("OrderID");

                entity.Property(e => e.Uuid).HasColumnName("UUID");
            });

            modelBuilder.Entity<LogisticsCheckListQuestion>(entity =>
            {
                entity.HasKey(e => e.CheckListQuestionId);

                entity.ToTable("Logistics_CheckListQuestions");

                entity.Property(e => e.CheckListQuestionId).HasColumnName("CheckListQuestionID");

                entity.Property(e => e.CheckListId).HasColumnName("CheckListID");

                entity.Property(e => e.OrderId)
                    .HasMaxLength(50)
                    .HasColumnName("OrderID");

                entity.Property(e => e.Question).HasMaxLength(255);

                entity.Property(e => e.Type).HasMaxLength(100);

                entity.Property(e => e.Uuid).HasColumnName("UUID");
            });

            modelBuilder.Entity<LogisticsCongestionCharge>(entity =>
            {
                entity.HasKey(e => e.CongestionChargeId);

                entity.ToTable("Logistics_CongestionCharge");

                entity.Property(e => e.CongestionChargeId).HasColumnName("CongestionChargeID");

                entity.Property(e => e.CongestionCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogisticsCustomer>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.ToTable("Logistics_Customer");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.Address1).HasMaxLength(255);

                entity.Property(e => e.Address2).HasMaxLength(255);

                entity.Property(e => e.Address3).HasMaxLength(255);

                entity.Property(e => e.Address4).HasMaxLength(255);

                entity.Property(e => e.ContactEmail).HasMaxLength(255);

                entity.Property(e => e.ContactName).HasMaxLength(255);

                entity.Property(e => e.ContactNumber).HasMaxLength(20);

                entity.Property(e => e.CreatedAt).HasMaxLength(50);

                entity.Property(e => e.CreatedAtDateTime).HasColumnType("datetime");

                entity.Property(e => e.ExternalId)
                    .HasMaxLength(100)
                    .HasColumnName("ExternalID");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.PostCode).HasMaxLength(50);

                entity.Property(e => e.Siccode)
                    .HasMaxLength(255)
                    .HasColumnName("SICCode");

                entity.Property(e => e.UpdatedAt).HasMaxLength(50);

                entity.Property(e => e.UpdatedAtDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<LogisticsDeliveryContact>(entity =>
            {
                entity.HasKey(e => e.DeliveryContactId);

                entity.ToTable("Logistics_DeliveryContact");

                entity.Property(e => e.DeliveryContactId).HasColumnName("DeliveryContactID");

                entity.Property(e => e.DeliveryAddress1).HasMaxLength(255);

                entity.Property(e => e.DeliveryAddress2).HasMaxLength(255);

                entity.Property(e => e.DeliveryAddress3).HasMaxLength(255);

                entity.Property(e => e.DeliveryAddress4).HasMaxLength(255);

                entity.Property(e => e.DeliveryContactName).HasMaxLength(255);

                entity.Property(e => e.DeliveryContactNumber).HasMaxLength(20);

                entity.Property(e => e.DeliveryCustomerId).HasColumnName("DeliveryCustomerID");

                entity.Property(e => e.DeliveryPostCode).HasMaxLength(50);

                entity.Property(e => e.DeliveryUpdatedAt).HasMaxLength(50);

                entity.Property(e => e.JobId).HasColumnName("JobID");
            });

            modelBuilder.Entity<LogisticsItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("Logistics_Items");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Other).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Type).HasMaxLength(100);

                entity.Property(e => e.Uuid).HasColumnName("UUID");
            });

            modelBuilder.Entity<LogisticsJob>(entity =>
            {
                entity.HasKey(e => e.JobId);

                entity.ToTable("Logistics_Jobs");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.Address1).HasMaxLength(255);

                entity.Property(e => e.Address2).HasMaxLength(255);

                entity.Property(e => e.Address3).HasMaxLength(255);

                entity.Property(e => e.Address4).HasMaxLength(255);

                entity.Property(e => e.Attachments).HasMaxLength(255);

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.ContactName).HasMaxLength(255);

                entity.Property(e => e.ContactNumber1).HasMaxLength(20);

                entity.Property(e => e.ContactNumber2).HasMaxLength(20);

                entity.Property(e => e.ContactNumber3).HasMaxLength(20);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Duration).HasMaxLength(50);

                entity.Property(e => e.ExternalId)
                    .HasMaxLength(100)
                    .HasColumnName("ExternalID");

                entity.Property(e => e.FailureType).HasMaxLength(255);

                entity.Property(e => e.Fault).HasMaxLength(50);

                entity.Property(e => e.GasSafeInspection).HasMaxLength(255);

                entity.Property(e => e.JobNumber).HasMaxLength(100);

                entity.Property(e => e.MobileUserId).HasColumnName("MobileUserID");

                entity.Property(e => e.Parts).HasMaxLength(255);

                entity.Property(e => e.Photos).HasMaxLength(255);

                entity.Property(e => e.PostCode).HasMaxLength(50);

                entity.Property(e => e.PostJobCheck).HasMaxLength(255);

                entity.Property(e => e.PreJobCheck).HasMaxLength(255);

                entity.Property(e => e.Priority).HasMaxLength(255);

                entity.Property(e => e.Product).HasMaxLength(255);

                entity.Property(e => e.RepairCode).HasMaxLength(50);

                entity.Property(e => e.SecondaryUser).HasMaxLength(255);

                entity.Property(e => e.SignatureCreatedAt).HasColumnType("datetime");

                entity.Property(e => e.SignatureName).HasMaxLength(255);

                entity.Property(e => e.SignatureSignee).HasMaxLength(255);

                entity.Property(e => e.SignatureType).HasMaxLength(255);

                entity.Property(e => e.Signee).HasMaxLength(255);

                entity.Property(e => e.Status).HasMaxLength(100);

                entity.Property(e => e.Task).HasMaxLength(255);

                entity.Property(e => e.TimeSlot).HasColumnType("datetime");

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.Weight).HasMaxLength(50);
            });

            modelBuilder.Entity<LogisticsMobileUser>(entity =>
            {
                entity.HasKey(e => e.MobileUserId);

                entity.ToTable("Logistics_MobileUser");

                entity.Property(e => e.MobileUserId).HasColumnName("MobileUserID");

                entity.Property(e => e.Color).HasMaxLength(20);

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.EngineerRegistrationNumber).HasMaxLength(100);

                entity.Property(e => e.UserName).HasMaxLength(255);
            });

            modelBuilder.Entity<LogisticsOversizeItem>(entity =>
            {
                entity.HasKey(e => e.OversizeItemId);

                entity.ToTable("Logistics_OversizeItems");

                entity.Property(e => e.OversizeItemId).HasColumnName("OversizeItemID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MinimumCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e._17to24Items)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("17to24Items");

                entity.Property(e => e._1to2Items)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("1to2Items");

                entity.Property(e => e._24plusItems)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("24PlusItems");

                entity.Property(e => e._3to8Items)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("3to8Items");

                entity.Property(e => e._9to16Items)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("9to16Items");
            });

            modelBuilder.Entity<LogisticsPallet>(entity =>
            {
                entity.HasKey(e => e.PalletsId);

                entity.ToTable("Logistics_Pallets");

                entity.Property(e => e.PalletsId).HasColumnName("PalletsID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FirstItem).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e._14plusItems)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("14PlusItems");

                entity.Property(e => e._2to14Items)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("2to14Items");
            });

            modelBuilder.Entity<LogisticsPhoto>(entity =>
            {
                entity.HasKey(e => e.PhotoId);

                entity.ToTable("Logistics_Photos");

                entity.Property(e => e.PhotoId).HasColumnName("PhotoID");

                entity.Property(e => e.Annotation).HasMaxLength(255);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.FilePath).HasMaxLength(255);

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.LinkedUuid).HasColumnName("LinkedUUID");

                entity.Property(e => e.PhotoType).HasMaxLength(50);
            });

            modelBuilder.Entity<LogisticsPostCodeZone>(entity =>
            {
                entity.HasKey(e => e.LocationId);

                entity.ToTable("Logistics_PostCodeZones");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Location).HasMaxLength(255);

                entity.Property(e => e.Postcode).HasMaxLength(255);

                entity.Property(e => e.Rdtzone)
                    .HasMaxLength(50)
                    .HasColumnName("RDTZone");
            });

            modelBuilder.Entity<LogisticsProvider>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Logistic__3214EC06797309D9")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<LogisticsSerialisedSurcharge>(entity =>
            {
                entity.HasKey(e => e.SerialisedSurchargeId);

                entity.ToTable("Logistics_SerialisedSurcharge");

                entity.Property(e => e.SerialisedSurchargeId).HasColumnName("SerialisedSurchargeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Surcharge).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<LogisticsStandardItem>(entity =>
            {
                entity.HasKey(e => e.StandardItemId);

                entity.ToTable("Logistics_StandardItems");

                entity.Property(e => e.StandardItemId).HasColumnName("StandardItemID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MinimumCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e._100plusItems)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("100PlusItems");

                entity.Property(e => e._11to50Items)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("11to50Items");

                entity.Property(e => e._1to4Items)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("1to4Items");

                entity.Property(e => e._51to100Items)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("51to100Items");

                entity.Property(e => e._5to10Items)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("5to10Items");
            });

            modelBuilder.Entity<LogisticsStatisticsDashboard>(entity =>
            {
                entity.HasKey(e => e.LogisticsStatisticsId);

                entity.ToTable("LogisticsStatisticsDashboard");

                entity.Property(e => e.LogisticsStatisticsId).HasColumnName("LogisticsStatisticsID");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.DepotName).HasMaxLength(250);

                entity.Property(e => e.JobCompletedAt).HasColumnType("datetime");

                entity.Property(e => e.JobSubmittedAt).HasColumnType("datetime");

                entity.Property(e => e.LogisticsUser).HasMaxLength(250);
            });

            modelBuilder.Entity<LogisticsSystemOrderStatus>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Logistic__3214EC067D439ABD")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<LogisticsTrashCharge>(entity =>
            {
                entity.HasKey(e => e.TrashChargeId);

                entity.ToTable("Logistics_TrashCharges");

                entity.Property(e => e.TrashChargeId).HasColumnName("TrashChargeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TrashCharge).HasColumnType("decimal(18, 3)");
            });

            modelBuilder.Entity<LogisticsWorkflow>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Logistic__3214EC0601142BA1")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<LotErasureReport>(entity =>
            {
                entity.ToTable("LotErasureReport");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AssetId)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MasterCompany>(entity =>
            {
                entity.ToTable("MasterCompany");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<MiddlewareAssetComponentConversion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Middleware_AssetComponentConversion");

                entity.Property(e => e.AssetComponentId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("AssetComponentID");

                entity.Property(e => e.ComponentType)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Ltscomponent)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("LTSComponent");

                entity.Property(e => e.Mrmcomponent)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("MRMComponent");
            });

            modelBuilder.Entity<MiddlewareAssetComponentConversion20140717>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Middleware_AssetComponentConversion_20140717");

                entity.Property(e => e.AssetComponentId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("AssetComponentID");

                entity.Property(e => e.ComponentType)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.Ltscomponent)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("LTSComponent");

                entity.Property(e => e.Mrmcomponent)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("MRMComponent");
            });

            modelBuilder.Entity<MiddlewareAssetComponentConversionTextMatch>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Middleware_AssetComponentConversion_TextMatch");

                entity.Property(e => e.ComponentType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ltscomponent)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("LTSComponent");

                entity.Property(e => e.MrmcomponentText)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("MRMComponentText");

                entity.Property(e => e.TextMatchId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<MiddlewareAssetTypeConversion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Middleware_AssetTypeConversion");

                entity.Property(e => e.AssetTypeId).ValueGeneratedOnAdd();

                entity.Property(e => e.Itadclass)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("ITADClass");

                entity.Property(e => e.ItadsubClass)
                    .HasMaxLength(50)
                    .HasColumnName("ITADSubClass");

                entity.Property(e => e.LtseqsubType)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("LTSEQSubType");

                entity.Property(e => e.Ltseqtype)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("LTSEQType");
            });

            modelBuilder.Entity<MiddlewareCommentConversion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Middleware_CommentConversion");

                entity.Property(e => e.AssetType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodeType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Acceptable values are Cosmetic or Mechanical");

                entity.Property(e => e.ConversionId).ValueGeneratedOnAdd();

                entity.Property(e => e.Itadcomments)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("ITADComments");

                entity.Property(e => e.OutputCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OutputComments).IsUnicode(false);

                entity.Property(e => e.OutputGrade)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MiddlewareCputypeConversion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Middleware_CPUTypeConversion");

                entity.HasIndex(e => e.Mrmcputype, "_dta_index_Middleware_CPUTypeConversion_8_1499152386__K2_3")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.CputypeId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("CPUTypeID");

                entity.Property(e => e.Ltscputype)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("LTSCPUType");

                entity.Property(e => e.Mrmcputype)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("MRMCPUType");
            });

            modelBuilder.Entity<MiddlewareReferenceConversion>(entity =>
            {
                entity.HasKey(e => new { e.ReferenceType, e.ReferenceDescription });

                entity.ToTable("Middleware_ReferenceConversion");

                entity.Property(e => e.ReferenceType)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenceDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenceId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ModelMan>(entity =>
            {
                entity.HasKey(e => e.TableId);

                entity.ToTable("ModelMan");

                entity.Property(e => e.TableId).HasColumnName("TableID");

                entity.Property(e => e.Grade).HasMaxLength(50);

                entity.Property(e => e.Manufacturer).HasMaxLength(200);

                entity.Property(e => e.ProductFamily).HasMaxLength(200);

                entity.Property(e => e.ProductName).HasMaxLength(400);

                entity.Property(e => e.ProductSeries).HasMaxLength(200);

                entity.Property(e => e.UpdateMethod).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.AssetClassNavigation)
                    .WithMany(p => p.ModelMen)
                    .HasForeignKey(d => d.AssetClass)
                    .HasConstraintName("FK_ModelMan_AssetTypes");
            });

            modelBuilder.Entity<MrmlotIjobIdSync>(entity =>
            {
                entity.ToTable("MRMLotIJobIdSync");

                entity.Property(e => e.LastSyncDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<NickSp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("NickSP", "Materialised");

                entity.Property(e => e.DataBearing)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Dsmethod)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("DSMethod");

                entity.Property(e => e.Hddcount).HasColumnName("HDDCount");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MrmassetClass)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("MRMAssetClass");

                entity.Property(e => e.StreamsAssetClass)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Njhloc>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("_NJHLoc");

                entity.Property(e => e.Acc).HasColumnName("Acc #");

                entity.Property(e => e.Address1).HasMaxLength(255);

                entity.Property(e => e.Address2).HasMaxLength(255);

                entity.Property(e => e.Address3).HasMaxLength(255);

                entity.Property(e => e.AddressReference).HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.Code).HasMaxLength(255);

                entity.Property(e => e.County).HasMaxLength(255);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Postcode).HasMaxLength(255);
            });

            modelBuilder.Entity<NominalCode>(entity =>
            {
                entity.HasKey(e => e.TableId);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.FullDescription).HasMaxLength(255);

                entity.Property(e => e.NominalCode1)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("NominalCode");
            });

            modelBuilder.Entity<NominalCodeChargeTypeMapping>(entity =>
            {
                entity.HasKey(e => e.TableId);

                entity.ToTable("NominalCodeChargeTypeMapping");
            });

            modelBuilder.Entity<Note>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Notes__3214EC0604E4BC85")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AttachmentFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("Attachment_FileName");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.Notes)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Note.CreatedBy->Contact");
            });

            modelBuilder.Entity<OpiaImportStaging>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("OpiaImportStaging");

                entity.Property(e => e.FileApprovedAt)
                    .HasMaxLength(99)
                    .HasColumnName("file_approved_at");

                entity.Property(e => e.FileBarcode)
                    .HasMaxLength(99)
                    .HasColumnName("file_barcode");

                entity.Property(e => e.FileBrand)
                    .HasMaxLength(99)
                    .HasColumnName("file_brand");

                entity.Property(e => e.FileClass)
                    .HasMaxLength(99)
                    .HasColumnName("file_class");

                entity.Property(e => e.FileConsignmentId)
                    .HasMaxLength(99)
                    .HasColumnName("file_consignment_id");

                entity.Property(e => e.FileId)
                    .HasMaxLength(99)
                    .HasColumnName("file_id");

                entity.Property(e => e.FileMake)
                    .HasMaxLength(99)
                    .HasColumnName("file_make");

                entity.Property(e => e.FileSerial)
                    .HasMaxLength(255)
                    .HasColumnName("file_serial");

                entity.Property(e => e.FileSpecifics)
                    .HasMaxLength(99)
                    .HasColumnName("file_specifics");
            });

            modelBuilder.Entity<OpiaImportStagingFileList>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("OpiaImportStaging_FileList");

                entity.Property(e => e.FileName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PerAssetChargeType>(entity =>
            {
                entity.ToTable("PerAssetChargeType");

                entity.Property(e => e.PerAssetChargeTypeId).HasColumnName("PerAssetChargeTypeID");

                entity.Property(e => e.PerAssetChargeTypeName)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<PermissionArea>(entity =>
            {
                entity.Property(e => e.AreaName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PortalPermission>(entity =>
            {
                entity.Property(e => e.PortalPermissionId).HasColumnName("PortalPermissionID");

                entity.Property(e => e.ChildId).HasColumnName("ChildID");

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.PortalPermissionName).HasMaxLength(250);
            });

            modelBuilder.Entity<ProcessingStatisticsDashboard>(entity =>
            {
                entity.HasKey(e => e.ProcessingStatisticsId);

                entity.ToTable("ProcessingStatisticsDashboard");

                entity.Property(e => e.ProcessingStatisticsId).HasColumnName("ProcessingStatisticsID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateCompleted).HasColumnType("datetime");

                entity.Property(e => e.ProcessingSite).HasMaxLength(250);

                entity.Property(e => e.ProcessingStaff).HasMaxLength(250);
            });

            modelBuilder.Entity<ProcessingSystemProcess>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Processi__3214EC0608B54D69")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.ProcessingSystemId).HasColumnName("ProcessingSystemID");
            });

            modelBuilder.Entity<ProcessingWorkflow>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Processi__3214EC060C85DE4D")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Projects__3214EC0610566F31")
                    .IsClustered(false);

                entity.HasIndex(e => e.Customer, "IX_Projects->Customer")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.BillingCodeNavigation)
                    .WithMany(p => p.Projects)
                    .HasForeignKey(d => d.BillingCode)
                    .HasConstraintName("FK_Project.BillingCode");

                entity.HasOne(d => d.CustomerNavigation)
                    .WithMany(p => p.Projects)
                    .HasForeignKey(d => d.Customer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Project.Customer");

                entity.HasOne(d => d.CustomerContactNavigation)
                    .WithMany(p => p.ProjectCustomerContactNavigations)
                    .HasForeignKey(d => d.CustomerContact)
                    .HasConstraintName("FK_Project.CustomerContact->Contact");

                entity.HasOne(d => d.ProjectManagerNavigation)
                    .WithMany(p => p.ProjectProjectManagerNavigations)
                    .HasForeignKey(d => d.ProjectManager)
                    .HasConstraintName("FK_Project.ProjectManager->Contact");
            });

            modelBuilder.Entity<QualityControl>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__QualityC__3214EC0614270015")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Reference, e.JobAsset, e.SalesApprovalStatus, e.OperationApprovalStatus }, "ClusteredIndex-20190729-103750")
                    .IsClustered();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DeferredDateTime).HasColumnType("datetime");

                entity.Property(e => e.IsDeferred).HasDefaultValueSql("((0))");

                entity.Property(e => e.OperationApprovalDate).HasColumnType("datetime");

                entity.Property(e => e.OperationRejectionReason).HasMaxLength(200);

                entity.Property(e => e.Reference).ValueGeneratedOnAdd();

                entity.Property(e => e.SalesApprovalDate).HasColumnType("datetime");

                entity.Property(e => e.SalesRejectionReason).HasMaxLength(200);

                entity.HasOne(d => d.JobAssetNavigation)
                    .WithMany(p => p.QualityControls)
                    .HasForeignKey(d => d.JobAsset)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QualityControl.JobAsset");

                entity.HasOne(d => d.OperationApprovalStatusNavigation)
                    .WithMany(p => p.QualityControlOperationApprovalStatusNavigations)
                    .HasForeignKey(d => d.OperationApprovalStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QualityControl.OperationApprovalStatus->ApprovalStatus");

                entity.HasOne(d => d.OperationApprovedByNavigation)
                    .WithMany(p => p.QualityControlOperationApprovedByNavigations)
                    .HasForeignKey(d => d.OperationApprovedBy)
                    .HasConstraintName("FK_QualityControl.OperationApprovedBy->Contact");

                entity.HasOne(d => d.OperationRequeueToNavigation)
                    .WithMany(p => p.QualityControlOperationRequeueToNavigations)
                    .HasForeignKey(d => d.OperationRequeueTo)
                    .HasConstraintName("FK_QualityControl.OperationRequeueTo->ProcessingSystemProcess");

                entity.HasOne(d => d.SalesApprovalStatusNavigation)
                    .WithMany(p => p.QualityControlSalesApprovalStatusNavigations)
                    .HasForeignKey(d => d.SalesApprovalStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QualityControl.SalesApprovalStatus->ApprovalStatus");

                entity.HasOne(d => d.SalesApprovedByNavigation)
                    .WithMany(p => p.QualityControlSalesApprovedByNavigations)
                    .HasForeignKey(d => d.SalesApprovedBy)
                    .HasConstraintName("FK_QualityControlQueue.SalesApprovedBy->Contact");

                entity.HasOne(d => d.SalesRequeueToNavigation)
                    .WithMany(p => p.QualityControlSalesRequeueToNavigations)
                    .HasForeignKey(d => d.SalesRequeueTo)
                    .HasConstraintName("FK_QualityControl.SalesRequeueTo->ProcessingSystemProcess");
            });

            modelBuilder.Entity<ReportParameterContextValue>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__ReportPa__3214EC0650FB042B")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ReportingAuthorisedRecyclingPartner>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_AuthorisedRecyclingPartners");

                entity.Property(e => e.StreamsCustomerGuid).HasColumnName("StreamsCustomerGUID");

                entity.Property(e => e.StreamsCustomerNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.VendorName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingBlanccoControlDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_BlanccoControlData");

                entity.Property(e => e.LastRunDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReportingBlanccoReportImportDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_Blancco_ReportImportData");

                entity.Property(e => e.DocumentId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ReportGenerated).HasDefaultValueSql("((0))");

                entity.Property(e => e.State)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Stnumber)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STNumber");

                entity.Property(e => e.Timestamp)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Uuid)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<ReportingCalendarTable>(entity =>
            {
                entity.HasKey(e => e.FullDate)
                    .HasName("PK_FullDate");

                entity.ToTable("Reporting_CalendarTable");

                entity.Property(e => e.FullDate).HasColumnType("datetime");

                entity.Property(e => e.Isoweek).HasColumnName("ISOWeek");

                entity.Property(e => e.WorkingDay)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Y')");
            });

            modelBuilder.Entity<ReportingCollectionsAndMovesGroupedAccount>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("reporting_CollectionsAndMoves_GroupedAccounts");

                entity.Property(e => e.AuditCpuspeed)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUSpeed");

                entity.Property(e => e.AuditCputype)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUType");

                entity.Property(e => e.AuditHddcapacity)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDCapacity");

                entity.Property(e => e.AuditHddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDType");

                entity.Property(e => e.AuditScreenSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_ScreenSize");

                entity.Property(e => e.AuditTotalMemory)
                    .HasMaxLength(500)
                    .HasColumnName("Audit_TotalMemory");

                entity.Property(e => e.Class).HasMaxLength(400);

                entity.Property(e => e.CollectionDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerAssetId).HasMaxLength(150);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNo).HasMaxLength(200);

                entity.Property(e => e.CustomerReference).HasMaxLength(200);

                entity.Property(e => e.DestinationLocation).HasMaxLength(200);

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit");

                entity.Property(e => e.JobRecyclingCharges).HasColumnType("numeric(2, 2)");

                entity.Property(e => e.Manufacturer).HasMaxLength(400);

                entity.Property(e => e.Model).HasMaxLength(4000);

                entity.Property(e => e.OriginatingLocation).HasMaxLength(200);

                entity.Property(e => e.PromptBillingCode)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_BillingCode");

                entity.Property(e => e.PromptCollectionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("prompt_CollectionDate");

                entity.Property(e => e.PromptCollectionNo).HasColumnName("prompt_CollectionNo");

                entity.Property(e => e.PromptCustomerGuid).HasColumnName("prompt_CustomerGUID");

                entity.Property(e => e.PromptCustomerName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("prompt_CustomerName");

                entity.Property(e => e.PromptCustomerNo)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_CustomerNo");

                entity.Property(e => e.PromptProject)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_Project");

                entity.Property(e => e.ReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.Requestor)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.ResaleChannel).HasMaxLength(150);

                entity.Property(e => e.SecurityStatus).HasMaxLength(50);

                entity.Property(e => e.SerialNumber).HasMaxLength(50);

                entity.Property(e => e.SubClass).HasMaxLength(250);

                entity.Property(e => e.TotalServices).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<ReportingCollectionsAndMovesTest>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_CollectionsAndMoves_TEST");

                entity.HasIndex(e => new { e.PromptCustomerNo, e.CustomerName, e.CollectionDate, e.AssetId }, "_dta_index_Reporting_CollectionsAndMoves_TE_8_1824725553__K6_K10_K15_K24_1_2_3_4_5_7_8_9_11_12_13_14_16_17_18_19_20_21_22_23_");

                entity.Property(e => e.AdditionalServices).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.AuditCpuspeed)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUSpeed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCputype)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditHddcapacity)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDCapacity");

                entity.Property(e => e.AuditHddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDType");

                entity.Property(e => e.AuditScreenSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_ScreenSize");

                entity.Property(e => e.AuditTotalMemory)
                    .HasMaxLength(500)
                    .HasColumnName("Audit_TotalMemory")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Class)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CollectionDate).HasColumnType("datetime");

                entity.Property(e => e.ConditionComments).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CustomerAssetId)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNo).HasMaxLength(200);

                entity.Property(e => e.CustomerReference).HasMaxLength(200);

                entity.Property(e => e.DestinationLocation).HasMaxLength(200);

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.JobRecyclingCharges).HasColumnType("numeric(2, 2)");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Model)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OriginatingLocation).HasMaxLength(200);

                entity.Property(e => e.PromptBillingCode)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_BillingCode");

                entity.Property(e => e.PromptCollectionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("prompt_CollectionDate");

                entity.Property(e => e.PromptCollectionNo).HasColumnName("prompt_CollectionNo");

                entity.Property(e => e.PromptCustomerGuid).HasColumnName("prompt_CustomerGUID");

                entity.Property(e => e.PromptCustomerName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("prompt_CustomerName");

                entity.Property(e => e.PromptCustomerNo)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_CustomerNo");

                entity.Property(e => e.PromptProject)
                    .HasMaxLength(200)
                    .HasColumnName("prompt_Project");

                entity.Property(e => e.ReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.Requestor)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.ResaleChannel)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SecurityStatus)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SerialNumber)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StandardServices).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.SubClass)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TotalServices).HasColumnType("decimal(38, 4)");
            });

            modelBuilder.Entity<ReportingConsolidatedFinancial>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_ConsolidatedFinancials");

                entity.Property(e => e.AddDateTime).HasColumnType("datetime");

                entity.Property(e => e.BillingCode).HasMaxLength(200);

                entity.Property(e => e.ChargeType)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CollectionDate).HasColumnType("date");

                entity.Property(e => e.CollectionLocation).HasMaxLength(200);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.DeploymentLocation).HasMaxLength(200);

                entity.Property(e => e.JobType)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");

                entity.Property(e => e.LotId).HasColumnName("LotID");

                entity.Property(e => e.LotStatus).HasMaxLength(250);

                entity.Property(e => e.MasterCompany).HasMaxLength(255);

                entity.Property(e => e.Poreference).HasColumnName("POReference");

                entity.Property(e => e.PrimaryContact)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.ProductionCompletedOn).HasColumnType("date");

                entity.Property(e => e.ProjectName).HasMaxLength(200);

                entity.Property(e => e.PurchaseOrderNo).HasMaxLength(200);

                entity.Property(e => e.RequestedBy)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.Rmcredit).HasColumnName("RMCredit");

                entity.Property(e => e.SettlementStage).HasMaxLength(75);

                entity.Property(e => e.Total).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.WorkflowStatus)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingConsolidatedfinancialsRevisited>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("reporting_consolidatedfinancials_revisited");

                entity.Property(e => e.AdHoc)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Ad-Hoc");

                entity.Property(e => e.AuditTrash)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Audit Trash");

                entity.Property(e => e.BillingCode).HasMaxLength(200);

                entity.Property(e => e.ClientEarnings)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Client Earnings");

                entity.Property(e => e.CollectionDate).HasColumnType("date");

                entity.Property(e => e.CollectionLocation).HasMaxLength(200);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.DeploymentLocation).HasMaxLength(200);

                entity.Property(e => e.Engineering).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.JobType)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");

                entity.Property(e => e.Logistics).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.LotId).HasColumnName("LotID");

                entity.Property(e => e.LotStatus).HasMaxLength(250);

                entity.Property(e => e.MasterCompany).HasMaxLength(255);

                entity.Property(e => e.Poreference).HasColumnName("POReference");

                entity.Property(e => e.PrimaryContact)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.ProcessingCharge)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Processing Charge");

                entity.Property(e => e.ProcessingService)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Processing Service");

                entity.Property(e => e.ProductionCompletedOn).HasColumnType("date");

                entity.Property(e => e.ProjectName).HasMaxLength(200);

                entity.Property(e => e.PurchaseOrderNo).HasMaxLength(200);

                entity.Property(e => e.Rebate).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.RequestedBy)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.Rmcredit).HasColumnName("RMCredit");

                entity.Property(e => e.SettlementStage).HasMaxLength(75);

                entity.Property(e => e.WorkflowStatus)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingConsolidatedfinancialsRevisitedX>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("reporting_consolidatedfinancials_revisitedX");

                entity.Property(e => e.AdHoc)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Ad-Hoc");

                entity.Property(e => e.AuditTrash)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Audit Trash");

                entity.Property(e => e.BillingCode).HasMaxLength(200);

                entity.Property(e => e.ClientEarnings)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Client Earnings");

                entity.Property(e => e.CollectionDate).HasColumnType("date");

                entity.Property(e => e.CollectionLocation).HasMaxLength(200);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.DeploymentLocation).HasMaxLength(200);

                entity.Property(e => e.Engineering).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.JobType)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");

                entity.Property(e => e.Logistics).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.LotId).HasColumnName("LotID");

                entity.Property(e => e.LotStatus).HasMaxLength(250);

                entity.Property(e => e.MasterCompany).HasMaxLength(255);

                entity.Property(e => e.Poreference).HasColumnName("POReference");

                entity.Property(e => e.PrimaryContact)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.ProcessingCharge)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Processing Charge");

                entity.Property(e => e.ProcessingService)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Processing Service");

                entity.Property(e => e.ProductionCompletedOn).HasColumnType("date");

                entity.Property(e => e.ProjectName).HasMaxLength(200);

                entity.Property(e => e.PurchaseOrderNo).HasMaxLength(200);

                entity.Property(e => e.Rebate).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.RequestedBy)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.Rmcredit).HasColumnName("RMCredit");

                entity.Property(e => e.SettlementStage).HasMaxLength(75);

                entity.Property(e => e.WorkflowStatus)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingCustomerAccountGrouping>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_CustomerAccountGrouping");

                entity.Property(e => e.CustomerAccountId).ValueGeneratedOnAdd();

                entity.Property(e => e.CustomerGrouping)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MrmaccountId).HasColumnName("MRMAccountId");

                entity.Property(e => e.StreamsCustomerGuid).HasColumnName("StreamsCustomerGUID");

                entity.Property(e => e.StreamsCustomerNumber).HasMaxLength(200);
            });

            modelBuilder.Entity<ReportingDellDataJobNumber>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_DellData_JobNumbers");
            });

            modelBuilder.Entity<ReportingLiveForecastReport>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_LiveForecastReport");

                entity.HasIndex(e => new { e.Reference, e.CustomerId, e.ProcessingSystemJobId, e.CurrentWorkflow, e.Expected, e.Collected, e.Received, e.ExpectedTotal, e.CollectedTotal, e.ReceivedTotal }, "ClusteredIndex-20190729-131441")
                    .IsClustered()
                    .HasFillFactor((byte)80);

                entity.Property(e => e.AssetType).HasMaxLength(200);

                entity.Property(e => e.CollectedTotal)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("Collected_Total");

                entity.Property(e => e.CompletedTotal)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("Completed_Total");

                entity.Property(e => e.CurrentWorkflow)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.ExpectedTotal)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("Expected_Total");

                entity.Property(e => e.Forename).HasMaxLength(200);

                entity.Property(e => e.NewCollected).HasColumnName("New_Collected");

                entity.Property(e => e.NewCompleted).HasColumnName("New_Completed");

                entity.Property(e => e.NewExpected).HasColumnName("New_Expected");

                entity.Property(e => e.NewReceived).HasColumnName("New_Received");

                entity.Property(e => e.NewWip).HasColumnName("New_WIP");

                entity.Property(e => e.PrimaryContact).HasMaxLength(401);

                entity.Property(e => e.ProcessingSystemJobId).HasColumnName("ProcessingSystemJobID");

                entity.Property(e => e.ReceivedTotal)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("Received_Total");

                entity.Property(e => e.ReportGrouping)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RequestedJobType)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Surname).HasMaxLength(200);

                entity.Property(e => e.Wip).HasColumnName("WIP");

                entity.Property(e => e.WipTotal)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("WIP_Total");
            });

            modelBuilder.Entity<ReportingLiveForecastReportAssetTypeValue>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_LiveForecastReport_AssetTypeValues");

                entity.Property(e => e.AssetType).HasMaxLength(400);

                entity.Property(e => e.AveragePrice).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.CustomerAssetKey).HasMaxLength(601);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.TotalServices).HasColumnType("decimal(38, 2)");
            });

            modelBuilder.Entity<ReportingLiveForecastReportDashboard>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_LiveForecastReport_Dashboard");

                entity.HasIndex(e => new { e.ThisWeekStartDate, e.NextWeekStartDate, e.ExpectedThisWeek, e.ExpectedNextWeek, e.ExpectedFuture, e.Collected, e.Received, e.Wip }, "ClusteredIndex-20190729-104336")
                    .IsClustered();

                entity.Property(e => e.ExpectedFuture).HasColumnName("Expected_Future");

                entity.Property(e => e.ExpectedNextWeek).HasColumnName("Expected_NextWeek");

                entity.Property(e => e.ExpectedThisWeek).HasColumnName("Expected_ThisWeek");

                entity.Property(e => e.NextWeekStartDate).HasColumnType("date");

                entity.Property(e => e.ThisWeekStartDate).HasColumnType("date");

                entity.Property(e => e.Wip).HasColumnName("WIP");
            });

            modelBuilder.Entity<ReportingMrmassetDataStaging>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_MRMAssetData_Staging");

                entity.HasIndex(e => new { e.StreamsSerialNo, e.InboundCustomerGuid, e.AssetType, e.OemserialNo, e.ResaleChannel, e.StatusName, e.AssetTag, e.ShippedDate }, "ClusteredIndex-20190729-104636")
                    .IsClustered();

                entity.Property(e => e.AdditionalServices).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.AssetSubType).HasMaxLength(250);

                entity.Property(e => e.AssetTag).HasMaxLength(150);

                entity.Property(e => e.AssetType).HasMaxLength(400);

                entity.Property(e => e.BiosData)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.BiosDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientEarnings).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CommissionRate).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.ConditionCode).HasMaxLength(250);

                entity.Property(e => e.FairMarketValue).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit");

                entity.Property(e => e.HddservicePerformed)
                    .HasMaxLength(50)
                    .HasColumnName("HDDServicePerformed");

                entity.Property(e => e.HddserviceStatus)
                    .HasMaxLength(50)
                    .HasColumnName("HDDServiceStatus");

                entity.Property(e => e.Hddsize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDSize");

                entity.Property(e => e.Hddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDType");

                entity.Property(e => e.InboundCustomerGuid).HasColumnName("Inbound_CustomerGUID");

                entity.Property(e => e.InboundCustomerName)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_CustomerName");

                entity.Property(e => e.InboundCustomerNumber)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_CustomerNumber");

                entity.Property(e => e.InboundMasterCompany)
                    .HasMaxLength(255)
                    .HasColumnName("Inbound_MasterCompany");

                entity.Property(e => e.InboundProductionNo).HasColumnName("Inbound_ProductionNo");

                entity.Property(e => e.InventoryStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicedDate).HasColumnType("datetime");

                entity.Property(e => e.MemoryMb)
                    .HasMaxLength(500)
                    .HasColumnName("MemoryMB");

                entity.Property(e => e.ModelDescription).HasMaxLength(4000);

                entity.Property(e => e.MonitorSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.NetOutcome).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Oem)
                    .HasMaxLength(400)
                    .HasColumnName("OEM");

                entity.Property(e => e.OemserialNo)
                    .HasMaxLength(50)
                    .HasColumnName("OEMSerialNo");

                entity.Property(e => e.PalletId).HasMaxLength(250);

                entity.Property(e => e.PartsCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Processor).HasMaxLength(1000);

                entity.Property(e => e.ReportingStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResaleChannel).HasMaxLength(150);

                entity.Property(e => e.ResalePrice).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.SalePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SalePriceUnfiltered).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SecurityStatus).HasMaxLength(50);

                entity.Property(e => e.SettlementValue).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ShippedDate).HasColumnType("datetime");

                entity.Property(e => e.Speed).HasMaxLength(1000);

                entity.Property(e => e.StandardServices).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.StatusName).HasMaxLength(55);

                entity.Property(e => e.StreamsAssetType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsDispositionCalc)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalServices).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Warehouse).HasMaxLength(250);

                entity.Property(e => e.WarehouseLocation).HasMaxLength(150);

                entity.Property(e => e.WasteTransferNo).HasMaxLength(55);
            });

            modelBuilder.Entity<ReportingMrmassetDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_MRMAssetData");

                entity.HasIndex(e => new { e.StreamsSerialNo, e.InboundCustomerGuid, e.AssetType, e.OemserialNo, e.GradeAudit, e.ResaleChannel, e.StatusName, e.AssetTag, e.ShippedDate, e.StreamsAssetType }, "ClusteredIndex-20190729-104400")
                    .IsClustered()
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => new { e.ResaleChannel, e.StreamsSerialNo, e.InboundProductionNo }, "IX_Reporting_MRMAssetData_Channel_StreamsNo_JobNo");

                entity.HasIndex(e => e.StatusName, "IX_Reporting_MRMAssetData_StatusName");

                entity.HasIndex(e => new { e.InboundProductionNo, e.InboundCustomerNumber }, "ceri_2020_02_12");

                entity.Property(e => e.AdditionalServices).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.AssetSubType).HasMaxLength(250);

                entity.Property(e => e.AssetTag).HasMaxLength(150);

                entity.Property(e => e.AssetType).HasMaxLength(400);

                entity.Property(e => e.BiosData)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.BiosDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientEarnings).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CommissionRate).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.ConditionCode).HasMaxLength(250);

                entity.Property(e => e.FairMarketValue).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit");

                entity.Property(e => e.HddservicePerformed)
                    .HasMaxLength(50)
                    .HasColumnName("HDDServicePerformed");

                entity.Property(e => e.HddserviceStatus)
                    .HasMaxLength(50)
                    .HasColumnName("HDDServiceStatus");

                entity.Property(e => e.Hddsize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDSize");

                entity.Property(e => e.Hddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDType");

                entity.Property(e => e.InboundCustomerGuid).HasColumnName("Inbound_CustomerGUID");

                entity.Property(e => e.InboundCustomerName)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_CustomerName");

                entity.Property(e => e.InboundCustomerNumber)
                    .HasMaxLength(200)
                    .HasColumnName("Inbound_CustomerNumber");

                entity.Property(e => e.InboundMasterCompany)
                    .HasMaxLength(255)
                    .HasColumnName("Inbound_MasterCompany");

                entity.Property(e => e.InboundProductionNo).HasColumnName("Inbound_ProductionNo");

                entity.Property(e => e.InventoryStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicedDate).HasColumnType("datetime");

                entity.Property(e => e.MemoryMb)
                    .HasMaxLength(500)
                    .HasColumnName("MemoryMB");

                entity.Property(e => e.ModelDescription).HasMaxLength(4000);

                entity.Property(e => e.MonitorSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.NetOutcome).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Oem)
                    .HasMaxLength(400)
                    .HasColumnName("OEM");

                entity.Property(e => e.OemserialNo)
                    .HasMaxLength(50)
                    .HasColumnName("OEMSerialNo");

                entity.Property(e => e.PalletId).HasMaxLength(250);

                entity.Property(e => e.PartsCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Processor).HasMaxLength(1000);

                entity.Property(e => e.ReportingStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResaleChannel).HasMaxLength(150);

                entity.Property(e => e.ResalePrice).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.SalePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SalePriceUnfiltered).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SecurityStatus).HasMaxLength(50);

                entity.Property(e => e.SettlementValue).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ShippedDate).HasColumnType("datetime");

                entity.Property(e => e.Speed).HasMaxLength(1000);

                entity.Property(e => e.StandardServices).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.StatusName).HasMaxLength(55);

                entity.Property(e => e.StreamsAssetType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsDispositionCalc)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalServices).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Warehouse).HasMaxLength(250);

                entity.Property(e => e.WarehouseLocation).HasMaxLength(150);

                entity.Property(e => e.WasteTransferNo).HasMaxLength(55);
            });

            modelBuilder.Entity<ReportingMrmserviceAppliedGrouping>(entity =>
            {
                entity.HasKey(e => e.ServiceAppliedDetail);

                entity.ToTable("Reporting_MRMServiceAppliedGrouping");

                entity.Property(e => e.ServiceAppliedDetail)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceAppliedGroup)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceAppliedId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ReportingOpiaAssetDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("_Reporting_OpiaAssetData");

                entity.Property(e => e.AssetType)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditedDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Coa)
                    .HasMaxLength(350)
                    .HasColumnName("COA");

                entity.Property(e => e.CollectionDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ConditionCode)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ConditionComments)
                    .IsRequired()
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CustomerName).HasMaxLength(200);

                entity.Property(e => e.CustomerReference).HasMaxLength(200);

                entity.Property(e => e.Disposition)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.EquipmentDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Hddsize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDSize");

                entity.Property(e => e.Hddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("HDDType");

                entity.Property(e => e.ManufactureDate)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MemoryMb)
                    .HasMaxLength(500)
                    .HasColumnName("MemoryMB")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ModelDescription)
                    .HasMaxLength(4000)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MonitorSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Oem)
                    .HasMaxLength(400)
                    .HasColumnName("OEM")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OemserialNo)
                    .HasMaxLength(50)
                    .HasColumnName("OEMSerialNo")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ProcessedDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Processor)
                    .HasMaxLength(1000)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Project).HasMaxLength(200);

                entity.Property(e => e.ReceivedDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RecoveryMediaPresent)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenceId)
                    .HasMaxLength(150)
                    .HasColumnName("ReferenceID")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Requestor).HasMaxLength(401);

                entity.Property(e => e.SalePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SecurityStatus)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Speed)
                    .HasMaxLength(1000)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TotalServiceCharges).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<ReportingOpiaCertStnumber>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_Opia_Cert_STNumbers");

                entity.Property(e => e.LastExecutedDate).HasColumnType("datetime");

                entity.Property(e => e.Stnumber).HasColumnName("STNumber");
            });

            modelBuilder.Entity<ReportingOpiaControlDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_OpiaControlData");

                entity.Property(e => e.AssetControlDate).HasColumnType("date");

                entity.Property(e => e.FtpcontrolDate)
                    .HasColumnType("date")
                    .HasColumnName("FTPControlDate");
            });

            modelBuilder.Entity<ReportingOpiaInvoiceDataOld>(entity =>
            {
                entity.HasKey(e => e.Stnumber)
                    .HasName("PK_Reporting_OpiaInvoiceData");

                entity.ToTable("Reporting_OpiaInvoiceData__OLD");

                entity.Property(e => e.Stnumber)
                    .ValueGeneratedNever()
                    .HasColumnName("STNumber");

                entity.Property(e => e.InvoiceNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingOpiaInvoiceDataPricingLogic>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_OpiaInvoiceData_PricingLogic");

                entity.Property(e => e.ConditionCodeS).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Desktop).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DispositionRecycled).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.FastTrackThreshold)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Laptop).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Misc).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Mobile).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Monitor).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PromotionTag)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RemarketingPercReturn).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<ReportingOpiaInvoiceDatum>(entity =>
            {
                entity.HasKey(e => e.Stnumber)
                    .HasName("PK_Reporting_OpiaInvoiceData_New");

                entity.ToTable("Reporting_OpiaInvoiceData");

                entity.Property(e => e.Stnumber)
                    .ValueGeneratedNever()
                    .HasColumnName("STNumber");

                entity.Property(e => e.InvoiceNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingOpiaXmlConditionCode>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_OpiaXML_ConditionCodes");

                entity.Property(e => e.ConditionValue)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OpiaConditionCodeId).ValueGeneratedOnAdd();

                entity.Property(e => e.OutputCodeDesktop)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("OutputCode_Desktop");

                entity.Property(e => e.OutputCodeLaptop)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("OutputCode_Laptop");

                entity.Property(e => e.OutputCodeTablet)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("OutputCode_Tablet");

                entity.Property(e => e.OutputConditionCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OutputFinalDesktop)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("OutputFinal_Desktop");

                entity.Property(e => e.OutputFinalLaptop)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("OutputFinal_Laptop");

                entity.Property(e => e.OutputFinalTablet)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("OutputFinal_Tablet");

                entity.Property(e => e.OutputPrefix)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingOpiaXmlManualExport>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_OpiaXML_ManualExport");
            });

            modelBuilder.Entity<ReportingOpiaXmlReportProject>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_OpiaXML_ReportProjects");

                entity.Property(e => e.JobNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ReportProjectId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ReportingOpiaXmldataCopy>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_OpiaXMLData_Copy");

                entity.Property(e => e.Address1).HasMaxLength(255);

                entity.Property(e => e.Address2).HasMaxLength(255);

                entity.Property(e => e.Address3).HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.ClaimApproved).HasColumnType("date");

                entity.Property(e => e.ClaimApprovedText).HasMaxLength(20);

                entity.Property(e => e.ClaimFormLink).HasMaxLength(255);

                entity.Property(e => e.ClaimId).HasColumnName("ClaimID");

                entity.Property(e => e.Country).HasMaxLength(255);

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.FirstName).HasMaxLength(255);

                entity.Property(e => e.Imei)
                    .HasMaxLength(255)
                    .HasColumnName("IMEI");

                entity.Property(e => e.ImportFileName).HasMaxLength(255);

                entity.Property(e => e.JobNumber).HasMaxLength(255);

                entity.Property(e => e.LastName).HasMaxLength(255);

                entity.Property(e => e.Make).HasMaxLength(255);

                entity.Property(e => e.Model).HasMaxLength(255);

                entity.Property(e => e.Os)
                    .HasMaxLength(255)
                    .HasColumnName("OS");

                entity.Property(e => e.Phone).HasMaxLength(255);

                entity.Property(e => e.PostalCode).HasMaxLength(255);

                entity.Property(e => e.ProcessedDate).HasColumnType("date");

                entity.Property(e => e.ProgramId).HasColumnName("ProgramID");

                entity.Property(e => e.ReceivedDate).HasColumnType("date");

                entity.Property(e => e.ReferenceId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("ReferenceID");

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e.StreamsAssetType).HasMaxLength(400);

                entity.Property(e => e.StreamsAuditedDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsCoa)
                    .HasMaxLength(350)
                    .HasColumnName("StreamsCOA");

                entity.Property(e => e.StreamsCollectionDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsConditionCode).HasMaxLength(250);

                entity.Property(e => e.StreamsConditionComments).HasMaxLength(4000);

                entity.Property(e => e.StreamsCustomerName).HasMaxLength(200);

                entity.Property(e => e.StreamsCustomerReference).HasMaxLength(200);

                entity.Property(e => e.StreamsDisposition)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsEquipmentDescription).HasMaxLength(4000);

                entity.Property(e => e.StreamsHddsize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("StreamsHDDSize");

                entity.Property(e => e.StreamsHddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("StreamsHDDType");

                entity.Property(e => e.StreamsManufactureDate)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsMemoryMb)
                    .HasMaxLength(500)
                    .HasColumnName("StreamsMemoryMB");

                entity.Property(e => e.StreamsModelDescription).HasMaxLength(4000);

                entity.Property(e => e.StreamsMonitorSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsOem)
                    .HasMaxLength(400)
                    .HasColumnName("StreamsOEM");

                entity.Property(e => e.StreamsOemserialNo)
                    .HasMaxLength(50)
                    .HasColumnName("StreamsOEMSerialNo");

                entity.Property(e => e.StreamsProcessedDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsProcessor).HasMaxLength(1000);

                entity.Property(e => e.StreamsProject).HasMaxLength(200);

                entity.Property(e => e.StreamsReceivedDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsRecoveryMediaPresent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsReferenceId)
                    .HasMaxLength(150)
                    .HasColumnName("StreamsReferenceID");

                entity.Property(e => e.StreamsRequestor).HasMaxLength(401);

                entity.Property(e => e.StreamsSalePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.StreamsSecurityStatus).HasMaxLength(50);

                entity.Property(e => e.StreamsSpeed).HasMaxLength(1000);

                entity.Property(e => e.StreamsTotalServiceCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.Type).HasMaxLength(255);
            });

            modelBuilder.Entity<ReportingOpiaXmldatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_OpiaXMLData");

                entity.Property(e => e.Address1).HasMaxLength(255);

                entity.Property(e => e.Address2).HasMaxLength(255);

                entity.Property(e => e.Address3).HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.ClaimApproved).HasColumnType("date");

                entity.Property(e => e.ClaimApprovedText).HasMaxLength(20);

                entity.Property(e => e.ClaimFormLink).HasMaxLength(255);

                entity.Property(e => e.ClaimId).HasColumnName("ClaimID");

                entity.Property(e => e.Country).HasMaxLength(255);

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.FirstName).HasMaxLength(255);

                entity.Property(e => e.Imei)
                    .HasMaxLength(255)
                    .HasColumnName("IMEI");

                entity.Property(e => e.ImportFileName).HasMaxLength(255);

                entity.Property(e => e.JobNumber).HasMaxLength(255);

                entity.Property(e => e.LastName).HasMaxLength(255);

                entity.Property(e => e.Make).HasMaxLength(255);

                entity.Property(e => e.Model).HasMaxLength(255);

                entity.Property(e => e.Os)
                    .HasMaxLength(255)
                    .HasColumnName("OS");

                entity.Property(e => e.Phone).HasMaxLength(255);

                entity.Property(e => e.PostalCode).HasMaxLength(255);

                entity.Property(e => e.ProcessedDate).HasColumnType("date");

                entity.Property(e => e.ProgramId).HasColumnName("ProgramID");

                entity.Property(e => e.ReceivedDate).HasColumnType("date");

                entity.Property(e => e.ReferenceId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("ReferenceID");

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e.StreamsAssetType).HasMaxLength(400);

                entity.Property(e => e.StreamsAuditedDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsCoa)
                    .HasMaxLength(350)
                    .HasColumnName("StreamsCOA");

                entity.Property(e => e.StreamsCollectionDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsConditionCode).HasMaxLength(250);

                entity.Property(e => e.StreamsConditionComments).HasMaxLength(4000);

                entity.Property(e => e.StreamsCustomerName).HasMaxLength(200);

                entity.Property(e => e.StreamsCustomerReference).HasMaxLength(200);

                entity.Property(e => e.StreamsDisposition)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsEquipmentDescription).HasMaxLength(4000);

                entity.Property(e => e.StreamsHddsize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("StreamsHDDSize");

                entity.Property(e => e.StreamsHddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("StreamsHDDType");

                entity.Property(e => e.StreamsManufactureDate)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsMemoryMb)
                    .HasMaxLength(500)
                    .HasColumnName("StreamsMemoryMB");

                entity.Property(e => e.StreamsModelDescription).HasMaxLength(4000);

                entity.Property(e => e.StreamsMonitorSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsOem)
                    .HasMaxLength(400)
                    .HasColumnName("StreamsOEM");

                entity.Property(e => e.StreamsOemserialNo)
                    .HasMaxLength(50)
                    .HasColumnName("StreamsOEMSerialNo");

                entity.Property(e => e.StreamsProcessedDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsProcessor).HasMaxLength(1000);

                entity.Property(e => e.StreamsProject).HasMaxLength(200);

                entity.Property(e => e.StreamsReceivedDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsRecoveryMediaPresent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsReferenceId)
                    .HasMaxLength(150)
                    .HasColumnName("StreamsReferenceID");

                entity.Property(e => e.StreamsRequestor).HasMaxLength(401);

                entity.Property(e => e.StreamsSalePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.StreamsSecurityStatus).HasMaxLength(50);

                entity.Property(e => e.StreamsSpeed).HasMaxLength(1000);

                entity.Property(e => e.StreamsTotalServiceCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.Type).HasMaxLength(255);
            });

            modelBuilder.Entity<ReportingProcedureLog>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_ProcedureLog");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.ProcedureLogId).ValueGeneratedOnAdd();

                entity.Property(e => e.ProcedureName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProcedureParameters).IsUnicode(false);

                entity.Property(e => e.StartTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReportingReportCustomerControl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_ReportCustomerControl");

                entity.Property(e => e.CustomerGuid).HasColumnName("CustomerGUID");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerNumber)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingShiinvoiceDatum>(entity =>
            {
                entity.HasKey(e => e.Stnumber);

                entity.ToTable("Reporting_SHIInvoiceData");

                entity.Property(e => e.Stnumber)
                    .ValueGeneratedNever()
                    .HasColumnName("STNumber");

                entity.Property(e => e.EmployeeBadge)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ponumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PONumber");

                entity.Property(e => e.WaybillNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingWipUnbilledPca>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_WIP_Unbilled_PCA");

                entity.Property(e => e.AdHoc)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Ad-Hoc");

                entity.Property(e => e.AuditTrash).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.ClientEarnings)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Client Earnings");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.DeploymentLocation).HasMaxLength(200);

                entity.Property(e => e.Engineering).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.JobType)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Logistics).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.LotId).HasColumnName("LotID");

                entity.Property(e => e.MasterCompany).HasMaxLength(255);

                entity.Property(e => e.PartsCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.PrimaryContact)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.ProcessingService).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.ProfitShare).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Rebate).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.Rmcredit).HasColumnName("RMCredit");

                entity.Property(e => e.SalePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ServiceLevelName).HasMaxLength(255);

                entity.Property(e => e.WorkflowStatus)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingWorkflowForecastReportCategory>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_WorkflowForecastReportCategories");

                entity.Property(e => e.ReportGrouping)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StageName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StepName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportingWorkflowStatusDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Reporting_WorkflowStatusData");

                entity.HasIndex(e => new { e.JobGuid, e.WorkflowDate, e.JobReference, e.CustomerGuid, e.SerialNumber, e.Stnumber, e.CustomerAssetTag, e.Make, e.Model, e.AssetType, e.CollectionCompleteDate, e.SourceLocation }, "ClusteredIndex-20190729-104823")
                    .IsClustered();

                entity.Property(e => e.AssetType).HasMaxLength(400);

                entity.Property(e => e.CollectionCompleteDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerAssetTag).HasMaxLength(50);

                entity.Property(e => e.CustomerGuid).HasColumnName("CustomerGUID");

                entity.Property(e => e.JobGuid).HasColumnName("JobGUID");

                entity.Property(e => e.Make)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Model)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SerialNumber).HasMaxLength(200);

                entity.Property(e => e.SourceLocation).HasMaxLength(200);

                entity.Property(e => e.Stnumber).HasColumnName("STNumber");

                entity.Property(e => e.WorkflowDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReportingWtwinvoiceDatum>(entity =>
            {
                entity.HasKey(e => e.Stnumber);

                entity.ToTable("Reporting_WTWInvoiceData");

                entity.Property(e => e.Stnumber)
                    .ValueGeneratedNever()
                    .HasColumnName("STNumber");

                entity.Property(e => e.InvoiceNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportsConsolidatedFinancialsFilteredForRebate>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("Reports_Consolidated_Financials_Filtered_for_Rebates");

                entity.Property(e => e.AdHoc)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Ad-Hoc");

                entity.Property(e => e.AuditTrash)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Audit Trash");

                entity.Property(e => e.BillingCode).HasMaxLength(200);

                entity.Property(e => e.ClientEarnings)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Client Earnings");

                entity.Property(e => e.CollectionDate).HasColumnType("date");

                entity.Property(e => e.CollectionLocation).HasMaxLength(200);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.DeploymentLocation).HasMaxLength(200);

                entity.Property(e => e.Engineering).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.JobType)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.LastUpdate).HasColumnType("datetime");

                entity.Property(e => e.Logistics).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.LotId).HasColumnName("LotID");

                entity.Property(e => e.LotStatus).HasMaxLength(250);

                entity.Property(e => e.MasterCompany).HasMaxLength(255);

                entity.Property(e => e.Poreference).HasColumnName("POReference");

                entity.Property(e => e.PrimaryContact)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.ProcessingCharge)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Processing Charge");

                entity.Property(e => e.ProcessingService)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Processing Service");

                entity.Property(e => e.ProductionCompletedOn).HasColumnType("date");

                entity.Property(e => e.ProjectName).HasMaxLength(200);

                entity.Property(e => e.PurchaseOrderNo).HasMaxLength(200);

                entity.Property(e => e.Rebate).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.RequestedBy)
                    .IsRequired()
                    .HasMaxLength(401);

                entity.Property(e => e.Rmcredit).HasColumnName("RMCredit");

                entity.Property(e => e.SettlementStage).HasMaxLength(75);

                entity.Property(e => e.WorkflowStatus)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RequestLogApi>(entity =>
            {
                entity.HasKey(e => e.RequestLogId);

                entity.ToTable("RequestLog_API");

                entity.Property(e => e.RequestLogId).HasColumnName("RequestLogID");

                entity.Property(e => e.Apikey)
                    .HasMaxLength(20)
                    .HasColumnName("APIKey");

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.ItemGrade).HasMaxLength(50);

                entity.Property(e => e.ItemValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RequestedDateTime).HasColumnType("datetime");

                entity.Property(e => e.ReturnMessage).HasMaxLength(200);
            });

            modelBuilder.Entity<SalesDivision>(entity =>
            {
                entity.ToTable("SalesDivision");

                entity.Property(e => e.Division)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Salutation>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Salutati__3214EC0617F790F9")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ServiceBundle>(entity =>
            {
                entity.Property(e => e.ServiceBundleId).HasColumnName("ServiceBundleID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceBundleName).HasMaxLength(250);
            });

            modelBuilder.Entity<ServiceBundleLineMapping>(entity =>
            {
                entity.HasKey(e => e.MappinngTableId);

                entity.ToTable("ServiceBundleLine_Mapping");

                entity.Property(e => e.MappinngTableId).HasColumnName("MappinngTableID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceBundleId).HasColumnName("ServiceBundleID");

                entity.Property(e => e.ServiceLineId).HasColumnName("ServiceLineID");

                entity.HasOne(d => d.ServiceBundle)
                    .WithMany(p => p.ServiceBundleLineMappings)
                    .HasForeignKey(d => d.ServiceBundleId)
                    .HasConstraintName("FK_ServiceBundleLine_Mapping_ServiceBundles");
            });

            modelBuilder.Entity<ServiceLevel>(entity =>
            {
                entity.ToTable("ServiceLevel");

                entity.Property(e => e.ServiceLevelId).HasColumnName("ServiceLevelID");

                entity.Property(e => e.ServiceLevelName)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ServiceLevelCharge>(entity =>
            {
                entity.Property(e => e.ServiceLevelChargeId).HasColumnName("ServiceLevelChargeID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.PerAssetChargeTypeId).HasColumnName("PerAssetChargeTypeID");

                entity.Property(e => e.ServiceLevelCharge1)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("ServiceLevelCharge");

                entity.Property(e => e.ServiceLevelId).HasColumnName("ServiceLevelID");

                entity.HasOne(d => d.PerAssetChargeType)
                    .WithMany(p => p.ServiceLevelCharges)
                    .HasForeignKey(d => d.PerAssetChargeTypeId)
                    .HasConstraintName("FK_ServiceLevelCharges_PerAssetChargeType");

                entity.HasOne(d => d.ServiceLevel)
                    .WithMany(p => p.ServiceLevelCharges)
                    .HasForeignKey(d => d.ServiceLevelId)
                    .HasConstraintName("FK_ServiceLevelCharges_ServiceLevel");
            });

            modelBuilder.Entity<ServiceLine>(entity =>
            {
                entity.Property(e => e.ServiceLineId).HasColumnName("ServiceLineID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.NominalCode).HasMaxLength(50);

                entity.Property(e => e.ServiceLineName).HasMaxLength(250);
            });

            modelBuilder.Entity<ServiceType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__ServiceT__3214EC067EC1CEDB")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Setting>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Settings__3214EC061BC821DD")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AcceptableFileTypes).IsUnicode(false);

                entity.Property(e => e.BillingPeriod).HasMaxLength(6);

                entity.Property(e => e.BlanccoApitaskEndHour).HasColumnName("BlanccoAPITaskEndHour");

                entity.Property(e => e.BlanccoApitaskStartHour).HasColumnName("BlanccoAPITaskStartHour");

                entity.Property(e => e.BlanccoApiurl)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("BlanccoAPIUrl");

                entity.Property(e => e.BlanccoReportPath)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BlanccoRequestXmlpath)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("BlanccoRequestXMLPath");

                entity.Property(e => e.LogisticsSystemApiEndpoint).HasMaxLength(200);

                entity.Property(e => e.MaxFileSizeinMb).HasColumnName("MaxFileSizeinMB");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ProcessingSystemApiEndpoint).HasMaxLength(200);

                entity.Property(e => e.ReleaseDate).HasMaxLength(50);

                entity.Property(e => e.ReleaseVersion).HasMaxLength(50);

                entity.Property(e => e.SharedFolderForFileCreate)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SlareportRefreshRate).HasColumnName("SLAReportRefreshRate");

                entity.Property(e => e.SyncJobWithMrmtime)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("SyncJobWithMRMtime");

                entity.HasOne(d => d.DefaultRedemtechLocationNavigation)
                    .WithMany(p => p.Settings)
                    .HasForeignKey(d => d.DefaultRedemtechLocation)
                    .HasConstraintName("FK_Settings.DefaultRedemtechLocation->Location");
            });

            modelBuilder.Entity<SettingsReporting>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Settings_Reporting");

                entity.Property(e => e.ColBrm)
                    .HasMaxLength(20)
                    .HasColumnName("COL_BRM");

                entity.Property(e => e.ColBrs)
                    .HasMaxLength(20)
                    .HasColumnName("COL_BRS");

                entity.Property(e => e.ColCabinet)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Cabinet");

                entity.Property(e => e.ColCrd)
                    .HasMaxLength(20)
                    .HasColumnName("COL_CRD");

                entity.Property(e => e.ColDataStorage)
                    .HasMaxLength(20)
                    .HasColumnName("COL_DataStorage");

                entity.Property(e => e.ColDesktop)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Desktop");

                entity.Property(e => e.ColDun)
                    .HasMaxLength(20)
                    .HasColumnName("COL_DUN");

                entity.Property(e => e.ColHardDriveQueue)
                    .HasMaxLength(20)
                    .HasColumnName("COL_HardDriveQueue");

                entity.Property(e => e.ColHardDrives)
                    .HasMaxLength(20)
                    .HasColumnName("COL_HardDrives");

                entity.Property(e => e.ColLaptop)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Laptop");

                entity.Property(e => e.ColMisc)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Misc");

                entity.Property(e => e.ColMobile)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Mobile");

                entity.Property(e => e.ColMonitor)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Monitor");

                entity.Property(e => e.ColNetwork)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Network");

                entity.Property(e => e.ColOther)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Other");

                entity.Property(e => e.ColPor)
                    .HasMaxLength(20)
                    .HasColumnName("COL_POR");

                entity.Property(e => e.ColPreSort)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Pre-Sort");

                entity.Property(e => e.ColPrinter)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Printer");

                entity.Property(e => e.ColProduction)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Production");

                entity.Property(e => e.ColQualityControl)
                    .HasMaxLength(20)
                    .HasColumnName("COL_QualityControl");

                entity.Property(e => e.ColReceiving)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Receiving");

                entity.Property(e => e.ColRedeployment)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Redeployment");

                entity.Property(e => e.ColResale)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Resale");

                entity.Property(e => e.ColRun)
                    .HasMaxLength(20)
                    .HasColumnName("COL_RUN");

                entity.Property(e => e.ColServer)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Server");

                entity.Property(e => e.ColStorage)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Storage");

                entity.Property(e => e.ColTapes)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Tapes");

                entity.Property(e => e.ColTelecomms)
                    .HasMaxLength(20)
                    .HasColumnName("COL_Telecomms");
            });

            modelBuilder.Entity<SettlementModel>(entity =>
            {
                entity.ToTable("SettlementModel");

                entity.Property(e => e.SettlementModelId).HasColumnName("SettlementModelID");

                entity.Property(e => e.SettlementModel1)
                    .HasMaxLength(250)
                    .HasColumnName("SettlementModel");
            });

            modelBuilder.Entity<SqlReport>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__SqlRepor__3214EC061F98B2C1")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.ReportPath)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.SqlreportSubCategoriesId).HasColumnName("SQLReportSubCategoriesId");
            });

            modelBuilder.Entity<SqlReportParameter>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__SqlRepor__3214EC06236943A5")
                    .IsClustered(false);

                entity.HasIndex(e => e.Report, "IX_SqlReportParameters->Report")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Value).HasMaxLength(200);

                entity.HasOne(d => d.ContextValueNavigation)
                    .WithMany(p => p.SqlReportParameters)
                    .HasForeignKey(d => d.ContextValue)
                    .HasConstraintName("FK_SqlReportParameter.ContextValue->ReportParameterContextValue");

                entity.HasOne(d => d.ReportNavigation)
                    .WithMany(p => p.SqlReportParameters)
                    .HasForeignKey(d => d.Report)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SqlReportParameter.Report->SqlReport");
            });

            modelBuilder.Entity<SqlReportsAccessGroup>(entity =>
            {
                entity.HasKey(e => new { e.SqlReportId, e.AccessGroupId })
                    .HasName("SqlReports_AccessGroups_PK");

                entity.ToTable("SqlReports_AccessGroups");

                entity.HasOne(d => d.AccessGroup)
                    .WithMany(p => p.SqlReportsAccessGroups)
                    .HasForeignKey(d => d.AccessGroupId)
                    .HasConstraintName("FK_SqlReports_AccessGroups.AccessGroupId");

                entity.HasOne(d => d.SqlReport)
                    .WithMany(p => p.SqlReportsAccessGroups)
                    .HasForeignKey(d => d.SqlReportId)
                    .HasConstraintName("FK_SqlReports_AccessGroups.SqlReportId");
            });

            modelBuilder.Entity<SqlReportsContactRole>(entity =>
            {
                entity.HasKey(e => new { e.SqlReportId, e.ContactRoleId })
                    .HasName("SqlReports_ContactRoles_PK");

                entity.ToTable("SqlReports_ContactRoles");

                entity.HasOne(d => d.ContactRole)
                    .WithMany(p => p.SqlReportsContactRoles)
                    .HasForeignKey(d => d.ContactRoleId)
                    .HasConstraintName("FK_SqlReports_ContactRoles.ContactRoleId");

                entity.HasOne(d => d.SqlReport)
                    .WithMany(p => p.SqlReportsContactRoles)
                    .HasForeignKey(d => d.SqlReportId)
                    .HasConstraintName("FK_SqlReports_ContactRoles.SqlReportId");
            });

            modelBuilder.Entity<SqlReportsCustomer>(entity =>
            {
                entity.HasKey(e => new { e.SqlReportId, e.CustomerId })
                    .HasName("SqlReports_Customers_PK");

                entity.ToTable("SqlReports_Customers");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.SqlReportsCustomers)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_SqlReports_Customers.CustomerId");

                entity.HasOne(d => d.SqlReport)
                    .WithMany(p => p.SqlReportsCustomers)
                    .HasForeignKey(d => d.SqlReportId)
                    .HasConstraintName("FK_SqlReports_Customers.SqlReportId");
            });

            modelBuilder.Entity<SqlreportSubCategory>(entity =>
            {
                entity.HasKey(e => e.SqlreportSubCategoriesId)
                    .HasName("SQLReportSubCategoriesId");

                entity.ToTable("SQLReportSubCategories");

                entity.Property(e => e.SqlreportSubCategoriesId).HasColumnName("SQLReportSubCategoriesId");

                entity.Property(e => e.SqlreportSubCategory1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SQLReportSubCategory");
            });

            modelBuilder.Entity<StageType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__StageTyp__3214EC062739D489")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<StandardReportType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Standard__3214EC067814D14C")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<StreamsCharge>(entity =>
            {
                entity.HasKey(e => new { e.Reference, e.AssetId });

                entity.ToTable("StreamsCharge", "Materialised");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.StreamsCharges).HasColumnType("decimal(38, 2)");
            });

            modelBuilder.Entity<StreamsMrmstatus>(entity =>
            {
                entity.ToTable("StreamsMRMStatus");

                entity.Property(e => e.Mrmorder).HasColumnName("MRMOrder");

                entity.Property(e => e.Mrmstatus)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("MRMStatus");

                entity.Property(e => e.Stage)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Step)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StreamsOrder)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TaskApiworkflowMapping>(entity =>
            {
                entity.HasKey(e => e.TaskWorkflowMappingId);

                entity.ToTable("TaskAPIWorkflowMapping");

                entity.Property(e => e.TaskWorkflowMappingId).HasColumnName("TaskWorkflowMappingID");

                entity.Property(e => e.ApiworkflowId).HasColumnName("APIWorkflowID");

                entity.Property(e => e.TaskId).HasColumnName("TaskID");

                entity.HasOne(d => d.Apiworkflow)
                    .WithMany(p => p.TaskApiworkflowMappings)
                    .HasForeignKey(d => d.ApiworkflowId)
                    .HasConstraintName("FK_TaskAPIWorkflowMapping_APIWorkflows");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskApiworkflowMappings)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("FK_TaskAPIWorkflowMapping_TaskTypes");
            });

            modelBuilder.Entity<TaskParameterMapping>(entity =>
            {
                entity.HasKey(e => e.TaskParameterId);

                entity.ToTable("TaskParameterMapping");

                entity.Property(e => e.TaskParameterId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("TaskParameterID");

                entity.Property(e => e.ParameterName).HasMaxLength(250);

                entity.Property(e => e.ParameterValue).HasMaxLength(250);

                entity.Property(e => e.TaskId).HasColumnName("TaskID");

                entity.HasOne(d => d.TaskParameter)
                    .WithOne(p => p.TaskParameterMapping)
                    .HasForeignKey<TaskParameterMapping>(d => d.TaskParameterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TaskParameterMapping_TaskTypes");
            });

            modelBuilder.Entity<TaskType>(entity =>
            {
                entity.HasKey(e => e.TaskId);

                entity.Property(e => e.TaskId).HasColumnName("TaskID");

                entity.Property(e => e.TaskName).HasMaxLength(250);
            });

            modelBuilder.Entity<TransactionNature>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Transact__3214EC062B0A656D")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<TransactionType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Transact__3214EC062EDAF651")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<VDepot>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vDepot");

                entity.Property(e => e.DepotName).HasMaxLength(250);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");
            });

            modelBuilder.Entity<VReportsAssetFinancialDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_AssetFinancialData");

                entity.Property(e => e.AdditionalServices).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CommissionRate).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.PartsCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SalePrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SalePriceUnfiltered).HasColumnType("decimal(38, 8)");

                entity.Property(e => e.ShippedDate).HasColumnType("datetime");

                entity.Property(e => e.StandardServices).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.StatusName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TotalServices).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<VReportsCertifiedAssetRecyclingReport>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_CertifiedAssetRecyclingReport");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(400)
                    .HasColumnName("Category Name")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MrmLotNumber).HasColumnName("MRM Lot Number");

                entity.Property(e => e.StreamsJobNumber).HasColumnName("Streams Job Number");

                entity.Property(e => e.TotalWeight)
                    .HasColumnType("decimal(38, 4)")
                    .HasColumnName("Total Weight");
            });

            modelBuilder.Entity<VReportsCertifiedDataSecurityReport>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_CertifiedDataSecurityReport");

                entity.Property(e => e.DataSecurityLevelAssigned)
                    .HasMaxLength(50)
                    .HasColumnName("Data Security Level Assigned")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DataSecurityLevelPerformed)
                    .HasMaxLength(50)
                    .HasColumnName("Data Security Level Performed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.HddSerialNo)
                    .HasMaxLength(150)
                    .HasColumnName("HDD Serial No.")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.IsLooseDrive)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MrmLotNumber).HasColumnName("MRM Lot Number");

                entity.Property(e => e.ParentAssetTagNo)
                    .HasMaxLength(50)
                    .HasColumnName("Parent Asset Tag No.");

                entity.Property(e => e.ParentOemSerialNo)
                    .HasMaxLength(150)
                    .HasColumnName("Parent OEM Serial No.")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StNo).HasColumnName("ST No.");

                entity.Property(e => e.StreamsJobNumber).HasColumnName("Streams Job Number");
            });

            modelBuilder.Entity<VReportsCertifiedDetailReport>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_CertifiedDetailReport");

                entity.Property(e => e.AssetClass)
                    .HasMaxLength(400)
                    .HasColumnName("Asset Class")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AssetTag)
                    .HasMaxLength(50)
                    .HasColumnName("Asset Tag");

                entity.Property(e => e.Comment).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ConditionCode)
                    .HasMaxLength(250)
                    .HasColumnName("Condition Code")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CpuArchitecture)
                    .HasMaxLength(350)
                    .HasColumnName("CPU Architecture")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CpuSpeed)
                    .HasMaxLength(350)
                    .HasColumnName("CPU Speed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FirstHddSize)
                    .HasMaxLength(350)
                    .HasColumnName("First HDD Size")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FirstHddType)
                    .HasMaxLength(350)
                    .HasColumnName("First HDD Type")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Memory)
                    .HasMaxLength(350)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ModelDescription)
                    .HasMaxLength(400)
                    .HasColumnName("Model Description")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ModelNo)
                    .HasMaxLength(100)
                    .HasColumnName("Model No.")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MonitorSize)
                    .HasMaxLength(350)
                    .HasColumnName("Monitor Size")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MonitorType)
                    .HasMaxLength(350)
                    .HasColumnName("Monitor Type")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MrmLotNumber).HasColumnName("MRM Lot Number");

                entity.Property(e => e.NoOfHdds).HasColumnName("No. of HDDs");

                entity.Property(e => e.Oem)
                    .HasMaxLength(400)
                    .HasColumnName("OEM")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OemSerialNo)
                    .HasMaxLength(200)
                    .HasColumnName("OEM Serial No.");

                entity.Property(e => e.StNo).HasColumnName("ST No.");

                entity.Property(e => e.StreamsJobNumber).HasColumnName("Streams Job Number");
            });

            modelBuilder.Entity<VReportsMasterAssetDetail>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_MasterAssetDetails");

                entity.Property(e => e.AssetDisposition).HasMaxLength(200);

                entity.Property(e => e.AssetStatus).HasMaxLength(200);

                entity.Property(e => e.AssetType)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Condition).HasMaxLength(2);

                entity.Property(e => e.Cost).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.CustomerAssetTag).HasMaxLength(50);

                entity.Property(e => e.CustomerGuid).HasColumnName("CustomerGUID");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.Manufacturer).HasMaxLength(200);

                entity.Property(e => e.MasterCompany).HasMaxLength(250);

                entity.Property(e => e.Model).HasMaxLength(200);

                entity.Property(e => e.Price).HasColumnType("numeric(27, 2)");

                entity.Property(e => e.ProcessingSystemAssetId).HasColumnName("ProcessingSystemAssetID");

                entity.Property(e => e.SerialNumber).HasMaxLength(200);

                entity.Property(e => e.Stnumber).HasColumnName("STNumber");
            });

            modelBuilder.Entity<VReportsMasterJobDetail>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_MasterJobDetails");

                entity.Property(e => e.ActualCollectionDate).HasColumnType("datetime");

                entity.Property(e => e.ActualReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.BillingCode).HasMaxLength(200);

                entity.Property(e => e.CancelledAt).HasColumnType("datetime");

                entity.Property(e => e.CancelledByForename).HasMaxLength(200);

                entity.Property(e => e.CancelledBySurname).HasMaxLength(200);

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.CompletedByForename).HasMaxLength(200);

                entity.Property(e => e.CompletedBySurname).HasMaxLength(200);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedByForename)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CreatedBySurname)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerApprovalStatus)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerApprovedByForename).HasMaxLength(200);

                entity.Property(e => e.CustomerApprovedBySurname).HasMaxLength(200);

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CustomerNumber).HasMaxLength(200);

                entity.Property(e => e.CustomerPrintName).HasMaxLength(2000);

                entity.Property(e => e.CustomerSignatureFileName)
                    .HasMaxLength(1500)
                    .HasColumnName("CustomerSignature_FileName");

                entity.Property(e => e.CustomizableField1).HasMaxLength(200);

                entity.Property(e => e.CustomizableField2).HasMaxLength(200);

                entity.Property(e => e.CustomizableField3).HasMaxLength(200);

                entity.Property(e => e.DestinationAddressLogisticsSystemId).HasMaxLength(200);

                entity.Property(e => e.DestinationAddressReference).HasMaxLength(200);

                entity.Property(e => e.DestinationContactForename).HasMaxLength(200);

                entity.Property(e => e.DestinationContactSurname).HasMaxLength(200);

                entity.Property(e => e.InvoicedAt).HasColumnType("datetime");

                entity.Property(e => e.InvoicedByForename).HasMaxLength(200);

                entity.Property(e => e.InvoicedBySurname).HasMaxLength(200);

                entity.Property(e => e.LogisticsApprovalStatus)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.LogisticsApprovedByForename).HasMaxLength(200);

                entity.Property(e => e.LogisticsApprovedBySurname).HasMaxLength(200);

                entity.Property(e => e.LogisticsCollectionComplete).HasColumnType("datetime");

                entity.Property(e => e.LogisticsJobType).HasMaxLength(200);

                entity.Property(e => e.PalletsLocation).HasMaxLength(200);

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(200)
                    .HasColumnName("PONumber");

                entity.Property(e => e.ProcessingJobType).HasMaxLength(200);

                entity.Property(e => e.ProcessingSystemJobId).HasColumnName("ProcessingSystemJobID");

                entity.Property(e => e.ProjectName).HasMaxLength(200);

                entity.Property(e => e.QuotedPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ReadyToInvoiceAt).HasColumnType("datetime");

                entity.Property(e => e.ReadyToInvoiceByForename).HasMaxLength(200);

                entity.Property(e => e.ReadyToInvoiceBySurname).HasMaxLength(200);

                entity.Property(e => e.RequestedByForename)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.RequestedBySurname)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.RequestedDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.RequestedJobType)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.SalesApprovalStatus)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.SalesApprovedByForename).HasMaxLength(200);

                entity.Property(e => e.SalesApprovedBySurname).HasMaxLength(200);

                entity.Property(e => e.SiteVisitScheduledAt).HasColumnType("datetime");

                entity.Property(e => e.SourceAddressLogisticsSystemId).HasMaxLength(200);

                entity.Property(e => e.SourceAddressPostCode).HasMaxLength(200);

                entity.Property(e => e.SourceAddressReference).HasMaxLength(200);

                entity.Property(e => e.SourceContactForename).HasMaxLength(200);

                entity.Property(e => e.SourceContactSurname).HasMaxLength(200);

                entity.Property(e => e.SubmittedAt).HasColumnType("datetime");

                entity.Property(e => e.SubmittedByForename).HasMaxLength(200);

                entity.Property(e => e.SubmittedBySurname).HasMaxLength(200);

                entity.Property(e => e.VehicleNumber).HasMaxLength(200);
            });

            modelBuilder.Entity<VReportsMasterMrminventoryDetail>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_MasterMRMInventoryDetails");

                entity.Property(e => e.AccountName)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AccountingRefNum)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AdditionalTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.AssetType)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCompleteBy)
                    .HasMaxLength(256)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCompletedOn).HasColumnType("datetime");

                entity.Property(e => e.AuditCosmeticDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCpuspeed)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUSpeed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCputype)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditFunctionalDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditHddcapacity)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDCapacity");

                entity.Property(e => e.AuditHddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDType");

                entity.Property(e => e.AuditOperatingSystem)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_OperatingSystem");

                entity.Property(e => e.AuditScreenSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_ScreenSize");

                entity.Property(e => e.AuditScreenType)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("Audit_ScreenType");

                entity.Property(e => e.AuditTotalMemory)
                    .HasMaxLength(500)
                    .HasColumnName("Audit_TotalMemory")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.BiosData)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Class)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerAssetId)
                    .HasMaxLength(256)
                    .HasColumnName("CustomerAssetID")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CustomerRefNum1)
                    .HasMaxLength(256)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CustomerRefNum2)
                    .HasMaxLength(256)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CustomerRefNum3)
                    .HasMaxLength(256)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FairMarketValue).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.FinalCpuspeed)
                    .HasMaxLength(350)
                    .HasColumnName("Final_CPUSpeed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalCputype)
                    .HasMaxLength(350)
                    .HasColumnName("Final_CPUType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalHddcapacity)
                    .HasMaxLength(350)
                    .HasColumnName("Final_HDDCapacity")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalHddtype)
                    .HasMaxLength(350)
                    .HasColumnName("Final_HDDType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalScreenSize)
                    .HasMaxLength(350)
                    .HasColumnName("Final_ScreenSize")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalScreenType)
                    .HasMaxLength(350)
                    .HasColumnName("Final_ScreenType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalTotalMemory)
                    .HasMaxLength(350)
                    .HasColumnName("Final_TotalMemory")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinancialTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.Grade)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Model)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ModelNumber)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OrigAccount)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OrigLocation)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PalletId)
                    .HasMaxLength(250)
                    .HasColumnName("PalletID")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ProcurementMethod)
                    .HasMaxLength(21)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PurchaseTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.QccompleteDate)
                    .HasColumnType("datetime")
                    .HasColumnName("QCCompleteDate");

                entity.Property(e => e.RemarketingPercentage).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.ResaleChannel)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ResalePrice).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.SerialNumber)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ServiceStatus)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SettlementValue).HasColumnType("money");

                entity.Property(e => e.StandardTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.StatusModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StatusName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubClass)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TotalUpgradeValue).HasColumnType("money");

                entity.Property(e => e.TrueModelDescription)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TruePartNumber)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UpgradeCosmeticsDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UpgradeDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UpgradeFunctionalDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Warehouse)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.WarehouseLocation)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Weight).HasColumnType("decimal(18, 4)");
            });

            modelBuilder.Entity<VReportsMasterMrminventoryDetailsMinimal>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_MasterMRMInventoryDetails_Minimal");

                entity.Property(e => e.AuditCpuspeed)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUSpeed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCputype)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditHddcapacity)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDCapacity");

                entity.Property(e => e.AuditHddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDType");

                entity.Property(e => e.AuditScreenSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_ScreenSize");

                entity.Property(e => e.AuditTotalMemory)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Audit_TotalMemory");

                entity.Property(e => e.Class)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CustomerAssetNumber)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Grade)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ResaleChannel)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SerialNumber)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StatusName)
                    .HasMaxLength(55)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TrueModelDescription)
                    .HasMaxLength(4000)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<VReportsMasterMrminventoryDetailsNew>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_MasterMRMInventoryDetails_NEW");

                entity.Property(e => e.AccountName)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AccountingRefNum)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AdditionalTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.AssetType)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCompleteBy)
                    .HasMaxLength(256)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCompletedOn).HasColumnType("datetime");

                entity.Property(e => e.AuditCosmeticDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCpuspeed)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUSpeed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCputype)
                    .HasMaxLength(1000)
                    .HasColumnName("Audit_CPUType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditFunctionalDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditHddcapacity)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDCapacity");

                entity.Property(e => e.AuditHddtype)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_HDDType");

                entity.Property(e => e.AuditOperatingSystem)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_OperatingSystem");

                entity.Property(e => e.AuditScreenSize)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Audit_ScreenSize");

                entity.Property(e => e.AuditScreenType)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("Audit_ScreenType");

                entity.Property(e => e.AuditTotalMemory)
                    .HasMaxLength(500)
                    .HasColumnName("Audit_TotalMemory")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Class)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerAssetId)
                    .HasMaxLength(256)
                    .HasColumnName("CustomerAssetID")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FairMarketValue).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.FinalCpuspeed)
                    .HasMaxLength(350)
                    .HasColumnName("Final_CPUSpeed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalCputype)
                    .HasMaxLength(350)
                    .HasColumnName("Final_CPUType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalHddcapacity)
                    .HasMaxLength(350)
                    .HasColumnName("Final_HDDCapacity")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalHddtype)
                    .HasMaxLength(350)
                    .HasColumnName("Final_HDDType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalScreenSize)
                    .HasMaxLength(350)
                    .HasColumnName("Final_ScreenSize")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalScreenType)
                    .HasMaxLength(350)
                    .HasColumnName("Final_ScreenType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalTotalMemory)
                    .HasMaxLength(350)
                    .HasColumnName("Final_TotalMemory")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinancialTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.Grade)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Model)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ModelNumber)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OrigAccount)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OrigLocation)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PalletId)
                    .HasMaxLength(250)
                    .HasColumnName("PalletID")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ProcurementMethod)
                    .HasMaxLength(21)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PurchaseTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.QccompleteDate)
                    .HasColumnType("datetime")
                    .HasColumnName("QCCompleteDate");

                entity.Property(e => e.RemarketingPercentage).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.ResaleChannel)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ResalePrice).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.SerialNumber)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ServiceStatus)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SettlementValue).HasColumnType("money");

                entity.Property(e => e.StandardTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.StatusModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StatusName)
                    .HasMaxLength(55)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubClass)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TotalUpgradeValue).HasColumnType("money");

                entity.Property(e => e.TrueModelDescription)
                    .HasMaxLength(4000)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TruePartNumber)
                    .HasMaxLength(551)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UpgradeCosmeticsDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UpgradeDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UpgradeFunctionalDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Warehouse)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.WarehouseLocation)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Weight).HasColumnType("decimal(18, 4)");
            });

            modelBuilder.Entity<VReportsMasterMrminventoryDetailsOld>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_MasterMRMInventoryDetails_OLD");

                entity.Property(e => e.AccountName)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AccountingRefNum)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AdditionalTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.AssetType)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCompleteBy)
                    .HasMaxLength(256)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCompletedOn).HasColumnType("datetime");

                entity.Property(e => e.AuditCosmeticDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCpuspeed)
                    .HasMaxLength(350)
                    .HasColumnName("Audit_CPUSpeed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditCputype)
                    .HasMaxLength(350)
                    .HasColumnName("Audit_CPUType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditFunctionalDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditHddcapacity)
                    .HasMaxLength(350)
                    .HasColumnName("Audit_HDDCapacity")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditHddtype)
                    .HasMaxLength(350)
                    .HasColumnName("Audit_HDDType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditOperatingSystem)
                    .HasMaxLength(350)
                    .HasColumnName("Audit_OperatingSystem")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditScreenSize)
                    .HasMaxLength(350)
                    .HasColumnName("Audit_ScreenSize")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditScreenType)
                    .HasMaxLength(350)
                    .HasColumnName("Audit_ScreenType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.AuditTotalMemory)
                    .HasMaxLength(350)
                    .HasColumnName("Audit_TotalMemory")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Class)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerAssetId)
                    .HasMaxLength(256)
                    .HasColumnName("CustomerAssetID")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FairMarketValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.FinalCpuspeed)
                    .HasMaxLength(350)
                    .HasColumnName("Final_CPUSpeed")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalCputype)
                    .HasMaxLength(350)
                    .HasColumnName("Final_CPUType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalHddcapacity)
                    .HasMaxLength(350)
                    .HasColumnName("Final_HDDCapacity")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalHddtype)
                    .HasMaxLength(350)
                    .HasColumnName("Final_HDDType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalScreenSize)
                    .HasMaxLength(350)
                    .HasColumnName("Final_ScreenSize")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalScreenType)
                    .HasMaxLength(350)
                    .HasColumnName("Final_ScreenType")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinalTotalMemory)
                    .HasMaxLength(350)
                    .HasColumnName("Final_TotalMemory")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FinancialTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.Grade)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.GradeAudit)
                    .HasMaxLength(250)
                    .HasColumnName("Grade_Audit")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Model)
                    .HasMaxLength(400)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ModelNumber)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OrigAccount)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OrigLocation)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PalletId)
                    .HasMaxLength(250)
                    .HasColumnName("PalletID")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ProcurementMethod)
                    .HasMaxLength(21)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.QccompleteDate)
                    .HasColumnType("datetime")
                    .HasColumnName("QCCompleteDate");

                entity.Property(e => e.RemarketingPercentage).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.ResaleChannel)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ResalePrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SerialNumber)
                    .HasMaxLength(256)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ServiceStatus)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SettlementValue).HasColumnType("money");

                entity.Property(e => e.StandardTotal).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.StatusModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StatusName)
                    .HasMaxLength(55)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubClass)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TotalUpgradeValue).HasColumnType("money");

                entity.Property(e => e.TrueModelDescription)
                    .HasMaxLength(4000)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TruePartNumber)
                    .HasMaxLength(551)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UpgradeCosmeticsDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UpgradeDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UpgradeFunctionalDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Warehouse)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.WarehouseLocation)
                    .HasMaxLength(150)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<VReportsMiddlewareSourceDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("_vReports_MiddlewareSourceData");

                entity.Property(e => e.CosmeticDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FunctionalDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAbatdetail)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasColumnName("MiscABATDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAcacdetail)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("MiscACACDetail");

                entity.Property(e => e.MiscAccddetail)
                    .HasMaxLength(350)
                    .HasColumnName("MiscACCDDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAcsndetail)
                    .HasMaxLength(256)
                    .HasColumnName("MiscACSNDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAfdddetail)
                    .IsRequired()
                    .HasMaxLength(1)
                    .HasColumnName("MiscAFDDDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAhdddetail)
                    .HasMaxLength(350)
                    .HasColumnName("MiscAHDDDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAnicdetail)
                    .HasMaxLength(350)
                    .HasColumnName("MiscANICDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAramdetail)
                    .HasMaxLength(350)
                    .HasColumnName("MiscARAMDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAsctdetail)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("MiscASCTDetail");

                entity.Property(e => e.MiscAusbdetail)
                    .IsRequired()
                    .HasMaxLength(4000)
                    .IsUnicode(false)
                    .HasColumnName("MiscAUSBDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscButydetail)
                    .HasMaxLength(250)
                    .HasColumnName("MiscBUTYDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscJobndetail)
                    .HasMaxLength(200)
                    .HasColumnName("MiscJOBNDetail");

                entity.Property(e => e.MiscKeybdetail)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("MiscKEYBDetail");

                entity.Property(e => e.MiscLicedetail)
                    .HasMaxLength(350)
                    .HasColumnName("MiscLICEDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscManudetail)
                    .HasMaxLength(400)
                    .HasColumnName("MiscMANUDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscMfpndetail)
                    .HasMaxLength(551)
                    .HasColumnName("MiscMFPNDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscModemdetail)
                    .HasMaxLength(350)
                    .HasColumnName("MiscMODEMDetail");

                entity.Property(e => e.MiscModldetail)
                    .HasMaxLength(4000)
                    .HasColumnName("MiscMODLDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscPlatdetail)
                    .HasMaxLength(400)
                    .HasColumnName("MiscPLATDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscProcdetail)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("MiscPROCDetail");

                entity.Property(e => e.MiscScszdetail)
                    .HasMaxLength(350)
                    .HasColumnName("MiscSCSZDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscSprndetail).HasColumnName("MiscSPRNDetail");

                entity.Property(e => e.MrmlogInfo)
                    .HasMaxLength(8000)
                    .IsUnicode(false)
                    .HasColumnName("MRMLogInfo");
            });

            modelBuilder.Entity<VReportsMiddlewareSourceDatum1>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_MiddlewareSourceData");

                entity.Property(e => e.AuditDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CosmeticDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FunctionalDescription).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAbatdetail)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("MiscABATDetail");

                entity.Property(e => e.MiscAcacdetail)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("MiscACACDetail");

                entity.Property(e => e.MiscAccddetail)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("MiscACCDDetail");

                entity.Property(e => e.MiscAcsndetail)
                    .HasMaxLength(50)
                    .HasColumnName("MiscACSNDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAfdddetail)
                    .IsRequired()
                    .HasMaxLength(1)
                    .HasColumnName("MiscAFDDDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAhdddetail)
                    .IsRequired()
                    .HasMaxLength(8000)
                    .IsUnicode(false)
                    .HasColumnName("MiscAHDDDetail");

                entity.Property(e => e.MiscAnicdetail)
                    .HasMaxLength(350)
                    .HasColumnName("MiscANICDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscAramdetail)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("MiscARAMDetail");

                entity.Property(e => e.MiscAsctdetail)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("MiscASCTDetail");

                entity.Property(e => e.MiscAusbdetail).HasColumnName("MiscAUSBDetail");

                entity.Property(e => e.MiscButydetail)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("MiscBUTYDetail");

                entity.Property(e => e.MiscJobndetail)
                    .HasMaxLength(200)
                    .HasColumnName("MiscJOBNDetail");

                entity.Property(e => e.MiscKeybdetail)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("MiscKEYBDetail");

                entity.Property(e => e.MiscLicedetail)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("MiscLICEDetail");

                entity.Property(e => e.MiscManudetail)
                    .HasMaxLength(400)
                    .HasColumnName("MiscMANUDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscMfpndetail)
                    .HasMaxLength(1000)
                    .HasColumnName("MiscMFPNDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscModemdetail)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("MiscMODEMDetail");

                entity.Property(e => e.MiscModldetail)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("MiscMODLDetail");

                entity.Property(e => e.MiscPlatdetail)
                    .HasMaxLength(400)
                    .HasColumnName("MiscPLATDetail")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MiscProcdetail)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("MiscPROCDetail");

                entity.Property(e => e.MiscScszdetail)
                    .HasMaxLength(8000)
                    .IsUnicode(false)
                    .HasColumnName("MiscSCSZDetail");

                entity.Property(e => e.MiscSprndetail).HasColumnName("MiscSPRNDetail");

                entity.Property(e => e.Mrmgrade)
                    .HasMaxLength(250)
                    .HasColumnName("MRMGrade")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MrmlogInfo)
                    .HasMaxLength(8000)
                    .IsUnicode(false)
                    .HasColumnName("MRMLogInfo");

                entity.Property(e => e.MrmlogInfoNotes)
                    .HasMaxLength(8000)
                    .IsUnicode(false)
                    .HasColumnName("MRMLogInfoNotes");

                entity.Property(e => e.SecurityFailureReason)
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VReportsMrmtrashDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_MRMTrashData");

                entity.Property(e => e.ClassName)
                    .HasMaxLength(75)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.NetWeight).HasColumnType("decimal(38, 4)");
            });

            modelBuilder.Entity<VReportsSlareportingInProgressDraftWip>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_SLAReporting_InProgress_DraftWIP");

                entity.Property(e => e.ClientSla).HasColumnName("ClientSLA");

                entity.Property(e => e.CollectionDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CustomerNumber)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DateToBeCompleted).HasColumnType("datetime");

                entity.Property(e => e.DestinationDepot)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.LogisticsType).HasMaxLength(200);

                entity.Property(e => e.ReceiveDock)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ReceivingDate).HasColumnType("datetime");

                entity.Property(e => e.RecoveryStatus)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Rtcollected).HasColumnName("RTCollected");

                entity.Property(e => e.Rtexpected).HasColumnName("RTExpected");

                entity.Property(e => e.RtinQc).HasColumnName("RTInQC");

                entity.Property(e => e.Rtremaining).HasColumnName("RTRemaining");

                entity.Property(e => e.Rttotal).HasColumnName("RTTotal");

                entity.Property(e => e.SlaonTarget)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("SLAOnTarget");

                entity.Property(e => e.SourceAddr1)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.SourceAddr2).HasMaxLength(200);

                entity.Property(e => e.SourceCity)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.SourceLocName)
                    .IsRequired()
                    .HasMaxLength(403);

                entity.Property(e => e.SourceLocnRef)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Today).HasColumnType("date");
            });

            modelBuilder.Entity<VReportsSlareportingInProgressDraftWipDetV3>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vReports_SLAReporting_InProgress_DraftWIP_DetV3");

                entity.Property(e => e.ClientSla).HasColumnName("ClientSLA");

                entity.Property(e => e.CollectionDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CustomerNumber)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DateToBeCompleted).HasColumnType("datetime");

                entity.Property(e => e.DestinationDepot)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.LogisticsType).HasMaxLength(200);

                entity.Property(e => e.ReceiveDock)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ReceivingDate).HasColumnType("datetime");

                entity.Property(e => e.RecoveryStatus)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.RtcolCab).HasColumnName("RTCol_Cab");

                entity.Property(e => e.RtcolDatStr).HasColumnName("RTCol_DatStr");

                entity.Property(e => e.RtcolDsk).HasColumnName("RTCol_Dsk");

                entity.Property(e => e.RtcolLap).HasColumnName("RTCol_Lap");

                entity.Property(e => e.RtcolMob).HasColumnName("RTCol_Mob");

                entity.Property(e => e.RtcolMon).HasColumnName("RTCol_Mon");

                entity.Property(e => e.RtcolMsc).HasColumnName("RTCol_Msc");

                entity.Property(e => e.RtcolNet).HasColumnName("RTCol_Net");

                entity.Property(e => e.RtcolOth).HasColumnName("RTCol_Oth");

                entity.Property(e => e.RtcolPrt).HasColumnName("RTCol_Prt");

                entity.Property(e => e.RtcolStr).HasColumnName("RTCol_Str");

                entity.Property(e => e.RtcolSvr).HasColumnName("RTCol_Svr");

                entity.Property(e => e.RtcolTel).HasColumnName("RTCol_Tel");

                entity.Property(e => e.Rtcollected).HasColumnName("RTCollected");

                entity.Property(e => e.RtexpCab).HasColumnName("RTExp_Cab");

                entity.Property(e => e.RtexpDatStr).HasColumnName("RTExp_DatStr");

                entity.Property(e => e.RtexpDsk).HasColumnName("RTExp_Dsk");

                entity.Property(e => e.RtexpLap).HasColumnName("RTExp_Lap");

                entity.Property(e => e.RtexpMob).HasColumnName("RTExp_Mob");

                entity.Property(e => e.RtexpMon).HasColumnName("RTExp_Mon");

                entity.Property(e => e.RtexpMsc).HasColumnName("RTExp_Msc");

                entity.Property(e => e.RtexpNet).HasColumnName("RTExp_Net");

                entity.Property(e => e.RtexpOth).HasColumnName("RTExp_Oth");

                entity.Property(e => e.RtexpPrt).HasColumnName("RTExp_Prt");

                entity.Property(e => e.RtexpStr).HasColumnName("RTExp_Str");

                entity.Property(e => e.RtexpSvr).HasColumnName("RTExp_Svr");

                entity.Property(e => e.RtexpTel).HasColumnName("RTExp_Tel");

                entity.Property(e => e.Rtexpected).HasColumnName("RTExpected");

                entity.Property(e => e.RtinQc).HasColumnName("RTInQC");

                entity.Property(e => e.RtremCab).HasColumnName("RTRem_Cab");

                entity.Property(e => e.RtremDatStr).HasColumnName("RTRem_DatStr");

                entity.Property(e => e.RtremDsk).HasColumnName("RTRem_Dsk");

                entity.Property(e => e.RtremLap).HasColumnName("RTRem_Lap");

                entity.Property(e => e.RtremMob).HasColumnName("RTRem_Mob");

                entity.Property(e => e.RtremMon).HasColumnName("RTRem_Mon");

                entity.Property(e => e.RtremMscOther).HasColumnName("RTRem_MscOther");

                entity.Property(e => e.RtremNet).HasColumnName("RTRem_Net");

                entity.Property(e => e.RtremPrt).HasColumnName("RTRem_Prt");

                entity.Property(e => e.RtremStr).HasColumnName("RTRem_Str");

                entity.Property(e => e.RtremSvr).HasColumnName("RTRem_Svr");

                entity.Property(e => e.RtremTel).HasColumnName("RTRem_Tel");

                entity.Property(e => e.Rtremaining).HasColumnName("RTRemaining");

                entity.Property(e => e.Rttotal).HasColumnName("RTTotal");

                entity.Property(e => e.SlaonTarget)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("SLAOnTarget");

                entity.Property(e => e.SourceAddr1)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.SourceAddr2).HasMaxLength(200);

                entity.Property(e => e.SourceCity)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.SourceLocName)
                    .IsRequired()
                    .HasMaxLength(403);

                entity.Property(e => e.SourceLocnRef)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Today).HasColumnType("date");
            });

            modelBuilder.Entity<VarianceReason>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<Vatcode>(entity =>
            {
                entity.HasKey(e => e.TableId)
                    .HasName("PK_VATCode");

                entity.ToTable("VATCodes");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Vatcode1)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("VATCode");
            });

            modelBuilder.Entity<Vatrate>(entity =>
            {
                entity.HasKey(e => e.TableId);

                entity.ToTable("VATRates");

                entity.Property(e => e.TableId).HasColumnName("TableID");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.Vatcode)
                    .HasMaxLength(50)
                    .HasColumnName("VATCode");

                entity.Property(e => e.Vatdescription)
                    .HasMaxLength(250)
                    .HasColumnName("VATDescription");

                entity.Property(e => e.Vatrate1)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("VATRate");
            });

            modelBuilder.Entity<VwAwaitingInvoicingQueue>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_AwaitingInvoicingQueue");

                entity.Property(e => e.AccountingBranch).HasMaxLength(250);

                entity.Property(e => e.AddressReference).HasMaxLength(200);

                entity.Property(e => e.BillingCode).HasMaxLength(200);

                entity.Property(e => e.BillingCodeId).HasColumnName("BillingCodeID");

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.Contact).HasMaxLength(401);

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.Customer).HasMaxLength(200);

                entity.Property(e => e.CustomerDefaultEntity).HasMaxLength(200);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DepotSettingsId).HasColumnName("DepotSettingsID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.JobStatus)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.JobType).HasMaxLength(200);

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(200)
                    .HasColumnName("PONumber");

                entity.Property(e => e.Project).HasMaxLength(200);

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceLevelId).HasColumnName("ServiceLevelID");

                entity.Property(e => e.StageName)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<VwDownloadCollection>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_DownloadCollection");

                entity.Property(e => e.Agent).HasMaxLength(200);

                entity.Property(e => e.Company).HasMaxLength(200);

                entity.Property(e => e.JobType).HasMaxLength(200);

                entity.Property(e => e.Phone1).HasMaxLength(200);

                entity.Property(e => e.Phone2).HasMaxLength(200);

                entity.Property(e => e.PrimaryContact).HasMaxLength(402);

                entity.Property(e => e.ScheduledPickUpDate).HasColumnType("datetime");

                entity.Property(e => e.SourceAddress)
                    .HasMaxLength(1412)
                    .HasColumnName("Source_Address");
            });

            modelBuilder.Entity<VwInventory>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_Inventory");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<VwInventorySerialized>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_Inventory_Serialized");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<VwInvoiceApprovalQueue>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_InvoiceApprovalQueue");

                entity.Property(e => e.AccountingBranch).HasMaxLength(250);

                entity.Property(e => e.AddressReference).HasMaxLength(200);

                entity.Property(e => e.BillingCode).HasMaxLength(200);

                entity.Property(e => e.BillingCodeId).HasColumnName("BillingCodeID");

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.Contact).HasMaxLength(401);

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.Customer).HasMaxLength(200);

                entity.Property(e => e.CustomerDefaultEntity).HasMaxLength(200);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DepotSettingsId).HasColumnName("DepotSettingsID");

                entity.Property(e => e.JobId).HasColumnName("JobID");

                entity.Property(e => e.JobStatus).HasMaxLength(200);

                entity.Property(e => e.JobType).HasMaxLength(200);

                entity.Property(e => e.Ponumber)
                    .HasMaxLength(200)
                    .HasColumnName("PONumber");

                entity.Property(e => e.Project).HasMaxLength(200);

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceLevelId).HasColumnName("ServiceLevelID");

                entity.Property(e => e.StageName)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<WorkflowFinishStage>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Workflow__3214EC062EDAF651")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.WorkflowFinishStage)
                    .HasForeignKey<WorkflowFinishStage>(d => d.Id)
                    .HasConstraintName("FK_WorkflowFinishStage.Id->WorkflowStage");
            });

            modelBuilder.Entity<WorkflowLogisticsInhouseStage>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Workflow__3214EC0632AB8735")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.HubLocationNavigation)
                    .WithMany(p => p.WorkflowLogisticsInhouseStages)
                    .HasForeignKey(d => d.HubLocation)
                    .HasConstraintName("FK_WorkflowLogisticsInhouseStage.HubLocation->Location");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.WorkflowLogisticsInhouseStage)
                    .HasForeignKey<WorkflowLogisticsInhouseStage>(d => d.Id)
                    .HasConstraintName("FK_WorkflowLogisticsInhouseStage.Id->WorkflowStage");
            });

            modelBuilder.Entity<WorkflowLogisticsThirdpartyStage>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Workflow__3214EC06367C1819")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.WorkflowLogisticsThirdpartyStage)
                    .HasForeignKey<WorkflowLogisticsThirdpartyStage>(d => d.Id)
                    .HasConstraintName("FK_WorkflowLogisticsThirdpartyStage.Id->WorkflowStage");
            });

            modelBuilder.Entity<WorkflowProcessingInboundStage>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Workflow__3214EC063A4CA8FD")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.WorkflowProcessingInboundStage)
                    .HasForeignKey<WorkflowProcessingInboundStage>(d => d.Id)
                    .HasConstraintName("FK_WorkflowProcessingInboundStage.Id->WorkflowStage");
            });

            modelBuilder.Entity<WorkflowProcessingOutboundStage>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Workflow__3214EC063E1D39E1")
                    .IsClustered(false);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.WorkflowProcessingOutboundStage)
                    .HasForeignKey<WorkflowProcessingOutboundStage>(d => d.Id)
                    .HasConstraintName("FK_WorkflowProcessingOutboundStage.Id->WorkflowStage");
            });

            modelBuilder.Entity<WorkflowStage>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Workflow__3214EC0641EDCAC5")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.StageType, e.Job, e.Order, e.Status }, "ClusteredIndex-20190729-105042")
                    .IsClustered();

                entity.HasIndex(e => e.Job, "IX_WorkflowStages->Job")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.JobNavigation)
                    .WithMany(p => p.WorkflowStages)
                    .HasForeignKey(d => d.Job)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WorkflowStage.Job");

                entity.HasOne(d => d.StageTypeNavigation)
                    .WithMany(p => p.WorkflowStages)
                    .HasForeignKey(d => d.StageType)
                    .HasConstraintName("FK_WorkflowStage.StageType");

                entity.HasOne(d => d.StatusNavigation)
                    .WithMany(p => p.WorkflowStages)
                    .HasForeignKey(d => d.Status)
                    .HasConstraintName("FK_WorkflowStage.Status->WorkflowStageStatus");
            });

            modelBuilder.Entity<WorkflowStageStatus>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Workflow__3214EC0645BE5BA9")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Name, e.Order }, "ClusteredIndex-20190729-105102")
                    .IsClustered();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(200);
            });

            modelBuilder.Entity<WorkflowStartStage>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Workflow__3214EC0651300E55")
                    .IsClustered(false);

                entity.HasIndex(e => e.Id, "ClusteredIndex-20190729-105123")
                    .IsClustered();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.WorkflowStartStage)
                    .HasForeignKey<WorkflowStartStage>(d => d.Id)
                    .HasConstraintName("FK_WorkflowStartStage.Id->WorkflowStage");
            });

            modelBuilder.Entity<WorkflowStep>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK__Workflow__3214EC0655009F39")
                    .IsClustered(false);

                entity.HasIndex(e => new { e.Id, e.Name, e.WorkflowStage, e.Order, e.IsManual, e.CompletedAt, e.CompletedBy }, "ClusteredIndex-20190729-105151")
                    .IsClustered();

                entity.HasIndex(e => e.WorkflowStage, "IX_WorkflowSteps->WorkflowStage")
                    .HasFillFactor((byte)80);

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CompletedAt).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.CompletedByNavigation)
                    .WithMany(p => p.WorkflowSteps)
                    .HasForeignKey(d => d.CompletedBy)
                    .HasConstraintName("FK_WorkflowStep.CompletedBy->Contact");

                entity.HasOne(d => d.WorkflowStageNavigation)
                    .WithMany(p => p.WorkflowSteps)
                    .HasForeignKey(d => d.WorkflowStage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WorkflowStep.WorkflowStage");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);        
    }
}
