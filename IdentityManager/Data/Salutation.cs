﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Salutation
    {
        public Salutation()
        {
            Contacts = new HashSet<Contact>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
