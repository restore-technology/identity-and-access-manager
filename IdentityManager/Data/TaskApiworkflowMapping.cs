﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class TaskApiworkflowMapping
    {
        public int TaskWorkflowMappingId { get; set; }
        public int? TaskId { get; set; }
        public int? ApiworkflowId { get; set; }

        public virtual Apiworkflow Apiworkflow { get; set; }
        public virtual TaskType Task { get; set; }
    }
}
