﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsStandardItem
    {
        public long StandardItemId { get; set; }
        public int? PostCodeZone { get; set; }
        public decimal? MinimumCharge { get; set; }
        public decimal? _1to4Items { get; set; }
        public decimal? _5to10Items { get; set; }
        public decimal? _11to50Items { get; set; }
        public decimal? _51to100Items { get; set; }
        public decimal? _100plusItems { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
