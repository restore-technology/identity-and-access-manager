﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class AccessGroup
    {
        public AccessGroup()
        {
            Contacts = new HashSet<Contact>();
            SqlReportsAccessGroups = new HashSet<SqlReportsAccessGroup>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual ICollection<SqlReportsAccessGroup> SqlReportsAccessGroups { get; set; }
    }
}
