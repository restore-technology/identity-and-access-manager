﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Elog
    {
        public int Id { get; set; }
        public string ApplicationName { get; set; }
        public string VersionNo { get; set; }
        public string Environment { get; set; }
        public DateTime? LogDateTime { get; set; }
        public string UserId { get; set; }
        public string Context { get; set; }
        public string UniqueKeyName { get; set; }
        public string UniqueKeyValue { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorDescription0 { get; set; }
        public string ErrorDescription1 { get; set; }
        public string ErrorDescription2 { get; set; }
        public int? SeverityLevel { get; set; }
        public string Jsondata { get; set; }
    }
}
