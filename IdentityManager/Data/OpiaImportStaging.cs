﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class OpiaImportStaging
    {
        public string FileId { get; set; }
        public string FileConsignmentId { get; set; }
        public string FileBarcode { get; set; }
        public string FileClass { get; set; }
        public string FileMake { get; set; }
        public string FileSpecifics { get; set; }
        public string FileSerial { get; set; }
        public string FileBrand { get; set; }
        public string FileApprovedAt { get; set; }
    }
}
