﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ExternalProcessingJob
    {
        public Guid Id { get; set; }
        public Guid? Job { get; set; }
        public DateTime CreatedAt { get; set; }
        public int ProcessingSystemJobId { get; set; }
        public bool IsFinished { get; set; }

        public virtual Job JobNavigation { get; set; }
    }
}
