﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsMiddlewareSourceDatum
    {
        public int MiscSprndetail { get; set; }
        public string MiscAcsndetail { get; set; }
        public string MiscMfpndetail { get; set; }
        public string MiscManudetail { get; set; }
        public string MiscModldetail { get; set; }
        public string MiscModemdetail { get; set; }
        public string MiscAramdetail { get; set; }
        public string MiscAhdddetail { get; set; }
        public string MiscAccddetail { get; set; }
        public string MiscAfdddetail { get; set; }
        public string MiscAusbdetail { get; set; }
        public string MiscProcdetail { get; set; }
        public string MiscAnicdetail { get; set; }
        public string MiscAsctdetail { get; set; }
        public string MiscScszdetail { get; set; }
        public string MiscAbatdetail { get; set; }
        public string MiscAcacdetail { get; set; }
        public string MiscPlatdetail { get; set; }
        public string MiscButydetail { get; set; }
        public string MiscKeybdetail { get; set; }
        public string MiscLicedetail { get; set; }
        public string MiscJobndetail { get; set; }
        public string FunctionalDescription { get; set; }
        public string CosmeticDescription { get; set; }
        public string MrmlogInfo { get; set; }
    }
}
