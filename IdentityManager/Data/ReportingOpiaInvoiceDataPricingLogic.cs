﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingOpiaInvoiceDataPricingLogic
    {
        public string PromotionTag { get; set; }
        public string Name { get; set; }
        public decimal Laptop { get; set; }
        public decimal Desktop { get; set; }
        public decimal Mobile { get; set; }
        public decimal Misc { get; set; }
        public decimal Monitor { get; set; }
        public decimal ConditionCodeS { get; set; }
        public decimal DispositionRecycled { get; set; }
        public string FastTrackThreshold { get; set; }
        public decimal RemarketingPercReturn { get; set; }
    }
}
