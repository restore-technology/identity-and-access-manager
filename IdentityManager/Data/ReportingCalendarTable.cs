﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingCalendarTable
    {
        public DateTime FullDate { get; set; }
        public int? Period { get; set; }
        public int? Isoweek { get; set; }
        public string WorkingDay { get; set; }
    }
}
