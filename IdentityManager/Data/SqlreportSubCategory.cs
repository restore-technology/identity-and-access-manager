﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class SqlreportSubCategory
    {
        public int SqlreportSubCategoriesId { get; set; }
        public string SqlreportSubCategory1 { get; set; }
        public int ReportSortOrder { get; set; }
        public bool IsActive { get; set; }
    }
}
