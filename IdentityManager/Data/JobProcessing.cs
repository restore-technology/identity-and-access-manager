﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobProcessing
    {
        public int ProcessingId { get; set; }
        public string ProcessingName { get; set; }
        public int? ProcessingOrder { get; set; }
    }
}
