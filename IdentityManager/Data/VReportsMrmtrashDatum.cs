﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsMrmtrashDatum
    {
        public int? LotNumber { get; set; }
        public string ClassName { get; set; }
        public decimal? NetWeight { get; set; }
    }
}
