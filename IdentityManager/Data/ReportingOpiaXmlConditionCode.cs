﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingOpiaXmlConditionCode
    {
        public int OpiaConditionCodeId { get; set; }
        public int ComponentTypeRecId { get; set; }
        public int RecId { get; set; }
        public string ConditionValue { get; set; }
        public string OutputPrefix { get; set; }
        public string OutputCodeDesktop { get; set; }
        public string OutputCodeLaptop { get; set; }
        public string OutputCodeTablet { get; set; }
        public string OutputConditionCode { get; set; }
        public string OutputFinalDesktop { get; set; }
        public string OutputFinalLaptop { get; set; }
        public string OutputFinalTablet { get; set; }
    }
}
