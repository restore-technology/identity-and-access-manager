﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsCustomer
    {
        public long CustomerId { get; set; }
        public string ExternalId { get; set; }
        public string Name { get; set; }
        public string ContactName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Siccode { get; set; }
        public string PostCode { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public string CreatedAt { get; set; }
        public DateTime? CreatedAtDateTime { get; set; }
        public string UpdatedAt { get; set; }
        public DateTime? UpdatedAtDateTime { get; set; }
    }
}
