﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ContactsRuncornImportTemp
    {
        public Guid? Customer { get; set; }
        public Guid? Location { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string JobTitle { get; set; }
        public string Telephone { get; set; }
        public string Ddi { get; set; }
        public string Mobile { get; set; }
        public Guid? Salutation { get; set; }
        public bool IsSystemUser { get; set; }
        public Guid? AccessGroup { get; set; }
        public string Username { get; set; }
        public Guid? Status { get; set; }
        public bool IsActive { get; set; }
        public bool? IsAssetVarianceOverride { get; set; }
        public string HashPassword { get; set; }
        public string Salt { get; set; }
        public bool IsDpo { get; set; }
        public string ExternalData1 { get; set; }
        public string ExternalData2 { get; set; }
    }
}
