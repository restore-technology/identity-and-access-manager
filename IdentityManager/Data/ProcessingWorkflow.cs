﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ProcessingWorkflow
    {
        public ProcessingWorkflow()
        {
            Jobs = new HashSet<Job>();
            Jobs4444s = new HashSet<Jobs4444>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }

        public virtual ICollection<Job> Jobs { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444s { get; set; }
    }
}
