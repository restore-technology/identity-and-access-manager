﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsMasterAssetDetail
    {
        public Guid AssetId { get; set; }
        public int Reference { get; set; }
        public string SerialNumber { get; set; }
        public int? Stnumber { get; set; }
        public string CustomerAssetTag { get; set; }
        public int? ProcessingSystemAssetId { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
        public string Condition { get; set; }
        public string AssetType { get; set; }
        public Guid CustomerGuid { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string AssetStatus { get; set; }
        public string AssetDisposition { get; set; }
        public string MasterCompany { get; set; }
    }
}
