﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class MiddlewareAssetComponentConversionTextMatch
    {
        public int TextMatchId { get; set; }
        public string MrmcomponentText { get; set; }
        public string Ltscomponent { get; set; }
        public string ComponentType { get; set; }
    }
}
