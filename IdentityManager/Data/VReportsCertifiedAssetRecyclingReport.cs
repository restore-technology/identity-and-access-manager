﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsCertifiedAssetRecyclingReport
    {
        public int? StreamsJobNumber { get; set; }
        public int? MrmLotNumber { get; set; }
        public string CategoryName { get; set; }
        public int? Quantity { get; set; }
        public decimal? TotalWeight { get; set; }
    }
}
