﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class DashboardSetting
    {
        public int? Id { get; set; }
        public string ReleaseDate { get; set; }
        public string ReleaseVersion { get; set; }
        public string Environment { get; set; }
        public int? SlarefreshRate { get; set; }
        public int? ProductionReportRefreshrate { get; set; }
        public int? PageRefreshRate { get; set; }
        public int? PageReloadRefreshRate { get; set; }
    }
}
