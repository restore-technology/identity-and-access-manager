﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class SqlReportParameter
    {
        public Guid Id { get; set; }
        public Guid Report { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public Guid? ContextValue { get; set; }

        public virtual ReportParameterContextValue ContextValueNavigation { get; set; }
        public virtual SqlReport ReportNavigation { get; set; }
    }
}
