﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingOpiaXmlReportProject
    {
        public int ReportProjectId { get; set; }
        public string ProjectName { get; set; }
        public string JobNumber { get; set; }
    }
}
