﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingOpiaXmldatum
    {
        public string JobNumber { get; set; }
        public string ReferenceId { get; set; }
        public string ClaimFormLink { get; set; }
        public int? ClaimId { get; set; }
        public byte? ProgramId { get; set; }
        public string Type { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Os { get; set; }
        public string Imei { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ImportFileName { get; set; }
        public string ClaimApprovedText { get; set; }
        public DateTime? ClaimApproved { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public int? StreamsJobNo { get; set; }
        public string StreamsCustomerName { get; set; }
        public string StreamsCustomerReference { get; set; }
        public string StreamsProject { get; set; }
        public string StreamsRequestor { get; set; }
        public string StreamsCollectionDate { get; set; }
        public string StreamsReceivedDate { get; set; }
        public string StreamsAuditedDate { get; set; }
        public string StreamsProcessedDate { get; set; }
        public string StreamsAssetType { get; set; }
        public string StreamsSecurityStatus { get; set; }
        public string StreamsOemserialNo { get; set; }
        public string StreamsReferenceId { get; set; }
        public int? StreamsStreamsSerialNo { get; set; }
        public string StreamsOem { get; set; }
        public string StreamsModelDescription { get; set; }
        public string StreamsEquipmentDescription { get; set; }
        public string StreamsRecoveryMediaPresent { get; set; }
        public string StreamsProcessor { get; set; }
        public string StreamsSpeed { get; set; }
        public string StreamsMemoryMb { get; set; }
        public int? StreamsHardDriveCount { get; set; }
        public int? StreamsHardDriveWiped { get; set; }
        public int? StreamsHardDriveDestroyed { get; set; }
        public string StreamsHddsize { get; set; }
        public string StreamsHddtype { get; set; }
        public string StreamsMonitorSize { get; set; }
        public string StreamsManufactureDate { get; set; }
        public string StreamsCoa { get; set; }
        public string StreamsDisposition { get; set; }
        public string StreamsConditionCode { get; set; }
        public string StreamsConditionComments { get; set; }
        public decimal? StreamsTotalServiceCharges { get; set; }
        public decimal? StreamsSalePrice { get; set; }
    }
}
