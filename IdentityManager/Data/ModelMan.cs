﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ModelMan
    {
        public long TableId { get; set; }
        public Guid? AssetClass { get; set; }
        public string Manufacturer { get; set; }
        public string ProductFamily { get; set; }
        public string ProductSeries { get; set; }
        public string ProductName { get; set; }
        public string Grade { get; set; }
        public decimal? Value { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdateMethod { get; set; }

        public virtual AssetType AssetClassNavigation { get; set; }
    }
}
