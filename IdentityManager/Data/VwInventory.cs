﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VwInventory
    {
        public int Ref { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public int Expected { get; set; }
        public int Collected { get; set; }
    }
}
