﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Vatcode
    {
        public int TableId { get; set; }
        public string Vatcode1 { get; set; }
        public string Description { get; set; }
    }
}
