﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerInvoiceAsset
    {
        public int AssetClassId { get; set; }
        public string AssetClassName { get; set; }
        public int? AssetOrder { get; set; }
    }
}
