﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsActivity
    {
        public long ActivityId { get; set; }
        public long? JobId { get; set; }
        public string Type { get; set; }
        public string Provider { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Accuracy { get; set; }
        public string FixTime { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
