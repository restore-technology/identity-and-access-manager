﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VwDownloadCollection
    {
        public Guid Id { get; set; }
        public int JobReference { get; set; }
        public string JobType { get; set; }
        public string Agent { get; set; }
        public string Company { get; set; }
        public DateTime? ScheduledPickUpDate { get; set; }
        public string PrimaryContact { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public bool IsSerializedCollection { get; set; }
        public bool ArePalletisedForTransportation { get; set; }
        public int? NumberOfPallets { get; set; }
        public string SourceAddress { get; set; }
    }
}
