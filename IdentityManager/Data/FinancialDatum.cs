﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class FinancialDatum
    {
        public int AssetId { get; set; }
        public decimal? GrandTotal { get; set; }
        public decimal? StandardTotal { get; set; }
    }
}
