﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerCharge
    {
        public long CustomerChargeId { get; set; }
        public Guid? CustomerId { get; set; }
        public int? PerAssetChargeTypeId { get; set; }
        public decimal? CustomerCharge1 { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? AssetTypeId { get; set; }
        public int? CustomerInvoiceAssetId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual PerAssetChargeType PerAssetChargeType { get; set; }
    }
}
