﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Setting
    {
        public Guid Id { get; set; }
        public string ReleaseVersion { get; set; }
        public string ReleaseDate { get; set; }
        public string Name { get; set; }
        public string LogisticsSystemApiEndpoint { get; set; }
        public string ProcessingSystemApiEndpoint { get; set; }
        public Guid? DefaultRedemtechLocation { get; set; }
        public string ClientLandingPage { get; set; }
        public string BillingPeriod { get; set; }
        public long? StartingInvoiceNumber { get; set; }
        public long? LatestInvoiceNumber { get; set; }
        public string MessageOfTheDay { get; set; }
        public long? SlareportRefreshRate { get; set; }
        public bool? RunTaskManager { get; set; }
        public string AcceptableFileTypes { get; set; }
        public int? MaxFileSizeinMb { get; set; }
        public int? MaxNumberofFilesForUpload { get; set; }
        public int? ProductionReportRefreshRate { get; set; }
        public string SyncJobWithMrmtime { get; set; }
        public bool? ShowQuickJobsIcon { get; set; }
        public string SharedFolderForFileCreate { get; set; }
        public string BlanccoApiurl { get; set; }
        public int? BlanccoApitaskStartHour { get; set; }
        public int? BlanccoApitaskEndHour { get; set; }
        public string BlanccoRequestXmlpath { get; set; }
        public string BlanccoReportPath { get; set; }

        public virtual Location DefaultRedemtechLocationNavigation { get; set; }
    }
}
