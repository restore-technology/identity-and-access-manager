﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class SqlReportsCustomer
    {
        public Guid SqlReportId { get; set; }
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual SqlReport SqlReport { get; set; }
    }
}
