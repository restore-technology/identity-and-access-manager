﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class SqlReport
    {
        public SqlReport()
        {
            SqlReportParameters = new HashSet<SqlReportParameter>();
            SqlReportsAccessGroups = new HashSet<SqlReportsAccessGroup>();
            SqlReportsContactRoles = new HashSet<SqlReportsContactRole>();
            SqlReportsCustomers = new HashSet<SqlReportsCustomer>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ReportPath { get; set; }
        public bool HideToolbar { get; set; }
        public bool HideParameterPrompts { get; set; }
        public string Description { get; set; }
        public int? SqlreportSubCategoriesId { get; set; }

        public virtual ICollection<SqlReportParameter> SqlReportParameters { get; set; }
        public virtual ICollection<SqlReportsAccessGroup> SqlReportsAccessGroups { get; set; }
        public virtual ICollection<SqlReportsContactRole> SqlReportsContactRoles { get; set; }
        public virtual ICollection<SqlReportsCustomer> SqlReportsCustomers { get; set; }
    }
}
