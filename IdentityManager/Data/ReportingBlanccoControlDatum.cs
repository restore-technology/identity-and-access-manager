﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingBlanccoControlDatum
    {
        public DateTime LastRunDate { get; set; }
    }
}
