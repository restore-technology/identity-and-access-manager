﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsBranch
    {
        public long BranchId { get; set; }
        public string ExternalId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public DateTime? CreatedAtDateTime { get; set; }
        public string CreatedAt { get; set; }
    }
}
