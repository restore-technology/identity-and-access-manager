﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsCertifiedDataSecurityReport
    {
        public int? StreamsJobNumber { get; set; }
        public int? MrmLotNumber { get; set; }
        public int? StNo { get; set; }
        public string ParentOemSerialNo { get; set; }
        public string ParentAssetTagNo { get; set; }
        public string HddSerialNo { get; set; }
        public string DataSecurityLevelAssigned { get; set; }
        public string DataSecurityLevelPerformed { get; set; }
        public string IsLooseDrive { get; set; }
    }
}
