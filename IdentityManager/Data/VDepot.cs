﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VDepot
    {
        public long Id { get; set; }
        public string DepotName { get; set; }
    }
}
