﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class DelmenowJob
    {
        public int ProcessingSystemJobId { get; set; }
        public int? PromptCollectionNo { get; set; }
        public DateTime? PromptCollectionDate { get; set; }
        public string PromptProject { get; set; }
        public string PromptBillingCode { get; set; }
        public Guid? PromptCustomerGuid { get; set; }
        public string PromptCustomerNo { get; set; }
        public string PromptCustomerName { get; set; }
        public int? JobNo { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string Requestor { get; set; }
        public string OriginatingLocation { get; set; }
        public string DestinationLocation { get; set; }
        public string CustomerReference { get; set; }
        public DateTime? CollectionDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
    }
}
