﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsDeliveryContact
    {
        public long DeliveryContactId { get; set; }
        public long? JobId { get; set; }
        public long? DeliveryCustomerId { get; set; }
        public string DeliveryContactName { get; set; }
        public string DeliveryAddress1 { get; set; }
        public string DeliveryAddress2 { get; set; }
        public string DeliveryAddress3 { get; set; }
        public string DeliveryAddress4 { get; set; }
        public string DeliveryPostCode { get; set; }
        public string DeliveryContactNumber { get; set; }
        public string DeliveryUpdatedAt { get; set; }
    }
}
