﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingOpiaControlDatum
    {
        public int? OffsetDays { get; set; }
        public DateTime? FtpcontrolDate { get; set; }
        public DateTime? AssetControlDate { get; set; }
    }
}
