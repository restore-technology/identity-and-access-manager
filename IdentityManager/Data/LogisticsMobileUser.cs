﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsMobileUser
    {
        public long MobileUserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Color { get; set; }
        public string EngineerRegistrationNumber { get; set; }
    }
}
