﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class AssetStatus
    {
        public AssetStatus()
        {
            Assets = new HashSet<Asset>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }

        public virtual ICollection<Asset> Assets { get; set; }
    }
}
