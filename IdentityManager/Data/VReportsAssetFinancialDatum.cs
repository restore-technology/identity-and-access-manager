﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsAssetFinancialDatum
    {
        public int AssetId { get; set; }
        public string StatusName { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? PartsCharges { get; set; }
        public decimal? CommissionRate { get; set; }
        public decimal? TotalServices { get; set; }
        public decimal? StandardServices { get; set; }
        public decimal? AdditionalServices { get; set; }
        public long? Sequence { get; set; }
        public DateTime? ShippedDate { get; set; }
        public decimal SalePriceUnfiltered { get; set; }
    }
}
