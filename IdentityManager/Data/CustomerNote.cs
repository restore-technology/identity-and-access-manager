﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerNote
    {
        public Guid Id { get; set; }
        public Guid Customer { get; set; }

        public virtual Customer CustomerNavigation { get; set; }
        public virtual Note IdNavigation { get; set; }
    }
}
