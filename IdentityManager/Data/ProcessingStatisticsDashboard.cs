﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ProcessingStatisticsDashboard
    {
        public long ProcessingStatisticsId { get; set; }
        public string ProcessingSite { get; set; }
        public string ProcessingStaff { get; set; }
        public int? Assets { get; set; }
        public DateTime? DateCompleted { get; set; }
        public int? ProcessStep { get; set; }
        public int? BacklogAssets { get; set; }
        public int? AssetsLastTenDays { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
