﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class InvoiceType
    {
        public long InvoiceTypeId { get; set; }
        public string InvoiceType1 { get; set; }
    }
}
