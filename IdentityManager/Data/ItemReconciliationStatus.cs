﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ItemReconciliationStatus
    {
        public ItemReconciliationStatus()
        {
            JobAssetLogisticsSystemStatusNavigations = new HashSet<JobAsset>();
            JobAssetProcessingSystemReconciliationStatusNavigations = new HashSet<JobAsset>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<JobAsset> JobAssetLogisticsSystemStatusNavigations { get; set; }
        public virtual ICollection<JobAsset> JobAssetProcessingSystemReconciliationStatusNavigations { get; set; }
    }
}
