﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingLiveForecastReportAssetTypeValue
    {
        public Guid CustomerId { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string AssetType { get; set; }
        public string CustomerAssetKey { get; set; }
        public decimal? TotalServices { get; set; }
        public int? AssetCount { get; set; }
        public decimal? AveragePrice { get; set; }
    }
}
