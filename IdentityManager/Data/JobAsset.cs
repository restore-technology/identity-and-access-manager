﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobAsset
    {
        public JobAsset()
        {
            QualityControls = new HashSet<QualityControl>();
        }

        public Guid Id { get; set; }
        public Guid Asset { get; set; }
        public Guid Job { get; set; }
        public Guid? ProcessingSystemProcess { get; set; }
        public Guid? ProcessingSystemReconciliationStatus { get; set; }
        public Guid? LogisticsSystemStatus { get; set; }
        public Guid? Container { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public DateTime? CollectionDate { get; set; }
        public Guid? AssetType { get; set; }
        public Guid? Customer { get; set; }
        public int? Stnumber { get; set; }
        public Guid? AssetStatus { get; set; }

        public virtual Asset AssetNavigation { get; set; }
        public virtual AssetType AssetTypeNavigation { get; set; }
        public virtual Container ContainerNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual Job JobNavigation { get; set; }
        public virtual ItemReconciliationStatus LogisticsSystemStatusNavigation { get; set; }
        public virtual ProcessingSystemProcess ProcessingSystemProcessNavigation { get; set; }
        public virtual ItemReconciliationStatus ProcessingSystemReconciliationStatusNavigation { get; set; }
        public virtual ICollection<QualityControl> QualityControls { get; set; }
    }
}
