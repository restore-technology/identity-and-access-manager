﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingWorkflowForecastReportCategory
    {
        public string StageName { get; set; }
        public string StepName { get; set; }
        public string ReportGrouping { get; set; }
        public int? GroupingOrder { get; set; }
    }
}
