﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ServiceBundle
    {
        public ServiceBundle()
        {
            ServiceBundleLineMappings = new HashSet<ServiceBundleLineMapping>();
        }

        public long ServiceBundleId { get; set; }
        public string ServiceBundleName { get; set; }
        public bool? IsEnabled { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual ICollection<ServiceBundleLineMapping> ServiceBundleLineMappings { get; set; }
    }
}
