﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LoginAttempt
    {
        public Guid Id { get; set; }
        public string Browser { get; set; }
        public DateTime Date { get; set; }
        public string Ip { get; set; }
        public bool Successful { get; set; }
        public Guid? Contact { get; set; }
        public string InputtedLoginUser { get; set; }
        public string InputtedPassword { get; set; }
        public string Salt { get; set; }

        public virtual Contact ContactNavigation { get; set; }
    }
}
