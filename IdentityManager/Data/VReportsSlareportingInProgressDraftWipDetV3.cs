﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsSlareportingInProgressDraftWipDetV3
    {
        public string CustomerName { get; set; }
        public string SourceLocnRef { get; set; }
        public string DestinationDepot { get; set; }
        public string ReceiveDock { get; set; }
        public string SourceAddr1 { get; set; }
        public string SourceAddr2 { get; set; }
        public string SourceCity { get; set; }
        public string SourceLocName { get; set; }
        public string CustomerNumber { get; set; }
        public int RecoveryNumber { get; set; }
        public int StreamJobNumber { get; set; }
        public string LogisticsType { get; set; }
        public string RecoveryStatus { get; set; }
        public DateTime? ReceivingDate { get; set; }
        public DateTime? CollectionDate { get; set; }
        public DateTime? Today { get; set; }
        public int? InProcessTimeline { get; set; }
        public int? ClientSla { get; set; }
        public int? Difference { get; set; }
        public string SlaonTarget { get; set; }
        public DateTime? DateToBeCompleted { get; set; }
        public int? Rttotal { get; set; }
        public int? RtinQc { get; set; }
        public int Rtremaining { get; set; }
        public int RtremLap { get; set; }
        public int RtremDsk { get; set; }
        public int RtremMon { get; set; }
        public int RtremPrt { get; set; }
        public int RtremSvr { get; set; }
        public int RtremMob { get; set; }
        public int RtremNet { get; set; }
        public int RtremTel { get; set; }
        public int RtremCab { get; set; }
        public int RtremStr { get; set; }
        public int RtremDatStr { get; set; }
        public int RtremMscOther { get; set; }
        public int Rtexpected { get; set; }
        public int RtexpLap { get; set; }
        public int RtexpDsk { get; set; }
        public int RtexpMon { get; set; }
        public int RtexpPrt { get; set; }
        public int RtexpSvr { get; set; }
        public int RtexpMob { get; set; }
        public int RtexpNet { get; set; }
        public int RtexpTel { get; set; }
        public int RtexpCab { get; set; }
        public int RtexpMsc { get; set; }
        public int RtexpStr { get; set; }
        public int RtexpOth { get; set; }
        public int RtexpDatStr { get; set; }
        public int Rtcollected { get; set; }
        public int RtcolLap { get; set; }
        public int RtcolDsk { get; set; }
        public int RtcolMon { get; set; }
        public int RtcolPrt { get; set; }
        public int RtcolSvr { get; set; }
        public int RtcolMob { get; set; }
        public int RtcolNet { get; set; }
        public int RtcolTel { get; set; }
        public int RtcolCab { get; set; }
        public int RtcolMsc { get; set; }
        public int RtcolStr { get; set; }
        public int RtcolOth { get; set; }
        public int RtcolDatStr { get; set; }
    }
}
