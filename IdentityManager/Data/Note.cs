﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Note
    {
        public Guid Id { get; set; }
        public DateTime CreationDate { get; set; }
        public Guid CreatedBy { get; set; }
        public string Notes { get; set; }
        public string AttachmentFileName { get; set; }

        public virtual Contact CreatedByNavigation { get; set; }
        public virtual CustomerNote CustomerNote { get; set; }
    }
}
