﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class IndustrySector
    {
        public int Id { get; set; }
        public string Sector { get; set; }
    }
}
