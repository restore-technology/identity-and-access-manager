﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsPostCodeZone
    {
        public long LocationId { get; set; }
        public string Postcode { get; set; }
        public string Rdtzone { get; set; }
        public string Location { get; set; }
        public bool? CongestionCharge { get; set; }
    }
}
