﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingConsolidatedfinancialsRevisitedX
    {
        public int StreamsJobNo { get; set; }
        public string ProjectName { get; set; }
        public string Rmcredit { get; set; }
        public int? LotId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNumber { get; set; }
        public string MasterCompany { get; set; }
        public string PrimaryContact { get; set; }
        public string JobType { get; set; }
        public string LotStatus { get; set; }
        public string SettlementStage { get; set; }
        public string BillingCode { get; set; }
        public int? AssetCount { get; set; }
        public int? InventoryCount { get; set; }
        public int? DaysOnShelf { get; set; }
        public int? DaysTurnaround { get; set; }
        public DateTime? ProductionCompletedOn { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string WorkflowStatus { get; set; }
        public string RequestedBy { get; set; }
        public DateTime? CollectionDate { get; set; }
        public string CollectionLocation { get; set; }
        public string DeploymentLocation { get; set; }
        public string Poreference { get; set; }
        public string InvoiceReference { get; set; }
        public int? CountRedeployed { get; set; }
        public int? CountSold { get; set; }
        public int? CountRecycled { get; set; }
        public int? DataStorageCount { get; set; }
        public decimal? ProcessingService { get; set; }
        public decimal? Rebate { get; set; }
        public decimal? AdHoc { get; set; }
        public decimal? ProcessingCharge { get; set; }
        public decimal? Logistics { get; set; }
        public decimal? Engineering { get; set; }
        public decimal? AuditTrash { get; set; }
        public decimal? ClientEarnings { get; set; }
        public string CustomizableField1 { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
