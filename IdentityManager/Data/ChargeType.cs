﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ChargeType
    {
        public ChargeType()
        {
            CustomerTransactions = new HashSet<CustomerTransaction>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CustomerTransaction> CustomerTransactions { get; set; }
    }
}
