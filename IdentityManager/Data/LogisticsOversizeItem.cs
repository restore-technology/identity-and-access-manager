﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsOversizeItem
    {
        public long OversizeItemId { get; set; }
        public int? PostCodeZone { get; set; }
        public decimal? MinimumCharge { get; set; }
        public decimal? _1to2Items { get; set; }
        public decimal? _3to8Items { get; set; }
        public decimal? _9to16Items { get; set; }
        public decimal? _17to24Items { get; set; }
        public decimal? _24plusItems { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
