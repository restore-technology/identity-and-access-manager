﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Container
    {
        public Container()
        {
            JobAssetTypes = new HashSet<JobAssetType>();
            JobAssets = new HashSet<JobAsset>();
        }

        public Guid Id { get; set; }
        public string ContainerId { get; set; }
        public string Seal1 { get; set; }
        public string Seal2 { get; set; }
        public string Seal3 { get; set; }
        public string Seal4 { get; set; }
        public Guid? Job { get; set; }

        public virtual Job JobNavigation { get; set; }
        public virtual ICollection<JobAssetType> JobAssetTypes { get; set; }
        public virtual ICollection<JobAsset> JobAssets { get; set; }
    }
}
