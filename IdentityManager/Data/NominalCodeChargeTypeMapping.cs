﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class NominalCodeChargeTypeMapping
    {
        public int TableId { get; set; }
        public int NominalCodeId { get; set; }
        public Guid ChargeTypeId { get; set; }
    }
}
