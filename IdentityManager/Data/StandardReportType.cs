﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class StandardReportType
    {
        public StandardReportType()
        {
            JobNotes = new HashSet<JobNote>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<JobNote> JobNotes { get; set; }
    }
}
