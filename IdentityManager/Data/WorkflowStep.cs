﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class WorkflowStep
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid WorkflowStage { get; set; }
        public int Order { get; set; }
        public bool IsManual { get; set; }
        public DateTime? CompletedAt { get; set; }
        public Guid? CompletedBy { get; set; }
        public bool IsEmailed { get; set; }

        public virtual Contact CompletedByNavigation { get; set; }
        public virtual WorkflowStage WorkflowStageNavigation { get; set; }
    }
}
