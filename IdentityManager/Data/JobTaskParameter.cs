﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobTaskParameter
    {
        public long JobTaskParameterId { get; set; }
        public Guid? JobId { get; set; }
        public int? TaskId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }

        public virtual TaskType Task { get; set; }
    }
}
