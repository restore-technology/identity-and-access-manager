﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class InvoiceOverrideTemp
    {
        public int InvoiceOverrideTempId { get; set; }
        public int JobNumber { get; set; }
        public int AssetId { get; set; }
        public int AssetClassId { get; set; }
        public string AssetClass { get; set; }
        public int ServiceId { get; set; }
        public int ServiceType { get; set; }
        public string ServiceName { get; set; }
        public decimal PreviousCharge { get; set; }
        public decimal? NewCharge { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ContractId { get; set; }
        public int Type { get; set; }
    }
}
