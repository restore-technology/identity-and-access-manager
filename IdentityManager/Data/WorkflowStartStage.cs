﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class WorkflowStartStage
    {
        public Guid Id { get; set; }

        public virtual WorkflowStage IdNavigation { get; set; }
    }
}
