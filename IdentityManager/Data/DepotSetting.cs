﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class DepotSetting
    {
        public long DepotSettingsId { get; set; }
        public string DepotName { get; set; }
        public string DepotPrefix { get; set; }
        public int? DepotDockId { get; set; }
        public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }
        public string DepotAddress { get; set; }
        public string DepotCity { get; set; }
        public string DepotCounty { get; set; }
        public string DepotPostCode { get; set; }
        public bool? IsActive { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Analysis { get; set; }
        public bool? RequireSerialsExempted { get; set; }
    }
}
