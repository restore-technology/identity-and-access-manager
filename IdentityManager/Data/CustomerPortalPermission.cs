﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerPortalPermission
    {
        public long CustomerPortalPermissionId { get; set; }
        public long? PortalPermissionId { get; set; }
        public Guid? CustomerId { get; set; }
        public bool? IsChecked { get; set; }
    }
}
