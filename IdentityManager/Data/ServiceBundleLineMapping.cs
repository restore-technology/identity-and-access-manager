﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ServiceBundleLineMapping
    {
        public long MappinngTableId { get; set; }
        public long? ServiceBundleId { get; set; }
        public long? ServiceLineId { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual ServiceBundle ServiceBundle { get; set; }
    }
}
