﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class StreamsMrmstatus
    {
        public int Id { get; set; }
        public string StreamsOrder { get; set; }
        public string Stage { get; set; }
        public string Step { get; set; }
        public int? Mrmorder { get; set; }
        public string Mrmstatus { get; set; }
    }
}
