﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsJob
    {
        public long JobId { get; set; }
        public string JobNumber { get; set; }
        public long? CustomerId { get; set; }
        public long? BranchId { get; set; }
        public long? MobileUserId { get; set; }
        public string ExternalId { get; set; }
        public string Type { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan? Time { get; set; }
        public string Status { get; set; }
        public string Duration { get; set; }
        public string Weight { get; set; }
        public string ContactName { get; set; }
        public string PostCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Notes { get; set; }
        public string ContactNumber1 { get; set; }
        public string ContactNumber2 { get; set; }
        public string ContactNumber3 { get; set; }
        public DateTime? TimeSlot { get; set; }
        public string SpecialInstructions { get; set; }
        public string Signature { get; set; }
        public DateTime? SignatureCreatedAt { get; set; }
        public string SignatureSignee { get; set; }
        public string SignatureType { get; set; }
        public string SignatureName { get; set; }
        public string Fault { get; set; }
        public string RepairCode { get; set; }
        public string RepairDescription { get; set; }
        public string Signee { get; set; }
        public int? Satisfaction { get; set; }
        public string SecondaryUser { get; set; }
        public string Task { get; set; }
        public string Product { get; set; }
        public string GasSafeInspection { get; set; }
        public string Priority { get; set; }
        public string FailureType { get; set; }
        public string Attachments { get; set; }
        public string Parts { get; set; }
        public string PreJobCheck { get; set; }
        public string PostJobCheck { get; set; }
        public string Photos { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
