﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ContactRole
    {
        public ContactRole()
        {
            ContactsRoles = new HashSet<ContactsRole>();
            EmailPreferencesContactRoles = new HashSet<EmailPreferencesContactRole>();
            EmailPreferencesDefaultToContactRoles = new HashSet<EmailPreferencesDefaultToContactRole>();
            EmailTemplatesContactRoles = new HashSet<EmailTemplatesContactRole>();
            EmailTemplatesDefaultToContactRoles = new HashSet<EmailTemplatesDefaultToContactRole>();
            SqlReportsContactRoles = new HashSet<SqlReportsContactRole>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public bool? IsInternal { get; set; }

        public virtual ICollection<ContactsRole> ContactsRoles { get; set; }
        public virtual ICollection<EmailPreferencesContactRole> EmailPreferencesContactRoles { get; set; }
        public virtual ICollection<EmailPreferencesDefaultToContactRole> EmailPreferencesDefaultToContactRoles { get; set; }
        public virtual ICollection<EmailTemplatesContactRole> EmailTemplatesContactRoles { get; set; }
        public virtual ICollection<EmailTemplatesDefaultToContactRole> EmailTemplatesDefaultToContactRoles { get; set; }
        public virtual ICollection<SqlReportsContactRole> SqlReportsContactRoles { get; set; }
    }
}
