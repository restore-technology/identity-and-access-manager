﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CurrencyVat
    {
        public long CurrencyId { get; set; }
        public string Currency { get; set; }
        public string DisplayName { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string ImageUrl { get; set; }
        public decimal? Vatrate { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal? RemarketingPercentage { get; set; }
        public long? VatcodeId { get; set; }
    }
}
