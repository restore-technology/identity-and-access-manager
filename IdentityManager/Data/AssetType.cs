﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class AssetType
    {
        public AssetType()
        {
            Assets = new HashSet<Asset>();
            JobAssetTypes = new HashSet<JobAssetType>();
            JobAssets = new HashSet<JobAsset>();
            ModelMen = new HashSet<ModelMan>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public bool? DataBearingAssumed { get; set; }

        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<JobAssetType> JobAssetTypes { get; set; }
        public virtual ICollection<JobAsset> JobAssets { get; set; }
        public virtual ICollection<ModelMan> ModelMen { get; set; }
    }
}
