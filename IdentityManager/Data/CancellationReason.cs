﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CancellationReason
    {
        public int CancellationReasonId { get; set; }
        public string CancellationReason1 { get; set; }
        public bool IsActive { get; set; }
    }
}
