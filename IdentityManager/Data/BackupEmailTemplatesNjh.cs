﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class BackupEmailTemplatesNjh
    {
        public Guid Id { get; set; }
        public int Reference { get; set; }
        public Guid EmailTriggerEvent { get; set; }
        public string Name { get; set; }
        public bool Internal { get; set; }
        public bool Active { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
