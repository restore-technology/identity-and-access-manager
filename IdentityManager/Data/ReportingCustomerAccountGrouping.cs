﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingCustomerAccountGrouping
    {
        public int CustomerAccountId { get; set; }
        public Guid? StreamsCustomerGuid { get; set; }
        public string StreamsCustomerNumber { get; set; }
        public int? MrmaccountId { get; set; }
        public string CustomerGrouping { get; set; }
    }
}
