﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsCheckList
    {
        public long CheckListId { get; set; }
        public long? JobId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public bool? Failure { get; set; }
        public Guid? Uuid { get; set; }
        public string OrderId { get; set; }
        public DateTime? CreatedAtDateTime { get; set; }
        public string CreatedAt { get; set; }
    }
}
