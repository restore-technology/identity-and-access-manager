﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Jobs4444
    {
        public Guid Id { get; set; }
        public int Reference { get; set; }
        public Guid? Project { get; set; }
        public Guid Customer { get; set; }
        public Guid? LogisticsJobType { get; set; }
        public Guid ProcessingJobType { get; set; }
        public string Ponumber { get; set; }
        public DateTime RequestedDate { get; set; }
        public Guid RequestedBy { get; set; }
        public string CustomizableField1 { get; set; }
        public string CustomizableField2 { get; set; }
        public string CustomizableField3 { get; set; }
        public Guid? SourceContact { get; set; }
        public Guid? SourceLocation { get; set; }
        public Guid? DestinationContact { get; set; }
        public Guid? DestinationLocation { get; set; }
        public bool IsSerializedCollection { get; set; }
        public bool ArePalletisedForTransportation { get; set; }
        public int? NumberOfPallets { get; set; }
        public string PalletsLocation { get; set; }
        public bool MultipleAssetsLocations { get; set; }
        public string AssetsLocation { get; set; }
        public string AssetsDeploymentLocation { get; set; }
        public bool AreOversizedItems { get; set; }
        public string CustomerComments { get; set; }
        public string RedemtechNotes { get; set; }
        public Guid CustomerApprovalStatus { get; set; }
        public string CustomerApprovalComments { get; set; }
        public Guid? CustomerApprovedBy { get; set; }
        public Guid SalesApprovalStatus { get; set; }
        public string SalesApprovalComments { get; set; }
        public Guid? SalesApprovedBy { get; set; }
        public Guid LogisticsApprovalStatus { get; set; }
        public string LogisticsApprovalComments { get; set; }
        public Guid? LogisticsApprovedBy { get; set; }
        public Guid? CancelledBy { get; set; }
        public DateTime? CancelledAt { get; set; }
        public string UploadedAssets { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid? SubmittedBy { get; set; }
        public DateTime? SubmittedAt { get; set; }
        public DateTime? RequestedDeliveryDate { get; set; }
        public DateTime? SiteVisitScheduledAt { get; set; }
        public Guid? LogisticsSupplier { get; set; }
        public DateTime? CompletedAt { get; set; }
        public Guid? CompletedBy { get; set; }
        public DateTime? ReadyToInvoiceAt { get; set; }
        public Guid? ReadyToInvoiceBy { get; set; }
        public DateTime? InvoicedAt { get; set; }
        public Guid? InvoicedBy { get; set; }
        public Guid? RequestedJobType { get; set; }
        public Guid? BillingCode { get; set; }
        public bool IsLogisticsCompleted { get; set; }
        public string CustomerPrintName { get; set; }
        public string CustomerSignatureFileName { get; set; }
        public DateTime? LogisticsCollectionComplete { get; set; }
        public string VehicleNumber { get; set; }
        public DateTime? LogisticsCollectionOnSite { get; set; }
        public Guid? ClientStatus { get; set; }
        public Guid? Itadstatus { get; set; }
        public bool? Mmwlogistics { get; set; }
        public string WorkflowType { get; set; }
        public int? OversizedItems { get; set; }
        public bool? IsHoldAfterProduction { get; set; }
        public bool? IsSerialiseOnSite { get; set; }
        public Guid? BillingEntity { get; set; }
        public int? ChargingModel { get; set; }
        public decimal? QuotedPrice { get; set; }
        public decimal? ConvertedPrice { get; set; }
        public bool? IsDeferredInvoicing { get; set; }
        public int? IsQuotedPrice { get; set; }
        public decimal? ExchangeRate { get; set; }
        public decimal? ProcessingCharge { get; set; }
        public decimal? LogisticsCharge { get; set; }
        public decimal? AdHocCharge { get; set; }
        public bool? IsQuotedLogistics { get; set; }
        public decimal? QuotedLogisticsCharge { get; set; }
        public int? RemarketingSettlementModel { get; set; }
        public int? AssetsSoldCount { get; set; }
        public decimal? GrossRebate { get; set; }
        public int? TaskId { get; set; }
        public int? JobProcessing { get; set; }
        public decimal? AuditTrash { get; set; }
        public bool? IsQuotationJob { get; set; }
        public decimal? EngineeringCharge { get; set; }
        public string WasteCollection { get; set; }
        public bool? AnyHazardousWaste { get; set; }
        public Guid? ResellerForThisJob { get; set; }
        public int? PercentageofVehicle { get; set; }

        public virtual BillingCode BillingCodeNavigation { get; set; }
        public virtual Contact CancelledByNavigation { get; set; }
        public virtual JobClientStatus ClientStatusNavigation { get; set; }
        public virtual Contact CompletedByNavigation { get; set; }
        public virtual Contact CreatedByNavigation { get; set; }
        public virtual ApprovalStatus CustomerApprovalStatusNavigation { get; set; }
        public virtual Contact CustomerApprovedByNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual Contact DestinationContactNavigation { get; set; }
        public virtual Location DestinationLocationNavigation { get; set; }
        public virtual Contact InvoicedByNavigation { get; set; }
        public virtual JobStatus1 ItadstatusNavigation { get; set; }
        public virtual ApprovalStatus LogisticsApprovalStatusNavigation { get; set; }
        public virtual Contact LogisticsApprovedByNavigation { get; set; }
        public virtual LogisticsWorkflow LogisticsJobTypeNavigation { get; set; }
        public virtual LogisticsProvider LogisticsSupplierNavigation { get; set; }
        public virtual ProcessingWorkflow ProcessingJobTypeNavigation { get; set; }
        public virtual Project ProjectNavigation { get; set; }
        public virtual Contact ReadyToInvoiceByNavigation { get; set; }
        public virtual SettlementModel RemarketingSettlementModelNavigation { get; set; }
        public virtual Contact RequestedByNavigation { get; set; }
        public virtual JobType RequestedJobTypeNavigation { get; set; }
        public virtual ApprovalStatus SalesApprovalStatusNavigation { get; set; }
        public virtual Contact SalesApprovedByNavigation { get; set; }
        public virtual Contact SourceContactNavigation { get; set; }
        public virtual Location SourceLocationNavigation { get; set; }
        public virtual Contact SubmittedByNavigation { get; set; }
    }
}
