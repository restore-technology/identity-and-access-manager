﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class SettlementModel
    {
        public SettlementModel()
        {
            CustomerInvoices = new HashSet<CustomerInvoice>();
            Jobs = new HashSet<Job>();
            Jobs4444s = new HashSet<Jobs4444>();
        }

        public int SettlementModelId { get; set; }
        public string SettlementModel1 { get; set; }

        public virtual ICollection<CustomerInvoice> CustomerInvoices { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444s { get; set; }
    }
}
