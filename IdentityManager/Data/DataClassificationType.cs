﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class DataClassificationType
    {
        public DataClassificationType()
        {
            Jobs = new HashSet<Job>();
        }

        public int Id { get; set; }
        public string DataClassificationName { get; set; }

        public virtual ICollection<Job> Jobs { get; set; }
    }
}
