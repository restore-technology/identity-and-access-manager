﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingLiveForecastReport
    {
        public Guid Id { get; set; }
        public int Reference { get; set; }
        public bool IsSerializedCollection { get; set; }
        public string RequestedJobType { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public int? ProcessingSystemJobId { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string PrimaryContact { get; set; }
        public string CurrentWorkflow { get; set; }
        public string ReportGrouping { get; set; }
        public int? GroupingOrder { get; set; }
        public string AssetType { get; set; }
        public int? Expected { get; set; }
        public int? Collected { get; set; }
        public int? Received { get; set; }
        public int? Wip { get; set; }
        public int? Completed { get; set; }
        public decimal? ExpectedTotal { get; set; }
        public decimal? CollectedTotal { get; set; }
        public decimal? ReceivedTotal { get; set; }
        public decimal? WipTotal { get; set; }
        public decimal? CompletedTotal { get; set; }
        public int? NewExpected { get; set; }
        public int? NewCollected { get; set; }
        public int? NewReceived { get; set; }
        public int? NewWip { get; set; }
        public int? NewCompleted { get; set; }
    }
}
