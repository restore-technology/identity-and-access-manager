﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Apiworkflow
    {
        public Apiworkflow()
        {
            TaskApiworkflowMappings = new HashSet<TaskApiworkflowMapping>();
        }

        public int ApiworkflowId { get; set; }
        public string ApiworkflowName { get; set; }
        public bool? ApiworkflowStatus { get; set; }

        public virtual ICollection<TaskApiworkflowMapping> TaskApiworkflowMappings { get; set; }
    }
}
