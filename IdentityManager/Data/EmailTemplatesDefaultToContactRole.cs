﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailTemplatesDefaultToContactRole
    {
        public Guid EmailTemplateId { get; set; }
        public Guid DefaultToContactRoleId { get; set; }

        public virtual ContactRole DefaultToContactRole { get; set; }
        public virtual EmailTemplate EmailTemplate { get; set; }
    }
}
