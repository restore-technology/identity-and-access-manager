﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LotErasureReport
    {
        public long Id { get; set; }
        public Guid? ReportId { get; set; }
        public string AssetId { get; set; }
        public int? LotNumber { get; set; }
    }
}
