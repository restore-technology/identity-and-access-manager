﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ApplicationEvent
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        public string Event { get; set; }
        public string ItemType { get; set; }
        public string ItemKey { get; set; }
        public string Data { get; set; }
        public string Ip { get; set; }
    }
}
