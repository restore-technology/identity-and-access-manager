﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ContractStatus
    {
        public int ContractStatusId { get; set; }
        public string ContractStatusName { get; set; }
    }
}
