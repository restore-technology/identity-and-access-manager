﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingWorkflowStatusDatum
    {
        public Guid JobGuid { get; set; }
        public DateTime? WorkflowDate { get; set; }
        public int JobReference { get; set; }
        public Guid CustomerGuid { get; set; }
        public string SerialNumber { get; set; }
        public int? Stnumber { get; set; }
        public string CustomerAssetTag { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string AssetType { get; set; }
        public DateTime? CollectionCompleteDate { get; set; }
        public string SourceLocation { get; set; }
    }
}
