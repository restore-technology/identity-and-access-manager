﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingDellDataJobNumber
    {
        public int? JobNumber { get; set; }
    }
}
