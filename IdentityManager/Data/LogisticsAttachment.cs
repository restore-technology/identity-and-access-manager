﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsAttachment
    {
        public long AttachmentId { get; set; }
        public long? JobId { get; set; }
        public string AttachmentFile { get; set; }
        public string Comment { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool? AvailableOnDevice { get; set; }
        public string JobStatus { get; set; }
        public bool? CreatedOnDevice { get; set; }
        public bool? VisibleToCustomer { get; set; }
    }
}
