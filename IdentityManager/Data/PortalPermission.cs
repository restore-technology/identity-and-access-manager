﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class PortalPermission
    {
        public long PortalPermissionId { get; set; }
        public string PortalPermissionName { get; set; }
        public int? ParentId { get; set; }
        public int? ChildId { get; set; }
        public bool? IsActive { get; set; }
    }
}
