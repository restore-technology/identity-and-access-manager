﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ApprovalStatus
    {
        public ApprovalStatus()
        {
            JobCustomerApprovalStatusNavigations = new HashSet<Job>();
            JobLogisticsApprovalStatusNavigations = new HashSet<Job>();
            JobSalesApprovalStatusNavigations = new HashSet<Job>();
            Jobs4444CustomerApprovalStatusNavigations = new HashSet<Jobs4444>();
            Jobs4444LogisticsApprovalStatusNavigations = new HashSet<Jobs4444>();
            Jobs4444SalesApprovalStatusNavigations = new HashSet<Jobs4444>();
            QualityControlOperationApprovalStatusNavigations = new HashSet<QualityControl>();
            QualityControlSalesApprovalStatusNavigations = new HashSet<QualityControl>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }

        public virtual ICollection<Job> JobCustomerApprovalStatusNavigations { get; set; }
        public virtual ICollection<Job> JobLogisticsApprovalStatusNavigations { get; set; }
        public virtual ICollection<Job> JobSalesApprovalStatusNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444CustomerApprovalStatusNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444LogisticsApprovalStatusNavigations { get; set; }
        public virtual ICollection<Jobs4444> Jobs4444SalesApprovalStatusNavigations { get; set; }
        public virtual ICollection<QualityControl> QualityControlOperationApprovalStatusNavigations { get; set; }
        public virtual ICollection<QualityControl> QualityControlSalesApprovalStatusNavigations { get; set; }
    }
}
