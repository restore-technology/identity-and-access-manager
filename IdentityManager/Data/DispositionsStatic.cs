﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class DispositionsStatic
    {
        public int StreamsSerialNo { get; set; }
        public int? InboundProductionNo { get; set; }
        public string AssetType { get; set; }
        public string OemserialNo { get; set; }
        public string AssetTag { get; set; }
        public string Oem { get; set; }
        public string ModelDescription { get; set; }
        public string EquipmentDescription { get; set; }
        public string Processor { get; set; }
        public string Speed { get; set; }
        public string MemoryMb { get; set; }
        public int? HardDriveCount { get; set; }
        public int? HardDriveWiped { get; set; }
        public int? HardDriveDestroyed { get; set; }
        public string Hddsize { get; set; }
        public string Hddtype { get; set; }
        public string MonitorSize { get; set; }
        public string StreamsDisposition { get; set; }
        public string ConditionCode { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? PartsCharges { get; set; }
        public decimal? NetOutcome { get; set; }
        public string StatusName { get; set; }
        public string WasteTransferNo { get; set; }
        public DateTime? ShippedDate { get; set; }
        public DateTime? InvoicedDate { get; set; }
        public byte? IsAuthorised { get; set; }
        public int? SalesOrderNo { get; set; }
        public string InventoryStatus { get; set; }
        public decimal? ClientEarnings { get; set; }
        public DateTime? OutboundCompletedAt { get; set; }
        public DateTime? OutboundInvoicedAt { get; set; }
        public Guid InboundJobId { get; set; }
        public int InboundReference { get; set; }
        public string InboundPonumber { get; set; }
        public string InboundCustomerNumber { get; set; }
        public string InboundCustomerName { get; set; }
        public string InboundRequestedBy { get; set; }
        public string InboundOriginatingLocation { get; set; }
        public string InboundDestinationLocation { get; set; }
        public DateTime? InboundCollectionDate { get; set; }
        public string InboundBillingCode { get; set; }
        public string InboundProject { get; set; }
        public DateTime? InboundActualCollectionDate { get; set; }
        public DateTime? InboundActualReceivedDate { get; set; }
        public int? InboundJobNo { get; set; }
        public int? OutboundAssetId { get; set; }
        public int? OutboundJobId { get; set; }
        public int? OutboundReference { get; set; }
        public string OutboundPonumber { get; set; }
        public int? OutboundCustomerNumber { get; set; }
        public string OutboundCustomerName { get; set; }
        public int? OutboundRequestedBy { get; set; }
        public string OutboundDestinationLocation { get; set; }
        public int? OutboundJobNo2 { get; set; }
        public decimal? OutboundOrderValue { get; set; }
        public DateTime? OutboundShippedDate { get; set; }
        public DateTime? OutboundInvoicedDate { get; set; }
        public int OutboundJobNo { get; set; }
        public string StreamsDispositionFinal { get; set; }
    }
}
