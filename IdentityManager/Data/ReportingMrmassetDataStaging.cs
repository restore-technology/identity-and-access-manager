﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingMrmassetDataStaging
    {
        public int StreamsSerialNo { get; set; }
        public int? InboundProductionNo { get; set; }
        public Guid? InboundCustomerGuid { get; set; }
        public string InboundCustomerNumber { get; set; }
        public string InboundCustomerName { get; set; }
        public string InboundMasterCompany { get; set; }
        public string AssetType { get; set; }
        public string AssetSubType { get; set; }
        public string OemserialNo { get; set; }
        public string Oem { get; set; }
        public string ModelDescription { get; set; }
        public string EquipmentDescription { get; set; }
        public string Processor { get; set; }
        public string Speed { get; set; }
        public string MemoryMb { get; set; }
        public int? HardDriveCount { get; set; }
        public string Hddsize { get; set; }
        public string Hddtype { get; set; }
        public string MonitorSize { get; set; }
        public string BiosData { get; set; }
        public string BiosDate { get; set; }
        public string HddservicePerformed { get; set; }
        public string HddserviceStatus { get; set; }
        public string GradeAudit { get; set; }
        public string ResaleChannel { get; set; }
        public string ConditionCode { get; set; }
        public string StatusName { get; set; }
        public int? Qty { get; set; }
        public int? QtyOnHand { get; set; }
        public string SecurityStatus { get; set; }
        public string ConditionComments { get; set; }
        public string PalletId { get; set; }
        public string WarehouseLocation { get; set; }
        public string Warehouse { get; set; }
        public string AssetTag { get; set; }
        public int? HardDriveWiped { get; set; }
        public int? HardDriveDestroyed { get; set; }
        public string WasteTransferNo { get; set; }
        public DateTime? ShippedDate { get; set; }
        public DateTime? InvoicedDate { get; set; }
        public byte? IsAuthorised { get; set; }
        public int? SalesOrderNo { get; set; }
        public string StreamsAssetType { get; set; }
        public string StreamsDispositionCalc { get; set; }
        public string InventoryStatus { get; set; }
        public string ReportingStatus { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? SalePriceUnfiltered { get; set; }
        public decimal? PartsCharges { get; set; }
        public decimal? ClientEarnings { get; set; }
        public decimal? TotalServices { get; set; }
        public decimal? NetOutcome { get; set; }
        public decimal? StandardServices { get; set; }
        public decimal? AdditionalServices { get; set; }
        public decimal? FairMarketValue { get; set; }
        public decimal? ResalePrice { get; set; }
        public decimal? SettlementValue { get; set; }
        public decimal? CommissionRate { get; set; }
    }
}
