﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class InvoiceHistoryOld
    {
        public long InvoiceHistoryId { get; set; }
        public Guid? MasterCompanyId { get; set; }
        public string InvoiceNumber { get; set; }
        public Guid? CustomerId { get; set; }
        public Guid? PrimaryContactId { get; set; }
        public Guid? SecondaryContactId { get; set; }
        public Guid? JobId { get; set; }
        public decimal? NetInvoiceValue { get; set; }
        public decimal? Vatamount { get; set; }
        public decimal? GrossInvoiceValue { get; set; }
        public bool? IsVatcharged { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
