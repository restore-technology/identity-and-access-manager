﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class WorkflowStage
    {
        public WorkflowStage()
        {
            WorkflowSteps = new HashSet<WorkflowStep>();
        }

        public Guid Id { get; set; }
        public Guid? StageType { get; set; }
        public Guid Job { get; set; }
        public int Order { get; set; }
        public Guid? Status { get; set; }

        public virtual Job JobNavigation { get; set; }
        public virtual StageType StageTypeNavigation { get; set; }
        public virtual WorkflowStageStatus StatusNavigation { get; set; }
        public virtual WorkflowFinishStage WorkflowFinishStage { get; set; }
        public virtual WorkflowLogisticsInhouseStage WorkflowLogisticsInhouseStage { get; set; }
        public virtual WorkflowLogisticsThirdpartyStage WorkflowLogisticsThirdpartyStage { get; set; }
        public virtual WorkflowProcessingInboundStage WorkflowProcessingInboundStage { get; set; }
        public virtual WorkflowProcessingOutboundStage WorkflowProcessingOutboundStage { get; set; }
        public virtual WorkflowStartStage WorkflowStartStage { get; set; }
        public virtual ICollection<WorkflowStep> WorkflowSteps { get; set; }
    }
}
