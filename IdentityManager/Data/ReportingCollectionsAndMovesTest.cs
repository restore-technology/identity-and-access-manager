﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingCollectionsAndMovesTest
    {
        public int PromptCollectionNo { get; set; }
        public DateTime? PromptCollectionDate { get; set; }
        public string PromptProject { get; set; }
        public string PromptBillingCode { get; set; }
        public Guid PromptCustomerGuid { get; set; }
        public string PromptCustomerNo { get; set; }
        public string PromptCustomerName { get; set; }
        public int JobNo { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string Requestor { get; set; }
        public string OriginatingLocation { get; set; }
        public string DestinationLocation { get; set; }
        public string CustomerReference { get; set; }
        public DateTime? CollectionDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public string Class { get; set; }
        public string SubClass { get; set; }
        public int PartsQuantityFree { get; set; }
        public int PartsQuantityOnHand { get; set; }
        public string SecurityStatus { get; set; }
        public string SerialNumber { get; set; }
        public string CustomerAssetId { get; set; }
        public int AssetId { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string AuditDescription { get; set; }
        public string AuditCputype { get; set; }
        public string AuditCpuspeed { get; set; }
        public string AuditTotalMemory { get; set; }
        public string AuditHddcapacity { get; set; }
        public string AuditHddtype { get; set; }
        public string AuditScreenSize { get; set; }
        public string ResaleChannel { get; set; }
        public string GradeAudit { get; set; }
        public string ConditionComments { get; set; }
        public decimal StandardServices { get; set; }
        public decimal? AdditionalServices { get; set; }
        public decimal? TotalServices { get; set; }
        public int? HardDriveCount { get; set; }
        public int? HardDriveWiped { get; set; }
        public int? HardDriveDestroyed { get; set; }
        public decimal JobRecyclingCharges { get; set; }
    }
}
