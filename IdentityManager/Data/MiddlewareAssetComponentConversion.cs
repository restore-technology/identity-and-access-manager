﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class MiddlewareAssetComponentConversion
    {
        public int AssetComponentId { get; set; }
        public string ComponentType { get; set; }
        public string Mrmcomponent { get; set; }
        public string Ltscomponent { get; set; }
        public DateTime DateAdded { get; set; }
        public byte AddedManually { get; set; }
    }
}
