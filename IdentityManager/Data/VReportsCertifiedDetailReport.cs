﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsCertifiedDetailReport
    {
        public int? StreamsJobNumber { get; set; }
        public int? MrmLotNumber { get; set; }
        public int? StNo { get; set; }
        public string OemSerialNo { get; set; }
        public string AssetTag { get; set; }
        public string AssetClass { get; set; }
        public string Oem { get; set; }
        public string ModelNo { get; set; }
        public string ModelDescription { get; set; }
        public string CpuArchitecture { get; set; }
        public string Memory { get; set; }
        public int? NoOfHdds { get; set; }
        public string FirstHddSize { get; set; }
        public string FirstHddType { get; set; }
        public string MonitorSize { get; set; }
        public string MonitorType { get; set; }
        public string ConditionCode { get; set; }
        public string CpuSpeed { get; set; }
        public string Comment { get; set; }
    }
}
