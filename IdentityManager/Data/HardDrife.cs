﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class HardDrife
    {
        public int OriginLot { get; set; }
        public int AssetId { get; set; }
        public int ParentAssetId { get; set; }
        public string ServicePerformed { get; set; }
        public string ServiceStatus { get; set; }
        public string Capacity { get; set; }
        public string Interface { get; set; }
        public string ParentClass { get; set; }
        public string SerialNumber { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string ServiceAssigned { get; set; }
        public string CustomerAssetId { get; set; }
    }
}
