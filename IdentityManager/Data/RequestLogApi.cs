﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class RequestLogApi
    {
        public long RequestLogId { get; set; }
        public string CustomerNumber { get; set; }
        public string Apikey { get; set; }
        public DateTime? RequestedDateTime { get; set; }
        public string ItemQueried { get; set; }
        public string ItemGrade { get; set; }
        public bool? IsMatch { get; set; }
        public decimal? ItemValue { get; set; }
        public string ReturnMessage { get; set; }
    }
}
