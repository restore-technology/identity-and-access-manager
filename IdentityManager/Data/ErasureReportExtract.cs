﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ErasureReportExtract
    {
        public int DepotId { get; set; }
        public string DepotName { get; set; }
        public int? WareHouseId { get; set; }
        public string WarehouseName { get; set; }
        public int? OriginLot { get; set; }
        public int? ParentAssetId { get; set; }
        public string SerialNumber { get; set; }
        public string Capacity { get; set; }
        public string ServiceCompletedBy { get; set; }
        public DateTime? ServiceCompletedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
    }
}
