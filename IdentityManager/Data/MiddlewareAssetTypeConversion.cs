﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class MiddlewareAssetTypeConversion
    {
        public int AssetTypeId { get; set; }
        public string Itadclass { get; set; }
        public string ItadsubClass { get; set; }
        public string Ltseqtype { get; set; }
        public string LtseqsubType { get; set; }
    }
}
