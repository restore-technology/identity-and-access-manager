﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportParameterContextValue
    {
        public ReportParameterContextValue()
        {
            SqlReportParameters = new HashSet<SqlReportParameter>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SqlReportParameter> SqlReportParameters { get; set; }
    }
}
