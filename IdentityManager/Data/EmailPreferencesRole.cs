﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailPreferencesRole
    {
        public long EmailPreferenceContactRoles { get; set; }
        public long EmailPreferenceContactId { get; set; }
        public Guid ContactRoles { get; set; }
        public bool IsChecked { get; set; }

        public virtual EmailPreferencesContact EmailPreferenceContact { get; set; }
    }
}
