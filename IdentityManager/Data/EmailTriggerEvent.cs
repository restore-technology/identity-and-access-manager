﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailTriggerEvent
    {
        public EmailTriggerEvent()
        {
            EmailTemplates = new HashSet<EmailTemplate>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string MandatoryPlaceholders { get; set; }

        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
    }
}
