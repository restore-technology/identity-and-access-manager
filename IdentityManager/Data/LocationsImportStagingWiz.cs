﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LocationsImportStagingWiz
    {
        public Guid? Id { get; set; }
        public int Reference { get; set; }
        public Guid Customer { get; set; }
        public bool IsHeadOffice { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public Guid Country { get; set; }
        public string Postcode { get; set; }
        public bool LoadingBay { get; set; }
        public bool GoodsLift { get; set; }
        public string Comments { get; set; }
        public string AddressReference { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string LogisticsSystemId { get; set; }
        public bool IsActive { get; set; }
        public bool? IsBillingEntity { get; set; }
        public bool? IsSecureLoadingArea { get; set; }
        public string FromAvoidTime { get; set; }
        public string ToAvoidTime { get; set; }
        public string DriverComments { get; set; }
        public bool? IsLoadingAreaCctv { get; set; }
        public bool? IsInCongestionZone { get; set; }
        public bool? IsInUlezzone { get; set; }
        public string ParkingDistance { get; set; }
        public int? VehicleSizeLimit { get; set; }
        public string AttachmentFileName { get; set; }
        public string ProcessGivingRiseToWaste { get; set; }
        public string Siccode { get; set; }
        public string LocalAuthority { get; set; }
        public string WasteLicenceType { get; set; }
        public string WasteLicenceNumber { get; set; }
        public string WastenoteDeliveryEmail { get; set; }
        public bool? IsProducerOfWaste { get; set; }
        public bool? ProducesHazardousWaste { get; set; }
    }
}
