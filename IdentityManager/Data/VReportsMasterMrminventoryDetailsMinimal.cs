﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsMasterMrminventoryDetailsMinimal
    {
        public int AssetId { get; set; }
        public int? OriginLot { get; set; }
        public string Class { get; set; }
        public string Manufacturer { get; set; }
        public string SerialNumber { get; set; }
        public string CustomerAssetNumber { get; set; }
        public string Grade { get; set; }
        public string AuditCputype { get; set; }
        public string AuditCpuspeed { get; set; }
        public string AuditTotalMemory { get; set; }
        public string AuditHddcapacity { get; set; }
        public string AuditHddtype { get; set; }
        public string AuditScreenSize { get; set; }
        public int? HardDriveCount { get; set; }
        public string AuditDescription { get; set; }
        public string GradeAudit { get; set; }
        public string StatusName { get; set; }
        public string ResaleChannel { get; set; }
        public string TrueModelDescription { get; set; }
    }
}
