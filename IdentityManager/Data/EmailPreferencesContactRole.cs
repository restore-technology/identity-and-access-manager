﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailPreferencesContactRole
    {
        public Guid EmailPreferenceId { get; set; }
        public Guid ContactRoleId { get; set; }

        public virtual ContactRole ContactRole { get; set; }
        public virtual EmailPreference EmailPreference { get; set; }
    }
}
