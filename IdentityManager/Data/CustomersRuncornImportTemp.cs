﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomersRuncornImportTemp
    {
        public string Name { get; set; }
        public string CustomerNumber { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public Guid? Type { get; set; }
        public string CustomizableField1Name { get; set; }
        public string CustomizableField2Name { get; set; }
        public string CustomizableField3Name { get; set; }
        public bool IsRedemtech { get; set; }
        public bool SerialisedCollections { get; set; }
        public bool AddingAssetsViaSerialisedCollections { get; set; }
        public string LogisticsSystemId { get; set; }
        public int ProcessingSystemId { get; set; }
        public bool IsActive { get; set; }
        public bool JobsRequireApproval { get; set; }
        public Guid? PrimaryContact { get; set; }
        public Guid? SecondaryContact { get; set; }
        public string CustomerSpecificContent { get; set; }
        public string CustomerApikey { get; set; }
        public bool? GrantApiaccess { get; set; }
        public string DataApiaccess { get; set; }
        public bool? AllowDeploymentOrder { get; set; }
        public int? DeliveryTimescale { get; set; }
        public string AllowedGrades { get; set; }
        public bool? ModelManagement { get; set; }
        public Guid? MasterCompanyId { get; set; }
        public bool? UseGlobalInvoiceLayout { get; set; }
        public string CustomInvoiceTemplate { get; set; }
        public bool? ChargedVat { get; set; }
        public decimal? RemarketingPercentage { get; set; }
        public bool? IsMandatoryCustomizableField1 { get; set; }
        public bool? IsMandatoryCustomizableField2 { get; set; }
        public bool? IsMandatoryCustomizableField3 { get; set; }
        public bool? HideCancelledJobs { get; set; }
        public int? ServiceLevelId { get; set; }
        public string Collection { get; set; }
        public string PayMethod { get; set; }
        public decimal? CreditLimit { get; set; }
        public string PaymentTerms { get; set; }
        public string CompanyRegNo { get; set; }
        public string VatReg { get; set; }
        public string TaxCode { get; set; }
        public bool? BadDebtor { get; set; }
        public string CreditController { get; set; }
        public string WebAddress { get; set; }
        public string AccountStatus { get; set; }
        public string Group { get; set; }
        public string CorpType { get; set; }
        public string Telemarketing { get; set; }
        public string Comments { get; set; }
        public bool? PORequired { get; set; }
        public bool? CorporateAccount { get; set; }
        public string Company { get; set; }
        public bool? Currency { get; set; }
        public DateTime? CuDatePutin { get; set; }
        public DateTime? CuDateEdited { get; set; }
        public DateTime? AdDatePutin { get; set; }
        public DateTime? AdDateEdited { get; set; }
        public string Branch { get; set; }
        public long? DepotSettingsId { get; set; }
        public string ExternalDataNotes { get; set; }
        public string ExternalData1 { get; set; }
        public string ExternalData2 { get; set; }
        public string ExternalData3 { get; set; }
        public bool? Contract { get; set; }
        public bool? CreditChecked { get; set; }
        public bool? SubProcessorAgreement { get; set; }
        public int? Sla { get; set; }
        public int? Adisadiallevel { get; set; }
        public string Adisadialreference { get; set; }
        public Guid? ParentCompanyId { get; set; }
        public Guid? ResellerCustomerId { get; set; }
        public int? ContractStatusId { get; set; }
        public int? Sladays { get; set; }
    }
}
