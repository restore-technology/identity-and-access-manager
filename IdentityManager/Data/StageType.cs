﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class StageType
    {
        public StageType()
        {
            WorkflowStages = new HashSet<WorkflowStage>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<WorkflowStage> WorkflowStages { get; set; }
    }
}
