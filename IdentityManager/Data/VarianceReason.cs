﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VarianceReason
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool? IsReceiving { get; set; }
    }
}
