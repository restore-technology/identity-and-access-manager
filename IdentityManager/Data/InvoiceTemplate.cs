﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class InvoiceTemplate
    {
        public long InvoiceTemplateId { get; set; }
        public string TemplateHeader { get; set; }
        public string TemplateBody { get; set; }
        public string TemplateBodyMultiple { get; set; }
        public string TemplateFooter { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreditNoteTemplate { get; set; }
    }
}
