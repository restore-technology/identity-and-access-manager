﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class SqlReportsAccessGroup
    {
        public Guid SqlReportId { get; set; }
        public Guid AccessGroupId { get; set; }

        public virtual AccessGroup AccessGroup { get; set; }
        public virtual SqlReport SqlReport { get; set; }
    }
}
