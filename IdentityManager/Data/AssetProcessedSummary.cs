﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class AssetProcessedSummary
    {
        public int? DepotId { get; set; }
        public string DepotName { get; set; }
        public string TechId { get; set; }
        public string TechName { get; set; }
        public int? TodaysCount { get; set; }
        public int? WeeksTotal { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
    }
}
