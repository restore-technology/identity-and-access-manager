﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Country
    {
        public Country()
        {
            Locations = new HashSet<Location>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }

        public virtual ICollection<Location> Locations { get; set; }
    }
}
