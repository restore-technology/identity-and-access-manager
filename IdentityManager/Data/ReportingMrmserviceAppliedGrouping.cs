﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingMrmserviceAppliedGrouping
    {
        public int ServiceAppliedId { get; set; }
        public string ServiceAppliedDetail { get; set; }
        public string ServiceAppliedGroup { get; set; }
    }
}
