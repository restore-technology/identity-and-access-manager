﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogSeverityLevel
    {
        public int SeverityLevelId { get; set; }
        public string Description { get; set; }
    }
}
