﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsStatisticsDashboard
    {
        public long LogisticsStatisticsId { get; set; }
        public string DepotName { get; set; }
        public int? JobNumber { get; set; }
        public DateTime? JobSubmittedAt { get; set; }
        public string LogisticsUser { get; set; }
        public DateTime? JobCompletedAt { get; set; }
        public int? ExpectedAssets { get; set; }
        public int? CollectedAssets { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
