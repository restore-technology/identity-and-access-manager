﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingLiveForecastReportDashboard
    {
        public DateTime? ThisWeekStartDate { get; set; }
        public DateTime? NextWeekStartDate { get; set; }
        public int? ExpectedThisWeek { get; set; }
        public int? ExpectedNextWeek { get; set; }
        public int? ExpectedFuture { get; set; }
        public int? Collected { get; set; }
        public int? Received { get; set; }
        public int? Wip { get; set; }
    }
}
