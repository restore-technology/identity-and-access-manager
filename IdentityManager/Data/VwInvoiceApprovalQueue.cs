﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VwInvoiceApprovalQueue
    {
        public Guid JobId { get; set; }
        public Guid? CustomerId { get; set; }
        public Guid? RequestedJobTypeId { get; set; }
        public long? DepotSettingsId { get; set; }
        public int? ServiceLevelId { get; set; }
        public Guid? ProjectId { get; set; }
        public Guid? BillingCodeId { get; set; }
        public Guid? ContactId { get; set; }
        public int Reference { get; set; }
        public string AccountingBranch { get; set; }
        public string Contact { get; set; }
        public string Customer { get; set; }
        public DateTime RequestedDate { get; set; }
        public string Project { get; set; }
        public string Ponumber { get; set; }
        public string BillingCode { get; set; }
        public string JobType { get; set; }
        public int? ChargingModel { get; set; }
        public int InvoiceValue { get; set; }
        public string JobStatus { get; set; }
        public string StageName { get; set; }
        public Guid? BillingEntityId { get; set; }
        public string AddressReference { get; set; }
        public string CustomerDefaultEntity { get; set; }
        public DateTime? CompletedAt { get; set; }
        public bool? IsDeferredInvoicing { get; set; }
    }
}
