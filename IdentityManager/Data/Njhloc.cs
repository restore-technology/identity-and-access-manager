﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Njhloc
    {
        public string Code { get; set; }
        public double? Acc { get; set; }
        public string Name { get; set; }
        public string AddressReference { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public Guid? CustId { get; set; }
    }
}
