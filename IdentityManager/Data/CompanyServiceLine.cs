﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CompanyServiceLine
    {
        public long CompanyServiceLineId { get; set; }
        public Guid? MasterCompanyId { get; set; }
        public long? ServiceLineId { get; set; }
        public bool? IsAvailable { get; set; }
        public decimal? ApplicableCharge { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? DepotSettingsId { get; set; }
    }
}
