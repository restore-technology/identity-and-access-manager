﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsCheckListQuestion
    {
        public long CheckListQuestionId { get; set; }
        public long? CheckListId { get; set; }
        public string Type { get; set; }
        public string Notes { get; set; }
        public Guid? Uuid { get; set; }
        public string OrderId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public bool? Failed { get; set; }
    }
}
