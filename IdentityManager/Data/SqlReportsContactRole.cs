﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class SqlReportsContactRole
    {
        public Guid SqlReportId { get; set; }
        public Guid ContactRoleId { get; set; }

        public virtual ContactRole ContactRole { get; set; }
        public virtual SqlReport SqlReport { get; set; }
    }
}
