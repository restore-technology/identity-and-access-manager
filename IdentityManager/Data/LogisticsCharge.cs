﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsCharge
    {
        public long LogisticsChargesId { get; set; }
        public int? LogisticsChargingModelId { get; set; }
        public int? ServiceLevelId { get; set; }
        public bool? IsChecked { get; set; }
        public decimal? LogisticsCharge1 { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
