﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class CustomerInvoiceAssetsDefault
    {
        public int CustomerInvoiceAssetsDefaultsId { get; set; }
        public int? AssetClassId { get; set; }
        public int? PerAssetChargeTypeId { get; set; }
        public int? DefaultValue { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
