﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingAuthorisedRecyclingPartner
    {
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public Guid? StreamsCustomerGuid { get; set; }
        public string StreamsCustomerNumber { get; set; }
    }
}
