﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailQueueItem
    {
        public bool Deleted { get; set; }
        public Guid Id { get; set; }
        public string Body { get; set; }
        public DateTime Date { get; set; }
        public bool EnableSsl { get; set; }
        public bool Html { get; set; }
        public string SenderAddress { get; set; }
        public string SenderName { get; set; }
        public string Subject { get; set; }
        public string To { get; set; }
        public string Attachments { get; set; }
        public string Bcc { get; set; }
        public string Cc { get; set; }
        public int Retries { get; set; }
        public string VcalendarView { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SmtpHost { get; set; }
        public int? SmtpPort { get; set; }
        public string Category { get; set; }
    }
}
