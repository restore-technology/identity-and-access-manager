﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailPreferencesContact1
    {
        public Guid EmailPreferenceId { get; set; }
        public Guid ContactId { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual EmailPreference EmailPreference { get; set; }
    }
}
