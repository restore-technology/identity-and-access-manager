﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class LogisticsPhoto
    {
        public long PhotoId { get; set; }
        public long? JobId { get; set; }
        public string Annotation { get; set; }
        public string FilePath { get; set; }
        public DateTime? CreatedAt { get; set; }
        public Guid? LinkedUuid { get; set; }
        public string PhotoType { get; set; }
    }
}
