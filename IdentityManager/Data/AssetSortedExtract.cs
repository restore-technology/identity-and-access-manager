﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class AssetSortedExtract
    {
        public int? DockId { get; set; }
        public string DockName { get; set; }
        public Guid? TechId { get; set; }
        public string TechName { get; set; }
        public DateTime? DateCompleted { get; set; }
        public int? AssetId { get; set; }
        public string Class { get; set; }
        public DateTime? DateEnteredSort { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
    }
}
