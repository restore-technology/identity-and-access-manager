﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class DeploymentAssetsTemp
    {
        public long DeploymentAssetsTempId { get; set; }
        public Guid CustomerId { get; set; }
        public int AssetRecId { get; set; }
        public int CategoryId { get; set; }
        public int ManufacturerId { get; set; }
        public int ProductId { get; set; }
        public int? ProductNumber { get; set; }
        public string SerialNumber { get; set; }
        public int? Grade { get; set; }
        public string CustomerAssetNumber { get; set; }
        public string AuditDescription { get; set; }
    }
}
