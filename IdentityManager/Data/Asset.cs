﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Asset
    {
        public Asset()
        {
            JobAssets = new HashSet<JobAsset>();
        }

        public Guid Id { get; set; }
        public int Reference { get; set; }
        public Guid AssetType { get; set; }
        public Guid Customer { get; set; }
        public string SerialNumber { get; set; }
        public int? Stnumber { get; set; }
        public string CustomerAssetTag { get; set; }
        public Guid? Status { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
        public string Condition { get; set; }
        public string Disposition { get; set; }
        public string Comment { get; set; }
        public decimal? DataSecurityCharge { get; set; }
        public int? DataSecurityQuantity { get; set; }
        public decimal? DiscoveryCharge { get; set; }
        public int? DiscoveryQuantity { get; set; }
        public decimal? OtherCharge { get; set; }
        public int? OtherQuantity { get; set; }
        public decimal? RecyclingCharge { get; set; }
        public int? RecyclingQuantity { get; set; }
        public decimal? RepairCharge { get; set; }
        public int? RepairQuantity { get; set; }
        public int? ServiceTotalQuantity { get; set; }
        public decimal? TestingCharge { get; set; }
        public int? TestingQuantity { get; set; }

        public virtual AssetType AssetTypeNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual AssetStatus StatusNavigation { get; set; }
        public virtual ICollection<JobAsset> JobAssets { get; set; }
    }
}
