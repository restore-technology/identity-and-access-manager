﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class MrmlotIjobIdSync
    {
        public int Id { get; set; }
        public Guid? JobId { get; set; }
        public DateTime? LastSyncDateTime { get; set; }
    }
}
