﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ErasureReportSummary
    {
        public int? DepotId { get; set; }
        public string DepotName { get; set; }
        public int? TodaysCount { get; set; }
        public int? ThisWeekCount { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
    }
}
