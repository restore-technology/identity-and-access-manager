﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobsTemp
    {
        public Guid? JobId { get; set; }
        public int? Reference { get; set; }
        public Guid? Itadstatus { get; set; }
    }
}
