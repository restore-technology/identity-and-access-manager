﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class EmailTemplate
    {
        public EmailTemplate()
        {
            EmailPreferences = new HashSet<EmailPreference>();
            EmailTemplatesContactRoles = new HashSet<EmailTemplatesContactRole>();
            EmailTemplatesDefaultToContactRoles = new HashSet<EmailTemplatesDefaultToContactRole>();
        }

        public Guid Id { get; set; }
        public int Reference { get; set; }
        public Guid EmailTriggerEvent { get; set; }
        public string Name { get; set; }
        public bool Internal { get; set; }
        public bool Active { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public virtual EmailTriggerEvent EmailTriggerEventNavigation { get; set; }
        public virtual ICollection<EmailPreference> EmailPreferences { get; set; }
        public virtual ICollection<EmailTemplatesContactRole> EmailTemplatesContactRoles { get; set; }
        public virtual ICollection<EmailTemplatesDefaultToContactRole> EmailTemplatesDefaultToContactRoles { get; set; }
    }
}
