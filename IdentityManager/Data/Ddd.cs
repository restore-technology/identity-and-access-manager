﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class Ddd
    {
        public string MasterCompany { get; set; }
        public int StreamsJobNo { get; set; }
        public int? LotId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNumber { get; set; }
        public string PrimaryContact { get; set; }
        public string JobType { get; set; }
        public int? AssetCount { get; set; }
        public int? DataStorageCount { get; set; }
        public string WorkflowStatus { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string DeploymentLocation { get; set; }
        public string InvoiceReference { get; set; }
        public string Rmcredit { get; set; }
        public int? AssetsRedeployed { get; set; }
        public int? AssetsSold { get; set; }
        public int? AssetsRecycled { get; set; }
        public int? AssetsInInventory { get; set; }
        public int? DaysOnShelf { get; set; }
        public int? DaysTurnaround { get; set; }
        public decimal? ProfitShare { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? PartsCharges { get; set; }
        public decimal? ClientEarnings { get; set; }
        public decimal? Rebate { get; set; }
        public decimal? AdHoc { get; set; }
        public decimal? Engineering { get; set; }
        public decimal? Logistics { get; set; }
        public decimal? AuditTrash { get; set; }
        public decimal? Service { get; set; }
        public DateTime? Finalsettlementavailablesince { get; set; }
    }
}
