﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingConsolidatedFinancial
    {
        public int StreamsJobNo { get; set; }
        public string ProjectName { get; set; }
        public string Rmcredit { get; set; }
        public int? LotId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNumber { get; set; }
        public string MasterCompany { get; set; }
        public string PrimaryContact { get; set; }
        public string JobType { get; set; }
        public string LotStatus { get; set; }
        public string SettlementStage { get; set; }
        public string BillingCode { get; set; }
        public int? AssetCount { get; set; }
        public int? InventoryCount { get; set; }
        public int? DaysOnShelf { get; set; }
        public int? DaysTurnaround { get; set; }
        public DateTime? ProductionCompletedOn { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string WorkflowStatus { get; set; }
        public string RequestedBy { get; set; }
        public DateTime? CollectionDate { get; set; }
        public string CollectionLocation { get; set; }
        public string DeploymentLocation { get; set; }
        public string Poreference { get; set; }
        public string InvoiceReference { get; set; }
        public int? CountRedeployed { get; set; }
        public int? CountSold { get; set; }
        public int? CountRecycled { get; set; }
        public int? DataStorageCount { get; set; }
        public string ChargeType { get; set; }
        public int ChargeTypeOrder { get; set; }
        public decimal? Total { get; set; }
        public string CustomizableField1 { get; set; }
        public DateTime? LastUpdate { get; set; }
        public DateTime AddDateTime { get; set; }
    }
}
