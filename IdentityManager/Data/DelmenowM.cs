﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class DelmenowM
    {
        public int OriginLot { get; set; }
        public int AssetRecId { get; set; }
        public string Class { get; set; }
        public string SubClass { get; set; }
        public string Manufacturer { get; set; }
        public string AuditDescription { get; set; }
        public string AuditCosmeticDescription { get; set; }
        public string AuditFunctionalDescription { get; set; }
        public string ResaleChannel { get; set; }
        public string SerialNumber { get; set; }
        public int? AssetTypeId { get; set; }
        public string ProductName { get; set; }
        public int? QuantityOnHand { get; set; }
        public int? QuantityFree { get; set; }
        public string GradeAudit { get; set; }
        public decimal? Weight { get; set; }
    }
}
