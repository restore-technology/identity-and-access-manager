﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class InvoiceHistory
    {
        public long ExportInvoiceId { get; set; }
        public Guid? CustomerId { get; set; }
        public Guid? JobId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string InvoiceType { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerOrderNumber { get; set; }
        public string JobNumber { get; set; }
        public Guid? SalesPersonId { get; set; }
        public string SalesPersonName { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string Currency { get; set; }
        public long? CustomerInvoiceType { get; set; }
        public string TaxCode { get; set; }
        public decimal? ValueNet { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? ValueGrossBase { get; set; }
        public decimal? ValueNetCurrency { get; set; }
        public decimal? VatamountCurrency { get; set; }
        public decimal? ValueGrossCurrency { get; set; }
        public string InvoiceDescription { get; set; }
        public string Glcode { get; set; }
        public string Analysis { get; set; }
        public bool? ImportStatus { get; set; }
        public Guid? CreatedBy { get; set; }
    }
}
