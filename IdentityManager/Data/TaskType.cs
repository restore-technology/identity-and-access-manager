﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class TaskType
    {
        public TaskType()
        {
            JobTaskParameters = new HashSet<JobTaskParameter>();
            TaskApiworkflowMappings = new HashSet<TaskApiworkflowMapping>();
        }

        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public bool? TaskStatus { get; set; }
        public bool? IsOtherServicesTask { get; set; }

        public virtual TaskParameterMapping TaskParameterMapping { get; set; }
        public virtual ICollection<JobTaskParameter> JobTaskParameters { get; set; }
        public virtual ICollection<TaskApiworkflowMapping> TaskApiworkflowMappings { get; set; }
    }
}
