﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ContactsRole
    {
        public Guid ContactId { get; set; }
        public Guid ContactRoleId { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual ContactRole ContactRole { get; set; }
    }
}
