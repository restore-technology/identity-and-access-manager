﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobNote
    {
        public Guid Id { get; set; }
        public Guid Job { get; set; }
        public DateTime CreationDate { get; set; }
        public Guid? CreatedBy { get; set; }
        public string Notes { get; set; }
        public string AttachmentFileName { get; set; }
        public Guid? StandardReportType { get; set; }
        public byte? VisibleToCustomer { get; set; }
        public bool? IsInvoice { get; set; }

        public virtual Contact CreatedByNavigation { get; set; }
        public virtual Job JobNavigation { get; set; }
        public virtual StandardReportType StandardReportTypeNavigation { get; set; }
    }
}
