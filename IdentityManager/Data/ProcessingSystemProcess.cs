﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ProcessingSystemProcess
    {
        public ProcessingSystemProcess()
        {
            JobAssets = new HashSet<JobAsset>();
            QualityControlOperationRequeueToNavigations = new HashSet<QualityControl>();
            QualityControlSalesRequeueToNavigations = new HashSet<QualityControl>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public int ProcessingSystemId { get; set; }

        public virtual ICollection<JobAsset> JobAssets { get; set; }
        public virtual ICollection<QualityControl> QualityControlOperationRequeueToNavigations { get; set; }
        public virtual ICollection<QualityControl> QualityControlSalesRequeueToNavigations { get; set; }
    }
}
