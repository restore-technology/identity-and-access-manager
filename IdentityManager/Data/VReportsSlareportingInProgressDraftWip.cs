﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class VReportsSlareportingInProgressDraftWip
    {
        public string CustomerName { get; set; }
        public string SourceLocnRef { get; set; }
        public string DestinationDepot { get; set; }
        public string ReceiveDock { get; set; }
        public string SourceAddr1 { get; set; }
        public string SourceAddr2 { get; set; }
        public string SourceCity { get; set; }
        public string SourceLocName { get; set; }
        public string CustomerNumber { get; set; }
        public int RecoveryNumber { get; set; }
        public int StreamJobNumber { get; set; }
        public string LogisticsType { get; set; }
        public string RecoveryStatus { get; set; }
        public DateTime? ReceivingDate { get; set; }
        public DateTime? CollectionDate { get; set; }
        public DateTime? Today { get; set; }
        public int? InProcessTimeline { get; set; }
        public int? ClientSla { get; set; }
        public int? Difference { get; set; }
        public string SlaonTarget { get; set; }
        public DateTime? DateToBeCompleted { get; set; }
        public int? Rttotal { get; set; }
        public int? RtinQc { get; set; }
        public int? Rtremaining { get; set; }
        public int Rtexpected { get; set; }
        public int Rtcollected { get; set; }
    }
}
