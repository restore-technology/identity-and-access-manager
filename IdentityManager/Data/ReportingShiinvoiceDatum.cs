﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class ReportingShiinvoiceDatum
    {
        public int Stnumber { get; set; }
        public string Ponumber { get; set; }
        public string EmployeeBadge { get; set; }
        public string WaybillNumber { get; set; }
    }
}
