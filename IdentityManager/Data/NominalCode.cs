﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class NominalCode
    {
        public int TableId { get; set; }
        public string NominalCode1 { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }
    }
}
