﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IdentityManager.Data
{
    public partial class JobStatus
    {
        public Guid JobId { get; set; }
        public int? Reference { get; set; }
        public Guid? JobStatusId { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
    }
}
