﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityManager.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace IdentityManager.Controllers
{
    public class PermissionAreasController : Controller
    {
        private readonly StreamsContext _context;
        private readonly IConfiguration _conf;

        public PermissionAreasController(StreamsContext context, IConfiguration conf)
        {
            _context = context;            
            _conf = conf;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult GetPermissionsAreas()
        {
            var list = _context.PermissionAreas.ToList<PermissionArea>();

            var jsonReturn = (from rec in list
                              select new
                              {
                                  Id = rec.Id,
                                  AreaName = rec.AreaName,
                                  Create = rec.Create,
                                  Read = rec.Read,
                                  Update = rec.Update,
                                  Delete = rec.Delete
                              }).ToList();

            return Json(jsonReturn);
        }
    }
}
