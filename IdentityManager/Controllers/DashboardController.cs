﻿using IdentityManager.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Controllers
{
    public class DashboardController : Controller
    {
        private readonly StreamsContext _context;
        public DashboardController(StreamsContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Authentication");
            }
                        
            return View();
        }

        public IActionResult LoadLoggedInUsers()
        {
            var loginattempts = _context.LoginAttempts.Where(a => a.Date > DateTime.Now.AddDays(-365)).ToList<LoginAttempt>();
            var totalusers = _context.Contacts.Where(ct => ct.IsSystemUser == true);
            var totaluserscount = totalusers.Count();
            var activeusers = totalusers.Where(b => b.IsActive == true).ToList<Contact>();
            var activeuserscount = activeusers.Count();

            var activeuserslist = from a in activeusers
                         join l in loginattempts on a.Username equals l.InputtedLoginUser
                         select new
                         {
                             a.Username,
                             l.Date                             
                         };

            var todaycount = activeuserslist.Where(l => l.Date.Date == DateTime.Now.Date).Select(a => a.Username).Distinct().Count();
            var lastoneweek = activeuserslist.Where(d => d.Date.Date >= DateTime.Now.Date.AddDays(-7)).Select(a=>a.Username);
            var lastoneweekcount = lastoneweek.Distinct().Count();

            var last30days = activeuserslist.Where(e => e.Date.Date >= DateTime.Now.Date.AddDays(-30)).Select(a => a.Username);
            var last30dayscount = last30days.Distinct().Count();

            var last1year = activeuserslist.Where(e => e.Date.Date >= DateTime.Now.Date.AddDays(-30)).Select(a => a.Username);
            var last1yearcount = last1year.Distinct().Count();

            //var jsonReturn = (from rec in loginattempts
            //                  select new
            //                  {
            //                      Id = rec.Id,
            //                      LoginUser = rec.InputtedLoginUser,
            //                      LoggedInDate = rec.Date.Date                                  
            //                  }).ToList();

            string jsonReturn = totaluserscount.ToString() + "|" + activeuserscount.ToString() + "|" + todaycount.ToString() + "|" + lastoneweekcount.ToString() + "|" + last30dayscount.ToString() + "|" + last1yearcount.ToString();
            return Json(jsonReturn);
        }
        public IActionResult LoadUsersTypeCount()
        {
            var contacts = _context.Contacts.Where(a => a.IsSystemUser).ToList<Contact>();
            var allaccessgroups = _context.AccessGroups.ToList<AccessGroup>();
            var jsonReturn = contacts.GroupBy(n => n.AccessGroup)
                         .Select(n => new
                         {
                             accessgroup = (allaccessgroups.Where(a=>a.Id == n.Key).First().Name),
                             groupcount = n.Count()
                         })                         
                         .OrderBy(n => n.accessgroup).ToList();
            return Json(jsonReturn);
        }
    }
}
