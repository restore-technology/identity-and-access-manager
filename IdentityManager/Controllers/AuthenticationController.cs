﻿using IdentityManager.Code.Interface;
using IdentityManager.Data;
using IdentityManager.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static IdentityManager.Code.Enumeration;

namespace IdentityManager.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly StreamsContext _context;
        private readonly IELog _elog;
        private readonly IUser _user;
        private readonly ISetting _dashboardsettings;
        private readonly IConfiguration _conf;
        public AuthenticationController(StreamsContext context, IELog elog, IUser user, ISetting dashboardsettings, IConfiguration conf )
        {
            _context = context;
            _elog = elog;
            _user = user;
            _dashboardsettings = dashboardsettings;
            _conf = conf;

        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            HttpContext.Session.Clear();
            HttpContext.Session.SetString("", "");

            Login model = new Login();

            model.Environment = _conf.GetValue<string>("Environment");
            model.ReleaseDate = _conf.GetValue<string>("ReleaseDate");
            model.ReleaseVersion = _conf.GetValue<string>("AppVersion");
            ViewData["ReleaseVersion"] = model.ReleaseVersion;
            ViewData["ReleaseDate"] = model.ReleaseDate;

            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(Login model)
        {
            _elog.LogInsert("Authentication", model.ReleaseVersion, model.Environment, null, "Login", "Username", model.Username, "Checking login credentials ", null, null, null, (int)ELogSeverityLevel.Information, null);
            if (ModelState.IsValid)
            {
                try
                {
                    ContactsBLL bll = new ContactsBLL(_context, _conf, _user);
                    var contact = bll.CanLogin(model.Username, model.Password, this.HttpContext.Connection.RemoteIpAddress.ToString(), Request.Headers["User-Agent"].ToString());
                    if (contact != null)
                    {
                        if (!CanAccess(contact))
                        {
                            return RedirectToAction("Index", "Unauthorised");
                        }
                        var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, model.Username)
                    };
                        var userIdentity = new ClaimsIdentity(claims, "login");
                        ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);
                        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(userIdentity));
                        HttpContext.Session.SetString("LoggedInUser", contact.Forename + " " + contact.Surname);
                        _elog.LogInsert("Authentication", model.ReleaseVersion, model.Environment, null, "Login", "Username", model.Username, "User authenticated. Redirecting to Dashboard ", null, null, null, (int)ELogSeverityLevel.Information, null);
                        return RedirectToAction("Index", "Dashboard");
                    }
                    else
                    {
                        //TempData["ErrMsg"] = "Cannot login with username or password.";
                        _elog.LogInsert("Authentication", model.ReleaseVersion, model.Environment, null, "Login", "Username", model.Username, "Cannot login with username or password.", null, null, null, (int)ELogSeverityLevel.Warning, null);
                    }
                }
                catch (Exception ex)
                {
                    var innerException = string.Empty;
                    if (ex.InnerException != null)
                    {
                        innerException = ex.InnerException.ToString();
                    }
                    _elog.LogInsert("Authentication", model.ReleaseVersion, model.Environment, null, "Login", "Username", model.Username, ex.Message, innerException, ex.StackTrace, null, (int)ELogSeverityLevel.Critical, null);
                }
            }
            else
            {
                //TempData["ErrMsg"] = "Login model is invalid.";
                _elog.LogInsert("Authentication", model.ReleaseVersion, model.Environment, null, "Login", "Username", model.Username, "Login model is invalid.", null, null, null, (int)ELogSeverityLevel.Information, null);
            }
            return View("~/Views/Authentication/Index.cshtml", model);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            // await HttpContext.SignOutAsync();
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Authentication");
        }
        private bool ProcessLogin(string username, string password)
        {

            List<Contact> depots = _context.Contacts.ToList<Contact>();
            return true;
        }
        private bool CanAccess(User contact)
        {
            var user = _context.AccessGroups.Where(a => a.Id == Guid.Parse(contact.AccessGroupId)).FirstOrDefault();
            if (user.Name == "Sys Admin")
            {
                return true;
            }
            return false;
        }
    }
}

