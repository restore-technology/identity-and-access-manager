﻿using IdentityManager.Code.Interface;
using IdentityManager.Data;
using IdentityManager.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Controllers
{
    public class UsersController : Controller
    {
        private readonly StreamsContext _context;
        private readonly IELog _elog;
        private readonly IConfiguration _conf;
        private readonly IUser _user;
        public UsersController(StreamsContext context, IELog elog, IConfiguration conf, IUser user)
        {
            _context = context;
            _elog = elog;           
            _conf = conf;
            _user = user;
        }
        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Authentication");
            }
            return View();
        }

        [HttpGet]
        public IActionResult GetUsersList()
        {
            List<User> list = _user.GetSytemUsersList().Where(a=> a.IsActive == true && a.Username != null && a.Username != "").ToList<IdentityManager.Models.User>();
            var jsonReturn = (from rec in list
                              select new
                              {
                                  Id = rec.Id,
                                  Username = rec.Username,
                                  Forename = rec.Forename,
                                  Surname = rec.Surname,
                                  IsActive = rec.IsActive,
                                  IsSystemUser = rec.IsSystemUser, 
                                  AccessGroup = rec.AccessGroupName
                              }).ToList();

            return Json(jsonReturn);

        }
        [HttpGet]
        public IActionResult Edit(string id)
        {
            if(id != "")
            {
                
            }
            return View();
        }
    }
}
