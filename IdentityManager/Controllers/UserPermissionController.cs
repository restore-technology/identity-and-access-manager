﻿using IdentityManager.Data;
using IdentityManager.Models;
using IdentityManager.Models.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Controllers
{
    public class UserPermissionController : Controller
    {
        private readonly StreamsContext _context;
        private readonly IConfiguration _conf;
        private readonly IAccessGroupPermissions _permissions;

        public UserPermissionController(StreamsContext context, IAccessGroupPermissions permissions, IConfiguration conf)
        {
            _context = context;
            _permissions = permissions;
            _conf = conf;
        }
        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Authentication");
            }
            
            //Get User Permission
            return View();
        }
        [HttpGet]
        public IActionResult GetUsersPermissions(string UserId)
        {
            var roleid = _context.Contacts.ToList().Where(a => a.Username == UserId).First().AccessGroup;
            List<AccessGroupPermissions> list = _permissions.GetAllPermissions(roleid.ToString());            
            var jsonReturn = (from rec in list
                              select new
                              {
                                  Id = rec.Id,
                                  AreaName = rec.AreaName,
                                  Create = rec.Create,
                                  Read = rec.Read,
                                  Update = rec.Update,
                                  Delete = rec.Delete
                              }).ToList();

            return Json(jsonReturn);
        }
        [HttpGet]
        public IActionResult UserPermissions(string id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Authentication");
            }
            RolePermissionViewModel model = new RolePermissionViewModel();            
           
            //model.PermissionAreaList = GetPermissionAreas();
            ViewBag.PermissionAreaList = GetPermissionAreas();
            ViewBag.PermissionOptions = GetOptions();
            if (id != "")
            {                
                Contact contact = _context.Contacts.Where(a => a.Id == Guid.Parse(id)).First();
                model.RoleId = contact.AccessGroup;
                ViewBag.RoleName = _context.AccessGroups.Where(a => a.Id == model.RoleId).FirstOrDefault().Name.ToString();
            }
            return View(model);
        }
        
        public IActionResult GetPermissions(string roleid, int id)
        {
            AccessGroupPermissions permission = _permissions.GetRolePermissions(roleid,id);            
            return Json(permission);
        }
        public IEnumerable<SelectListItem> GetPermissionAreas()
        {
            List<SelectListItem> list = null;

            using (var context = new StreamsContext(_conf))
            {
                // Queries DB for list of categories by AccountID
                var query = (from ca in context.PermissionAreas                             
                             orderby ca.AreaName
                             select new SelectListItem { Text = ca.AreaName, Value = ca.Id.ToString() }).Distinct();
                list = query.ToList();
            }
            return list;
        }
        [HttpPost]
        public IActionResult SavePermissions(RolePermissionViewModel model)
        {
            RolePermission rolePermission = _context.RolePermissions.Where(a => a.Id == model.RecId).FirstOrDefault();

            //rolePermission.Create = Convert.ToBoolean(int.Parse(Request.Form["CreateValue"].ToString()));
            //rolePermission.Read = Convert.ToBoolean(int.Parse(Request.Form["ReadValue"].ToString()));
            //rolePermission.Update = Convert.ToBoolean(int.Parse(Request.Form["UpdateValue"].ToString()));
            //rolePermission.Delete = Convert.ToBoolean(int.Parse(Request.Form["DeleteValue"].ToString()));
            rolePermission.Create = model.CreateValue == true ? true : false;
            rolePermission.Read = model.ReadValue == true ? true : false;
            rolePermission.Update = model.UpdateValue == true ? true : false;
            rolePermission.Delete = model.DeleteValue == true ? true : false;
            _context.Update(rolePermission);
            _context.SaveChanges();

            return RedirectToAction("Index", "Users");
        }
        public IEnumerable<SelectListItem> GetOptions()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem() { Text = "Yes", Value = "1" });
            list.Add(new SelectListItem() { Text = "No", Value = "0" });
            return list;
        }

    }
}
