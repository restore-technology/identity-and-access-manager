﻿using IdentityManager.Data;
using IdentityManager.Models;
using IdentityManager.Models.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace IdentityManager.Controllers
{
    public class RolesController : Controller
    {
        private readonly StreamsContext _context;
        private readonly IAccessGroupPermissions _accessGroupPermisssion;
        private readonly IConfiguration _conf;
        public RolesController(StreamsContext context, IAccessGroupPermissions accessGroupPermissions, IConfiguration conf)
        {
            _context = context;
            _accessGroupPermisssion = accessGroupPermissions;
            _conf = conf;
        }
        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Authentication");
            }
            List<AccessGroup> accessgroups = new List<AccessGroup>();
            accessgroups = _context.AccessGroups.ToList<AccessGroup>();
            return View(accessgroups);
        }
        [HttpGet]
        public IActionResult GetAccessGroups()
        {
            List<AccessGroup> accessgroups = new List<AccessGroup>();
            accessgroups = _context.AccessGroups.OrderBy(a => a.Order).ToList<AccessGroup>();
            var jsonReturn = (from rec in accessgroups
                              select new
                              {
                                  Id = rec.Id,
                                  RoleName = rec.Name,
                                  RoleOrder = rec.Order
                              }).ToList();

            return Json(jsonReturn);
        }
        public IActionResult EditUserRolePermissions(string id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Authentication");
            }

            RolePermissionViewModel rolepermissions = new RolePermissionViewModel();
            ViewBag.PermissionAreaList = GetPermissionAreas();
            ViewBag.PermissionOptions = GetOptions();
            if (id != "")
            {
                var permissions = _context.RolePermissions.Where(a => a.RoleId == Guid.Parse(id)).First();
                ViewBag.RoleName = _context.AccessGroups.Where(a => a.Id == Guid.Parse(id)).FirstOrDefault().Name.ToString();
                //ViewBag.AreaName = _context.PermissionAreas.Where(a => a.Id == rolepermissions.AreaId).FirstorDefault().AreaName.ToString();
                rolepermissions.RecId = permissions.Id;
                rolepermissions.RoleId = permissions.RoleId;
                rolepermissions.AreaId = permissions.AreaId;
                bool newCreateBool = permissions.Create.HasValue ? permissions.Create.Value : false; 
                rolepermissions.CreateValue = newCreateBool ? true : false;

                bool newReadBool = permissions.Read.HasValue ? permissions.Read.Value : false;
                rolepermissions.ReadValue = newReadBool ? true : false;

                bool newUpdateBool = permissions.Update.HasValue ? permissions.Update.Value : false;
                rolepermissions.UpdateValue = newUpdateBool ? true : false;

                bool newDeleteBool = permissions.Delete.HasValue ? permissions.Delete.Value : false;
                rolepermissions.DeleteValue = newDeleteBool ? true : false;
            }
            rolepermissions.RoleId = Guid.Parse(id);
            return View(rolepermissions);
        }
        public IActionResult EditRolePermissions(int id = 0)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Authentication");
            }

            RolePermissionViewModel rolepermissions = new RolePermissionViewModel();
            ViewBag.PermissionAreaList = GetPermissionAreas();
            ViewBag.PermissionOptions = GetOptions();
            if (id > 0)
            {
                var permissions = _context.RolePermissions.Where(a => a.Id == id).First();
                ViewBag.RoleName = _context.AccessGroups.Where(a => a.Id == permissions.RoleId).FirstOrDefault().Name.ToString();                
                rolepermissions.RecId = permissions.Id;
                rolepermissions.RoleId = permissions.RoleId;
                rolepermissions.AreaId = permissions.AreaId;
                bool newCreateBool = permissions.Create.HasValue ? permissions.Create.Value : false;
                rolepermissions.CreateValue = newCreateBool ? true : false;

                bool newReadBool = permissions.Read.HasValue ? permissions.Read.Value : false;
                rolepermissions.ReadValue = newReadBool ? true : false;

                bool newUpdateBool = permissions.Update.HasValue ? permissions.Update.Value : false;
                rolepermissions.UpdateValue = newUpdateBool ? true : false;

                bool newDeleteBool = permissions.Delete.HasValue ? permissions.Delete.Value : false;
                rolepermissions.DeleteValue = newDeleteBool ? true : false;
            }
            return View(rolepermissions);            
        }
        [HttpPost]
        public IActionResult SavePermissions(RolePermissionViewModel model)
        {
            RolePermission rolePermission = _context.RolePermissions.Where(a => a.Id == model.RecId).FirstOrDefault();

            rolePermission.Create = model.CreateValue;
            rolePermission.Read = model.ReadValue;
            rolePermission.Update = model.UpdateValue;
            rolePermission.Delete = model.DeleteValue;
            _context.Update(rolePermission);
            _context.SaveChanges();

            return RedirectToAction("Index", "Roles");
        }
        [HttpPost]
        public IActionResult SaveRolePermissions(RolePermissionViewModel model)
        {
            RolePermission rolePermission = _context.RolePermissions.Where(a => a.Id == model.RecId).FirstOrDefault();
            rolePermission.Create = model.CreateValue;
            rolePermission.Read = model.ReadValue;
            rolePermission.Update = model.UpdateValue;
            rolePermission.Delete = model.DeleteValue;
            _context.Update(rolePermission);
            _context.SaveChanges();

            return RedirectToAction("GetRolePermissions", "Roles");
        }
        [HttpPost]
        public IActionResult SaveNewRole(string RoleName)
        {
            try
            {
                var lastroleinorder = _context.AccessGroups.OrderByDescending(a => a.Order).First();
                var role = new AccessGroup();
                role.Id = Guid.NewGuid();
                role.Name = RoleName;
                role.Order = lastroleinorder.Order + 1;
                _context.Add(role);
                _context.SaveChanges();
                return Ok(role);
            }
            catch(Exception ex)
            {
                var error = new { message = ex.Message }; //<-- anonymous object
                return this.Content(HttpStatusCode.Conflict.ToString(), error.ToString());
            }

            
        }
        public IEnumerable<SelectListItem> GetPermissionAreas()
        {
            List<SelectListItem> list = null;

            using (var context = new StreamsContext(_conf))
            {
                // Queries DB for list of categories by AccountID
                var query = (from ca in context.PermissionAreas
                             orderby ca.AreaName
                             select new SelectListItem { Text = ca.AreaName, Value = ca.Id.ToString() }).Distinct();
                list = query.ToList();
            }
            return list;
        }
        public IEnumerable<SelectListItem> GetOptions()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            
            list.Add(new SelectListItem() { Text = "Yes", Value = "1" });
            list.Add(new SelectListItem() { Text = "No", Value = "0" });
            return list;
        }

        [HttpGet]
        public IActionResult GetRolePermissions()
        {
            ViewBag.AccessGroupList = GetRoles();
            return View();
        }
        [HttpGet]
        public IActionResult GetAllAccessGroups(string RoleId)
        {
            List<AccessGroupPermissions> list = new List<AccessGroupPermissions>();
            list = _accessGroupPermisssion.GetRolesPermissionsByRoleId(RoleId);
            var jsonReturn = (from rec in list
                              select new
                              {
                                  Id = rec.Id,
                                  RoleId = rec.RoleId,
                                  RoleName = rec.AccessGroupName,
                                  AreaName = rec.AreaName,
                                  CreateValue = rec.Create ? "Y": "N",
                                  ReadValue = rec.Read ? "Y" : "N",
                                  UpdateValue = rec.Update ? "Y" : "N",
                                  DeleteValue = rec.Delete ? "Y" : "N"
                              }).ToList();

            return Json(jsonReturn);
        }
        public IEnumerable<SelectListItem> GetRoles()
        {
            List<SelectListItem> list = null;

            using (var context = new StreamsContext(_conf))
            {
                // Queries DB for list of categories by AccountID
                var query = (from ca in context.AccessGroups
                             orderby ca.Name
                             select new SelectListItem { Text = ca.Name, Value = ca.Id.ToString() }).Distinct();
                list = query.ToList();
            }

            return list;
        }
    }
}
