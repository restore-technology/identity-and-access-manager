﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models
{
    public class LogModel
    {
        public LogModel()
        {

        }

        [Required]
        public string ApplicationName { get; set; }
        [Required]
        public string VersionNo { get; set; }
        [Required]
        public string Environment { get; set; }
        public Guid? ContextID { get; set; }
        public string UniqueKeyName { get; set; }
        public string UniqueKeyValue { get; set; }
        public string UserID { get; set; }
        public string Message { get; set; }
        public string ErrorDescription0 { get; set; }
        public string ErrorDescription1 { get; set; }
        public string ErrorDescription2 { get; set; }
        [Required]
        public int SeverityLevel { get; set; }
        public string JSONData { get; set; }
    }
}
