﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models
{
    public class RolePermissionViewModel
    {
        public int RecId { get; set; }
        public Guid? RoleId { get; set; }
        public int? AreaId { get; set; }
        public bool CreateValue { get; set; }
        public bool ReadValue { get; set; }
        public bool UpdateValue { get; set; }
        public bool DeleteValue { get; set; }
    }
}
