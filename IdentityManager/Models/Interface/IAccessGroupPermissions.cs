﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models.Interface
{
    public interface IAccessGroupPermissions
    {
        List<AccessGroupPermissions> GetAllPermissions(string RoleId);
        AccessGroupPermissions GetRolePermissions(string roleid, int Id);
        List<AccessGroupPermissions> GetRolesPermissionsByRoleId(string RoleId);
    }
}
