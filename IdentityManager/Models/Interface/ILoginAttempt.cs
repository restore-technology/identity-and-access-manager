﻿using IdentityManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Code.Interface
{
    public interface ILoginAttempt
    {        
        void Insert(LoginAttempt la);
    }
}
