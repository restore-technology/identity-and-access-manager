﻿using IdentityManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Code.Interface
{
    public interface IUser
    {
        User GetUser(string Username);
        List<User> GetUsersList();
        List<User> GetSytemUsersList();
    }
}
