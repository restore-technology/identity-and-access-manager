﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Code.Interface
{
    public interface IELog
    {
        void LogInsert(string ApplicationName, string VersionNo, string Environment, string UserID, string Context, string UniqueKeyName, string UniqueKeyValue, string Message, string ErrorDescription0, string ErrorDescription1, string ErrorDescription2, int Severity, string JSONData);
       
    }
}
