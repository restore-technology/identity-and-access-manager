﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models
{
    public class BaseViewModel
    {
        public int DepotId { get; set; }
        public string DepotName { get; set; }
        public string ReleaseDate { get; set; }
        public string ReleaseVersion { get; set; }
        public IDictionary<string, string> DepotColours { get; set; }
        public int ProductionReportRefreshRate { get; set; }
        public int PageRefreshRate { get; set; }
        public int PageReloadRefreshRate { get; set; }
        public string LoggedInUser { get; set; }
        public string Environment { get; set; }
    }
}
