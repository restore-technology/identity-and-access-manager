﻿using IdentityManager.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models
{
    public class UserPermissionModel
    {
        public string UserId { get; set; }        
        public int UserReference { get; set; }
        public IEnumerable<SelectListItem> PermissionAreaList { get; set; }
        public string PermissionArea { get; set; }
        public RolePermission rolePermission { get; set; }

    }
}
