﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models
{
    public class User
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; } = null;
        public string HashPassword { get; set; }
        public string Salt { get; set; }
        public bool IsSystemUser { get; set; }
        public bool IsActive { get; set; }
        public string AccessGroupId { get; set; }
        public string AccessGroupName { get; set; }

        public User(string Id, string Username, string Forename, string Surname, string Password, string HashPassword, string Salt, bool IsSystemUser, bool IsActive, string AccessGroupId, string AccessGroupName)
        {
            this.Id = Id;
            this.Username = Username;
            this.Forename = Forename;
            this.Surname = Surname;
            this.Password = Password;
            this.HashPassword = HashPassword;
            this.Salt = Salt;
            this.IsSystemUser = IsSystemUser;
            this.IsActive = IsActive;
            this.AccessGroupId = AccessGroupId;
            this.AccessGroupName = AccessGroupName;
        }
    }
}
