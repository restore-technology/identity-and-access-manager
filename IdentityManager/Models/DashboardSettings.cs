﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models
{
    public class DashboardSettings
    {
        public int Id { get; set; }
        public string ReleaseDate { get; set; }
        public string ReleaseVersion { get; set; }
        public string Environment { get; set; }
        public int SLAReportRefreshRate { get; set; }
        public int ProductionReportRefreshRate { get; set; }
        public int PageRefreshRate { get; set; }
        public int PageReloadRefreshRate { get; set; }

        public DashboardSettings(int Id, string ReleaseDate, string ReleaseVersion, string Environment, int SLAReportRefreshRate, int ProductionReportRefreshRate, int PageRefreshRate, int PageReloadRefreshRate)
        {
            this.Id = Id;
            this.ReleaseDate = ReleaseDate;
            this.ReleaseVersion = ReleaseVersion;
            this.Environment = Environment;
            this.SLAReportRefreshRate = SLAReportRefreshRate;
            this.ProductionReportRefreshRate = ProductionReportRefreshRate;
            this.PageRefreshRate = PageRefreshRate;
            this.PageReloadRefreshRate = PageReloadRefreshRate;
        }
    }
}
