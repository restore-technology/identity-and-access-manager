﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models.DAL
{
    public class ELogDataProvider : DALBase
    {
        public readonly IConfiguration _conf;

        public ELogDataProvider(IConfiguration configuration) : base(configuration)
        {
            _conf = configuration;
        }
        public void LogInsert(string ApplicationName, string VersionNo, string Environment, string UserID, string Context, string UniqueKeyName, string UniqueKeyValue, string Message, string ErrorDescription0, string ErrorDescription1, string ErrorDescription2, int Severity, string JSONData)
        {
            var _connectionString = _conf.GetConnectionString("StreamsConnection");
            SqlConnection sqlConnection = new SqlConnection(_connectionString);
            SqlCommand sqlCommand = new SqlCommand("DashboardLogInsert", sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ApplicationName;
            sqlCommand.Parameters.Add("@VersionNo", SqlDbType.VarChar).Value = VersionNo;
            sqlCommand.Parameters.Add("@Environment", SqlDbType.VarChar).Value = Environment;
            sqlCommand.Parameters.Add("@UserID", SqlDbType.VarChar).Value = UserID;
            sqlCommand.Parameters.Add("@Context", SqlDbType.VarChar).Value = Context;
            sqlCommand.Parameters.Add("@UniqueKeyName", SqlDbType.VarChar).Value = UniqueKeyName;
            sqlCommand.Parameters.Add("@UniqueKeyValue", SqlDbType.VarChar).Value = UniqueKeyValue;
            sqlCommand.Parameters.Add("@ErrorMessage", SqlDbType.VarChar).Value = Message;
            sqlCommand.Parameters.Add("@ErrorDescription0", SqlDbType.VarChar).Value = ErrorDescription0;
            sqlCommand.Parameters.Add("@ErrorDescription1", SqlDbType.VarChar).Value = ErrorDescription1;
            sqlCommand.Parameters.Add("@ErrorDescription2", SqlDbType.VarChar).Value = ErrorDescription2;
            sqlCommand.Parameters.Add("@SeverityLevel", SqlDbType.Int).Value = Severity;
            sqlCommand.Parameters.Add("@JSONData", SqlDbType.VarChar).Value = JSONData;
            using (sqlConnection)
            {
                sqlConnection.Open();
                int id = sqlCommand.ExecuteNonQuery();
            }
        }
    }
}
