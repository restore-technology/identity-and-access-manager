﻿using IdentityManager.Models;
using IdentityManager.Models.DAL;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models.DAL
{
    public class AccessGroupPermissionsProvider : DALBase
    {
        public readonly IConfiguration _conf;

        public AccessGroupPermissionsProvider(IConfiguration configuration) : base(configuration)
        {
            _conf = configuration;
        }
        public AccessGroupPermissions GetRolePermissions(string roleid, int areaId)
        {
            var _connectionString = _conf.GetConnectionString("StreamsConnection");

            AccessGroupPermissions permissions = null;

            //Create db connection
            SqlConnection con = new SqlConnection(_connectionString);

            //Create command
            SqlCommand cmd = new SqlCommand("GetAccessGroupsByRoleandArea", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RoleId", SqlDbType.VarChar).Value = roleid;
            cmd.Parameters.Add("@AreaId", SqlDbType.Int).Value = areaId;

            //Execute command
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    if (reader.Read())
                    {
                        permissions = new AccessGroupPermissions(
                            Convert.ToInt32(Convert.ToString(Convert.IsDBNull(reader["Id"]) ? "" : reader["Id"])),
                            Convert.ToString(Convert.IsDBNull(reader["RoleId"]) ? "" : reader["RoleId"]),
                            Convert.ToString(Convert.IsDBNull(reader["AccessGroupName"]) ? "" : reader["AccessGroupName"]),
                            Convert.ToInt32(Convert.IsDBNull(reader["AreaId"]) ? "" : reader["AreaId"]),
                            Convert.ToString(Convert.IsDBNull(reader["AreaName"]) ? "" : reader["AreaName"]),
                            Convert.ToBoolean(Convert.IsDBNull(reader["CCreate"]) ? "" : reader["CCreate"]),
                            Convert.ToBoolean(Convert.IsDBNull(reader["RRead"]) ? "" : reader["RRead"]),
                            Convert.ToBoolean(Convert.IsDBNull(reader["UUpdate"]) ? "" : reader["UUpdate"]),
                            Convert.ToBoolean(Convert.IsDBNull(reader["DDelete"]) ? "" : reader["DDelete"]));
                    }


                }
            }

            return permissions;

        }

        public List<AccessGroupPermissions> GetRolesPermissionsByRoleId(string roleid)
        {
            var _connectionString = _conf.GetConnectionString("StreamsConnection");

            List<AccessGroupPermissions> permissions = new List<AccessGroupPermissions>();

            //Create db connection
            SqlConnection con = new SqlConnection(_connectionString);

            //Create command
            SqlCommand cmd = new SqlCommand("GetAccessGroupsByRoleId", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RoleId", SqlDbType.VarChar).Value = roleid;

            //Execute command
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var permission = new AccessGroupPermissions(
                        Convert.ToInt32(Convert.ToString(Convert.IsDBNull(reader["Id"]) ? "" : reader["Id"])),
                        Convert.ToString(Convert.IsDBNull(reader["RoleId"]) ? "" : reader["RoleId"]),
                        Convert.ToString(Convert.IsDBNull(reader["AccessGroupName"]) ? "" : reader["AccessGroupName"]),
                        Convert.ToInt32(Convert.IsDBNull(reader["AreaId"]) ? "" : reader["AreaId"]),
                        Convert.ToString(Convert.IsDBNull(reader["AreaName"]) ? "" : reader["AreaName"]),
                        Convert.ToBoolean(Convert.IsDBNull(reader["Create"]) ? "" : reader["Create"]),
                        Convert.ToBoolean(Convert.IsDBNull(reader["Read"]) ? "" : reader["Read"]),
                        Convert.ToBoolean(Convert.IsDBNull(reader["Update"]) ? "" : reader["Update"]),
                        Convert.ToBoolean(Convert.IsDBNull(reader["Delete"]) ? "" : reader["Delete"]));

                    permissions.Add(permission);
                }
            }
            return permissions;

        }

    }
}
