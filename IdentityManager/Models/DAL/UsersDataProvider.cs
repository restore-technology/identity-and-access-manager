﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models.DAL
{
    
    public class UsersDataProvider :DALBase
    {
        public readonly IConfiguration _conf;

        public UsersDataProvider(IConfiguration configuration) : base(configuration)
        {
            _conf = configuration;
        }
        public List<User> GetSystemUsers()
        {
            var _connectionString = _conf.GetConnectionString("StreamsConnection");

            List<User> users = new List<User>();

            //Create db connection
            SqlConnection con = new SqlConnection(_connectionString);

            //Create command
            SqlCommand cmd = new SqlCommand("GetSystemUsers", con);
            cmd.CommandType = CommandType.StoredProcedure;

            //Execute command
            using (con)
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (reader.Read())
                    {
                        User user = new User(
                            Convert.ToString(Convert.IsDBNull(reader["Id"]) ? "" : reader["Id"]),
                            Convert.ToString(Convert.IsDBNull(reader["Username"]) ? "" : reader["Username"]),                            
                            Convert.ToString(Convert.IsDBNull(reader["Forename"]) ? "" : reader["Forename"]),
                            Convert.ToString(Convert.IsDBNull(reader["Surname"]) ? "" : reader["Surname"]),
                            Convert.ToString(Convert.IsDBNull(reader["Password"]) ? "" : reader["Password"]),
                            Convert.ToString(Convert.IsDBNull(reader["HashPassword"]) ? "" : reader["HashPassword"]),
                            Convert.ToString(Convert.IsDBNull(reader["Salt"]) ? "" : reader["Salt"]),
                             Convert.ToBoolean(Convert.IsDBNull(reader["IsSystemUser"]) ? "" : reader["IsSystemUser"]),
                            Convert.ToBoolean(Convert.IsDBNull(reader["IsActive"]) ? "" : reader["IsActive"]),
                            Convert.ToString(Convert.IsDBNull(reader["AccessGroupId"]) ? "" : reader["AccessGroupId"]),
                            Convert.ToString(Convert.IsDBNull(reader["AccessGroupName"]) ? "" : reader["AccessGroupName"]));
                        users.Add(user);
                    }


                }
            }

            return users;
        }
    }
}
