﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models.DAL
{
    public class DALBase
    {
        protected readonly string _connectionString = String.Empty;
        private readonly IConfiguration _configuration;
        #region "Constructors"
        //Load connection string from Web.Config

        public DALBase(IConfiguration configuration)
        {            
            _configuration = configuration;            
        }

        #endregion

        #region "Methods"
        /// <summary>
        /// Returns a sqlcommand object
        /// </summary>
        /// <param name="con"></param>
        /// <param name="commandText"></param>
        /// <returns></returns>
        protected static SqlCommand ReturnSqlCommand(SqlConnection con, string commandText)
        {
            var cmd = new SqlCommand(commandText, con) { CommandType = CommandType.StoredProcedure };
            return cmd;
        }

        /// <summary>
        /// Execute the specified SQL command returning the results in a dataset
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="cmd"></param>

        protected void ExecuteQueryForDS(ref DataSet ds, SqlCommand cmd)
        {
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            try
            {
                if ((ds == null))
                {
                    ds = new DataSet();
                }

                cmd.Connection.Open();
                da.Fill(ds);
                cmd.Connection.Close();

            }
            catch (ArgumentException ex)
            {
                ds = null;
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }

        }

        /// <summary>
        /// Creates a connection to the database
        /// </summary>
        //protected SqlConnection ProjectConnection()
        //{
        //    return new SqlConnection(this._connectionString);
        //}

        /// <summary>
        /// Creates a command with the default connection
        /// </summary>
        /// <param name="cmdText">store procedure name</param>
        /// <returns>instantiated command object</returns>
        //protected SqlCommand GetCommand(string cmdText)
        //{

        //    SqlCommand returnVal = new SqlCommand(cmdText, ProjectConnection());

        //    returnVal.CommandType = CommandType.StoredProcedure;

        //    return returnVal;

        //}

        /// <summary>
        /// Creates a command with the default connection
        /// </summary>
        /// <param name="cmdText">store procedure name</param>
        /// <returns>instantiated command object</returns>
        protected SqlCommand GetCommand(SqlConnection conn, string cmdText, SqlTransaction trans)
        {

            SqlCommand returnVal = new SqlCommand(cmdText, conn, trans);

            returnVal.CommandType = CommandType.StoredProcedure;

            return returnVal;

        }

        /// <summary>
        /// Executes a Command and returns a data reader
        /// </summary>
        /// <param name="cmd">cmd to execute</param>
        /// <returns>sql data reader</returns>
        protected SqlDataReader ExecuteReader(SqlCommand cmd)
        {
            SqlDataReader reader = default(SqlDataReader);
            try
            {
                cmd.Connection.Open();
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                if ((cmd.Connection != null))
                {
                    cmd.Connection.Close();
                }
                throw ex;
            }

            return reader;
        }


        /// <summary>
        /// Return a DBDataReader object for the specified command.
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        protected DbDataReader ExecuteDBReader(SqlCommand cmd)
        {
            DbDataReader reader = default(DbDataReader);
            try
            {
                cmd.Connection.Open();
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                if ((cmd.Connection != null))
                {
                    cmd.Connection.Close();
                }
                throw ex;
            }

            return reader;
        }

        /// <summary>
        /// Executes a Command and returns the object returned
        /// </summary>
        /// <param name="cmd">cmd to execute</param>
        /// <returns>object returned</returns>
        protected object ExecuteScalar(SqlCommand cmd)
        {

            object returnVal = null;

            try
            {
                cmd.Connection.Open();
                returnVal = cmd.ExecuteScalar();
                cmd.Connection.Close();
            }
            finally
            {
                cmd.Connection.Close();
            }

            return returnVal;

        }

        /// <summary>
        /// Executes a Command with no output
        /// </summary>
        /// <param name="cmd">command to execute</param>
        /// <returns>number of rows affected</returns>
        protected long ExecuteNonQuery(SqlCommand cmd)
        {
            long returnVal = 0;

            try
            {
                cmd.Connection.Open();
                returnVal = cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
            finally
            {
                cmd.Connection.Close();

            }
            return returnVal;
        }

        /// <summary>
        /// Executes a Command and populates the provided Data Row with the results.
        /// </summary>
        /// <param name="cmd">
        /// command to execute
        /// </param>

        protected void ExecuteQuery(ref DataRow dr, SqlCommand cmd)
        {
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            try
            {
                cmd.Connection.Open();
                da.Fill(dt);

                if ((dt.Rows.Count == 1))
                {
                    dr = dt.Rows[0];
                }
                cmd.Connection.Close();
            }
            finally
            {
                cmd.Connection.Close();

            }

        }

        /// <summary>
        /// Executes a Command and populates the provided Data Table with the results.
        /// </summary>
        /// <param name="cmd">
        /// command to execute
        /// </param>
        protected void ExecuteQuery(ref DataTable dt, SqlCommand cmd)
        {
            SqlDataAdapter da = new SqlDataAdapter(cmd);


            try
            {
                if ((dt == null))
                {
                    dt = new DataTable();
                }

                cmd.Connection.Open();
                da.Fill(dt);

                cmd.Connection.Close();
            }
            catch (Exception e)
            {
                string d = null;
                d = e.Message;

            }
            finally
            {
                cmd.Connection.Close();

            }

        }

        /// <summary>
        /// Executes a Command and populates the provided Data Set with the results.
        /// </summary>
        /// <param name="cmd">
        /// command to execute
        /// </param>

        protected void ExecuteQuery(ref DataSet ds, SqlCommand cmd)
        {
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            try
            {
                if ((ds == null))
                {
                    ds = new DataSet();
                }

                cmd.Connection.Open();
                da.Fill(ds);

                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                ds = null;
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }

        }

        /// <summary>
        /// Executes a Command and returns @RETURN_VALUE from the Stored Procedure. This method
        /// will automatically add the output parameter.
        /// </summary>
        /// <param name="cmd">
        /// command to execute
        /// </param>
        /// <returns>
        /// Value returned by Stored procedure via the @RETURN_VALUE parameter.
        /// </returns>
        protected long ExecuteQueryWithReturnVal(SqlCommand cmd)
        {

            long returnVal = 0;

            try
            {
                AddReturnParameter(ref cmd);

                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

                returnVal = GetReturnParameterValue(cmd);

                cmd.Connection.Close();


            }
            finally
            {
                cmd.Connection.Close();

            }

            return returnVal;

        }

        private void AddReturnParameter(ref SqlCommand cmd)
        {
            SqlParameter returnValueParam = null;

            // Add output param '@RETURN_VALUE' if not already added.

            if ((!(cmd.Parameters.Contains("@RETURN_VALUE"))))
            {
                returnValueParam = new SqlParameter("@RETURN_VALUE", SqlDbType.BigInt);
                returnValueParam.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(returnValueParam);
            }
        }

        private Int64 GetReturnParameterValue(SqlCommand cmd)
        {

            Int64 returnVal = 0;

            returnVal = Convert.ToInt64(cmd.Parameters.Add("@RETURN_VALUE"));

            return returnVal;

        }

        /// <summary>
        /// Adds '@' in front of param name if needed.
        /// </summary>
        /// <param name="paramName">
        /// Parameter name to 
        /// </param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string BuildParamName(string paramName)
        {
            if ((paramName.Length != 0))
            {
                if ((paramName.Substring(0, 1) != "@"))
                {
                    paramName = "@" + paramName;
                }
            }

            return paramName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string GetDataString(object value)
        {
            string retval = String.Empty;

            if ((!ReferenceEquals(value, DBNull.Value)))
            {
                retval = value.ToString();
            }

            return retval;
        }

        /// <summary>
        /// Return an object as a date time value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected DateTime GetDataDateTime(object value)
        {
            DateTime retval = DateTime.MinValue;
            if ((!ReferenceEquals(value, DBNull.Value)))
            {
                retval = Convert.ToDateTime(value);
            }

            return retval;
        }



        #endregion
    }
}
