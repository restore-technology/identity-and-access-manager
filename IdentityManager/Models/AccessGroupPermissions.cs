﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models
{
    public class AccessGroupPermissions
    {
        public int Id {get;set;}
        public string RoleId { get; set; }
        public string AccessGroupName { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public bool Create { get; set; }
        public bool Read { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }

        public AccessGroupPermissions(int Id, string RoleId, string AccessGroupName, int AreaId, string AreaName, bool Create, bool Read, bool Update, bool Delete)
        {
            this.Id = Id;
            this.RoleId = RoleId;
            this.AccessGroupName = AccessGroupName;
            this.AreaId = AreaId;
            this.AreaName = AreaName;
            this.Create = Create;
            this.Read = Read;
            this.Update = Update;
            this.Delete = Delete;
        }
    }
}
