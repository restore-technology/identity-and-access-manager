﻿using Microsoft.Extensions.Configuration;
using IdentityManager.Code.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityManager.Data;
using IdentityManager.Models;
using IdentityManager.Models.DAL;

namespace IdentityManager.Model.Repository
{
    public class UserRepository : IUser
    {
        private readonly IConfiguration _conf;
        private readonly StreamsContext _context;
        public UserRepository(IConfiguration conf, StreamsContext context)
        {
            _conf = conf;
            _context = context;
        }

        public User GetUser(string Username)
        {
            Contact contact = _context.Contacts.Where(a => a.Username == Username).FirstOrDefault();
            User user = new User(contact.Id.ToString(), contact.Username, contact.Forename, contact.Surname, contact.Password, contact.HashPassword, contact.Salt, contact.IsSystemUser, contact.IsActive, contact.AccessGroup.ToString(), null);
            return user;
        }
        public List<User> GetUsersList()
        {
            List<User> users = new List<User>();
            List <Contact> contacts = _context.Contacts.ToList<Contact>();
            foreach(Contact contact in contacts)
            {
                User user = new User(contact.Id.ToString(), contact.Username, contact.Forename, contact.Surname, contact.Password, contact.HashPassword, contact.Salt, contact.IsSystemUser, contact.IsActive, contact.AccessGroup.ToString(), null);
                users.Add(user);
            }
            return users;
        }
        public List<User> GetSytemUsersList()
        {
            //List<User> users = new List<User>();
            //List<Contact> contacts = _context.Contacts.Where(r=>r.IsSystemUser ==  true).ToList<Contact>();
            //foreach (Contact contact in contacts)
            //{
            //    var accessgroupname = _context.AccessGroups.Where(rec => rec.Id == contact.AccessGroup).FirstOrDefault().Name;
            //    User user = new User(contact.Id.ToString(), contact.Username, contact.Forename, contact.Surname, contact.Password, contact.HashPassword, contact.Salt, contact.IsSystemUser, contact.IsActive, contact.AccessGroup.ToString(), accessgroupname);
            //    users.Add(user);
            //}
            //return users;
            UsersDataProvider dp = new UsersDataProvider(_conf);
            return dp.GetSystemUsers();
        }
    }
}
