﻿using IdentityManager.Data;
using IdentityManager.Models.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using IdentityManager.Models.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using IdentityManager.Code.Interface;
using DashboardLog = IdentityManager.Code.Interface.DashboardLog;
using static IdentityManager.Code.Enumeration;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace IdentityManager.Models.Repository
{
    public class AccessGroupPermissionsRepository :IAccessGroupPermissions
    {
        private readonly StreamsContext _context;
        private readonly IConfiguration _conf;
        private readonly IELog _elog;
        public AccessGroupPermissionsRepository(StreamsContext context, IConfiguration conf, IELog elog)
        {
            _context = context;
            _conf = conf;
            _elog = elog;
        }
        public List<AccessGroupPermissions> GetAllPermissions(string RoleId)
        {           
            SqlParameter param = new SqlParameter();

            param.ParameterName = "@RoleId";            
            param.SqlDbType = SqlDbType.VarChar;
            param.Value = RoleId;
            var rowsAffected = _context.Database.ExecuteSqlRaw("EXECUTE GetAccessGroupsByRoleId {0}", param);
            List<AccessGroupPermissions> rows = new List<AccessGroupPermissions>();
            return rows;
        }
        public AccessGroupPermissions GetRolePermissions(string roleid, int id)
        {
            SqlParameter param = new SqlParameter();

            //param.ParameterName = "@RoleId";
            //param.SqlDbType = SqlDbType.UniqueIdentifier;
            //param.Value = roleid;

            //param.ParameterName = "@Id";
            //param.SqlDbType = SqlDbType.Int;
            //param.Value = id;
            //var listrows = _context.Set<AccessGroupPermissions>().FromSqlRaw("EXECUTE GetAccessGroupsById {0}", param);

            AccessGroupPermissionsProvider dp = new AccessGroupPermissionsProvider(_conf);
            var listrow = dp.GetRolePermissions(roleid,id);
            return listrow;
        }

        public List<AccessGroupPermissions> GetRolesPermissionsByRoleId(string RoleId)
        {
            List<AccessGroupPermissions> list = new List<AccessGroupPermissions>();
            try
            {
                AccessGroupPermissionsProvider dp = new AccessGroupPermissionsProvider(_conf);
                list = dp.GetRolesPermissionsByRoleId(RoleId);
                _elog.LogInsert(_conf["ApplicationName"], _conf["Version"], _conf["Environment"], null, "Repository-GetRolesPermissionsByRoleId", "RoleId", RoleId, list.Count.ToString() + " records extracted from the database ", null, null, null, (int)ELogSeverityLevel.Information, null);
            }
            catch(Exception ex)
            {
                var innerException = string.Empty;
                if (ex.InnerException != null)
                {
                    innerException = ex.InnerException.ToString();
                }
                _elog.LogInsert(_conf["ApplicationName"], _conf["Version"], _conf["Environment"], null, "Repository-GetRolesPermissionsByRoleId", "RoleId", RoleId.ToString(), ex.Message, innerException, ex.StackTrace, null, (int)ELogSeverityLevel.Critical, null);
            }
            
            return list;
        }


    }
}
