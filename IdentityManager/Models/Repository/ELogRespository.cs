﻿using IdentityManager.Code.Interface;
using IdentityManager.Data;
using IdentityManager.Models.DAL;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models.Repository
{
    public class ELogRespository : IELog
    {
        private readonly IConfiguration _conf;
        private readonly StreamsContext _context;
        public ELogRespository(IConfiguration conf, StreamsContext context)
        {
            _conf = conf;
            _context = context;
        }
        public void LogInsert(string ApplicationName, string VersionNo, string Environment, string UserID, string Context, string UniqueKeyName, string UniqueKeyValue, string Message, string ErrorDescription0, string ErrorDescription1, string ErrorDescription2, int Severity, string JSONData)
        {
            ELogDataProvider elog = new ELogDataProvider(_conf);
            elog.LogInsert(ApplicationName, VersionNo, Environment, UserID, Context, UniqueKeyName, UniqueKeyValue, Message, ErrorDescription0, ErrorDescription1, ErrorDescription2, Severity, JSONData);

        }        
    }
}
