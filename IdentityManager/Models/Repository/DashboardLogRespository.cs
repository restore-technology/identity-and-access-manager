﻿using IdentityManager.Code.Interface;
using IdentityManager.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashboardLog = IdentityManager.Code.Interface.DashboardLog;

namespace IdentityManager.Models.Repository
{
    public class DashboardLogRespository : DashboardLog
    {
        private readonly IConfiguration _conf;
        private readonly StreamsContext _context;
        public DashboardLogRespository(IConfiguration conf, StreamsContext context)
        {
            _conf = conf;
            _context = context;
        }
        public void LogInsert(string ApplicationName, string VersionNo, string Environment, string UserID, string Context, string UniqueKeyName, string UniqueKeyValue, string Message, string ErrorDescription0, string ErrorDescription1, string ErrorDescription2, int Severity, string JSONData)
        {
            var log = new Data.DashboardLog();
            log.ApplicationName = ApplicationName;
            log.VersionNo = VersionNo;
            log.Environment = Environment;
            log.UserId = UserID;
            log.Context = Context;
            log.UniqueKeyName = UniqueKeyName;
            log.UniqueKeyValue = UniqueKeyValue;
            log.ErrorMessage = Message;
            log.ErrorDescription0 = ErrorDescription0;
            log.ErrorDescription1 = ErrorDescription1;
            log.ErrorDescription1 = ErrorDescription1;
            log.SeverityLevel = Severity;
            log.Jsondata = JSONData;
            //_context.DashboardLogs.Add(log);
            //_context.SaveChanges();
        }
    }
}
