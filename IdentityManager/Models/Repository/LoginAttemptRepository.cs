﻿using IdentityManager.Code.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityManager.Data;
using Microsoft.AspNetCore.Http;
using static IdentityManager.Code.Enumeration;
using System.Text.Json;

namespace IdentityManager.Code.Repository
{
    public class LoginAttemptRepository : ILoginAttempt
    {
        private readonly IConfiguration _conf;
        private readonly StreamsContext _context;
        private readonly IELog _elog;
        public LoginAttemptRepository(IConfiguration conf, StreamsContext context, IELog eLog)
        {
            _conf = conf;
            _context = context;
            _elog = eLog;
        }
        public void Insert(LoginAttempt la)
        {
            try
            {
                _context.LoginAttempts.Add(la);
            }
            catch(Exception ex)
            {
                var innerException = string.Empty;
                if (ex.InnerException != null)
                {
                    innerException = ex.InnerException.ToString();
                }
                _elog.LogInsert(_conf["ApplicationName"], _conf["Version"], _conf["Environment"], null, "LoginAttemptRepository-Insert", "LoginAttempt", la.InputtedLoginUser.ToString(), ex.Message, innerException, ex.StackTrace, null, (int)ELogSeverityLevel.Critical, JsonSerializer.Serialize(la));
            }
        }
    }
}
