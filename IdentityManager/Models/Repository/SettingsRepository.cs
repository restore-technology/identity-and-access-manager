﻿using IdentityManager.Code.Interface;
using IdentityManager.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Models.Repository
{
    public class SettingRepository : ISetting
    {
        private readonly IConfiguration _conf;
        private readonly StreamsContext _context;
        public SettingRepository(IConfiguration conf, StreamsContext context)
        {
            _conf = conf;
            _context = context;
        }
        public DashboardSetting GetSettings()
        {
            DashboardSetting settings = _context.DashboardSettings.First();
            return settings;
        }
    }
}
