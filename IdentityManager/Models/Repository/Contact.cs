﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;
using System.Security.Cryptography;
using IdentityManager.Models;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Configuration;
using IdentityManager.Data;
using IdentityManager.Code.Interface;

/// <summary>
/// Provides the business logic for Contact class.
/// </summary>
public class ContactsBLL
{
    private readonly StreamsContext _context;
    private readonly IConfiguration _conf;
    private readonly IUser _user;
    public ContactsBLL(StreamsContext context, IConfiguration conf, IUser user)
    {
        _context = context;
        _conf = conf;
        _user = user;
    }
    /// <summary>
    /// Gets the roles of this user.
    /// </summary>
    //public virtual IEnumerable<string> GetRoles()
    //{
    //    yield return this.AccessGroup.ToStringOrEmpty().Or("Guest");
    //}

    public User FindByUsername(string username)
    {
        //var contactlist = _context.Contacts.ToList<Contact>();

        return _user.GetUser(username);


        // return contactlist.Find(a => a.Username == username);
    }

    /// <summary>
    ///
    /// </summary>
    public User CanLogin(string loginUser, string password, string ip, string browser)
    {
        string strHashPassword = string.Empty, strSalt = string.Empty;
        var contact = FindByUsername(loginUser);
        //RecordLoginAttempt(ip, browser, password, loginUser, contact);
        string passwordAndSalt = RecordLoginAttempt(ip, browser, password, loginUser, contact);

        var strArray = passwordAndSalt.Split('~').ToArray();
        if (strArray.Any())
        {
            strHashPassword = Convert.ToString(strArray[0]);
            strSalt = Convert.ToString(strArray[1]);
        }        
        if (contact == null || (contact.Password == null && contact.HashPassword == null && contact.Salt == null))
        {
            throw new ValidationException("Invalid username and/or password. Please try again.");
        }
        if (contact == null || (contact.Password == null || contact.Password == ""))
        {
            string strSavedHashPassword = contact.HashPassword;
            string strSavedSalt = contact.Salt;

            if (!string.IsNullOrEmpty(strSavedHashPassword) && !string.IsNullOrEmpty(strSavedSalt))
            {
                Byte[] savedSalt = Convert.FromBase64String(strSavedSalt);
                string hashPassword = GetPasswordDigest(password, savedSalt, 10000, 32);
                if (!hashPassword.SequenceEqual(strSavedHashPassword))
                    throw new ValidationException("Invalid username and/or password. Please try again.");
            }
        }
        if (contact.Password != null && contact.Password != "")
        {
            if (contact == null || contact.Password != password)
            {
                throw new ValidationException("Invalid username and/or password. Please try again.");
            }
            else
            {

                #region Generate HashPassword & Salt
                //Byte[] salt = Contact.GetSalt(Contact.SaltLengthLimit);
                //string strSalt = Convert.ToBase64String(salt);
                //string strHashPassword = Contact.GetPasswordDigest(password, salt, 10000, 32);

                contact.Password = null;
                contact.HashPassword = strHashPassword;
                contact.Salt = strSalt;
                _context.Update(contact);
                _context.SaveChanges();
                //_context.Update(contact, j =>
                //{
                //    j.Password = null;
                //    j.HashPassword = strHashPassword;
                //    j.Salt = strSalt;
                //});
                #endregion
            }
        }

        //if (contact == null || contact.Password != password)
        //    throw new ValidationException("Invalid username and/or password. Please try again.");

        if (!contact.IsSystemUser)
            throw new ValidationException("User is not a System User");

        if (!contact.IsActive)
            throw new ValidationException("User is not active.");


        return contact;
    }

    //public IEnumerable<App.SqlReport> GetVisibleReports()
    //{
    //    return Database.GetList<SqlReport>(report => CanSeeReport(report));
    //}

    //private bool CanSeeReport(SqlReport report)
    //{
    //    return report.AccessGroups.Contains(this.AccessGroup)
    //        && (report.ContactRoles.None() || report.ContactRoles.Intersects(this.Roles))
    //        && (report.Customers.None() || report.Customers.Contains(this.Customer)); // 1. Modified By Shobhita 2. Roll back after new requirement
    //}

    //private  void RecordLoginAttempt(string ip, string browser, string password, string loginUser, Contact contact)
    //{
    //    var successful = false;

    //    if (contact != null)
    //        successful = (contact.Password == password && contact.IsSystemUser);

    //    Database.Save(new LoginAttempt
    //    {
    //        IP = ip,
    //        Browser = browser,
    //        Successful = successful,
    //        Contact = contact,
    //        InputtedPassword = password,
    //        InputtedLoginUser = loginUser,
    //        Salt = strSalt
    //    });
    //}

    private string RecordLoginAttempt(string ip, string browser, string password, string loginUser, User contact)
    {
        var successful = false;

        if (contact != null)
            successful = (contact.Password == password && contact.IsSystemUser);

        #region Generate HashPassword & Salt
        string strHashPassword = string.Empty, strSalt = string.Empty;
        if (contact != null)
        {
            if (contact.Password == null && (contact.HashPassword != null && contact.Salt != null))
            {
                strHashPassword = contact.HashPassword;
                strSalt = contact.Salt;
                if (!string.IsNullOrEmpty(strHashPassword) && !string.IsNullOrEmpty(strSalt))
                {
                    Byte[] savedSalt = Convert.FromBase64String(strSalt);
                    string hashPassword = GetPasswordDigest(password, savedSalt, 10000, 32);
                    if (!hashPassword.SequenceEqual(strHashPassword))
                    {
                        strHashPassword = hashPassword;
                    }
                }
            }
            else
            {
                Byte[] salt = GetSalt(SaltLengthLimit);
                strSalt = Convert.ToBase64String(salt);
                strHashPassword = GetPasswordDigest(password, salt, 10000, 32);
            }
        }
        else
        {
            Byte[] salt = GetSalt(SaltLengthLimit);
            strSalt = Convert.ToBase64String(salt);
            strHashPassword = GetPasswordDigest(password, salt, 10000, 32);
        }
        #endregion


        LoginAttempt la = new LoginAttempt();

        la.Ip = ip;
        la.Browser = browser;
        la.Successful = successful;
        la.Contact = Guid.Parse(contact.Id);
        la.InputtedPassword = strHashPassword;
        la.InputtedLoginUser = loginUser;
        la.Salt = strSalt;
        _context.LoginAttempts.Add(la);

        return strHashPassword + "~" + strSalt;
    }

    //public  IEnumerable<Contact> GetContactsDataSource(Customer customer, List<Criterion> filters = null)
    //{
    //    if (filters == null) filters = new List<Criterion>();

    //    if (customer != null)
    //    {
    //        filters.Add(Criterion.From<Contact>(each => each.Customer == customer));
    //    }
    //    return Database.GetList<Contact>(filters, new QueryOption[] { SortQueryOption.OrderByDescending<Contact>(c => c.Reference) });
    //    //return Database.GetList<Contact>(filters, new QueryOption[] { QueryOption.Take(Config.Get<int>("Contacts.Query.Quantity")), SortQueryOption.OrderByDescending<Contact>(c => c.Reference) });

    //}

    //public void SetActivationStatus(bool activate)
    //{            
    //    Database.Update(this, a => a.IsActive = activate);
    //}

    //public override void Validate()
    //{
    //    base.Validate();

    //    if (this.IsSystemUser)
    //    {

    //        if (this.Username.IsEmpty())
    //            throw new ValidationException("Username is required in order for the contact to be able to login to the system");

    //        if (Username.HasValue())
    //        {
    //            // Ensure uniqueness of Username.

    //            if (Database.Any<Contact>(c => c.Username == this.Username && c != this))
    //            {
    //                throw new ValidationException("Username must be unique. There is an existing Contact record with the provided Username.");
    //            }
    //        }
    //        if (!string.IsNullOrEmpty(Password))
    //            EnsurePasswordPolicy(Password);
    //    }

    //}

    //private  Dictionary<Func<string, bool>, string> PasswordPolicies = new Dictionary<Func<string, bool>, string>
    //    {
    //    { s=> s.Length >= 8, "Password must have a minimum length of 8 characters." },
    //    { s=> Helper.AtLeastOneAlphaAndOneNumberAndMixOfUpperLowerCasePattern.IsMatch(s) , "Password must be between 8-100 characters and must contain one uppercase, one lowercase and one numeric character.\nSpecial characters and spaces are also allowed." }
    //};

    ///// <summary>
    ///// Returns all password policy rule messages
    ///// </summary>
    //public  IEnumerable<string> GetAllPasswordPolicyMessages()
    //{
    //    return PasswordPolicies.Values;
    //}

    ///// <summary>
    ///// Goes over all password policies and if there is a violation throws a ValidationException
    ///// </summary>
    ///// <param name="plainTextPassword"></param>
    //private  void EnsurePasswordPolicy(String plainTextPassword)
    //{
    //    foreach (var policy in PasswordPolicies)
    //    {
    //        if (!policy.Key(plainTextPassword))
    //            throw new ValidationException(policy.Value);
    //    }
    //}

    //public List<App.Contact> GetCustomerLocationList(Guid customerId)
    //{
    //    AppData.ContactDataProvider obj = new AppData.ContactDataProvider();
    //    try
    //    {
    //        return obj.GetCustomerLocationList(customerId);
    //    }
    //    catch (Exception ex)
    //    {
    //        return null;
    //    }
    //    finally
    //    {
    //        obj = null;
    //    }
    //}

    #region Password Encryption Using Salt

    public int SaltLengthLimit = 32;

    /// <summary>
    /// GetSalt
    /// </summary>
    /// <returns>byte[]</returns>
    private byte[] GetSalt()
    {
        return GetSalt(SaltLengthLimit);
    }

    /// <summary>
    /// GetSalt
    /// </summary>
    /// <param name="maximumSaltLength"></param>
    /// <returns>byte[]</returns>
    public byte[] GetSalt(int maximumSaltLength)
    {
        var salt = new byte[maximumSaltLength];
        using (var random = new RNGCryptoServiceProvider())
        {
            random.GetNonZeroBytes(salt);
        }
        return salt;
    }

    /// <summary>
    /// GetPasswordDigest
    /// </summary>
    /// <param name="value"></param>
    /// <param name="salt"></param>
    /// <param name="iterations"></param>
    /// <param name="digestLength"></param>
    /// <returns>string</returns>
    public string GetPasswordDigest(string value, byte[] salt, Int32 iterations, Int32 digestLength)
    {
        UTF8Encoding utf8Encoding = new UTF8Encoding();
        byte[] bytePassword = utf8Encoding.GetBytes(value);
        Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes(bytePassword, salt, iterations);
        Byte[] hashedBytes = deriveBytes.GetBytes(digestLength);
        return ConvertToHexEncode(hashedBytes);
    }

    /// <summary>
    /// ConvertToHexEncode
    /// </summary>
    /// <param name="value"></param>
    /// <returns>string</returns>
    private string ConvertToHexEncode(byte[] value)
    {
        string hex = "0123456789abcdef";
        StringBuilder stringBuilder = new StringBuilder();
        if (value != null)
        {
            if (value.Length > 0)
            {
                for (int i = 0; i < value.Length; i++)
                {
                    stringBuilder.Append((hex[(value[i] & 0xf0) >> 4]));
                    stringBuilder.Append(hex[value[i] & 0x0f]);
                }
            }
        }
        return stringBuilder.ToString();
    }

    #endregion

    private bool CanAccess(User contact)
    {
        var user = _context.AccessGroups.Where(a => a.Id == Guid.Parse(contact.AccessGroupId)).FirstOrDefault();
        if (user.Name == "Sys Admin")
        {
            return true;
        }
        return false;
    }
}
