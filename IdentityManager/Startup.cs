using IdentityManager.Code.Interface;
using IdentityManager.Data;
using IdentityManager.Model.Repository;
using IdentityManager.Models.Interface;
using IdentityManager.Models.Repository;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashboardLog = IdentityManager.Code.Interface.DashboardLog;

namespace IdentityManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.LoginPath = "/Authentication/Index";
                        options.ExpireTimeSpan = TimeSpan.FromHours(12);
                        options.Cookie.Domain = Configuration["DomainName"];
                        options.SlidingExpiration = true;
                        options.Cookie.SameSite = SameSiteMode.None;
                    });

            services.AddControllersWithViews();
            services.AddMvc();
            services.AddControllersWithViews();
            var connectionString = Configuration.GetConnectionString("StreamsConnection");
 
            
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromHours(9);
            });
            services.AddMvc(
                options => options.EnableEndpointRouting = false
                ).SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddDbContext<StreamsContext>(options => options.UseSqlServer(connectionString));

            services.AddScoped<IELog, ELogRespository>();
            services.AddScoped<DashboardLog, DashboardLogRespository>();
            services.AddScoped<IUser, UserRepository>();
            services.AddScoped<ISetting, SettingRepository>();
            services.AddScoped<IAccessGroupPermissions, AccessGroupPermissionsRepository>();            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSession();
            // Add MVC to the request pipeline.           
            app.UseMvc();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
