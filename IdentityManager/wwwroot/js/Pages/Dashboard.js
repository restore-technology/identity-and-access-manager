﻿$(document).ready(function () {
    LoadLoggedUsers();
    LoadUsersType();
    function LoadLoggedUsers() {
        var obj = {};
        obj.DepotId = $("#DepotId").val();
        $.ajax({
            type: "GET",
            url: "/Dashboard/LoadLoggedInUsers",            
            //data: JSON.stringify(DepotId + DepotId),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (responsedata) {
                var userscount = responsedata.split('|');
                $("#TotalUsers").html(userscount[0]);
                $("#ActiveUsers").html(userscount[1]);
                $("#UserLoggedinToday").html(userscount[2]);
                $("#UserLoggedinLast1Week").html(userscount[3]);
                $("#UserLoggedinLast1Month").html(userscount[4]);
                $("#UserLoggedinLast1Year").html(userscount[5]);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }
    function LoadUsersType() {
        var obj = {};
        obj.DepotId = $("#DepotId").val();
        $.ajax({
            type: "GET",
            url: "/Dashboard/LoadUsersTypeCount",
            //data: JSON.stringify(DepotId + DepotId),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (responsedata) {
                populateusertypeandcount(responsedata);              
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }
    function populateusertypeandcount(data) {
        $(".spinner-borderassetsorted").css("display", "block");

        var table1 = $("#UsersGroupDataTable").DataTable({
            "destroy": true,
            "data": data,
            "paging": false,
            "ordering": true,
            "bFilter": false,
            "bLengthChange": true, // show x
            "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]], // value : text - first is the default displayed (20 in this case).
            "pageLength": 10, // default number to show            
            "fixedHeader": true,
            "responsive": true,            
            "language": {
               
                "zeroRecords": "Currently there are no records to display"
            },

            //"dom": '<"toolbar">frtip',
            //"dom": "lBfrtip",
            "order": [[1, 'asc']],
            "columns": [
                { "data": "accessgroup", "className": "text-left noshow" },
                { "data": "groupcount", "className": "text-center" }               
            ],
            "initComplete": function (settings, json) {
                var $wrapper = $(this).parents(".dataTables_wrapper");
                ////Create button
                //insertToolbarButtonsRight("JobLotStatusMismatchTable", "GroupPlusJobLotStatusMismatch");
            },
            "createdRow": function (row, data, dataIndex) {
                if (data["statusColour"] == "Orange") {
                    $(row).css("background-color", "#FFA500");
                }
                else if (data["statusColour"] == "Green") {
                    $(row).css("background-color", "#00FF00");
                }
                else if (data["statusColour"] == "Yellow") {
                    $(row).css("background-color", "#FFFF00");
                }
                else {
                    $(row).css("background-color", "#FFFFFF");
                }
            }
        });
        $(".spinner-borderassetsorted").css("display", "none");
    }
});