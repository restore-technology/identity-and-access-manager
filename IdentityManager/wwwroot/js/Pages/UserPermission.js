﻿$(document).ready(function () {
    $("#editpermissions").hide();
    loadareapermissions();
    $("#AreaId").change(function () {

    });
    $("#Cancel").click(function () {

    });
    
    function populateData(response) {
        $("#RecId").val(response.id);
        if (response.create == true) {
            $("#CreateValue").prop("checked", true);
        }
        else {
            $("#CreateValue").prop("checked", false);
        }
        if (response.read == true) {
            $("#ReadValue").prop("checked", true);
        }
        else {
            $("#ReadValue").prop("checked", false);
        }
        if (response.update == true) {
            $("#UpdateValue").prop("checked", true);
        }
        else {
            $("#UpdateValue").prop("checked", false);
        }
        if (response.delete == true) {
            $("#DeleteValue").prop("checked", true);
        }
        else {
            $("#DeleteValue").prop("checked", false);
        }
        $("#editpermissions").show();
    }
    function loadareapermissions() {
        var permissionArea = $("#AreaId").val();
        var roleId = $("#RoleId").val();
        if (permissionArea != "") {
            //load permissions
            var model = {
                roleid: roleId,
                id: permissionArea

            };
            $.ajax({
                type: 'GET',
                url: "/UserPermission/GetPermissions",
                contentType: "text/plain",
                data: model,
                dataType: 'json',
                success: function (response) {
                    populateData(response);
                },
                error: function (e) {
                    console.log("There was an error with your request...");
                    console.log("error: " + JSON.stringify(e));
                }
            });
        }
    }
});