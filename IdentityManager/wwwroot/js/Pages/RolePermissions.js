﻿$(document).ready(function () {
    loadareapermission();
    
    $("#btnSave").click(function () {
        var permissionArea = $("#ddlPermissionArea").val();
        var roleId = $("#RoleId").val();
        if (permissionArea != "") {
            //load permissions
            var model = {
                Id: $("#Id").val(),
                RoleId: roleId,
                AreaId: permissionArea,
                Create: $("#ddlCreate").val(),
                Read: $("#ddlRead").val(),
                Update: $("#ddlUpdate").val(),
                Delete: $("#ddlDelete").val()
            };
            $.ajax({
                type: 'POST',
                url: "/Roles/SaveRolePermissions",
                //data: '{rolePermission: ' + JSON.stringify(model) + '}',
                data: JSON.stringify({ RolePermission: model }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    populateData(response);
                },
                error: function (e) {
                    console.log("There was an error with your request...");
                    console.log("error: " + JSON.stringify(e));
                }
            });
        }
    });

    function populateData(response) {
        $("#RecId").val(response.id);
        //$("#RoleId").val(response.roleId);
        //$("#AreaId").val(response.areaId);        
        if (response.create == true) {
            $("#CreateValue").prop("checked", true);
        }
        else {         
            $("#CreateValue").prop("checked", false);
        }
        if (response.read == true) {            
            $("#ReadValue").prop("checked", true);
        }
        else {            
            $("#ReadValue").prop("checked", false);
        }
        if (response.update == true) {            
            $("#UpdateValue").prop("checked", true);
        }
        else {            
            $("#UpdateValue").prop("checked", false);
        }
        if (response.delete == true) {            
            $("#DeleteValue").prop("checked", true);
        }
        else {            
            $("#DeleteValue").prop("checked", false);
        }
        
    }

    $("#AreaId").change(function () {
        loadareapermission();
    });

    function loadareapermission() {
        var permissionArea = $("#AreaId").val();
        var roleId = $("#RoleId").val();
        if (permissionArea != "") {
            //load permissions
            var model = {
                roleid: roleId,
                id: permissionArea

            };
            $.ajax({
                type: 'GET',
                url: "/UserPermission/GetPermissions",
                contentType: "text/plain",
                data: model,
                dataType: 'json',
                success: function (response) {
                    populateData(response);
                },
                error: function (e) {
                    console.log("There was an error with your request...");
                    console.log("error: " + JSON.stringify(e));
                }
            });
        }
    }
});