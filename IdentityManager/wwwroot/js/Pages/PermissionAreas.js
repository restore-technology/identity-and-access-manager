﻿$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: "/PermissionAreas/GetPermissionsAreas",
        contentType: "text/plain",
        //data: model,
        dataType: 'json',
        success: function (response) {
            populateDataTable(response);
        },
        error: function (e) {
            console.log("There was an error with your request...");
            console.log("error: " + JSON.stringify(e));
        }
    });
    //hide the first column
    hidecolumn(0);
    
    $('#PermissionAreasDataTable tbody').on('click', '[id*=btnEditUserPermission]', function () {
        var data = table1.row($(this).parents('tr')).data();
        var UserId = data[0];
        var name = data[1];
        var title = data[2];
        var city = data[3];
        //var url = "/UserPermission/Index?Id=" + UserId;
        //window.location.href = url;
    });

    function hidecolumn(column) {
        var table = $('#PermissionAreasDataTable').DataTable();
        // Hide a column
        table.column(column).visible(false);
    }
    function populateDataTable(data) {
        $(".spinner-borderassetsorted").css("display", "block");

        var table1 = $("#PermissionAreasDataTable").DataTable({
            "destroy": true,
            "data": data,
            "paging": true,
            "ordering": true,
            "info": true,
            "bLengthChange": true, // show x
            "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]], // value : text - first is the default displayed (20 in this case).
            "pageLength": 10, // default number to show
            "fixedHeader": true,
            "responsive": true,
            "bFilter": true, // the search box,
            "language": {
                "search": "Find",
                "zeroRecords": "Currently there are no records to display"
            },
                  
            //"dom": '<"toolbar">frtip',
            //"dom": "lBfrtip",
            "order": [[1, 'asc']],
            "columns": [
                { "data": "id", "className": "text-left noshow", "visible": false },
                { "data": "areaName", "className": "text-left" },
                { "data": "create", "className": "text-center" },
                { "data": "read", "className": "text-center" },
                { "data": "update", "className": "text-center" },
                { "data": "delete", "className": "text-center" }

            ],
            
            "columnDefs": [
                {
                    "targets": [0],                    
                    "searchable": false,
                    "visible": false
                },
                {

                    "targets": [6],
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "defaultContent": "<button type=\"button\" disabled class=\"btn btn-sm btn-primary btn-edit\"><i class=\"fas fa-edit\"></i></button>"
                },
            ],
            "mRender": function (data, type, full) {
               // return '<a class="btn btn-info btn-sm" href=#/' + data[0] + '>' + 'Edit' + '</a>';
            },
            "initComplete": function (settings, json) {
                var $wrapper = $(this).parents(".dataTables_wrapper");
                ////Create button
                insertToolbarButtonsRight("PermissionAreasDataTable", "GroupIcons");
            },
            "createdRow": function (row, data, dataIndex) {
                if (data["statusColour"] == "Orange") {
                    $(row).css("background-color", "#FFA500");
                }
                else if (data["statusColour"] == "Green") {
                    $(row).css("background-color", "#00FF00");
                }
                else if (data["statusColour"] == "Yellow") {
                    $(row).css("background-color", "#FFFF00");
                }
                else {
                    $(row).css("background-color", "#FFFFFF");
                }
            }
        });
        $(".spinner-borderassetsorted").css("display", "none");
    }

    //When a row is clicked on; redirect to the edit screen
    $('#PermissionAreasDataTable tbody').on('click', 'tr', function () {

        var table = $('#AccessGroupsDataTable').DataTable();
        var id = table.cells(table.row(this), 0).data()[0];
        var url = "/Roles/EditUserRolePermissions/" + id;
        //blockUIWithSpinner();
        //Load Partial view
        //if (id.length !== 0) {   //Load Partial view
        //    window.location.href = url;
        //}
        //else {
        //    //$.unblockUI();
        //}

    });
    $("#exampleModal").modal({
        show: true,
        backdrop: 'static'
    });
    $("#btnAddNewPermissionAreas").click(function () {
        $("#PermissionAreaModal").modal('show');
    });
    $("#btnModalSave").click(function () {
        var regEx = /^[0-9a-zA-Z]+$/;
        var rolename = $("#RoleName").val();
        if (rolename.match(regEx)) {
            //Saving role
        }
        else {
            $("#LabelError").html("Please enter letters and numbers only.");
            return false;
        }
    });
    function populateData(response) {
        $("#RecId").val(response.id);
        if (response.create == true) {
            $("#CreateValue").prop("checked", true);
        }
        else {
            $("#CreateValue").prop("checked", false);
        }
        if (response.read == true) {
            $("#ReadValue").prop("checked", true);
        }
        else {
            $("#ReadValue").prop("checked", false);
        }
        if (response.update == true) {
            $("#UpdateValue").prop("checked", true);
        }
        else {
            $("#UpdateValue").prop("checked", false);
        }
        if (response.delete == true) {
            $("#DeleteValue").prop("checked", true);
        }
        else {
            $("#DeleteValue").prop("checked", false);
        }
        $("#editpermissions").show();
    }
});