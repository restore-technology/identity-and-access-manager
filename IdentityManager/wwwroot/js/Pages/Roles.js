﻿$(document).ready(function () {
    //hide the first column
    hidecolumn(0);
    $.ajax({
        type: 'GET',
        url: "/Roles/GetAccessGroups",
        contentType: "text/plain",
        //data: model,
        dataType: 'json',
        success: function (response) {
            populateDataTable(response);
        },
        error: function (e) {
            console.log("There was an error with your request...");
            console.log("error: " + JSON.stringify(e));
        }
    });
    x
    
    $('#AccessGroupsDataTable tbody').on('click', '[id*=btnEditUserPermission]', function () {
        var data = table1.row($(this).parents('tr')).data();
        var UserId = data[0];
        var name = data[1];
        var title = data[2];
        var city = data[3];
        var url = "/UserPermission/Index?Id=" + UserId;
        window.location.href = url;
    });

    function hidecolumn(column) {
        var table = $('#AccessGroupsDataTable').DataTable();
        // Hide a column
        table.column(column).visible(false);
    }
    function populateDataTable(data) {
        $(".spinner-borderassetsorted").css("display", "block");

        var table1 = $("#AccessGroupsDataTable").DataTable({
            "destroy": true,
            "data": data,
            "paging": false,
            "ordering": true,
            "info": true,
            "bLengthChange": true, // show x
            "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]], // value : text - first is the default displayed (20 in this case).
            "pageLength": 10, // default number to show
            "fixedHeader": true,
            "responsive": true,
            "bFilter": true, // the search box,
            "language": {
                "search": "Find",
                "zeroRecords": "Currently there are no records to display"
            },
                  
            //"dom": '<"toolbar">frtip',
            //"dom": "lBfrtip",
            "order": [[1, 'asc']],
            "columns": [
                { "data": "id", "className": "text-left noshow", "visible": false },
                { "data": "roleName", "className": "text-center" },
                { "data": "roleOrder", "className": "text-center" }                
            ],
            
            "columnDefs": [
                {
                    "targets": [0],                    
                    "searchable": false,
                    "visible": false
                },
                {

                    "targets": 3,
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "defaultContent": "<button type=\"button\" disabled class=\"btn btn-sm btn-primary btn-edit\"><i class=\"fas fa-edit\"></i></button>"
                },
            ],
            "mRender": function (data, type, full) {
                return '<a class="btn btn-info btn-sm" href=#/' + data[0] + '>' + 'Edit' + '</a>';
            },
            "initComplete": function (settings, json) {
                var $wrapper = $(this).parents(".dataTables_wrapper");
                ////Create button
                insertToolbarButtonsRight("AccessGroupsDataTable", "GroupIcons");
            },
            "createdRow": function (row, data, dataIndex) {
                if (data["statusColour"] == "Orange") {
                    $(row).css("background-color", "#FFA500");
                }
                else if (data["statusColour"] == "Green") {
                    $(row).css("background-color", "#00FF00");
                }
                else if (data["statusColour"] == "Yellow") {
                    $(row).css("background-color", "#FFFF00");
                }
                else {
                    $(row).css("background-color", "#FFFFFF");
                }
            }
        });
        $(".spinner-borderassetsorted").css("display", "none");
    }

    //When a row is clicked on; redirect to the edit screen
    $('#AccessGroupsDataTable tbody').on('click', 'tr', function () {

        var table = $('#AccessGroupsDataTable').DataTable();
        var id = table.cells(table.row(this), 0).data()[0];
        var url = "/Roles/EditUserRolePermissions/" + id;
        //blockUIWithSpinner();
        //Load Partial view
        if (id.length !== 0) {   //Load Partial view
            window.location.href = url;
        }
        else {
            //$.unblockUI();
        }

    });
    $("#exampleModal").modal({
        show: true,
        backdrop: 'static'
    });
    $("#btnAddNewRoles").click(function () {
        $("#RoleModal").modal('show');
    });
    $("#btnModalSave").click(function () {
        var regEx = /^[0-9a-zA-Z]+$/;
        var rolename = $("#RoleName").val();
        if (rolename.match(regEx)) {
            //Saving role
        }
        else {
            $("#LabelError").html("Please enter letters and numbers only.");
            return false;
        }
    });
    
});