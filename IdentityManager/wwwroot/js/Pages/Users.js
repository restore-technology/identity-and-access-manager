﻿$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: "/Users/GetUsersList",
        contentType: "text/plain",
        //data: model,
        dataType: 'json',
        success: function (response) {
            populateDataTable(response);
        },
        error: function (e) {
            console.log("There was an error with your request...");
            console.log("error: " + JSON.stringify(e));
        }
    });
    //hide the first column
    hidecolumn(0);
    
    $('#UsersDataTable tbody').on('click', '[id*=btnEditUserPermission]', function () {
        var data = table1.row($(this).parents('tr')).data();
        var UserId = data[0];
        var name = data[1];
        var title = data[2];
        var city = data[3];
        var url = "/UserPermission/Index?Id=" + UserId;
        window.location.href = url;
    });

    function hidecolumn(column) {
        var table = $('#UsersDataTable').DataTable();
        // Hide a column
        table.column(column).visible(false);
    }
    function populateDataTable(data) {
        $(".spinner-borderassetsorted").css("display", "block");

        var table1 = $("#UsersDataTable").DataTable({
            "destroy": true,
            "data": data,
            "paging": false,
            "ordering": true,
            "info": true,
            "bLengthChange": true, // show x
            "lengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]], // value : text - first is the default displayed (20 in this case).
            "pageLength": 10, // default number to show
            "paging": true,
            "info": true,
            "fixedHeader": true,
            "responsive": true,
            "bFilter": true, // the search box,
            "language": {
                "search": "Find",
                "zeroRecords": "Currently there are no records to display"
            },
                  
            //"dom": '<"toolbar">frtip',
            //"dom": "lBfrtip",
            "order": [[1, 'asc']],
            "columns": [
                { "data": "id", "className": "text-left noshow" },
                { "data": "username", "className": "text-center" },
                { "data": "forename", "className": "text-center" },
                { "data": "surname", "className": "text-center" },
                { "data": "isActive", "className": "text-center" },
                { "data": "isSystemUser", "className": "text-center" },
                { "data": "accessGroup", "className": "text-center" }
            ],
            
            "columnDefs": [
                {
                    "targets": [0],                    
                    "searchable": false,
                    "visible": false
                },
                {

                    "targets": 7,
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "defaultContent": "<button type=\"button\" class=\"btn btn-sm btn-primary btn-edit\"><i class=\"fas fa-edit\"></i></button>"
                },
            ],
            "mRender": function (data, type, full) {
                return '<a class="btn btn-info btn-sm" href=#/' + data[0] + '>' + 'Edit' + '</a>';
            },
            "initComplete": function (settings, json) {
                var $wrapper = $(this).parents(".dataTables_wrapper");
                ////Create button
                //insertToolbarButtonsRight("JobLotStatusMismatchTable", "GroupPlusJobLotStatusMismatch");
            },
            "createdRow": function (row, data, dataIndex) {
                if (data["statusColour"] == "Orange") {
                    $(row).css("background-color", "#FFA500");
                }
                else if (data["statusColour"] == "Green") {
                    $(row).css("background-color", "#00FF00");
                }
                else if (data["statusColour"] == "Yellow") {
                    $(row).css("background-color", "#FFFF00");
                }
                else {
                    $(row).css("background-color", "#FFFFFF");
                }
            }
        });
        $(".spinner-borderassetsorted").css("display", "none");
    }

    //When a row is clicked on; redirect to the edit screen
    $('#UsersDataTable tbody').on('click', 'tr', function () {

        var table = $('#UsersDataTable').DataTable();
        var id = table.cells(table.row(this), 0).data()[0];
        var url = "/UserPermission/UserPermissions/" + id;
        //blockUIWithSpinner();
        //Load Partial view
        if (id.length !== 0) {   //Load Partial view
            window.location.href = url;
        }
        else {
            //$.unblockUI();
        }

    });
    
    

    
});