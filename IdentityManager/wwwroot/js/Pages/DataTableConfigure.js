﻿function setDataTableBehaviour(tableName) {

    $('#' + tableName + ' tbody').on('mouseenter', 'tr', function () {
        $(this).addClass('datatable_RowHighlight');
    });

    $('#' + tableName + ' tbody').on('mouseleave', 'tr', function () {
        $(this).removeClass('datatable_RowHighlight');
    });
}



//Insert a div with button definitions into the toolbar of the named table on the right
function insertToolbarButtonsRight(tablename, buttongroup) {
    // Right, before to search   
    if ($("#" + tablename + "_wrapper").length) {
        var htmlstr = $("#" + buttongroup);
        $("#" + tablename + "_filter").append(htmlstr);
    }
}

//Insert a div with button definitions into the toolbar of the named table on the left
function insertToolbarButtonsLeft(tablename, buttongroup) {
    // Left, after pagination
    if ($("#" + tablename + "_wrapper div.dataTables_length").length) {
        $("#" + tablename + "_filter").append($("#" + buttongroup));
    }
}
