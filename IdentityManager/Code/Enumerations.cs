﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Code
{
    public static class Enumerations
    {
        public enum ShowMistmatchType
        {
            All = 0,
            Green = 1,
            Orange = 2,
            Yellow = 3            
        }
    }
}
