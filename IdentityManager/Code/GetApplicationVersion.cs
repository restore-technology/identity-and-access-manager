﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Code
{
    public class GetApplicationVersion
    {
        private readonly IConfiguration _configuration;
        private static string _appversion;
        private static string _env;

        public GetApplicationVersion(IConfiguration configuration)
        {
            _configuration = configuration;
            _appversion = _configuration.GetSection("AppVersion").ToString();
            _env = _configuration.GetSection("Environment").ToString();
        }
        public static string GetVersion()
        {
            var env = Settings.Environment;
            string version = "";
            //Generate a version number if desktop dev
            if (env == Constants.CONST_ENVIRONMENT_DEV_LOCAL)
            {
                Random rnd = new Random();
                int major = rnd.Next(999);
                int minor = rnd.Next(999);
                int revision = rnd.Next(999);

                version = major + "." + minor + "." + revision;
                return version;
            }
            version = _appversion;
            return version;


        }
    }
}
