﻿using IdentityManager.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Code
{
    public class Elog
    {
        private readonly IConfiguration _configuration;
        private readonly StreamsContext _context;
        public string _version = WebConfigurationManager.AppSettings["AppVersion"];
        public string _environment = WebConfigurationManager.AppSettings["Environment"];
        public string _applicationName;
        private static string _appversion;

        public Elog(string applicationName = "", IConfiguration configuration, StreamsContext context)
        {
            if (applicationName == "")
            {
                _applicationName = _configuration.GetSection("AppVersion").ToString();["ApplicationName"];
                return;
            }
            _applicationName = applicationName;
            _appversion = _configuration.GetSection("AppVersion").ToString();
            _context = context;
        }


        public int Insert(int severity = 0,
                               Guid? contextId = null,
                               string message = null,
                               string uniqueKeyName = null,
                               string uniqueKeyValue = null,
                               string errorDescription0 = null,
                               string errorDescription1 = null,
                               string errorDescription2 = null,
                               string jsonData = null)
        {
            LogError logerror = new LogError();

            int id = 0;
            try
            {
                LogModel model = CreateLogModel(_applicationName, _version, _environment, contextId, uniqueKeyName, uniqueKeyValue, message, errorDescription0, errorDescription1, errorDescription2, severity, jsonData);

                //Log the request in the database
                id = logerror.LogInfo(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return id;
        }

        public LogModel CreateLogModel(string ApplictionName, string VersionNo, string Environment, Guid? ContextID, string UniqueKeyName, string UniqueKeyValue,
            string Message, string ErrorDescription0, string ErrorDescription1, string ErrorDescription2, int severity, string JSONData)
        {

            LogModel model = new LogModel();
            model.ApplicationName = ApplictionName;
            model.VersionNo = VersionNo;
            //If we have an empty environment this is LIVE
            model.Environment = string.Equals(Environment, string.Empty, StringComparison.Ordinal) ? "LIVE" : Environment;
            model.ContextID = ContextID;
            model.UniqueKeyName = UniqueKeyName;
            model.UniqueKeyValue = UniqueKeyValue;
            model.UserID = HttpContext.Current.User.Identity.Name;
            model.Message = Message;
            model.ErrorDescription0 = ErrorDescription0;
            model.ErrorDescription1 = ErrorDescription1;
            model.ErrorDescription2 = ErrorDescription2;
            model.SeverityLevel = severity;
            model.JSONData = JSONData;
            if (model.ApplicationName == null || (model.ApplicationName != null && model.ApplicationName == "") || model.VersionNo == null || (model.VersionNo != null && model.VersionNo == "")
              || model.Message == null || model.Environment == null || (model.Environment != null && model.Environment == "") || model.SeverityLevel == 0)
            {
                throw new Exception("One or more required fields is empty");
            }
            return model;
        }
    }
}
