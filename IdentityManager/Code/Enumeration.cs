﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Code
{
    public class Enumeration
    {
        public enum ELogSeverityLevel
        {
            Emergency = 1,
            Critical = 2,
            Warning = 3,
            Information = 4
        }
    }
}
