﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityManager.Code
{

    
    public static class Settings
    {
        public static string Environment = "";
        public static IConfiguration AppSetting { get; }
        static Settings()
        {
            AppSetting = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
            
            Environment = AppSetting["Environment"].ToString();
        }
    }
}
