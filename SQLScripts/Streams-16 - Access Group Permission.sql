USE [Streams]
GO
/****** Object:  Table [dbo].[PermissionAreas]    Script Date: 28/04/2022 08:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionAreas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaName] [varchar](100) NULL,
	[Create] [bit] NULL,
	[Read] [bit] NULL,
	[Update] [bit] NULL,
	[Delete] [bit] NULL,
 CONSTRAINT [PK_PermissionAreas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolePermissions]    Script Date: 28/04/2022 08:10:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePermissions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [uniqueidentifier] NULL,
	[AreaId] [int] NULL,
	[Create] [bit] NULL,
	[Read] [bit] NULL,
	[Update] [bit] NULL,
	[Delete] [bit] NULL,
 CONSTRAINT [PK_RolePermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PermissionAreas] ON 
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (1, N'Customers', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (2, N'Contacts', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (3, N'Location', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (4, N'BillingCodes', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (5, N'Assets', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (6, N'BusinessRules', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (7, N'CustomerInvoices', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (8, N'Jobs', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (9, N'CollectionJob', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (10, N'DeploymentJob', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (11, N'RecoveryJob', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (12, N'SiteMoveJob', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (13, N'ServiceJob', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (14, N'CustomError', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (15, N'Home', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (16, N'AdminTopMenu', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (17, N'Invoice', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (18, N'Projects', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (19, N'Transactions', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (20, N'Quality', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (21, N'Reports', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (22, N'Queues', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (23, N'QuickJob', 1, 1, 1, 1)
GO
INSERT [dbo].[PermissionAreas] ([Id], [AreaName], [Create], [Read], [Update], [Delete]) VALUES (24, N'Integration', 1, 1, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[PermissionAreas] OFF
GO
SET IDENTITY_INSERT [dbo].[RolePermissions] ON 
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (1, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 1, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (2, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 1, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (3, N'281e820c-912e-4464-ab40-a44965a473fc', 1, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (4, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 1, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (5, N'7300f1c5-8562-4e84-9670-771b0908adb6', 1, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (6, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 2, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (7, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 2, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (8, N'281e820c-912e-4464-ab40-a44965a473fc', 2, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (9, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 2, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (10, N'7300f1c5-8562-4e84-9670-771b0908adb6', 2, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (11, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 3, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (12, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 3, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (13, N'281e820c-912e-4464-ab40-a44965a473fc', 3, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (14, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 3, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (15, N'7300f1c5-8562-4e84-9670-771b0908adb6', 3, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (16, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 4, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (17, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 4, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (18, N'281e820c-912e-4464-ab40-a44965a473fc', 4, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (19, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 4, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (20, N'7300f1c5-8562-4e84-9670-771b0908adb6', 4, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (21, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 5, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (22, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 5, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (23, N'281e820c-912e-4464-ab40-a44965a473fc', 5, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (24, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 5, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (25, N'7300f1c5-8562-4e84-9670-771b0908adb6', 5, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (26, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 6, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (27, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 6, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (28, N'281e820c-912e-4464-ab40-a44965a473fc', 6, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (29, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 6, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (30, N'7300f1c5-8562-4e84-9670-771b0908adb6', 6, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (31, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 7, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (32, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 7, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (33, N'281e820c-912e-4464-ab40-a44965a473fc', 7, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (34, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 7, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (35, N'7300f1c5-8562-4e84-9670-771b0908adb6', 7, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (36, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 8, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (37, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 8, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (38, N'281e820c-912e-4464-ab40-a44965a473fc', 8, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (39, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 8, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (40, N'7300f1c5-8562-4e84-9670-771b0908adb6', 8, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (41, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 9, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (42, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 9, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (43, N'281e820c-912e-4464-ab40-a44965a473fc', 9, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (44, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 9, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (45, N'7300f1c5-8562-4e84-9670-771b0908adb6', 9, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (46, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 10, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (47, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 10, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (48, N'281e820c-912e-4464-ab40-a44965a473fc', 10, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (49, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 10, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (50, N'7300f1c5-8562-4e84-9670-771b0908adb6', 10, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (51, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 11, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (52, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 11, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (53, N'281e820c-912e-4464-ab40-a44965a473fc', 11, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (54, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 11, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (55, N'7300f1c5-8562-4e84-9670-771b0908adb6', 11, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (56, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 14, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (57, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 14, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (58, N'281e820c-912e-4464-ab40-a44965a473fc', 14, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (59, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 14, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (60, N'7300f1c5-8562-4e84-9670-771b0908adb6', 14, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (61, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 15, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (62, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 15, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (63, N'281e820c-912e-4464-ab40-a44965a473fc', 15, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (64, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 15, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (65, N'7300f1c5-8562-4e84-9670-771b0908adb6', 15, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (66, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 16, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (105, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 16, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (67, N'281e820c-912e-4464-ab40-a44965a473fc', 16, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (68, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 16, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (69, N'7300f1c5-8562-4e84-9670-771b0908adb6', 16, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (70, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 12, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (71, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 12, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (72, N'281e820c-912e-4464-ab40-a44965a473fc', 12, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (73, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 12, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (74, N'7300f1c5-8562-4e84-9670-771b0908adb6', 12, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (75, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 13, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (76, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 13, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (77, N'281e820c-912e-4464-ab40-a44965a473fc', 13, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (78, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 13, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (79, N'7300f1c5-8562-4e84-9670-771b0908adb6', 13, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (80, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 17, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (81, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 17, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (82, N'281e820c-912e-4464-ab40-a44965a473fc', 17, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (83, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 17, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (84, N'7300f1c5-8562-4e84-9670-771b0908adb6', 17, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (85, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 18, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (86, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 18, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (87, N'281e820c-912e-4464-ab40-a44965a473fc', 18, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (88, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 18, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (89, N'7300f1c5-8562-4e84-9670-771b0908adb6', 18, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (90, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 19, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (91, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 19, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (92, N'281e820c-912e-4464-ab40-a44965a473fc', 19, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (93, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 19, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (94, N'7300f1c5-8562-4e84-9670-771b0908adb6', 19, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (95, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 20, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (96, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 20, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (97, N'281e820c-912e-4464-ab40-a44965a473fc', 20, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (98, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 20, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (99, N'7300f1c5-8562-4e84-9670-771b0908adb6', 20, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (100, N'7e145d69-20d0-4d20-87fb-dcb3bb71d34e', 21, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (101, N'2c885d12-9af8-41ed-8a68-baa572e7d31d', 21, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (102, N'281e820c-912e-4464-ab40-a44965a473fc', 21, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (103, N'ece17f7b-e85c-4849-ac58-0fb3940054a0', 21, 1, 1, 1, 1)
GO
INSERT [dbo].[RolePermissions] ([Id], [RoleId], [AreaId], [Create], [Read], [Update], [Delete]) VALUES (104, N'7300f1c5-8562-4e84-9670-771b0908adb6', 21, 1, 1, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[RolePermissions] OFF
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anit Shekhar
-- Create date: 11/04/2022
-- Description:	Get Access by Area Name
-- =============================================
CREATE PROCEDURE [dbo].[GetAccessGroupsByAreaName]
	@AreaName varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT r.Id,r.RoleId,g.Name as AccessGroupName, a.Id as AreaId, a.AreaName, 
	CASE a.[Create] WHEN 0 THEN 0 ELSE r.[Create] END AS [Create], 
	CASE a.[Read] WHEN 0 THEN 0 ELSE r.[Read] END AS [Read], 
	CASE a.[Update] WHEN 0 THEN 0 ELSE r.[Update] END AS [Update], 
	CASE a.[Delete] WHEN 0 THEN 0 ELSE r.[Delete] END AS [Delete] 	
	FROM RolePermissions  r
	INNER JOIN PermissionAreas a ON r.AreaId = a.Id
	INNER JOIN AccessGroups g ON r.RoleId = g.Id
	WHERE AreaName = @AreaName
END
GO
/****** Object:  StoredProcedure [dbo].[GetAccessGroupsByRoleandArea]    Script Date: 28/04/2022 08:12:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anit Shekhar
-- Create date: 12/04/2022
-- Description:	Get Access by Id
-- =============================================
CREATE PROCEDURE [dbo].[GetAccessGroupsByRoleandArea]
	@RoleId varchar(100),
	@AreaId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT r.Id,r.RoleId,g.Name as AccessGroupName, a.Id as AreaId, a.AreaName, 
	CASE a.[Create] WHEN 0 THEN 0 ELSE r.[Create] END AS [CCreate], 
	CASE a.[Read] WHEN 0 THEN 0 ELSE r.[Read] END AS [RRead], 
	CASE a.[Update] WHEN 0 THEN 0 ELSE r.[Update] END AS [UUpdate], 
	CASE a.[Delete] WHEN 0 THEN 0 ELSE r.[Delete] END AS [DDelete] 	
	FROM RolePermissions  r
	INNER JOIN AccessGroups g ON r.RoleId = g.Id
	INNER JOIN PermissionAreas a ON r.AreaId = a.Id
	WHERE g.Id = @RoleId and a.Id = @AreaId
END
GO
/****** Object:  StoredProcedure [dbo].[GetAccessGroupsByRoleId]    Script Date: 28/04/2022 08:12:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anit Shekhar
-- Create date: 12/04/2022
-- Description:	Get Access by Role Id
-- =============================================
CREATE PROCEDURE [dbo].[GetAccessGroupsByRoleId]
	@RoleId varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT r.Id,r.RoleId,g.Name as AccessGroupName, a.Id as AreaId, a.AreaName, 
	CASE a.[Create] WHEN 0 THEN 0 ELSE r.[Create] END AS [Create], 
	CASE a.[Read] WHEN 0 THEN 0 ELSE r.[Read] END AS [Read], 
	CASE a.[Update] WHEN 0 THEN 0 ELSE r.[Update] END AS [Update], 
	CASE a.[Delete] WHEN 0 THEN 0 ELSE r.[Delete] END AS [Delete] 	
	FROM RolePermissions  r
	INNER JOIN AccessGroups g ON r.RoleId = g.Id
	INNER JOIN PermissionAreas a ON r.AreaId = a.Id
	WHERE RoleId = @RoleId
END
GO
/****** Object:  StoredProcedure [dbo].[GetSystemUsers]    Script Date: 28/04/2022 08:12:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSystemUsers]
	@IsActive bit = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT c.Id,Username,Forename,Surname,Password,HashPassword,Salt, IsSystemUser, IsActive,	
	a.Id as AccessGroupId,a. Name as AccessGroupName	
	FROM Contacts c
	inner join AccessGroups a on c.AccessGroup = a.Id
	where IsSystemUser = 1
	AND   (@IsActive is null OR IsActive = @IsActive)
END
GO
